import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Modal from '../controllers/components/modal.controller'
import {	changeSize, changeColor  } from '../actions/product.actions'
import {fetchprodBody, addToCart, getProductVariants, changeVariant, getRelatedProduct} from '../actions/product.actions'


class Modal extends Component{
  mapStateToProps = state => {
      
  }
}

const mapStateToProps = state => ({
    sku : state.modal.sku,
    price: state.modal.price,
    sku_list: state.modal.sku_list,
    color:  state.modal.color,
    size:  state.modal.size,
    product_get: state.modal.product_get
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getProductVariants,
  changeColor,
  changeSize,
  changeVariant,
  addToCart
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal)

