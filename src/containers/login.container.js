import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {login} from '../actions/login.actions'
import {setLoginDataToNull} from '../actions/login.actions'
import Login from '../controllers/views/login.controller'

class LoginContainer extends Component{
  // mapStateToProps = state => {
  // }
}

const mapStateToProps = state => ({
    loggedInData: state.login.loginData,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: (path = '/') => push(path),
   login,
    setLoginDataToNull,
}, dispatch);

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Login))