import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {addAsBillingAddress} from '../actions/myAccountAddress.action'
import Checkout from '../controllers/views/checkout.controller'
import {changeBox, setBillingAddress, handleSameShipping, setShippingAddress, getCartContent, loadCheckoutPrices} from '../actions/checkout.actions'

class CheckoutContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
  box_type: state.checkout.box_type,
  billing_addresS_id: state.checkout.billing_addresS_id,
  billing_address: state.checkout.billing_address,
  shipping_address: state.checkout.shipping_address,
  shipping_info: state.checkout.shipping_info,
  checkout_bill: state.checkout.checkout_bill,
  checkout_items: state.checkout.checkout_items,
  checkout_item_images: state.checkout.checkout_item_images,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  changeBox,
  setBillingAddress,
  setShippingAddress,
  handleSameShipping,
  getCartContent,
    addAsBillingAddress,
  loadCheckoutPrices
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkout)