import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {forgot, setIsForgotFalse} from '../actions/forgot.actions'
import Forgot from '../controllers/views/forgot.controller'

class ForgotContainer extends Component{
    // mapStateToProps = state => {
    // }
}

const mapStateToProps = state => ({
    isForgotMailSent: state.forgot.forgotResponse
});

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: (path = '/') => push(path),
    forgot,
    setIsForgotFalse
}, dispatch);

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Forgot))