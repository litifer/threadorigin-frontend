import React, {Component} from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Product from '../controllers/views/product.controller'
import {
	increment, decrement,changeSize, changeColor,fetchDetails,getColor, getSize, 
  getPrice, get_initial_price,goto_cart, addItemToWishlist
} from '../actions/product.actions'
import {changeModal} from '../actions/modal.actions'
import {fetchprodBody, addToCart, getProductVariants, changeVariant, getRelatedProduct} from '../actions/product.actions'


class Product1Container extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
  sku: state.product.sku,
  name: state.product.name,
  qty:             state.product.qty,
  color:           state.product.color,
  size:            state.product.size,
  price:           state.product.price,
  product_get:     state.product.product_get,
  // color_val:       state.product.color_val,
  // size_val:        state.product.size_val,
  price_array:     state.product.price_array,
  get_cart:        state.product.result,
  product_details:  state.product.product_details,
  color_list:       state.product.color_list,
  size_list:       state.product.size_list,
  variants:         state.product.variants,
  sku_list:         state.product.sku_list,
  name_list:        state.product.name_list,
  price_list:       state.product.price_list,
  size_attribute_id: state.product.size_attribute_id,
  color_attribute_id: state.product.color_attribute_id,
  all_colors:        state.product.all_colors,
  all_sizes:          state.product.all_sizes,
  cart_message:      state.product.cart_message,
  related_products:   state.product.related_products,
  carousel_products_obtained: state.product.carousel_products_obtained,
  itemAdded:      state.product.itemAdded,
  
   //modal props
   modal_item: state.modal.modal_item,
})

const mapDispatchToProps = dispatch => bindActionCreators({
   increment,
   decrement,
   changeSize,
   changeColor,
   fetchDetails,
   getColor,
   getSize,
    addItemToWishlist,
  //  get_initial_price,
  //  getPrice,
   goto_cart : () => push('/cart'),
   // add_wish,
   getProductVariants,
   fetchprodBody,
   changeVariant,
   addToCart,
   getRelatedProduct,
   changeModal,


  }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product)

