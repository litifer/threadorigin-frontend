import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {loadCart, increment_val, decrement_val,remove_item,get_discount, loadPrices, changeStatus, updateQuantity} from '../actions/cart.actions'
import Cart from '../controllers/views/cart.controller'

class CartContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
  cart_get : state.cart.cart_get,
  final_data: state.cart.final_data,
   discount: state.cart.discount,
   item_images: state.cart.item_images,
   bill: state.cart.bill,
   discount_applied: state.cart.discount_applied,
   calculate_price: state.cart.calculate_price,
   item_removed: state.cart.item_removed,
   updated_quantity: state.cart.updated_quantity
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loadCart,
  loadPrices,
  increment_val,
  decrement_val,
  remove_item,
  get_discount,
  changeStatus,
  updateQuantity
  // goToCheckout : () => push('/checkout')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)