import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {deleteSelectedAddress} from './../actions/myAccountAddress.action'
import {getUserData} from '../actions/myAccountDashboard.action'
import AccountDashboard from '../controllers/views/myAccountDashboard.controller'

class AccountDashboardContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
    userData: state.dashboard.userData
});

const mapDispatchToProps = dispatch => bindActionCreators({

    getUserData,
    deleteAddress: deleteSelectedAddress

}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountDashboard)