import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {} from '../actions/myAccountInfo.action'
import AccountInfo from '../controllers/views/myAccountInfo.controller'
import {getUserData} from '../actions/myAccountDashboard.action'
import {updateCustomerInfo} from '../actions/myAccountInfo.action'

class AccountInfoContainer extends Component{
    mapStateToProps = state => {
    }
}

const mapStateToProps = state => ({
    userData: state.dashboard.userData,
    isInfoUpdated: state.accountInfo.infoUpdated
});

const mapDispatchToProps = dispatch => bindActionCreators({

    getUserData,
    updateCustomerInfo

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountInfo)