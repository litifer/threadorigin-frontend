import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {subscribe_email} from '../actions/preLaunch.actions'
import PreLaunch from '../controllers/views/preLaunch.controller'

class PreLaunchContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
  subscribe_email
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreLaunch)

