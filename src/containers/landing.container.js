import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Landing from '../controllers/components/landing.controller'
import {loadCategoryPage} from '../actions/category.actions'


const mapStateToProps = state => ({
  title: state.landing.title,
  description: state.landing.description,
  img: state.landing.img,
  landingButton: state.landing.button,
  isVideo: state.landing.isVideo 
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loadCategoryPage : () => push('/category')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing)