import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {} from '../actions/payment.actions'
import Payment from '../controllers/views/payment.controller'

class PaymentContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Payment)