import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {register} from '../actions/register.actions'
import {setIsRegisteredFalse} from '../actions/register.actions'
import Register from '../controllers/views/register.controller'

class RegisterContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
    isRegistered: state.register.isRegistered
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: () => push('/'),
  register,
    setIsRegisteredFalse,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register)