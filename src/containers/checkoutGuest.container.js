import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {} from '../actions/checkoutGuest.actions'
import CheckoutGuest from '../controllers/views/checkoutGuest.controller'

class CheckoutGuestContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutGuest)