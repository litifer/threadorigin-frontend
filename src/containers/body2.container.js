import React, {Component} from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Body2 from '../controllers/components/productBody.controller'
import {fetchprodBody} from '../actions/product.actions'


const mapStateToProps = state => ({
  product_details: state.product.product_details,
  })

const mapDispatchToProps = dispatch => bindActionCreators({
   fetchprodBody,
  }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Body2)