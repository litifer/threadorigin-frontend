import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { addToCart } from './../actions/product.actions'
import {getWishlist, deleteItemInWishlist, getImages} from '../actions/wishlist.actions'
import wishlist from '../controllers/views/wishlist.controller'

class WishlistContainer extends Component{
    mapStateToProps = state => {
    }
}

const mapStateToProps = state => ({
    wishlistData: state.wishlist.wishlistData,
    addToCartResult: state.product.result,
    addToCartMessage: state.product.cart_message,
    item_images: state.wishlist.item_images,

})

const mapDispatchToProps = dispatch => bindActionCreators({
    getWishlist, deleteItemInWishlist, addToCart, getImages
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(wishlist)

