import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {subscribe_email} from '../actions/preWebsiteLaunch.actions'
import PreWebsiteLaunch from '../controllers/views/preWebsiteLaunch.controller'

class PreWebsiteLaunchContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
  subscribe_email
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreWebsiteLaunch)

