import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {getAddresses, setAddressAsDefault} from '../actions/myAccountAddress.action'
import {addNewDefaultAddress} from '../actions/myAccountAddress.action'
import {deleteSelectedAddress} from '../actions/myAccountAddress.action'
import {editExistingAddress} from '../actions/myAccountAddress.action'
import AccountAddress from '../controllers/views/myAccountAddress.controller'

class AccountAddressContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
    addressData: state.addresses.addressData,
    defaultAddress: state.addresses.defaultAddress,
    otherAddresses: state.addresses.otherAddresses,
    defaultAddressChanged: state.addresses.defaultAddressChanged,
    defaultAddressAdded: state.addresses.defaultAddressAdded,
    addressDeleted: state.addresses.addressDeleted,
    addressEdited: state.addresses.addressEdited,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    getAddresses,
    addNewDefaultAddress,
    setAddressAsDefault,
    deleteSelectedAddress,
    editExistingAddress
}, dispatch)


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountAddress)