import React, {Component} from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import CampignLooks from '../controllers/views/campignLooks.controller'
import {getCampaignLooks} from '../actions/campignLooks.actions'
import {getProductDetails} from '../actions/campignLooks.actions'
import {getColors} from '../actions/campignLooks.actions'
import {getSizes} from '../actions/campignLooks.actions'


const mapStateToProps = state => ({
    lookbookData: state.lookbook.lookbookData,
    sizeChart: state.lookbook.sizeChart,
    colorChart: state.lookbook.colorChart,
    productDetails: state.lookbook.productDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getCampaignLooks,
    getColors,
    getSizes,
    getProductDetails
  }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CampignLooks)