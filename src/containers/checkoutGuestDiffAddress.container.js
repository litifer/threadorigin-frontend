import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {} from '../actions/checkoutGuestDiffAddress.actions'
import CheckoutGuestDiffAddress from '../controllers/views/checkoutGuestDiffAddress.controller'

class CheckoutGuestDiffAddressContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutGuestDiffAddress)