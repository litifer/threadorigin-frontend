import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {getOrders} from '../actions/myAccountOrders.action'
import AccountOrders from '../controllers/views/myAccountOrders.controller'

class AccountOrdersContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
    ordersData: state.orders.ordersData
})

const mapDispatchToProps = dispatch => bindActionCreators({
    getOrders
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountOrders)