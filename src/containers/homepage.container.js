import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Homepage from '../controllers/views/homepage.controller'
// import {getBody3, getBody5, subscribe_email, getImgBody3, getImgBody5,getInsta} from '../actions/homepage.actions'
import {getCamp1, getCamp2, getCamp3, getBody5, subscribe_email,  getImgBody5,getInsta} from '../actions/homepage.actions'
import {fetchprodBody, getColor, getSize} from '../actions/product.actions'
import {fetchProductData, getAllCategories, getRelevantItems} from '../actions/category.actions'
import {changeModal} from '../actions/modal.actions'

const mapStateToProps = state => ({
    // product_details:state.product.product_details,
    body2: state.homepage.body2,
    body3: state.homepage.body3,
    body5: state.homepage.body5,
    img_body3:state.homepage.img_body3,
    img_body5:state.homepage.img_body5,
    insta_images:state.homepage.insta_images,
    camp1: state.homepage.camp1,
    camp2: state.homepage.camp2,
    camp3: state.homepage.camp3,

    //product container props
    all_colors:        state.product.all_colors,
    all_sizes:          state.product.all_sizes,
    color_attribute_id: state.product.color_attribute_id,
    size_attribute_id: state.product.size_attribute_id,
    
    //category props
    product_data: {items: state.category.product_data},
    categories: state.category.categories,
    product_details: state.category.products,

    //modal props
    modal_item: state.modal.modal_item,
})


const mapDispatchToProps = dispatch => bindActionCreators({
  loadCategoryPage : () => push('/category'),
  getAllCategories,
  getRelevantItems,
  getCamp1,
  getCamp2,
  getCamp3,
  //getBody3, 
  getBody5,
  fetchprodBody,
  //getImgBody3,
  getImgBody5,
  subscribe_email,
  getInsta,
  changeModal,
  getColor,
  getSize,
  fetchProductData
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Homepage)