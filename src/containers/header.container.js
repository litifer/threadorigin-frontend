import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Header from '../controllers/components/header.controller'


const mapStateToProps = state => ({
  title: state.landing.title,
  description: state.landing.description,
  img: state.landing.img,
  landingButton: state.landing.button,
  isVideo: state.landing.isVideo 
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loadDressesPage : () => push('/category?id=1'),
  loadBottomsPage : () => push('/category?id=2'),
  loadTopsPage : () => push('/category?id=3'),
  loadLookbooksPage : () => push('/category?id=4'),
  loadCollectionsPage : () => push('/category?id=5'),
  loadSandookPage : () => push('/category?id=6'),
  loadCartPage : () => push('/cart'),
  loadHomePage : () => push('/')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing)