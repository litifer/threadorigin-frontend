import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {set_newpass} from '../actions/newPassword.actions'
import newPassword from '../controllers/views/newPassword.controller'

class NewPasswordContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
    isPasswordChanged: state.newpass.passwordChanged
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: () => push('/'),
   set_newpass
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(newPassword)