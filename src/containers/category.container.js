import React, {Component}from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {fetchCategoryCamp,fetchImage,changeColorCategory, searchingCategory,
  changeSizeCategory,fetchColor, fetchSize} from '../actions/category.actions'
import Category from '../controllers/views/category.controller'
import {getRelevantItems, fetchProductData, productsDidUpdate} from '../actions/category.actions' 
import {addColorFilter, removeColorFilter, addSizeFilter, removeSizeFilter} from '../actions/category.actions' 

// modal specific actions
import {changeModal} from '../actions/modal.actions'

class CategoryContainer extends Component{
  mapStateToProps = state => {
  }
}

const mapStateToProps = state => ({
     color :  state.category.color,
      size :  state.category.size,
 color_val :  state.category.color_val,
  size_val :  state.category.size_val,
      camp :  state.category.camp,
  img_body :  state.category.product_data,
array_color:  state.category.array_color,
array_size :  state.category.array_size,
search_item: state.category.search_item,
products: state.category.products,
product_data: state.category.product_data,
products_updated: state.category.products_updated,
color_filters: state.category.color_filters,
size_filters: state.category.size_filters,

//modal specific props
modal_item: state.modal.modal_item,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: () => push('/about-us'),
  getRelevantItems,
  fetchProductData,
  productsDidUpdate,
  changeColorCategory,
  searchingCategory,
  changeSizeCategory,
  fetchColor,
  fetchSize,
  addColorFilter,
  removeColorFilter,
  addSizeFilter,
  removeSizeFilter,
  changeModal
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Category)