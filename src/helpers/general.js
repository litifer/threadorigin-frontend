const config = require('../config')

export const hasData = (data) => {
    if(data){
        return [...data]
    }
    else return []
}

export const find = (arr, prop, prop_name) => {
     return hasData(arr).find((element) => {
        return element[prop] === prop_name
     });
}

export const attribute_array_creator = (available, items, type) => {
    let size_array = []
    let label = ((type === 'color')? 'Color' : 'size')

    if(items && items.extension_attributes && items.extension_attributes.configurable_product_options && find(hasData(items.extension_attributes.configurable_product_options), 'label', label)){
        size_array = find(hasData(items.extension_attributes.configurable_product_options), 'label', label).values.map((val) => {
           return hasData(available).find((element) => {
               console.log("%c props are: ", "color: yellow");
               console.log(element.value, val.value_index)
               return element.value == val.value_index
           });
       })
     }
     console.clear()
     console.log('size_array')
     console.log(size_array)

     return size_array
}

export const findProductById = (id, products) => {
    return find(products, 'id', id)
}

export const getImgUrl = (item) => {
    let img_url = ""
    if(item && item.custom_attributes){
        img_url = item.custom_attributes.find(i => {
            return i.attribute_code == "image"
        })
    }
    

    return config.IMG_URL + (img_url? img_url.value : '')
}