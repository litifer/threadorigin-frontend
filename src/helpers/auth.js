const axios = require('axios')
const config = require('../config')

export const getAccessToken = () => {
    axios.get("http://localhost:8000/api" + config.ACCESS_ENDPOINT)
    .then(res => {
        let token = res.data.access_token
        console.log(`%c The token in getACCESSTOken function is:${token}`, "color: green;")
        console.log(token);
        return token    
    }).catch(err => {
        console.log(err)
    });
}
