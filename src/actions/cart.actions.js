import axios from 'axios';
import * as  config from '../config.js';
export const LOADCART = 'LOADCART'
export const INCREMENT_VALUE = 'INCREMENT_VALUE'
export const DECREMENT_VALUE = 'DECREMENT_VALUE'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const DISCOUNT_VAL = 'DISCOUNT_VAL'

export const loadCart = () => {
	return dispatch => {
		 if (window.localStorage.getItem('authToken')) {
             axios ({
                 method: 'POST',
                 url   : config.BASE_URL+'/get/user-cart',
                 data: {
                     "Authorization": window.localStorage.getItem('authToken')
                 }
             }).then(function(res){
                 console.log(res.data);
                 res.data.items.map(item => {
                     axios.get(config.BASE_URL + config.MEDIA_GALLERY + item.sku).then(response => {
                         return dispatch({
                             type: 'ITEM_IMAGE',
                             data: [...response.data],
                             sku: item.sku
                         })
                     }).catch(err => {
                         console.log("Error fetching images");
                         console.log(err)
                     })
                 })
                 return dispatch({
                     type: LOADCART,
                     data: res.data
                 })
             })
         } else {
             if (window.localStorage.getItem('cartId')) {
                 axios ({
                     method: 'get',
                     url   : config.BASE_URL+'/get/user/cart/' + window.localStorage.getItem('cartId'),
                 }).then(function(res){
                     console.log(JSON.parse(res.data.body));
                     JSON.parse(res.data.body).items.map(item => {
                         axios.get(config.BASE_URL + config.MEDIA_GALLERY + item.sku).then(response => {
                             return dispatch({
                                 type: 'ITEM_IMAGE',
                                 data: [...response.data],
                                 sku: item.sku
                             })
                         }).catch(err => {
                             console.log("Error fetching images")
                             console.log(err)
                         })
                     })
                     return dispatch({
                         type: LOADCART,
                         data: JSON.parse(res.data.body)
                     })
                 })
             }
         }
	   }
}

export const loadPrices = () => {
  return dispatch => {
      if (window.localStorage.getItem('authToken')) {
          axios({
              method: 'POST',
              url   : config.BASE_URL + '/user/get-user-payment-info',
              data: {
                  "Authorization": window.localStorage.getItem('authToken')
              }})
              .then(res => {
                  console.log('The cart bill is...');
                  console.log(res, "here is the data from get user payment information");
                  return dispatch({
                      type: 'LOAD_CART_PRICE',
                      data: res.data
                  })
              })
        return;
      }
    if(window.localStorage.getItem('cartId')){
      axios.get(config.BASE_URL + config.GET_GUEST_CART + window.localStorage.getItem('cartId') + config.PAYMENT_INFO)
          .then(res => {
            console.log('The cart bill is...');
            console.log(res)
            return dispatch({
              type: 'LOAD_CART_PRICE',
              data: res.data
            })
          })
    }
    
  }
}

export const increment_val =(qty, cart, item) =>{
 return dispatch => {
   var qty_inc = qty  + 1;
      axios ({
               method: 'post',
               url   : config.BASE_URL+'user/cart/update',
               headers:{'Authorization': '7skcinil7e2jii0vinakmnanotcddea6','Content-Type': 'application/json'},
               data:{
                'qty':qty_inc,
                'item_id':item,
                'cart_id':cart,
              }
            }).then(function(res){
                  return dispatch({
                      type   :INCREMENT_VALUE,
                      data   :res.data
              })
        })
     }
}

export const decrement_val =(qty, cart, item) =>{
  return dispatch =>{
      var qty_dec = qty - 1 
          axios ({
               method: 'post',
               url   : config.BASE_URL+'user/cart/update',
               headers:{'Authorization': '7skcinil7e2jii0vinakmnanotcddea6','Content-Type': 'application/json'},
               data:{
                'qty':qty_dec,
                'item_id':item,
                'cart_id':cart,
              }
            }).then(function(res){
                  return dispatch({
                      type : DECREMENT_VALUE,
                      data : res.data
               })
        })
     }
}

export const updateQuantity = (qty, cartId, itemId) => {
  let data = {
      cartId: cartId,
      itemId: itemId,
      qty: qty
  };

  let userCartId = window.localStorage.getItem('userCartId');
  let authToken = window.localStorage.getItem('authToken');

    return dispatch => {
         if (userCartId) {

             axios.post(config.BASE_URL + '/user/update-item-in-user-cart', {
                 "Authorization": authToken,
                 "itemId": itemId,
                 "qty": qty,
                 "userCartId": userCartId
             }).then(res => {
                 console.log(res);
                 return dispatch({
                     type: 'UPDATE_QUANTITY',
                     data: true
                 })
             }).catch(err => {
                 console.log(err);
             })
         } else {

             axios.post(config.BASE_URL + config.UPDATE_ITEM + cartId + '/items/' + itemId, {
                 cartId: cartId,
                 itemId: itemId,
                 qty: qty
             }).then(res => {
                 console.log(res);
                 return dispatch({
                     type: 'UPDATE_QUANTITY',
                     data: true
                 })
             }).catch(err => {
                 console.log(err);
             })
         }
  }
}

export const remove_item = (itemid) =>{
    let userCartId = window.localStorage.getItem('userCartId');
    let authToken = window.localStorage.getItem('authToken');

    return dispatch =>{
    if (userCartId) {
        axios.post(config.BASE_URL + '/user/delete-item-from-user-cart', {
            "Authorization": authToken,
            "itemId": itemid,
            "userCartId": userCartId
        })
            .then(function(res){
                return dispatch({
                    type: REMOVE_ITEM,
                    data: res.data
                })
            })
    } else {
        axios.post(config.BASE_URL + config.DELETE_ITEM + window.localStorage.getItem('cartId') + '/items/' + itemid, { 'authorization': window.localStorage.getItem('authToken')  })
            .then(function(res){
                return dispatch({
                    type: REMOVE_ITEM,
                    data: res.data
                })
            })
    }

  }
}


export const get_discount = (cartid, coupon) =>{
  return dispatch => {
     axios.post(config.BASE_URL + '/coupon/',{
       customerToken :  window.localStorage.getItem('authToken'),
       cartId : window.localStorage.getItem('cartId'),
       coupon :  coupon
      }).then(function(res){
       console.log(res.data)
       if(res.data === true){
        return dispatch ({
          type: 'DISCOUNT_APPLIED',
          data: true
       })
       }
       else if(res.data.code === 404){
         return dispatch ({
           type: 'DISCOUNT_APPLIED',
           data: false
         })
       }
        
     })
  }
}

export const changeStatus = (status) => {
  return dispatch => {
    return dispatch({
      type: 'CHANGE_STATUS',
      data: status
    })
  }
}