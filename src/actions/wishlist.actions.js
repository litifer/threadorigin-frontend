import axios from 'axios'
import * as  config from '../config.js';
export const GET_WISHLIST = 'GET_WISHLIST';
export const GET_IMAGES = 'GET_IMAGES';

export const getImages = (sku) => {
    return dispatch => {
        axios.get(config.BASE_URL + config.MEDIA_GALLERY + sku).then(response => {
            return dispatch({
                type: GET_IMAGES,
                data: [...response.data],
                sku
            })
        }).catch(err => {
            console.log("Error fetching images");
            console.log(err)
        })
    }
}

export const getWishlist = () => {
    return dispatch => {
        axios.post(config.BASE_URL + '/user/get-user-wishlist', {
            "Authorization" : window.localStorage.getItem('authToken'),
            "userId": window.localStorage.getItem('userCartId')
        }).then(function(res){
            console.log(res.data, "data coming after get wishlist request");
            res.data.wishlist.map(item => {
                axios.get(config.BASE_URL + config.MEDIA_GALLERY + item.sku).then(response => {
                    return dispatch({
                        type: GET_IMAGES,
                        data: [...response.data],
                        sku: item.sku
                    })
                }).catch(err => {
                    console.log("Error fetching images");
                    console.log(err)
                })
            })
            return dispatch({
                type: GET_WISHLIST,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
                console.log("oh come on");
            });
    }
};

export const deleteItemInWishlist = (sku) => {
    return dispatch => {
        axios.post(config.BASE_URL + '/user/delete-item-from-user-wishlist', {
            "Authorization" : window.localStorage.getItem('authToken'),
            "userId": window.localStorage.getItem('userCartId'),
            "sku": sku
        }).then(function(res){
            console.log(res.data, "data coming after get wishlist request");

            return dispatch({
                type: GET_WISHLIST,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
                console.log("oh come on");
            });
    }
};
