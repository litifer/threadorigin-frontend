import axios from 'axios'
import * as  config from '../config.js'

export const COLORSELECT = 'COLORSELECT'
export const SIZESELECT = 'SIZESELECT' 
export const CATEGORY_CAMP = 'CATEGORY_CAMP'
export const FETCH_CATEGORY = 'FETCH_CATEGORY'
export const COLOR_FETCH = 'COLOR_FETCH'
export const SIZE_FETCH = 'SIZE_FETCH'
export const SEARCH_CATEGORY = "SEARCH_CATEGORY"

/* 
  This action is called with proper parameter to call the required API and get the resulting products
*/
export const getRelevantItems = (params) => {
  return dispatch => {
     if(params.api === "SEARCH"){
        axios.get(config.BASE_URL + config.SEARCH_END_POINT + params.search).then(res => {
            return dispatch({
              type: 'SEARCH_PRODUCTS',
              data: res.data.items
            })
        }).catch(err => {
          console.log("%c Error in searching products for the given category", "color: red;")
          console.log(err);
        });
     }
    else{
      axios.get(config.BASE_URL + config.CATEGORY_PRODUCTS_END_POINT + params.categoryId).then( res => {
          return dispatch({
            type: 'FETCH_PRODUCTS',
            data: res.data
        })
      }).catch(err => {
        console.log("%c Error Fetching products for the given category", "color: red;");
        console.log(err);
      });  
  }
  } 
}

export const fetchProductData = (item) => {
    return dispatch => {
      axios.get(config.BASE_URL + config.GET_PRODUCT_DATA + item.sku).then(res => {
        return dispatch({
          type: 'FETCH_PRODUCT_DATA',
          data: res.data
        })
      })
    }
}

export const productsDidUpdate = () => {
  return dispatch => {
    return dispatch({
      type: 'SET_PRODUCT_UPDATE_FLAG',
      data: true
    })
  }
}

export const fetchSize = () => {
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_SIZES_END_POINT)
              .then(function(res){
                console.log(res)
                         return dispatch({
                            type: SIZE_FETCH,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}

export const fetchColor = () => {
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_COLOR_END_POINT)
              .then(function(res){
                         return dispatch({
                            type: COLOR_FETCH,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }

}


export const addColorFilter = (current) => {
  return dispatch => {
    return dispatch({
      type: 'ADD_COLOR_FILTER',
      data: current
    })
  }
}

export const removeColorFilter = (current) => {
  return dispatch => {
    return dispatch({
      type: 'REMOVE_COLOR_FILTER',
      data: current
    })
  }
}

export const addSizeFilter = (current) => {
  return dispatch => {
    return dispatch({
      type: 'ADD_SIZE_FILTER',
      data: current
    })
  }
}

export const removeSizeFilter = (current) => {
  return dispatch => {
    return dispatch({
      type: 'REMOVE_SIZE_FILTER',
      data: current
    })
  }
}


function checkSize(products,len, size){
  var array_size = []
  for(var i =0 ;i< len; i++){
    var var_len = products[i].variant_info.length
    for(var j = 0; j< var_len; j++){
      var sku = products[i].variant_info[j].sku
      var len_string = sku.length;
      var index1 = sku.indexOf('-');
      var index2 = sku.lastIndexOf('-');
      var size_product = sku.slice(index2+1, len_string);
      if(size_product == size){
        array_size.push(products[i].sku)
      }
    }
  }
  return array_size
}

function checkColor(products,len,color){
  var array_color = []
  for(var i =0 ;i< len; i++){
    var var_len = products[i].variant_info.length
    for(var j = 0; j< var_len; j++){
      var sku = products[i].variant_info[j].sku
      var len_string = sku.length;
      var index1 = sku.indexOf('-');
      var index2 = sku.lastIndexOf('-');
      var color_product = sku.slice(index1+1, index2);
      if(color_product == color){
         if(array_color.includes(products[i].sku)){}
          else{
            array_color.push(products[i].sku)
          }
      }
    }
  }
  return array_color
}

export const changeSizeCategory = (size) => {
        return dispatch => {
        console.log("In category Action : changeSizeCategory : before API call", 'color: #ff0000;')
        axios.get(config.BASE_URL+'products/info/4')
              .then(function(res){
                console.log("In category Action : changeSizeCategory : After API call")
                  var len = res.data.Products[0].Products.length
                  var array_size = []
                  if(size == "All Sizes"){
                    for(var i =1; i<13; i++)
                      array_size.push(i)
                  }
                  else{
                    array_size= checkSize(res.data.Products[0].Products, len,size)
                  }
                  console.log(array_size)
                  
                         return dispatch({
                            type: SIZESELECT,
                            array_size:array_size
                        })
         })
              .catch(function (error) {
                  console.log("In category Action : changeSizeCategory : Error in API call")
                  console.log(error);
        });
    }
}

export const changeColorCategory = (color) => {
        return dispatch => {
          console.log(color)
        axios.get(config.BASE_URL+'products/info/4')
              .then(function(res){
                  var len = res.data.Products[0].Products.length
                   var array_color = []
                  if(color == "All Colors"){
                    for(var i =1; i<13; i++)
                      array_color.push(i)
                  }
                  else{
                    array_color= checkColor(res.data.Products[0].Products, len,color)
                  }
                  console.log(array_color)
                         return dispatch({
                            type: COLORSELECT,
                            array_color: array_color
                        })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}

export const searchingCategory = (search) =>{
    return dispatch => {
      console.log(search);
      axios.get(config.BASE_URL+'product/find/'+search)
        .then(function(res){
            return dispatch({
                            type: SEARCH_CATEGORY,
                            data: res.data.items
                    })
           })
    }
}
  

export const getAllCategories = () => {
  return dispatch => {
    axios.get(config.BASE_URL + config.GET_ALL_CATEGORIES).then((res)=>{
      return dispatch({
        type: 'ALL_CATEGORIES',
        data: res.data
      })
    })
  }
}