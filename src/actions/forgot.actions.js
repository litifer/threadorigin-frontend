import axios from 'axios'
import * as  config from '../config.js';
export const FORGOT = 'FORGOT';
export const SET_IS_FORGOT_FALSE = 'SET_IS_FORGOT_FALSE';

export const setIsForgotFalse = () => {
    return dispatch => dispatch({type: SET_IS_FORGOT_FALSE});
};

export const forgot = (email) => {
    console.log('in forgot action');
    return dispatch => {
        axios.post(config.BASE_URL + '/account/reset', {
            "email"    : email,
        }).then(function(res){
            console.log(res.data, "data coming after logging in");
            // if (res.data.success === "false") {
            //     console.log('no token found');
            //     return false;
            // }
            console.log((typeof res.data));
            return dispatch({
                type: FORGOT,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

