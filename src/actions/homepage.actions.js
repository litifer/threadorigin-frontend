import axios from 'axios';
import * as  config from '../config.js';
import { push } from 'react-router-redux'
export const LOAD_CAMP1 = "LOAD_CAMP1"
export const LOAD_CAMP2 = "LOAD_CAMP2"
export const LOAD_CAMP3 = "LOAD_CAMP3"
export const LOAD_BODY1 = "LOAD_BODY1"
export const LOAD_BODY2 = "LOAD_BODY2"
// export const LOAD_BODY3 = "LOAD_BODY3"
// export const LOAD_BODY5 = "LOAD_BODY5"
export const IMG_BODY1 = "IMG_BODY1"
// export const IMG_BODY3 = "IMG_BODY3"
// export const IMG_BODY5 = "IMG_BODY5"


//export const LOAD_BODY3 = "LOAD_BODY3"
export const LOAD_BODY5 = "LOAD_BODY5"
//export const IMG_BODY3 = "IMG_BODY3"
export const IMG_BODY5 = "IMG_BODY5"
export const LOAD_INSTA = "LOAD_INSTA"
export const EMAIL_SUBSCRIBE = "EMAIL_SUBSCRIBE"



export const getCamp1 = () => {
  return dispatch => {
    axios.get(config.BASE_URL + config.CAMP_1)
           .then(function(res){
             console.log("hey",res.data)
                      return dispatch({
                         type: LOAD_CAMP1,
                         data: res.data
                       })
      })
           .catch(function (error) {
               console.log(error);
     });
  }
}

export const getCamp2 = () => {
  return dispatch => {
    axios.get(config.BASE_URL + config.CAMP_2)
           .then(function(res){
             console.log("hey",res.data)
                      return dispatch({
                         type: LOAD_CAMP2,
                         data: res.data
                       })
      })
           .catch(function (error) {
               console.log(error);
     });
  }
}

export const getCamp3 = () => {
  return dispatch => {
    axios.get(config.BASE_URL + config.CAMP_3)
           .then(function(res){
             console.log("hey",res.data)
                      return dispatch({
                         type: LOAD_CAMP3,
                         data: res.data
                       })
      })
           .catch(function (error) {
               console.log(error);
     });
  }
}

export const getBody1 = () => {
  return dispatch => {
   axios.get(config.BASE_URL + config.CAMP_1)
           .then(function(res){
             console.log("hey",res.data)
                      return dispatch({
                         type: LOAD_BODY1,
                         data: res.data
                       })
      })
           .catch(function (error) {
               console.log(error);
     });
  }   
}

export const getImgBody1 = () => {
  return dispatch => {
      axios.get('http://13.232.62.17:3000/api/product/camp1')
              .then(function(res){
                console.log("hii",res.data)
                         return dispatch({
                            type: IMG_BODY1,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}


export const getBody5 = () => {
  return dispatch => {
      axios.get(config.BASE_URL + config.CAMP_2)
              .then(function(res){
                        console.log("BOdy5 Loading");
                         return dispatch({
                            type: LOAD_BODY5,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}

export const getImgBody5 = () => {
  return dispatch => {
      axios.get(config.BASE_URL+'/product/camp2')
              .then(function(res){
                         return dispatch({
                            type: IMG_BODY5,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}

export const getInsta = () => {
  return dispatch => {
      axios.get(config.BASE_URL + config.INSTA_URL)
              .then(function(res){
                         return dispatch({
                            type: LOAD_INSTA,
                            data: res.data
                          })
         })
              .catch(function (error) {
                  console.log(error);
        });
    }
}


export const subscribe_email = (email) => {
  return dispatch => {
    axios.post(config.BASE_URL + config.SUBSCRIBE_NEWSLETTER, {email: email}).then(res => {
      dispatch(
        {
         type: EMAIL_SUBSCRIBE,
         data:email
       }
     )
    })
    
  }
}



export const categoryLink = (name) => {
  return dispatch => {
    // push('/category')

  }
}



