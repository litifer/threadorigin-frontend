import axios from 'axios'
import * as  config from '../config.js';
export const GET_ORDERS = 'GET_ORDERS';

export const getOrders = () => {
    console.log('in orders action');
    return dispatch => {
        axios.post(config.BASE_URL + '/getaccount', {
            "authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            // console.log(res.data, "data coming after dashboard request");

            axios.post(config.BASE_URL + '/user/order', {
                "email": res.data.email
            }).then(function(res1){
                console.log(res.data, "data coming after orders request");

                return dispatch({
                    type: GET_ORDERS,
                    data: res1.data
                })
            })
                .catch(function (error) {
                    console.log(error);
                });

        })
            .catch(function (error) {
                console.log(error);
            });

    }
};

