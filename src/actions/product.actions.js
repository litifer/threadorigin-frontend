import axios from 'axios';
import * as  config from '../config.js';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const CHANGECOLOR = 'CHANGECOLOR';
export const CHANGESIZE = 'CHANGESIZE';
export const PRODUCT_BODY = 'PRODUCT_BODY'
export const PRODUCT_NOW = 'PRODUCT_NOW'
export const SIZE_GET = 'SIZE_GET'
export const COLOR_GET = 'COLOR_GET'
export const PRICE_GET = 'PRICE_GET'
export const LOAD_PRICE = 'LOAD_PRICE'
export const ADD_KART = 'ADD_KART'
export const ADD_WISHLIST = "ADD_WISHLIST"


export const increment = () => {
    return dispatch => {
        dispatch({type: INCREMENT})
    }
}

export const decrement = () => {
    return dispatch => {
        dispatch({ type : DECREMENT})
    }
}

export const changeSize = (size) => {
    return dispatch => {

        dispatch({ type: CHANGESIZE, size:size})
    }
}

export const changeColor = (color) => {
    return dispatch => {
        dispatch({type : CHANGECOLOR, color:color})
    }
}

export const get_initial_price = (sku) => {
    return dispatch => {
        axios.get(config.BASE_URL+'/product/'+sku)
            .then(function(res){
                return dispatch({type : LOAD_PRICE})
            })
    }
}

export const fetchprodBody = () => {
    return dispatch => {
        axios.get(config.BASE_URL+'/homepage/exclusive')
            .then(function(res){
                console.log("Hao");
                return dispatch({
                    type: PRODUCT_BODY,
                    data: res.data
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

export const fetchDetails = (sku) => {
    return dispatch => {
        axios.get(config.BASE_URL+'/product/'+sku)
            .then(function(res){
                console.log("%c product data is ", "color: magenta")
                console.log(res.data)
                return dispatch({
                    type:PRODUCT_NOW,
                    data:res.data
                })
            })
            .catch(function(error){
                console.log(error);
            })
    }
}

export const getColor = (sku) =>{
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_COLORS).then(res => {
            return dispatch({
                type: 'AVAILABLE_COLORS',
                data: res.data
            })
        })
    }
}



// var array = []
// var obj = {}
// var myObj = {}
// function getVal(data_col, opt_label) {
//     for(var j = 0; j<data_col.length; j++){
//       for(var i =0; i<opt_label.length; i++){
//        if (data_col[j].value_index== opt_label[i].value){
//         obj = {'value':opt_label[i].value, 'label': opt_label[i].label}
//         array.push(obj)
//          }
//        }
//      }
//    myObj['options'] = array
//    console.log(myObj)
//     return myObj;
// }

export const getSize = () =>{
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_SIZES).then(res => {
            return dispatch({
                type: 'AVAILABLE_SIZES',
                data: res.data
            })
        })
    }
}

export const getProductVariants = (sku) =>{
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_VARIANTS + sku).then(res => {
            let data = {
                variants: res.data,
                color_list: [],
                size_list: [],
                sku_list: [],
                name_list: [],
                price_list: []
            }

            res.data.map((product, index) => {
                data.sku_list.push(product.sku);
                data.name_list.push(product.name);
                data.price_list.push(product.price);
                product.custom_attributes.map(attribute => {
                    if(attribute.attribute_code === 'color')
                    {
                        console.log("%c current color", "color: green")
                        console.log(data.color_list.find(color => {return color.value === attribute.value}))
                        if(data.color_list.find(color => {return color.value === attribute.value}) === undefined)
                            data.color_list.push({label: '', value: attribute.value});
                    }
                    else if(attribute.attribute_code === 'size')
                    {
                        if(data.size_list.find(size => {return size.value === attribute.value}) === undefined)
                            data.size_list.push({label: '', value: attribute.value});
                    }
                    // else continue;
                })
            })


            return dispatch({
                type: 'AVAILABLE_VARIANTS',
                data: data
            })
        })
    }
}

export const changeVariant = (variants, params) => {
    return dispatch => {
        let product_details = variants.find( element => {
            return compare(element, params)
        })
        if(product_details){
            axios.get(config.BASE_URL + config.MEDIA_GALLERY + product_details.sku).then(response => {
                console.log(`%c${response}`, "color: yellow");
                product_details["media_gallery_entries"] = [...response.data];
                // product_details["media_gallery_entries"].push(response.data)
                return dispatch({
                    type: 'CHANGE_VARIANT',
                    data: product_details,
                    size: params.size,
                    color: params.color
                })
            })
        }

    }
}

function compare(element, param){
    let found = element.custom_attributes.find( el => {
        return el.attribute_code === param.attribute_code && el.value === param.value
    })

    if(found){
        return true
    }
    else return false
}

// var array_size = []
// var obj2 = {}
// var myObj_size = {}
// function getVal2(data_size, opt_label) {
//   for(var i =0; i<opt_label.length; i++){
//     for(var j = 0; j<data_size.length; j++){
//      if (opt_label[i].value == data_size[j].value_index){
//         obj2 = {'value':opt_label[i].value,'label': opt_label[i].label}
//         array_size.push(obj2)
//      }
//     }
//   }
//   myObj_size['options'] = array_size
//   return myObj_size;
// }


// var myobj_detail = {};
// var array_detail = []
// function makeObj(data, length){
//   for(var i =0;i<length;i++){
//     var str = data[i].sku;
//     var len_string = str.length;
//     var index1 = str.indexOf('-');
//     var index2 = str.lastIndexOf('-');
//     var color = str.slice(index1 + 1,index2);
//     var size = str.slice(index2+1, len_string);
//     var price = data[i].price;
//     var myobj_detail = {'size':size, 'color':color, 'price':price};
//     array_detail.push(myobj_detail);
//   }
//   return array_detail;
// }


// var price = 0;
// export const getPrice = (sku)=> {

//   return dispatch => {
//     axios.get(config.BASE_URL+'/product/variants/'+sku)
//       .then(function(res){
//         var len = res.data.length;
//         var array_data= makeObj(res.data,len)
//         return dispatch({
//            type: PRICE_GET,
//            data:array_data
//         })
//       })
//   }
// }

// export const goto_cart = (qty, size, col, product_get, col_array, size_array) =>{
//   return dispatch => {
//     var get  = product_get.extension_attributes.configurable_product_options;
//     var result = 0;
//     console.log(qty);
//     console.log(product_get.sku)
//     if(size == undefined){size = size_array.options[0].value}
//     if(col == undefined){col = col_array.options[0].value}
//     console.log(size)
//     console.log(col)
//     console.log(get[0].attribute_id)
//     console.log(get[1].attribute_id)
//     axios({
//        method:'post',
//        url: config.BASE_URL+'user/cart',
//        headers:{'Authorization': config.token,'Content-Type': 'application/json'},
//        data:{
//          'qty':qty,
//          'sku':product_get.sku,
//          'color_value':col,
//          'size_value':size,
//          'color_id':get[0].attribute_id,
//          'size_id':get[1].attribute_id
//        }
//     }).then(function(res){
//           if(res.data.name.length > 0){
//             return dispatch({
//                 type:ADD_KART,
//                 data: "True"
//              })
//           }

//        }).catch(function (error) {
//                   console.log(error);
//         });
//    }
// }

export const addToCart = (sku, qty, color_id, color_value, size_id, size_value) => {
    return dispatch => {
        let data = {
            'qty': qty,
            'sku': sku,
            'color_id': color_id,
            'color_value': color_value,
            'size_id': size_id,
            'size_value': size_value
        };
        // console.log("%c Data for cart: ", "color: green");
        // console.log(data)
        let userCartId = window.localStorage.getItem('userCartId');
        if (userCartId) {
            data.Authorization = window.localStorage.getItem('authToken');
            data.id = userCartId;
            axios.post(config.BASE_URL + '/user/add-item-to-user-cart' , data)
                .then(res => {

                    console.log(window.localStorage.getItem('cartId'))
                    let modal;
                    try {
                        modal = document.getElementById('product_popup');
                        modal.className = modal.className + " fade show";
                    } catch (e) {
                        console.log(e)
                    }

                    let success_message = 'Yay! Your Product has been Added to Cart!'


                    return dispatch({
                        type: ADD_KART,
                        data: "True",
                        message: success_message
                    })
                }).catch(err => console.log('the product couldn\'t be added to cart'));
            return;
        }

        let cartId = window.localStorage.getItem('cartId');
        if(cartId){
            axios.post(config.BASE_URL + config.ADD_TO_CART + cartId , data)
                .then(res => {

                    console.log(window.localStorage.getItem('cartId'))

                    let modal = document.getElementById('product_popup');
                    modal.className = modal.className + " fade show";

                    let success_message = 'Yay! Your Product has been Added to Cart!'


                    return dispatch({
                        type: ADD_KART,
                        data: "True",
                        message: success_message
                    })
                })
        }
        else
        {
            axios.get(config.BASE_URL + config.CREATE_GUEST_CART).then(res => {

                // setting cart id in local storage
                window.localStorage.setItem('cartId', res.data);

                // request for adding product to the cart

                axios.post(config.BASE_URL + config.ADD_TO_CART + res.data , data)
                    .then(res => {

                        console.log(window.localStorage.getItem('cartId'))

                        let modal = document.getElementById('product_popup');
                        modal.className = modal.className + " fade show";


                        let success_message = 'Yay! Your Product has been Added to Cart!'


                        return dispatch({
                            type: ADD_KART,
                            data: "True",
                            message: success_message
                        })
                    }).catch(err => {
                    let error_message = 'Sorry... Your product could not be added...'

                    return dispatch({
                        type: ADD_KART,
                        message: error_message
                    })
                })
            })
        }

    }
}

export const addItemToWishlist = (sku, qty, color_id, color_value, size_id, size_value, name, price) => {
    return dispatch => {
        axios.post(config.BASE_URL + '/user/add-item-to-user-wishlist', {
            "Authorization": window.localStorage.getItem('authToken'),
            "userId": window.localStorage.getItem('userCartId'),
            "sku": sku,
            "qty": qty,
            "color_id": color_id,
            "color_value": color_value,
            "size_id": size_id,
            "size_value": size_value,
            "name": name,
            "price": price
        }).then(res => {
            // window.localStorage.setItem('')
            console.log(res.data, "add item to wishlist");
            return dispatch({
                type: ADD_WISHLIST,
                data: res.data
            })
        }).catch(err => {
            console.log(err);
        })
    }
}


export const getRelatedProduct = (sku) => {
    return dispatch => {
        axios.get(config.BASE_URL+'/product/'+sku)
            .then(function(res){
                console.log("%c related product data is ", "color: magenta")
                console.log(res.data)
                return dispatch({
                    type:'RELATED_PRODUCT',
                    data:res.data
                })
            })
            .catch(function(error){
                console.log(error);
            })
    }
}


// export const add_wish = () =>{
//     return dispatch => {
//         dispatch({type: ADD_WISH})
//     }
// };
