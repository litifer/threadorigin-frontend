import axios from 'axios';
import * as  config from '../config.js';
import {initWishlist} from "./login.actions";
import {LOADCART} from "./cart.actions";
export const DEFAULT_ADDRESS = 'DEFAULT_ADDRESS';

export const changeBox = (data) => {
    return dispatch => {
        console.log('inside checkout actio the data is', data)
        return dispatch({
            type: 'CHANGE_BOX',
            data: data
        })
    }
};

export const initOrder = (addressData, userOrGuest) => {

    if (userOrGuest) {
        return dispatch => {
            let dataToBeSent = {
                "email": "",
                "paymentMethod": {
                    "method": "razorpay"
                },
                "billingAddress": {
                    // "id": 29,
                    "countryId": "IN",
                    "street": addressData.street,
                    // "company": "Litifer",
                    "telephone": addressData.telephone,
                    // "fax": "123456789",
                    "postcode": addressData.postcode,
                    "city": addressData.city,
                    "firstname": addressData.firstname,
                    "lastname": addressData.lastname,
                    // "middlename": "",
                    // "prefix": "Mr",
                    // "suffix": "",
                    "vatId": null,
                    // "customerId": 1,
                    // "email": "kk@litifer.com",
                    "sameAsBilling": 1,
                    "customerAddressId": 0,
                    "saveInAddressBook": 0
                },
                "shippingAddress": {
                    "countryId": "IN",
                    "street": addressData.street,
                    // "company": "Litifer",
                    "telephone": addressData.telephone,
                    // "fax": "123456789",
                    "postcode": addressData.postcode,
                    "city": addressData.city,
                    "firstname": addressData.firstname,
                    "lastname": addressData.lastname,
                    // "middlename": "",
                    // "prefix": "Mr",
                    // "suffix": "",
                    "vatId": null,
                    // "customerId": 1,
                    // "email": "kk@litifer.com",
                    "sameAsBilling": 1,
                    "customerAddressId": 0,
                    "saveInAddressBook": 0
                }
            };
            axios.post(config.BASE_URL + '/user/init-order', {
                "Authorization": window.localStorage.getItem('authToken'),
                paymentData: dataToBeSent
            }).then(res => {
                return dispatch({
                    type: 'ORDER_MADE',
                    data: res.data
                })
            }).catch(err => {
                console.log(err);
            })
        }
    } else {
        return dispatch => {
            let dataToBeSent = {
                "email": addressData.email,
                "paymentMethod": {
                    "method": "razorpay"
                },
                "billingAddress": {
                    // "id": 29,
                    "countryId": "IN",
                    "street": [addressData.street + ', ' + addressData.street2 + ', ' + addressData.region],
                    // "company": "Litifer",
                    "telephone": addressData.phone,
                    // "fax": "123456789",
                    "postcode": addressData.pincode,
                    "city": addressData.city,
                    "firstname": addressData.fname,
                    "lastname": addressData.lname,
                    // "middlename": "",
                    // "prefix": "Mr",
                    // "suffix": "",
                    "vatId": null,
                    // "customerId": 1,
                    // "email": "kk@litifer.com",
                    "sameAsBilling": 1,
                    "customerAddressId": 0,
                    "saveInAddressBook": 0
                },
                "shippingAddress": {
                    "countryId": "IN",
                    "street": [addressData.street + ', ' + addressData.street2 + ', ' + addressData.region],
                    // "company": "Litifer",
                    "telephone": addressData.phone,
                    // "fax": "123456789",
                    "postcode": addressData.pincode,
                    "city": addressData.city,
                    "firstname": addressData.fname,
                    "lastname": addressData.lname,
                    // "middlename": "",
                    // "prefix": "Mr",
                    // "suffix": "",
                    "vatId": null,
                    // "customerId": 1,
                    // "email": "kk@litifer.com",
                    "sameAsBilling": 1,
                    "customerAddressId": 0,
                    "saveInAddressBook": 0
                }
            };
            axios.post(config.BASE_URL + '/guest/init-order', {
                "guestCartId": window.localStorage.getItem('cartId'),
                paymentData: dataToBeSent
            }).then(res => {
                return dispatch({
                    type: 'ORDER_MADE',
                    data: res.data
                })
            }).catch(err => {
                console.log(err);
            })
        }
    }
}

export const setBillingAddress = (cartId, data) => {
    let requestData = {}
    let i = 0;
    while(i< data.length){
        if(data[i].id === 'billingAddressSubmit') break;
        requestData[data[i].id.replace('billing-', '')] = data[i].value;
        i++;
    }
    
    console.log('The billing address data is');
    console.log(requestData);
    
    return dispatch => {
        axios.post(config.BASE_URL + config.ADD_BILLING_ADDRESS_ENDPOINT + cartId, requestData)
                .then(res => {
                    console.log(res);
                    return dispatch({
                        type: 'ADD_BILLING_ADDRESS',
                        address_id: res.data,
                        data: requestData
                    })
                })
    }
};

export const sendDefaultAddressToPaymentGateway = (defaultAddress) => {
    return dispatch => dispatch({
        type: DEFAULT_ADDRESS,
        data: defaultAddress
    })
}

export const setShippingAddress = (cartId, data) => {
    let requestData = {}
    let i = 0;
    while(i< data.length){
        if(data[i].id === 'shippingAddressSubmit') break;
        requestData[data[i].id.replace('shipping-', '')] = data[i].value;
        i++;
    }
    
    console.log('The shipping address data is');
    console.log(requestData, "shipping address");
    
    return dispatch => {
        axios.post(config.BASE_URL + config.ADD_SHIPPING_ADDRESS_ENDPOINT + cartId, requestData)
                .then(res => {
                    console.log(res);
                    return dispatch({
                        type: 'SHIPPING_INFO',
                        data: res.data
                    })
                })
    }
}

export const  handleSameShipping = (isSame, billing_address) => {
    
    return dispatch => {
        return dispatch({
            type: 'SHIPPING_SAME_AS_BILLING',
            data: isSame? {isSame: isSame, data: billing_address}: {isSame:isSame}
        })
    }

}

export const getCartContent = () => {
    return dispatch => {
        if (window.localStorage.getItem('authToken')){
            axios ({
                method: 'POST',
                url   : config.BASE_URL+'/get/user-cart',
                data: {
                    "Authorization": window.localStorage.getItem('authToken')
                }
            }).then(res => {
                console.log(res.data);
                if (res.data.items) {
                    res.data.items.map(item => {
                        axios.get(config.BASE_URL + config.MEDIA_GALLERY + item.sku).then(response => {
                            // console.clear()
                            console.log('dispatching image')
                            return dispatch({
                                type: 'CHECKOUT_ITEM_IMAGES',
                                data: [...response.data],
                                sku: item.sku
                            })
                        }).catch(err => {
                            console.log("Error fetching images")
                            console.log(err)
                        })
                    })
                    return dispatch({
                        type: 'LOAD_CHECKOUT_ITEMS',
                        data: res.data
                    })
                }
            })
        } else {
            axios.get(config.BASE_URL + '/get/user/cart/' + window.localStorage.getItem('cartId'))
                .then(res => {
                    if (JSON.parse(res.data.body).items) {
                        JSON.parse(res.data.body).items.map(item => {
                            axios.get(config.BASE_URL + config.MEDIA_GALLERY + item.sku).then(response => {
                                // console.clear()
                                console.log('dispatching image')
                                return dispatch({
                                    type: 'CHECKOUT_ITEM_IMAGES',
                                    data: [...response.data],
                                    sku: item.sku
                                })
                            }).catch(err => {
                                console.log("Error fetching images")
                                console.log(err)
                            })
                        })
                        return dispatch({
                            type: 'LOAD_CHECKOUT_ITEMS',
                            data: JSON.parse(res.data.body)
                        })
                    }
                })
        }
    }
}

export const loadCheckoutPrices = () => {
    return dispatch => {
      if (window.localStorage.getItem('authToken')) {
          axios.post(config.BASE_URL + config.GET_USER_CART_PAYMENT_INFO, {Authorization: window.localStorage.getItem('authToken')})
              .then(res => {
                  console.log('The cart bill is...');
                  console.log(res)
                  return dispatch({
                      type: 'LOAD_CHECKOUT_PRICE',
                      data: res.data
                  })
              })
      } else {
        axios.get(config.BASE_URL + config.GET_GUEST_CART + window.localStorage.getItem('cartId') + config.PAYMENT_INFO)
            .then(res => {
              console.log('The cart bill is...');
              console.log(res)
              return dispatch({
                type: 'LOAD_CHECKOUT_PRICE',
                data: res.data
              })
            })
      }
      
    }
  }

  export const loginAddress = (data) => {

    //axios request
  }