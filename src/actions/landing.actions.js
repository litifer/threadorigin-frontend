import backgroundImg from '../img/background.png'
import axios from 'axios';

export const LANDING_LOADED_SUCCESSFULLY = 'LANDING_LOADED_SUCCESSFULLY'

const initialState = {
    title : "Let's Start Shopping This August",
    description: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
    img: backgroundImg,
    button: 'EXPLORE COLLECTION',
    isVideo: false
}


//actions

export default (state = initialState, action) => {
  switch (action.type) {
    case LANDING_LOADED_SUCCESSFULLY:
      return {
        ...state
      }

    default:
      return state
    }
}




