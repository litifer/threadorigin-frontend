import axios from 'axios'
import * as  config from '../config.js';
import {push} from 'react-router-redux';
export const REGISTER = 'REGISTER';

export const setIsRegisteredFalse = () => {
    return dispatch =>
        dispatch({type: 'SET_IS_REGISTERED_FALSE'});

};

export const register = (fname, lname, email, pswd) => {
       return dispatch => {
         axios.post(config.BASE_URL + '/account', {
           "email"    : email,
           "password" : pswd,   
           "firstname": fname,
           "lastname" : lname
        }).then(function(res){
           console.log(res.data, 'in register actions');
           // push('/login');
              return dispatch({
                    type: REGISTER,
                    data: res.data
                })
         })
            .catch(function (error) {
              console.log(error);
        });
    }
};

