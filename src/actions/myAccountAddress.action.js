import axios from 'axios'
import * as  config from '../config.js';
export const GET_ADDRESSES = 'GET_ADDRESSES';
export const ADD_DEFAULT_ADDRESS = 'ADD_DEFAULT_ADDRESS';
export const CHANGE_DEFAULT_ADDRESS = 'CHANGE_DEFAULT_ADDRESS';
export const DELETE_ADDRESS = 'DELETE_ADDRESS';
export const EDIT_ADDRESS = 'EDIT_ADDRESS';
export const BILLING_ADDRESS = 'BILLING_ADDRESS';

export const getAddresses = () => {
    return dispatch => {
        axios.post(config.BASE_URL + '/account/get-addresses', {
            "Authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after address request");

            return dispatch({
                type: GET_ADDRESSES,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
                console.log("oh come on");
            });
    }
};

export const addAsBillingAddress = (data) => {

    // let dataToBeSent = {
    //     Authorization: window.localStorage.getItem('authToken'),
    //     addressId: data.addressId,
    //     country: data.country,
    //     street: [data.street],
    //     telephone: data.telephone.toString(),
    //     postcode: data.postcode.toString(),
    //     city: data.city,
    //     firstname: data.firstname,
    //     lastname: data.lastname
    // };
    let dataToBeSent = {
        "Authorization": window.localStorage.getItem('authToken'),
        "address": data
    }
    return dispatch => {
        console.log(dataToBeSent)
        axios.post(config.BASE_URL + '/user/add-user-shipping-address', dataToBeSent).then((res) => {
            console.log(res.data, "data coming after billing address request");

            return dispatch({
                type: BILLING_ADDRESS,
                data: res.data
            })
        }).catch((error) => {
            console.log("error has occured");
            console.log(error);
            });
    }
};



export const editExistingAddress = (data) => {

    let dataToBeSent = {
        Authorization: window.localStorage.getItem('authToken'),
        addressId: data.addressId,
        country: data.country,
        street: [data.street],
        telephone: data.telephone.toString(),
        postcode: data.postcode.toString(),
        city: data.city,
        firstname: data.firstname,
        lastname: data.lastname
    };
    return dispatch => {
        console.log(dataToBeSent)
        axios.post(config.BASE_URL + '/user/edit-selected-address', dataToBeSent).then(function(res){
            console.log(res.data, "data coming after edit address request");

            return dispatch({
                type: EDIT_ADDRESS,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

export const deleteSelectedAddress = (idToBeDeleted) => {
    return dispatch => {
        axios.post(config.BASE_URL + '/user/delete-selected-address', {
            "Authorization" : window.localStorage.getItem('authToken'),
            "addressId": idToBeDeleted
        }).then(function(res){
            console.log(res.data, "data coming after delete address request");

            return dispatch({
                type: DELETE_ADDRESS,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

export const setAddressAsDefault = (id) => {
    return dispatch => {
        // data.Authorization = window.localStorage.getItem('authToken');
        axios.post(config.BASE_URL + '/account/set-address-as-default/' + id, {Authorization: window.localStorage.getItem('authToken')}).then((res) => {
            console.log(res.data, "data coming after default address change request");

            return dispatch({
                type: CHANGE_DEFAULT_ADDRESS,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

export const addNewDefaultAddress = (data) => {
    return dispatch => {
        data.Authorization = window.localStorage.getItem('authToken');
        axios.post(config.BASE_URL + '/account/address/default', data).then(function(res){
            console.log(res.data, "data coming after default address addition request");

            return dispatch({
                type: ADD_DEFAULT_ADDRESS,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

