import axios from 'axios';
import * as  config from '../config.js';
// importing helpers
// import {attribute_array_creator} from '../helpers/general'
const attribute_array_creator = require('../helpers/general').attribute_array_creator;


export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const CHANGECOLOR = 'CHANGECOLOR';
export const CHANGESIZE = 'CHANGESIZE';
export const PRODUCT_BODY = 'PRODUCT_BODY'
export const PRODUCT_NOW = 'PRODUCT_NOW'
export const SIZE_GET = 'SIZE_GET'
export const COLOR_GET = 'COLOR_GET'
export const PRICE_GET = 'PRICE_GET'
export const LOAD_PRICE = 'LOAD_PRICE'
export const ADD_KART = 'MODAL_ADD_KART'
export const ADD_WISHLIST = "ADD_WISHLIST"

const hasData = require('../helpers/general').hasData


export const getProductVariants = (sku) =>{
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_VARIANTS + sku).then(res => {
            let data = {
                variants: res.data,
                color_list: [],
                size_list: [],
                sku_list: [],
                name_list: [],
                price_list: []
            }

            hasData(res.data).map((product, index) => {
                data.sku_list.push(product.sku);
                data.name_list.push(product.name);
                data.price_list.push(product.price);
                product.custom_attributes.map(attribute => {
                    if(attribute.attribute_code === 'color')
                    {
                        console.log("%c current color", "color: green")
                        console.log(data.color_list.find(color => {return color.value === attribute.value}))
                        if(data.color_list.find(color => {return color.value === attribute.value}) === undefined)
                            data.color_list.push({label: '', value: attribute.value});
                    }
                    else if(attribute.attribute_code === 'size')
                    {
                        if(data.size_list.find(size => {return size.value === attribute.value}) === undefined)
                            data.size_list.push({label: '', value: attribute.value});
                    }
                    // else continue;
                })
            })


            return dispatch({
                type: 'MODAL_AVAILABLE_VARIANTS',
                data: data
            })
        })
    }
}

export const changeVariant = (variants, params) => {
    return dispatch => {
        let product_details = variants.find( element => {
            return compare(element, params)
        })
        if(product_details){
            axios.get(config.BASE_URL + config.MEDIA_GALLERY + product_details.sku).then(response => {
                console.log(`%c${response}`, "color: yellow");
                product_details["media_gallery_entries"] = [...response.data];
                return dispatch({
                    type: 'MODAL_CHANGE_VARIANT',
                    data: product_details,
                    size: params.size,
                    color: params.color
                })
            })
        }

    }
}

function compare(element, param){
    let found = element.custom_attributes.find( el => {
        return el.attribute_code === param.attribute_code && el.value === param.value
    })

    if(found){
        return true
    }
    else return false
}

export const addToCart = (sku, qty, color_id, color_value, size_id, size_value) => {
    return dispatch => {
        let data = {
            'qty': qty,
            'sku': sku,
            'color_id': color_id,
            'color_value': color_value,
            'size_id': size_id,
            'size_value': size_value
        };
        console.clear()
        console.log("%c Data for cart: ", "color: green");
        console.log(data)
        let userCartId = window.localStorage.getItem('userCartId');
        if (userCartId) {
            data.Authorization = window.localStorage.getItem('authToken');
            data.id = userCartId;
            axios.post(config.BASE_URL + '/user/add-item-to-user-cart' , data)
                .then(res => {

                    console.log(window.localStorage.getItem('cartId'))

                    let success_message = 'Yay! Your Product has been Added to Cart!'


                    return dispatch({
                        type: ADD_KART,
                        data: "True",
                        message: success_message
                    })
                }).catch(err => console.log('the product couldn\'t be added to cart'));
            return;
        }

        let cartId = window.localStorage.getItem('cartId');
        if(cartId){
            axios.post(config.BASE_URL + config.ADD_TO_CART + cartId , data)
                .then(res => {

                    console.log(window.localStorage.getItem('cartId'))


                    let success_message = 'Yay! Your Product has been Added to Cart!'


                    return dispatch({
                        type: ADD_KART,
                        data: "True",
                        message: success_message
                    })
                })
        }
        else
        {
            axios.get(config.BASE_URL + config.CREATE_GUEST_CART).then(res => {

                // setting cart id in local storage
                window.localStorage.setItem('cartId', res.data);

                // request for adding product to the cart

                axios.post(config.BASE_URL + config.ADD_TO_CART + res.data , data)
                    .then(res => {

                        console.log(window.localStorage.getItem('cartId'))

             

                        let success_message = 'Yay! Your Product has been Added to Cart!'


                        return dispatch({
                            type: ADD_KART,
                            data: "True",
                            message: success_message
                        })
                    }).catch(err => {
                    let error_message = 'Sorry... Your product could not be added...'

                    return dispatch({
                        type: ADD_KART,
                        message: error_message
                    })
                })
            })
        }

    }
}

export const addItemToWishlist = (sku, qty, color_id, color_value, size_id, size_value, name, price) => {
    return dispatch => {
        axios.post(config.BASE_URL + '/user/add-item-to-user-wishlist', {
            "Authorization": window.localStorage.getItem('authToken'),
            "userId": window.localStorage.getItem('userCartId'),
            "sku": sku,
            "qty": qty,
            "color_id": color_id,
            "color_value": color_value,
            "size_id": size_id,
            "size_value": size_value,
            "name": name,
            "price": price
        }).then(res => {
            // window.localStorage.setItem('')
            console.log(res.data, "add item to wishlist");
            return dispatch({
                type: ADD_WISHLIST,
                data: res.data
            })
        }).catch(err => {
            console.log(err);
        })
    }
}

export const changeModal = (data) => {
    return dispatch => {
        return dispatch({
            type : 'CHANGE_MODAL_DATA',
            data: data
        })
    }
}

export const getArrayByType = (available, item, type) => {
    
    return dispatch => {
        
        let array = attribute_array_creator(available, item, type);
        let data = {
            "type": type,
            "data": array
        }
        return dispatch({
            type: 'CREATE_ATTRIBUTE_ARRAY',
            data: data
        })
    }
}
