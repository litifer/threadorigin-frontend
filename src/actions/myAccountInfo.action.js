import axios from 'axios'
import * as  config from '../config.js';
export const UPDATE_INFO = 'UPDATE_INFO';

export const updateCustomerInfo = (contactData) => {
    console.log('in login action');
    let data = {
        "Authorization": window.localStorage.getItem('authToken'),
        "email": contactData.email,
        "firstname": contactData.fname,
        "lastname": contactData.lname
    };
    return dispatch => {
        axios.post(config.BASE_URL + '/account/update', data).then(function(res){
            console.log(res.data, "data coming after address request");

            return dispatch({
                type: UPDATE_INFO,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};
