import axios from 'axios'
import * as  config from '../config.js';
export const GET_USER_DATA = 'GET_ADDRESSES';

export const getUserData = () => {
    console.log('in login action');
    return dispatch => {
        axios.post(config.BASE_URL + '/getaccount', {
            "authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after dashboard request");

            return dispatch({
                type: GET_USER_DATA,
                data: res.data
            })
        })
            .catch(function (error) {
                console.log(error);
            });
    }
};

