import axios from 'axios'
import * as  config from '../config.js';
export const GET_CAMPAIGN_LOOKS = 'GET_CAMPAIGN_LOOKS';
export const GET_LOOKBOOKS = 'GET_LOOKBOOKS';
export const GET_COLORS = 'GET_COLORS';
export const GET_SIZES = 'GET_SIZES';
export const GET_PRODUCT_DETAILS = 'GET_PRODUCT_DETAILS';

export const getCampaignLooks = () => {
    return dispatch => {
        axios.get(config.BASE_URL + '/category/get-campaign-looks', {
            "Authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after address request");

            return dispatch({
                type: GET_CAMPAIGN_LOOKS,
                data: res.data
            })
        }).catch(function (error) {
            console.log(error);
            console.log("oh come on");
        });
    }
};

export const getLookbooks = () => {
    return dispatch => {
        axios.get(config.BASE_URL + '/category/get-lookbooks', {
            "Authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after lookbooks request");

            return dispatch({
                type: GET_LOOKBOOKS,
                data: res.data
            })
        }).catch(function (error) {
            console.log(error);
            console.log("oh come on");
        });
    }
};

export const getProductDetails = (sku) => {
    return dispatch => {
        axios.get(config.BASE_URL + config.GET_PRODUCT_DATA + sku).then(res => {
            return dispatch({
                type: GET_PRODUCT_DETAILS,
                data: res.data
            })
        })
    }
};

export const getColors = () => {
    return dispatch => {
        axios.get(config.BASE_URL + '/products/colors', {
            "Authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after address request");

            return dispatch({
                type: GET_COLORS,
                data: res.data
            })
        }).catch(function (error) {
            console.log(error);
            console.log("oh come on");
        });
    }
};

export const getSizes = () => {
    return dispatch => {
        axios.get(config.BASE_URL + '/products/sizes', {
            "Authorization" : window.localStorage.getItem('authToken')
        }).then(function(res){
            console.log(res.data, "data coming after address request");

            return dispatch({
                type: GET_SIZES,
                data: res.data
            })
        }).catch(function (error) {
            console.log(error);
            console.log("oh come on");
        });
    }
};
