import axios from 'axios'
import * as  config from '../config.js';
import {push} from 'react-router-redux';
export const LOGIN = 'LOGIN';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const SET_LOGIN_DATA_TO_NULL = 'SET_LOGIN_DATA_TO_NULL';
export const SET_WISHLIST = 'SET_WISHLIST';


export const initWishlist = (authToken, cartId) => {
    axios.post(config.BASE_URL + '/user/create-user-wishlist', {
        "Authorization": authToken,
        "userId": cartId
    }).then(res => {
        // window.localStorage.setItem('')
        console.log(res.data, "wishlist initialization");
    }).catch(err => {
        console.log(err);
    })
}

export const mergeUserAndGuestCarts = () => {
    console.log('in merge data func');
    const guestCartId = window.localStorage.getItem('cartId');
    if (guestCartId) {

        axios.post(config.BASE_URL + '/user/merge-user-and-guest-cart/' + guestCartId, {
            "Authorization": window.localStorage.getItem('authToken')
        }).then(res => {
            console.log('carts merged successfully', res);
            // window.localStorage.setItem('')
            console.log(JSON.parse(JSON.parse(res.data.userCartId).toString()), "carts are merged, here is the id");
            window.localStorage.setItem('userCartId', JSON.parse(res.data.userCartId).toString());
            window.localStorage.removeItem('cartId');
            initWishlist(window.localStorage.getItem('authToken'), JSON.parse(res.data.userCartId).toString());
        }).catch(err => {
            console.log(err);
        })

    } else {
        axios.post(config.BASE_URL + '/user/get-cart-id', {
            "Authorization": window.localStorage.getItem('authToken')
        }).then(res => {
            console.log('carts merged successfully', res);
            // window.localStorage.setItem('')
            console.log(res.data.toString(), "carts did not require merging, userCartId stored");
            window.localStorage.setItem('userCartId', res.data.toString());
            window.localStorage.removeItem('cartId');
            initWishlist(window.localStorage.getItem('authToken'), res.data.toString());
        }).catch(err => {
            console.log(err);
        })
    }

};

export const login = (user, pswd) => {
    console.log('in login action');
    return dispatch => {
         axios.post(config.BASE_URL + '/account/validate', {
           "email"    : user,
           "password" : pswd
        }).then(function(res) {
             console.log(res.data, "data coming after logging in");
             if (res.data.success === "false") {
                 console.log('no token found');
             } else {
                 window.localStorage.setItem('authToken', res.data.success);
                 mergeUserAndGuestCarts();
                 push('/dev');

             }
             return dispatch({
                 type: LOGIN,
                 data: res.data
             })
         })
            .catch(function (error) {
              console.log(error);
        });
    }
};

export const setLoginDataToNull = () => {
    return dispatch => dispatch({
        type: SET_LOGIN_DATA_TO_NULL
    });
};


