module.exports = {

    BASE_URL: /*'https://api.threadorigin.com/api/v1'*/ /*'http://52.66.118.207:3000/api/v1/'*/ 'http://localhost:8000/api/v1',
    IMG_URL: 'https://dev.threadorigin.com/threadorigin/pub/media/catalog/product' /*'http://13.232.169.158/threadorigin/pub/media/catalog/product'*/ /*'http://13.232.65.165/threadorigin/pub/media/catalog/product'*/,
    ACCESS_ENDPOINT: '/auth/access-token',
    CATEGORY_PRODUCTS_END_POINT: '/product/all/',
    GET_SIZES_END_POINT: '/products/sizes',
    GET_COLOR_END_POINT: '/products/colors',
    SEARCH_END_POINT: '/product/find/',
    CAMP_1: '/homepage/camp1',
    CAMP_2: '/homepage/camp2',
    CAMP_3: '/homepage/camp3',
    INSTA_URL: '/homepage/instafeed',
    GET_PRODUCT_DATA: '/product/',
    GET_ALL_CATEGORIES: '/categories',

    //cart endpoints
    CREATE_GUEST_CART: '/create/guest-cart/',
    GET_GUEST_CART: '/get/guest-cart/',
    GET_USER_CART_PAYMENT_INFO: '/user/payment-info/',
    ADD_TO_CART: '/add/items/guest-cart/',
    ADD_TO_USER_CART: '',
    PAYMENT_INFO: '/payment-info',
    DELETE_ITEM: '/remove/cart/',
    UPDATE_ITEM: '/update/cart/',

    //newsletter endpoint
    SUBSCRIBE_NEWSLETTER: '/user/newsletter/subscribe',

    //product variants
    GET_COLORS: '/products/colors',
    GET_SIZES: '/products/sizes',
    GET_VARIANTS: '/product/variants/',
    MEDIA_GALLERY: '/media/images/',

    //Shipping Endpoints
    ADD_BILLING_ADDRESS_ENDPOINT: '/add/billing-address/cart/',
    ADD_SHIPPING_ADDRESS_ENDPOINT: '/add/shipping-address/cart/',

    //campaign looks
    GET_CAMPAIGN_LOOKS: '/category/get-campaign-looks'
}
