import React, {Component} from 'react'
import axios from'axios';
import {withRouter} from 'react-router-dom';
import indiaImg from '../../img/Bitmap-india.png';
import logo from '../../img/final-logo2.png'
import responsive_logo from '../../img/brown-logo-mobile.png'
import indiaArrow from '../../img/keyboard-right-arrow-button.png'
import searchIcon from '../../img/product_search1.png';
import cartIcon from '../../img/product_cart1.png';
import myaccount from '../../img/myaccount1.png'

import '../../css/header.css'
import '../../css/product_header.css'

import {Urls} from '../../consts'


// const Urls = {
//     dresses : "/category?id=10",
//     bottoms : "/category?id=13",
//     tops : "/category?id=12",
//     lookBooks : "/category?id=11",
//     collections : "/category?id=5",
//     sandook : "/category?id=6",
//     cart : "/cart",
//     myAccount : "/accountDashboard"
// }

class Header1 extends Component{

    constructor(props){
        super(props);
        this.state = {value : ""};
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        console.log(this.props, "homepage header token");
        // if (window.localStorage.getItem('authToken') !== "") {
        if (this.props.location.pathname !== '/register'
            && this.props.location.pathname !== '/forgot'
            && this.props.location.pathname !== '/newPassword'
            && this.props.location.pathname !== '/checkout'
            && this.props.location.pathname !== '/category'
            && this.props.location.pathname !== '/campaignLooks'
            && this.props.location.pathname !== '/checkout'
            && this.props.location.pathname !== '/product'
            && this.props.location.pathname !== '/termsAndCondition'
            && this.props.location.pathname !== '/faq'
            && this.props.location.pathname !== '/privacycontent'
            && this.props.location.pathname !== '/aboutus'
            && this.props.location.pathname !== '/cart') {
            axios.post('http://localhost:8000/api/v1/getaccount', {
                authorization: window.localStorage.getItem('authToken')
            }).then(res => {
                // console.log(res.data.message === "User token missing!" ? 'no teken' : 'stupid', "header product info");
                if (res.data.message === "User token missing!") {
                    // console.log(this.props);
                    return this.props.history.replace('/login');
                    // return <Redirect to="/login"/>
                }
            }).catch(err => {
                console.log(err);})
        }
        // }

        // window.localStorage.removeItem('authToken');
    }

    initLogout = () => {
        window.localStorage.removeItem('authToken');
        // window.localStorage.removeItem('cartId');
        window.localStorage.removeItem('userCartId');
        this.props.history.replace('/dev');
    };

    handleChange(event){
        this.setState({value: event.target.value});
    }

    render(){
        var link_categ = Urls.SEARCH_URL+this.state.value;
        return(
            <div className="header2-nav-bg header2-z-index fixed-top">
                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding header2-top-bar-bg" id="none">
                    <div className="col-md-10 col-sm-10 offset-1 float-md-left float-sm-left no-padding">
                        <div className="col-md-6 col-sm-6 float-md-left float-sm-left no-padding header2-left-text-margin">
                            <span className="header2-left-text">Get Free Shipping on Orders above Rs. 1999</span>
                        </div>
                        {/* <div className="col-md-6 col-sm-6 float-md-right float-sm-right no-padding header-right-text-margin">
                            <div className="col-md- col-sm- float-md-right float-sm-right no-padding">
                                <img src={indiaImg} /><span className="header2-left-text header2-right-margin">Indian Rupees</span><img src={indiaArrow} />
                            </div>
                        </div> */}
                    </div>
                </div>
                <nav className="navbar navbar-expand-lg navbar-light header2-nav-bottom-border header2-z-index">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon header2-nav-toggler"></span>
                    </button>
                    { (window.innerWidth > 420) ?
                        <a className="navbar-brand" href="/" id="brand-none"><img class="logo-img" src={logo}/></a>
                        : <a className="navbar-brand" href="/" id="brand-none"><img class="logo-img" src={responsive_logo}/></a>
                    }
                    <a className="navbar-brand" id="brand-none"href={Urls.CART_URL}><img src={cartIcon}/></a>
                    <div className="col-md-11 col-sm-11 mx-auto no-padding">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <div className="col-md-4 no-padding">
                                <div className="col-md-12 float-md-left no-padding links_head">
                                    <ul className="navbar-nav">
                                        <li className="nav-item active nav_header_list">
                                            <a className="nav-link header-padding" href={Urls.DRESSES_URL}><span className="header2-text">DRESSES</span></a>
                                        </li>
                                        <li className="nav-item active nav_header_list">
                                            <a className="nav-link header-padding" href={Urls.BOTTOMS_URL}><span className="header2-text">BOTTOMS</span></a>
                                        </li>
                                        <li className="nav-item active nav_header_list">
                                            <a className="nav-link header-padding" href={Urls.TOPS_URL}><span className="header2-text">TOPS</span></a>
                                        </li>
                                        <li className="nav-item active">
                                            <a className="nav-link header-padding" href={Urls.LOOKBOOKS_URL}><span className="header2-text">LOOKBOOKS</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <ul className="navbar-nav header-logo-margin">
                                <li className="nav-item active" id="none">
                                <a className="nav-link" href="/"><img src={logo}/></a>
                                </li>
                            </ul>

                            <div className="col no-padding" style={{"margin-right": "-7%"}}>
                                <div className="col-md-12 no-padding">
                                    <ul className="navbar-nav" id="nav-left-part">
                                        <li className="nav-item active col-6" style={{"padding":"0%"}}>
                                            <input className="form-control header2-search-background header2-search" type="text" placeholder="Search your products here" value = {this.state.value} onChange={this.handleChange} />
                                        </li>
                                        <li className="nav-item active header2-search-border">
                                            <a className="nav-link" href={Urls.SEARCH_URL+this.state.value}><img src={searchIcon}/></a>
                                        </li>
                                        <li className="nav-item active header-padding-left">
                                            <a className="nav-link" href={Urls.MYACCOUNT_URL}><img src={myaccount}/></a>
                                        </li>
                                        <li className="nav-item active header-padding-left">
                                            <a className="nav-link" href={Urls.CART_URL}><img src={cartIcon}/></a>
                                        </li>
                                        {window.localStorage.getItem('authToken') ? <li className="nav-item active header-padding-left">
                                            <button onClick={this.initLogout} style={{backgroundColor: 'transparent', border: 'none', cursor: 'pointer'}} className="nav-link" href={Urls.cart}><i style={{fontSize: '22px', color: '#85754E'}} className={'fa fa-sign-out'}></i></button>
                                        </li>: null}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default withRouter(Header1)