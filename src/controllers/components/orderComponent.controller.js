import React from 'react';

const orderComponent = (props) => (
    <div className="col-md-6 col-sm-6 float-md-left float-sm-left padding-left">
        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding account-box">
            <div className="col-md-11 col-sm-11 offset-1 float-md-left float-sm-left no-padding account-box-top-margin absolute">
                <div className="col-md-5 col-sm-5 float-md-left float-sm-left account-top-box">
                    <span className="account-top-box-text">ORDER # {props.orderNumber}</span>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                <div className="col-md-2 col-sm-2 float-md-left float-sm-left no-padding">
                    <span className="account-box-text">Order Date:</span>
                </div>
                <div className="col-md-10 col-sm-10 float-md-left float-sm-left">
                    <span className="account-box-text">{props.orderDate}</span>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                <div className="col-md-2 col-sm-2 float-md-left float-sm-left no-padding">
                    <span className="account-box-text">Shipped To:</span>
                </div>
                <div className="col-md-10 col-sm-10 float-md-left float-sm-left">
                    <span className="account-box-text">{props.personName}</span>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                <div className="col-md-2 col-sm-2 float-md-left float-sm-left no-padding">
                    <span className="account-box-text">Total:</span>
                </div>
                <div className="col-md-10 col-sm-10 float-md-left float-sm-left">
                    <span className="account-box-text">Rs. {props.orderAmount}</span>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                <div className="col-md-2 col-sm-2 float-md-left float-sm-left no-padding">
                    <span className="account-box-text">Status:</span>
                </div>
                <div className="col-md-10 col-sm-10 float-md-left float-sm-left">
                    <span className="account-box-text">{props.orderStatus}</span>
                </div>
            </div>
            <div className="col-md-10 col-sm-10 offset-1 float-md-left float-sm-left no-padding">
                <button onClick={props.onViewOrderClick} className="btn col-md-3 float-right acoount-btn"><span className="account-btn-text">VIEW ORDER</span></button>
            </div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
        </div>
    </div>
);

export default orderComponent;