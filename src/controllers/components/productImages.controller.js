import React, {Component} from 'react'
import PropTypes from 'prop-types';
import SimpleZoom from 'react-simple-zoom'
import * as  config_ from '../../config.js';
import '../../css/header.css'
import product_header from '../../img/product_header.png';
import { hasData } from '../../helpers/general.js';


export class Rows extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return <div className="row no-margin">
                    {
                        this.props.children.map((item, index) => {
                            return  <div className="product-images-box">
                                        <SimpleZoom thumbUrl={item.url_img} fullUrl={product_header} zoomScale={3.6} onExitTimeout={2000} height="100%" />
                                    </div>
                        })
                    }    
                </div>
    }
} 

// Required data for each carousel-item i.e children are required
Rows.propTypes = {
    children: PropTypes.arrayOf(PropTypes.object).isRequired
};

export class ProductImages extends Component{

    constructor(props){
        super(props)
    }

    componentDidMount(){
        
        /*Currently done in jquery but in future will be shifted to react*/ 
        // swipable carousel code
        // $(`#carousel${this.props.id}`).on("touchstart", function (event) {
        //     var xClick = event.originalEvent.touches[0].pageX;
        //     $(this).one("touchmove", function (event) {
        //         var xMove = event.originalEvent.touches[0].pageX;
        //         if (Math.floor(xClick - xMove) > 5) {
        //             $(this).carousel('next');
        //         }
        //         else if (Math.floor(xClick - xMove) < -5) {
        //             $(this).carousel('prev');
        //         }
        //     });
        //     $(`#carousel${this.props.id}`).on("touchend", function () {
        //         $(this).off("touchmove");
        //     });
        // });
    }

    handleChildrenImages(){
        let children = []
        let rows = []

        if(!hasData(this.props.product_get.media_gallery_entries).length){return []}
        
        console.log("%c The media gallery is:", "color:green;")
        console.log(this.props.product_get.media_gallery_entries)
        for(var i=1; i<this.props.product_get.media_gallery_entries.length-1; i++){
            let media_gallery_entry = this.props.product_get.media_gallery_entries[i];
            let img_url = media_gallery_entry.file;
            let item = {
                url_img: config_.IMG_URL + (img_url? img_url : '')
            }

            children.push(item);
                if(i%2 == 0){
                    rows.push(<Rows children={children}/>)
                    children = []
                }
        
        }

    return rows
    }

    render(){
        console.log("I am in row...")
        return(
        // <div className="container-fluid no-padding "> 
        //     <div id="body2-container-animation-trigger" className="col-md-12 no-padding">
        //         <div id="body2-top" class="body2-top anim_scroll_reveal_default product-body-heading-text">Beyond Fashion with Exclusive Products</div>
        //         <div className="col-md-12 no-padding body2_width carousel-container" >
        //             <div id={`carousel${this.props.id}`} className="carousel slide" data-interval="0" data-ride="carousel">
        //                 <div class="carousel-inner">
        //                 {
        //                     this.handleChildrenImages().map((item)=>{
        //                         return item
        //                     })
        //                 }
                        
        //                 </div>

        //                 <a class="carousel-control-prev carousel-control-btn" href={`#carousel${this.props.id}`} data-slide="prev">
        //                     <span class="carousel-control-prev-icon" ></span>
        //                 </a> 
        //                     <a class="carousel-control-next carousel-control-btn" href={`#carousel${this.props.id}`} data-slide="next">
        //                     <span class="carousel-control-next-icon"></span>
        //                 </a>
        //             </div>
        //         </div>
        //     </div>
        // </div>


        <div className="container-fluid no-padding product-image-top-margin d-none d-md-block">
            {
                this.handleChildrenImages().map((item)=>{
                    return item
                })
            }
        </div>
        )
    }
}
export default ProductImages