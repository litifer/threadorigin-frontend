import React, {Component} from 'react'
import {Urls,ContactInfo} from '../../consts'


import '../../css/product_footer.css'

var FontAwesome = require('react-fontawesome');
class Footer extends Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
          <div class="footer">
          
          <div id="collapse-footer" class="d-md-none collapsible-footer">
          <div class="footer-social-media-section">
              <div class="footer-social-media-header">Follow us on</div>
              <div class="footer-social-media-buttons">
              <a href="https://www.facebook.com/threadorigin/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
              <a href="https://twitter.com/threadorigin"><i class="fa fa-twitter"  aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/threadorigin/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </div>
          </div>
            <div class="card">
              <div class="card-header" id="headingOne">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <h5 class="mb-0">
                      Categories                
                    </h5>
                  </button>
              </div>

              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#collapse-footer">
                <div class="card-body">
                  <ul className = 'footer_listing footer_1'>
                          <li className="footer-li-padding">New Arrivals</li>
                          <li className="footer-li-padding">Dresses</li>
                          <li className="footer-li-padding">Footwear</li>
                          <li className="footer-li-padding">Accessories</li>
                          <li className="footer-li-padding">Lookbooks</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingTwo">
                
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <h5 class="mb-0">
                      Policies and Conditions
                    </h5>
                  </button>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#collapse-footer">
                <div class="card-body">
                   <ul className = 'footer_listing footer_3'>
                          <a href={Urls.TNC_URL}><li className="footer-li-padding">Terms and Condition</li></a>
                          <a href={Urls.PRIVACY_POLICY_URL}><li className="footer-li-padding">Privacy Policy</li></a>
                          {/* <a href={"#"}><li className="footer-li-padding">Size Guide</li></a> */}
                          <a href={Urls.FAQ_URL}><li className="footer-li-padding">FAQ's</li></a>
                          <a href={Urls.LOOKBOOKS_URL}><li className="footer-li-padding">Lookbooks</li></a>
                          <a href={Urls.CONTACTUS_URL}><li className="footer-li-padding">Contact Us</li></a>
                          <a href={Urls.ABOUTUS_URL}><li className="footer-li-padding">about us</li></a>
                    </ul> 
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <h5 class="mb-0">
                      Contact Information
                    </h5>
                  </button>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#collapse-footer">
                <div class="card-body">
                  <ul className = 'footer_listing footer_2'>
                          <li>Write to us on: <br />
                              {ContactInfo.EMAIL}</li><br />
                          <li>{ContactInfo.DAYS} <br />  
                              {ContactInfo.TIMINGS}</li><br />
                          <li>For Queries <br />
                              {ContactInfo.MOBILE_NO}</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
            

            {/*  Footer for big screens */}
                  <div className="col-md-12 footer_bg_col footer-inner-container foot_hide d-none d-lg-block">
                    <div className = 'row footer-paaing-top'>
                      <div className="col-md-4 col-xs-6">
                        <ul className = 'footer_listing footer_1'>
                          <a href="/category?id=4"><li className="footer-li-padding">New Arrivals</li></a>
                          <a href={Urls.DRESSES_URL}><li className="footer-li-padding">Dresses</li></a>
                          <a href={"#"}><li className="footer-li-padding">Footwear</li></a>
                          <a href="/category?id=4"><li className="footer-li-padding">Accessories</li></a>
                          <a href={Urls.LOOKBOOKS_URL}><li className="footer-li-padding">Lookbooks</li></a>
                        </ul>
                      </div>
                      <div className="col-md-4 column_style padding-left">
                        <ul className = 'footer_listing footer_2'>
                          <li>Write to us on:</li>
                          <li>{ContactInfo.EMAIL}</li><br />
                          <li>{ContactInfo.DAYS}</li>  
                          <li>{ContactInfo.TIMINGS}</li><br />
                          <li>For Queries</li>
                          <li>{ContactInfo.MOBILE_NO}</li>
                        </ul>
                      </div>
                      <div className="col-md-4">
                        <ul className = 'footer_listing footer_3'>
                          <a href={Urls.TNC_URL}><li className="footer-li-padding">terms and condition</li></a>
                          <a href={Urls.PRIVACY_POLICY_URL}><li className="footer-li-padding">privacy policy</li></a>
                          {/* <a href={"#"}><li className="footer-li-padding">size guide</li></a> */}
                          <a href={Urls.FAQ_URL}><li className="footer-li-padding">faq's</li></a>
                          <a href={Urls.LOOKBOOKS_URL}><li className="footer-li-padding">Lookbooks</li></a>
                          <a href={Urls.CONTACTUS_URL}><li className="footer-li-padding">contact us</li></a>
                          <a href={Urls.ABOUTUS_URL}><li className="footer-li-padding">about us</li></a>
                        </ul>
                      </div>
                      <div className="col-md-12 col-sm-12 no-padding footer-icons-div-margin">
                        <div className="col-md-5 col-sm-5 mx-auto no-padding text-center">
                          <a className="footer-padding" href="https://www.facebook.com/threadorigin/"><i class="fa fa-facebook fa-2x icon-color-white" aria-hidden="true"></i></a>
                          <a className="footer-padding" href="#"><i class="fa fa-youtube-square fa-2x icon-color-white" aria-hidden="true"></i></a>
                          <a className="footer-padding" href="https://twitter.com/threadorigin"><i class="fa fa-twitter fa-2x icon-color-white"  aria-hidden="true"></i></a>
                          <a className="footer-padding" href="https://www.instagram.com/threadorigin/"><i class="fa fa-instagram fa-2x icon-color-white" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        )
    }
}

export default Footer