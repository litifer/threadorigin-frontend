import React, {Component} from 'react'
import '../../css/header.css'

export class Bill extends Component{

    constructor(props){
        super(props)
    }

    componentDidMount(){
        
    }

    showDiscount(){
        return <tr className="col-md-12 no-padding cart-padding-patment">
                <td className="col-md-8 cart-right-rectangle-text">Discount</td>
                <td className="col-md-8 cart-right-rectangle-text text-right">&#x20b9;{this.props.bill.totals.discount_amount}</td>
               </tr> 
    }

    render(){
        return (
            <div className="">  
                <div className="">
                    <table>
                        <tbody className="row no-margin">
                            <tr className="col-md-12 no-padding cart-padding-patment">
                                <td className="col-md-8 d-inline-block cart-right-rectangle-text">Sub Total</td>
                                <td className="col-md-4 d-inline-block cart-right-rectangle-text text-right">&#x20b9;{this.props.bill.totals.subtotal}</td>
                            </tr>
                            <tr className="col-md-12 no-padding cart-padding-patment">
                                <td className="col-md-8 d-inline-block cart-right-rectangle-text">Shipping Costs</td>
                                <td className="col-md-4 d-inline-block cart-right-rectangle-text text-right">&#x20b9;{this.props.bill.totals.shipping_amount}</td>
                            </tr>
                                {this.props.bill.totals.discount_amount? this.showDiscount() : ""}
                        </tbody>    
                    </table>    
                </div>
                <div class="col-md-12 col-sm-12 d-inline-block" style={{"border-top" : "solid 2px #cbcbcb","position": "absolute","bottom":"0","right":"0","padding-top": "2%","padding-bottom": "2%"}}>   
                    <div class="col-md-8 col-sm-8 d-inline-block no-padding"><span className="cart-right-rectangle-text">Payable Amount</span></div>
                    <div class="col-md-4 col-sm-4 d-inline-block no-padding text-right">
                        <span className="cart-right-rectangle-text">&#x20b9;{this.props.bill.totals.grand_total}</span>
                    </div>
                </div>    
            </div>
        )
    }
}
export default Bill