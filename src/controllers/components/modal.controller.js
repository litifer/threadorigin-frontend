import React, {Component} from 'react'
import PropTypes from 'prop-types';
import '../../css/category.css'
import ModalImg from '../../img/product_body1_img3.png'

import {fetchprodBody, addToCart, getProductVariants, changeVariant, getArrayByType} from '../../actions/modal.actions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { hasData, getImgUrl } from '../../helpers/general';

export class ModalImgContainer extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className="modal-img">
                <img src={this.props.img} />
            </div>
        )
    }
}
// Required data for each products
ModalImgContainer.propTypes = {
    img: PropTypes.string.isRequired
};

export class ModalInfo extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className="modal-info">
                <div className="row no-margin">
                    <div className="col-md-12 no-padding"> 
                        <div className="col-md-8 text-left no-padding float-md-left">
                            <span className="modal-item-name">{this.props.name}</span>
                        </div>
                        <div className="col-md-4 text-right no-padding float-md-left">
                            <span className="modal-item-currency">Rs.</span><span className="modal-item-price"> {this.props.price}</span>
                        </div>
                    </div>
                    <div className="col-md-12">&nbsp;</div>
                    <div className="col-md-12 no-padding text-left">
                        <span className="modal-item-desc">{this.props.desc}</span>
                    </div>
                    <div className="col-md-12 modal-border">&nbsp;</div>
                    <div className="col-md-12">&nbsp;</div>
                    <div className="col-md-12 no-padding">
                        <div className="col-md-2 no-padding text-left float-md-left">
                            <span className="modal-size-text">Size:</span>
                        </div>
                        <div className="col-md-10 no-padding text-left float-md-left">
                            {
                                hasData(this.props.size_array).map((size)=>{
                                    return <div className="d-inline-block modal-size-margin" onClick = {event => this.props.handleVariant({type: 'CHANGE_SIZE', value:size.value})}>
                                                <span className="modal-size-text-inner">{size.label}</span>
                                            </div>
                                })
                            }
                            
                        </div>
                    </div>
                    <div className="col-md-12">&nbsp;</div>
                    <div className="col-md-12 no-padding ">
                        <div className="col-md-2 no-padding float-md-left text-left">
                            <span className="modal-size-text">Color:</span>
                        </div>
                        <div className="col-md-10 no-padding float-md-left text-left">
                        {
                                hasData(this.props.color_array).map((color)=>{
                                    return <div className = "product-size-circle float-left product-margin-circles" onClick = {event => this.props.handleVariant({type: 'CHANGE_COLOR', value:color.value})}>
                                              <div  className = "product-color-circle" style = {{'background-color':color.label}} ></div>
                                            </div>
                                })
                        }
                        </div>
                    </div>
                    <div className="col-md-12">&nbsp;</div>
                    <div className="col-md-12 no-padding">
                        <button className="col-md-5 btn text-center font_family1 font_size2 font_wt2 style11_prod" onClick={this.props.addToCart}>
                            <span className='style11_prod_text'>ADD TO CART &nbsp;&nbsp;---</span>
                        </button>
                        <button className="col-md-5 btn style12_prod modal-add-to-wishlist-btn" onClick={this.props.add_wish}>
                            <span className='style12_prod_text'><i class="heart-margin fa fa-heart-o fa-1x" aria-hidden="true"></i>Add to Wishlist</span>
                        </button>
                    </div>
                    <div className="flash-message">{this.props.flash_message}</div>
                    <div className="col-md-12 no-padding modal-link-padding-top text-left">
                        <a className="modal-next-item-link" href={this.props.url}>Click here to View Product</a>
                    </div>
                </div>
            </div>
        )
    }
}

// Required data for each products
ModalInfo.propTypes = {
    name: PropTypes.string.isRequired,
    colors: PropTypes.arrayOf(PropTypes.string),
    price: PropTypes.number.isRequired,
    url: PropTypes.string
};

class Modal extends Component{
    constructor(props){
        super(props)
        this.addToCart = this.addToCart.bind(this)
        this.handleVariant = this.handleVariant.bind(this)
        // this.state = {
        //     size_obtained: false,
        //     color_obtained: false
        // }
    }

    componentDidMount(){
        console.log('modal component props...')
        console.log(this.props)
    }
    
    componentDidUpdate(){
        if(!this.props.isSizeFetched){
            this.props.getArrayByType(this.props.all_sizes, this.props.modal_item, 'size');
            this.props.setSizeFetchedTrue()
            //reset the flag
        }
        if(!this.props.isColorFetched){
            this.props.getArrayByType(this.props.all_colors, this.props.modal_item, 'color');
            this.props.setColorFetchedTrue()
            //reset the flag
        }
        if(this.props.modal_item.sku && !this.props.isVariantFetched){
            this.props.getProductVariants(this.props.modal_item.sku);
            this.props.setVariantFetchedTrue()
        }
    }

    handleVariant(options){
        let params = {
            attribute_code : (options.type === 'CHANGE_COLOR')? 'color' : 'size',
            size : (options.type === 'CHANGE_SIZE')? options.value : this.props.size,
            color: (options.type === 'CHANGE_COLOR')? options.value : this.props.color,
            value: options.value
        }
        console.log(this.props)
        this.props.changeVariant(this.props.variants, params);
    }

    addToCart(){
        let qty = 1;
        console.log(this.props.sku1)
        this.props.addToCart(this.props.sku1, qty, this.props.color_attribute_id, this.props.color, this.props.size_attribute_id, this.props.size)
    }

    addToWishlist = () =>{
        let qty=1;
        this.props.addItemToWishlist(this.props.sku1, qty, this.props.color_attribute_id, this.props.color, this.props.size_attribute_id, this.props.size, this.props.name, this.props.price);
    };

    render(){
        return(
            <div class="modal fade" id={`productModal`} tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content modal-bg-color">
                        <div class="modal-header modal-heading-height">
                            <button type="button" className="close modal-cross" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body-padding">
                            <div className="container-fluid row no-margin no-padding">
                                <ModalImgContainer img={getImgUrl(this.props.modal_item)}/>
                                <ModalInfo name={this.props.modal_item.name} url={'../product?id='+this.props.modal_item.sku} price={this.props.modal_item.price} color_array={this.props.modal_color_array} size_array={this.props.modal_size_array} addToCart={this.addToCart} handleVariant={this.handleVariant} flash_message={this.props.flash_message || ""}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ) 
    }
} 

// Required data for each products
Modal.propTypes = {
    name: PropTypes.string.isRequired,
    colors: PropTypes.arrayOf(PropTypes.string),
    price: PropTypes.number.isRequired,
    url: PropTypes.string,
    img: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
};



const mapStateToProps = state => ({
    sku1 : state.modal.sku,
    price: state.modal.price,
    sku_list: state.modal.sku_list,
    color:  state.modal.color,
    size:  state.modal.size,
    product_get: state.modal.product_get,
    variants: state.modal.variants,
    modal_color_array: state.modal.modal_color_array,
    modal_size_array: state.modal.modal_size_array,
    flash_message: state.modal.flash_message 
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getProductVariants,
  changeVariant,
  addToCart,
  getArrayByType
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal)

