import React from 'react';
import '../../css/campignLooks.css';


const CampaignLooksBottoms = (props) => (
    <div className="container-fluid no-padding homepage-container">
        <div className="camp-container">
            <div className="camp-inner-container camp-inner-margin">
                <div className="d-md-inline-block campignlooks-z-index campignlooks-right-camp-imgbox2">
                    <img src={props.imgSmall} className="campignlooks-landing-img" alt=""/>
                    <div className="campignlooks-shopthislook-div2"><a href={props.lookLink}>Shop This Look</a></div>
                </div>
                <div className="d-md-inline-block campignlooks-left-camp-imgbox1">
                    <img src={props.imgBig} className="campignlooks-landing-img" alt=""/>
                    <div className="campignlooks-circle campignlooks-bottom-btn1-margin">
                        <button onClick={props.onTopPlusButtonClick} className="btn campignlooks-circle-btn">+</button>
                    </div>
                    <div className="campignlooks-circle campignlooks-bottom-btn2-margin">
                        <button onClick={props.onBottomPlusButtonClick} className="btn campignlooks-circle-btn">+</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
);

 
export default CampaignLooksBottoms