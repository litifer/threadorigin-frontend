import React, {Component} from 'react'
import PropTypes from 'prop-types';
import '../../css/checkout.css'
import cartItem from '../../img/cart_item.png'
import * as  config from '../../config.js';
import Bill from '../components/cartBill.controller'

class CheckoutFinalSummary extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className="col-md-3 col-sm-12 col-12 d-inline-block align-top no-padding checkout-margin1">
                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding checkout-payment-box checkout-mobile-none">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                        <div className="col-md-6 col-sm-6 col-6 d-inline-block">
                            <span className="checkout-final-summary-text">Final Summary</span>
                        </div> 
                        <div className="col-md-6 col-sm-6 col-6 d-inline-block text-right">
                            <span className="checkout-final-summary-right-text">{this.props.checkout_items.items.length} Products</span>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding" style={{"border-bottom":"solid 2px #cbcbcb"}}>&nbsp;</div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding checkout-payment-items-box">
                    {
                        this.props.checkout_items.items.map((product, index) => {
                            if(this.props.checkout_item_images === undefined || this.props.checkout_item_images[product.sku] === undefined) return
                            let url = `${config.IMG_URL}${this.props.checkout_item_images[product.sku][0].file}`
                            return <div className="col-md-12 col-sm-12 col-12 d-inline-block" style={{"margin-top":"2%"}}>
                                        <div className="col-md-8 col-sm-8 col-8 d-inline-block no-padding">
                                            <div className="col-md-4 col-sm-4 col-4 d-inline-block no-padding align-top"><img className="checkout-final-summary-img" src={url} /></div>
                                            <div className="col-md-8 col-sm-8 col-8 d-inline-block no-padding align-top"><span className="checkout-product-text">{product.name}</span ><br /><span style={{"font-size": "10px","color": "#4a4a4a"}}>Quantity: {product.qty}</span></div>
                                        </div>
                                        <div className="col-md-4 col-sm-4 col-4 d-inline-block no-padding text-right align-top">
                                            <span className="checkout-product-price">&#x20b9; {product.price}</span>
                                        </div>                                    
                                    </div>
                        })
                    }
                    </div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding" style={{ "background-color": "#fafafa","border": "solid 1px #dcdcdc"}}>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block"><span className="checkout-shipping-text-subscript">Shipping<span className="checkout-shipping-text">(Express Delivery for faster delivery)</span></span><br /></div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                            <div className="col-md-9 col-sm-9 col-9 d-inline-block no-padding checkout-shipping-text-padding">
                                <form>
                                    <input type="radio" className="align-top checkout-radio-margin" /><span className="align-top checkout-shipping-radio-text">Standard Delivery (5-7 Days)</span>
                                </form>
                            </div>
                            <div className="col-md-3 col-sm-3 col-3 d-inline-block no-padding text-right">
                                <span className="checkout-shipping-price">&#x20b9; 0</span>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                            <div className="col-md-9 col-sm-9 col-9 d-inline-block no-padding">
                                <form>
                                    <input type="radio" className="align-top checkout-radio-margin" /><span className="align-top checkout-shipping-radio-text">Express Delivery (2-3 Days)</span>
                                </form>
                            </div>
                            <div className="col-md-3 col-sm-3 col-3 d-inline-block no-padding text-right">
                                <span className="checkout-shipping-price">&#x20b9; 250</span>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                    </div>
                    <Bill bill={this.props.checkout_bill}/>
                </div>
                <button className="btn col-12 mx-auto cart-mobile-container-top-collapse checkout-web-none" data-toggle="modal" data-target=".bd-example-modal-sm">
                    <div className="col-12 d-inline-block no-padding">
                        <div className="col-8 d-inline-block cart-mobile-amt-text no-padding text-left">Payable Amount</div>
                        <div className="col-4 d-inline-block text-right cart-mobile-amt-text no-padding">&#x20b9;{this.props.checkout_bill.totals.grand_total}</div>
                    </div>
                </button>
                <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div className="checkout-modal-paybleamt-box col-12">
                                <div className="col-12 d-inline-block no-padding cart-mobile-amt-padding">
                                    <div className="col-8 d-inline-block cart-mobile-amt-text no-padding text-left">Payable Amount</div>
                                    <div className="col-4 d-inline-block text-right cart-mobile-amt-text no-padding">&#x20b9;{this.props.checkout_bill.totals.grand_total}</div>
                                </div>
                            </div>
                            <div className="col-12 d-inline-block no-padding checkout-payment-items-box">
                            {
                                this.props.checkout_items.items.map((product, index) => {
                                    if(this.props.checkout_item_images === undefined || this.props.checkout_item_images[product.sku] === undefined) return
                                    let url = `${config.IMG_URL}${this.props.checkout_item_images[product.sku][0].file}`
                                    return <div className="col-12 d-inline-block" style={{"margin-top":"2%"}}>
                                                <div className="col-8 d-inline-block no-padding">
                                                    <div className="col-4 d-inline-block no-padding align-top"><img className="checkout-final-summary-img" src={url} /></div>
                                                    <div className="col-8 d-inline-block no-padding align-top"><span className="checkout-product-text">{product.name}</span ><br /><span className="checkout-product-price">&#x20b9; {product.price}</span><br /><span style={{"font-size": "10px","color": "#4a4a4a"}}>Quantity: {product.qty}</span></div>
                                                </div>                                  
                                            </div>
                                })
                            }
                            </div>
                            <div className="col-11 mx-auto no-padding" style={{ "background-color": "#fafafa","border": "solid 1px #dcdcdc"}}>
                                <div className="col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-12 d-inline-block"><span className="checkout-shipping-text-subscript">Shipping<span className="checkout-shipping-text">(Express Delivery for faster delivery)</span></span><br /></div>
                                <div className="col-12 d-inline-block">
                                    <div className="col-9 d-inline-block no-padding checkout-shipping-text-padding">
                                        <form>
                                            <input type="radio" className="align-top checkout-radio-margin" /><span className="align-top checkout-shipping-radio-text">Standard Delivery (5-7 Days)</span>
                                        </form>
                                    </div>
                                    <div className="col-3 d-inline-block no-padding text-right">
                                        <span className="checkout-shipping-price">&#x20b9; 0</span>
                                    </div>
                                </div>
                                <div className="col-12 d-inline-block">
                                    <div className="col-md-9 col-sm-9 col-9 d-inline-block no-padding">
                                        <form>
                                            <input type="radio" className="align-top checkout-radio-margin" /><span className="align-top checkout-shipping-radio-text">Express Delivery (2-3 Days)</span>
                                        </form>
                                    </div>
                                    <div className="col-3 d-inline-block no-padding text-right">
                                        <span className="checkout-shipping-price">&#x20b9; 250</span>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            </div>
                            <div className="col-12 d-inline-block no-padding cart-mobile-amt-padding">
                                <div className="col-8 d-inline-block cart-mobile-amt-text text-left">Payable Amount</div>
                                <div className="col-4 d-inline-block text-right cart-mobile-amt-text">&#x20b9;{this.props.checkout_bill.totals.grand_total}</div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <button class="col-11 mx-auto btn checout-btn"><span className="style12_prod_text">Back To Shopping<span className="checkout-btn-line"></span></span></button>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        </div>
                    </div>
                </div>
                <a href="/checkout" className="btn col-12 mx-auto cart-mobile-btn checkout-web-none">Back to Shopping <span className="btn-line"></span></a>
                <button className="col-md-12 col-sm-12 col-12 d-inline-block checkout-menu-btn btn no-padding checkout-mobile-none"><span className="checkout-menu-btn-text">Back to Shopping<span className="cart-btn-line"></span></span></button>                                                             
            </div>
        ) 
    }
} 

export default CheckoutFinalSummary