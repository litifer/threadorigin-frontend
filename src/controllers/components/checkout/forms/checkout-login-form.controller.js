import React, {Component} from 'react'
import ScrollMagic from "scrollmagic";
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from "redux";
import {login, setLoginDataToNull} from "../../../../actions/login.actions";
import {push} from "react-router-redux";

class CheckoutLoginForm extends Component{
    constructor(props){
        super(props);

        this.state = {
            user : "",
            pass : "",
            dummy: '',
            errors: {

                emailError: null,
                passwordError: null
            }
        };


        // this.returnForm = this.returnForm.bind(this);
    }


    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }
            else
                return null;
        }

        return 'Email address cannot be empty';

    };

    validatePassword = (sPassword) => {
        if (sPassword) {
            let regPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
            if(!sPassword.match(regPassword)) {
                // alert("Invalid email address");
                return 'Password has to be at least 8 chars long including one small, one capital and one special character';
            }
            else
                return null;
        }
        return 'Password cannot be empty';

    };


    validateAllInputs = () => {
        const errors = {

            emailError: null,
            passwordError: null
        };

        errors.passwordError = this.validatePassword(this.state.pass);
        errors.emailError = this.validateEmail(this.state.user);

        this.setState({errors})
    };


    componentDidUpdate() {


        if (window.localStorage.getItem('authToken')) {
            console.log(this.props);
            // return this.props.history.replace('/dev');
            this.props.returnData({id: 'billingAddress'})
            // return <Redirect to="localhost:3000/login"/>
        }

        if (this.props.loggedInData) {
            console.log(this.props.loggedInData, "lakdsmkladsmas");
            if (this.props.loggedInData.success === "false" && this.state.errors.passwordError !== "Incorrect Email or password") {
                const errors = {
                    emailError: null,
                    passwordError: "Incorrect Email or password"
                };
                this.setState({errors})
            } else {
                if (this.state.errors.passwordError !== null && this.state.errors.emailError !== null) {
                    const errors = {
                        emailError: null,
                        passwordError: null
                    };
                    this.setState({errors})
                }
            }
            this.props.setLoginDataToNull();
        }

    }

    handleChangeEmail = (event) => {
        this.setState({user: event.target.value});
    }

    handleChangePass = (event) => {
        this.setState({pass: event.target.value});
    }

    handleSubmitLogin = () => {
        // event.preventDefault();
        this.validateAllInputs();
        if (this.state.errors.emailError || this.state.errors.passwordError) {
            return;
        }
        console.log('error did not occur during login');
        this.props.login(this.state.user, this.state.pass);
        console.log(window.localStorage.getItem('authToken'));
    };

    componentDidMount(){
        //footer scroll
        if (window.localStorage.getItem('authToken')) {
            console.log(this.props);
            // return this.props.history.replace('/dev');
            // return <Redirect to="localhost:3000/login"/>
        }

        console.log(window.localStorage.getItem('authToken'), 'login controller');

    }


    render(){
        return  <div>
                    <label className="checkout-input-label">Username:</label><br />
                    <input onChange={this.handleChangeEmail} type="email" className="form-control checkout-input-box" placeholder="username"/><br/>
                    {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null}

                    <label className="checkout-input-label">Password:</label><br />
                    <input onChange={this.handleChangePass} type="password" className="form-control checkout-input-box"  placeholder="password" />
                    {this.state.errors.passwordError ? <p style={{color: 'red'}}>{this.state.errors.passwordError}</p> : null}

                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-right">
                        <span className="login-forgot-pass"><a  className="login-forgot-pass">Forgot your Password</a></span>
                    </div>  
                    <button onClick={this.handleSubmitLogin} class="col-md-5 col-sm-7 col-6  btn checout-btn"><span className="style12_prod_text">LOGIN<span className="checkout-btn-line"></span></span></button>
                    
                </div>
    }
}

const mapStateToProps = state => ({
    loggedInData: state.login.loginData,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: (path = '/') => push(path),
    login,
    setLoginDataToNull,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckoutLoginForm))