import React, {Component} from 'react';
import {connect} from 'react-redux';
import AddressComp from './../../address.controller';
import {withRouter} from 'react-router-dom';
import {addAsBillingAddress, setAddressAsDefault} from './../../../../actions/myAccountAddress.action';
import {sendDefaultAddressToPaymentGateway} from './../../../../actions/checkout.actions';
import {bindActionCreators} from "redux";
import AddressForm from './../forms/address-form.controller';
import {
    addNewDefaultAddress,
    deleteSelectedAddress,
    editExistingAddress,
    getAddresses
} from "../../../../actions/myAccountAddress.action";

class LoggedInAddress extends Component {


    constructor(props){
        super(props);

        this.state = {
            defaultAddress: null,
            otherAddresses: null,
            showAddressForm: false,
        }
    }

    getAddressFromForm = (rawFormData) => {

        let address = {
            "country": rawFormData[9].value,
            "street": rawFormData[3].value + ' ' + rawFormData[4].value,
            "telephone": rawFormData[8].value,
            "postcode": rawFormData[5].value,
            "city": rawFormData[6].value,
            "firstname": rawFormData[1].value,
            "lastname": rawFormData[2].value
        };
        this.props.addNewDefaultAddress(address);
    };

    componentDidUpdate() {


        if (this.props.defaultAddressAdded === false) {
            alert('address couldn\'t be added');
            window.location.reload();
        } else if (this.props.defaultAddressAdded === true) {
            alert('default address added');
            window.location.reload();
        }

        if (this.props.defaultAddressChanged) {
            alert('default address changed');
            window.location.reload();
        }

        if (this.props.addressData) {
            const otherAddresses = [];
            for (let i = 0; i < this.props.addressData.addresses.length; i++) {
                console.log(this.props.addressData.addresses[i]);
                if (this.props.addressData.addresses[i].default_billing) {
                    if (!this.state.defaultAddress) {
                        this.setState({defaultAddress: {...this.props.addressData.addresses[i]}});
                        console.log(this.props.addressData.addresses[i].id, "-----------------id of default address--------------------");
                        this.props.sendDefaultAddressToPaymentGateway({...this.props.addressData.addresses[i]});
                    }
                } else {
                    otherAddresses.push(this.props.addressData.addresses[i]);
                    console.log(this.props.addressData.addresses[i].id, "-----------------id of other address--------------------");
                }
            }
            if (!this.state.otherAddresses) {
                this.setState({otherAddresses});
                console.log("address chla gya");
            }
        }
        console.log(this.props.billingAddressSet);
        if (this.props.billingAddressSet === true) {

                let data_to_be_sent = {
                    id: "addressLogin",
                    data: "ldkasn"
                };
                this.props.returnFormData(data_to_be_sent)
        }

    }

    componentDidMount(){
        console.log(window.localStorage.getItem('authToken') ? 'hello' : 'bye', 'auth token from my account address');

        if (!window.localStorage.getItem('authToken')) {
            console.log(this.props);
            // return this.props.history.push('/login');
            // return <Redirect to="localhost:3000/login"/>
        }


        if (!this.props.addressData) {
            this.props.getAddresses();
        }


    }

    // returnForm(data){
    //     console.log('The data inside the checkout type box controller is...');
    //     console.log(data)
    //     let data_to_be_sent = {
    //         id: "addressLogin",
    //         data: data
    //     }
    //     this.props.returnFormData(data_to_be_sent)
    // }


    render() {
        return (<div className="col-md-11 col-sm-11 float-sm-left float-md-left no-padding checkout-margin">
            <input type="checkbox" className="checkout-guest-checkbox" /><span className="checkout-guest-checkbox-text">Set Delivery Address same as the Billing Address</span><br />
            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
            <span className="checkout-guest-text">Billing Address</span>
            <br /><br />
            {this.state.showAddressForm ? <button onClick={() => this.setState({showAddressForm: !this.state.showAddressForm})} className="btn col-md-5 checkout-guest-btn"><span className="checkout-guest-btn-text">CANCEL ADDING NEW ADDRESS &nbsp;&nbsp;&nbsp;----</span></button> : null}
            {this.state.showAddressForm ? <AddressForm returnData={(rawData) => this.getAddressFromForm(rawData)} /> : null}
            {!this.state.showAddressForm ? <button onClick={() => this.setState({showAddressForm: !this.state.showAddressForm})} className="btn col-md-3 checkout-guest-btn"><span className="checkout-guest-btn-text">ADD NEW ADDRESS &nbsp;&nbsp;&nbsp;----</span></button> : null}

            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 float-md-left float-sm-left no-padding">

                {this.state.defaultAddress ? <AddressComp addressType={'Default Address'}
                                                          setAsDefault={() => alert('this is the default address')}
                                                          fullName={this.state.defaultAddress.firstname + ' ' + this.state.defaultAddress.lastname}
                                                          street={this.state.defaultAddress.street}
                                                          city={this.state.defaultAddress.city}
                                                          default={true}
                                                          pinCode={this.state.defaultAddress.postcode}
                                                          country={this.state.defaultAddress.country_id}
                                                          phone={this.state.defaultAddress.telephone} /> : null}

                {this.state.otherAddresses ? this.state.otherAddresses.map((otherAddress, index) => (
                    <AddressComp addressType={'Address ' + index}
                                 fullName={otherAddress.firstname + ' ' + otherAddress.lastname}
                                 street={otherAddress.street}
                                 city={otherAddress.city}
                                 default={false}
                                 setAsDefault={() => this.props.setAddressAsDefault(otherAddress.id)}
                                 pinCode={otherAddress.postcode}
                                 country={otherAddress.country_id}
                                 phone={otherAddress.telephone} />
                )): null}

                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                    <button onClick={() => {
                        if (this.state.defaultAddress) {
                            this.props.addAsBillingAddress(this.state.defaultAddress);
                        } else {
                            alert('please enter a default address');
                        }
                    }} className="btn checkout-guest-bottom-btn col-md-4 float-md-right checkout-guest-btn-margin-right"><span className="checkout-guest-bottom-btn-text">CONTINUE TO LAST STEP&nbsp;&nbsp;&nbsp;----</span></button>
                </div>
            </div>
        </div>);
    }
}

const mapStateToProps = state => ({
    addressData: state.addresses.addressData,
    defaultAddress: state.addresses.defaultAddress,
    defaultAddressChanged: state.addresses.defaultAddressChanged,
    otherAddresses: state.addresses.otherAddresses,
    defaultAddressAdded: state.addresses.defaultAddressAdded,
    billingAddressSet: state.addresses.billingAddressSet
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getAddresses,
    sendDefaultAddressToPaymentGateway,
    addNewDefaultAddress,
    addAsBillingAddress,
    setAddressAsDefault,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoggedInAddress));