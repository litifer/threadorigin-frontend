import React, {Component} from 'react'

class CheckoutTypesForm extends Component{
    constructor(props){
        super(props)

        this.returnForm = this.returnForm.bind(this);
    }

    returnForm(evt){
        evt.preventDefault();
        console.log(evt.target)
        this.props.returnData(evt.target)
    }

    render(){
        return  <form onSubmit={this.returnForm} id="checkoutType">
                    <input type="radio" name="checkoutType"/><span className="checkout-radio-text">Register and Checkout</span><br />
                        <span className="checkout-radio-subscript-text">- You’ll be able to track your orders</span><br />
                        <span className="checkout-radio-subscript-text">- Place an order quickly the next time</span><br />
                        <span className="checkout-radio-subscript-text">- Recieve amazing offers in your inbox</span><br />                                                 
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                    
                    <input type="radio" name="checkoutType" checked/><span className="checkout-radio-text">Checkout as a Guest</span><br />
                    
                    <button className="col-md-5 col-sm-6 col-6  btn checout-btn checkout-left-btn-margin" type="submit"><span className="style12_prod_text">CONTINUE<span className="checkout-btn-line"></span></span></button>
                    
                </form>
    }
}

export default CheckoutTypesForm