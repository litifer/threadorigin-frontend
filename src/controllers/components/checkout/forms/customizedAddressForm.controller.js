import React, {Component} from 'react'

class AddressForm extends Component{
    constructor(props){
        super(props)
        this.returnForm = this.returnForm.bind(this)
    }

    componentDidMount(){
        console.log("inside this component did mount")
        console.log(this.props.previousData)
        if(this.props.previousData){
            Object.keys(this.props.perviousData).map(key => {
                let element = document.getElementById(this.props.type + key);
                element.value = this.previousData[key];
            })
        }
    }

    returnForm(evt){
        evt.preventDefault();
        console.log(evt.target);
        this.props.returnData(evt.target);
    }

    render(){
        return <form id={this.props.type + "AddressForm"} onSubmit={this.returnForm} >
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">First Name:</span>
                    </div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <input onChange={evt => this.props.onChangeFname(evt)} value={this.props.formData.fname} id={this.props.type + '-fname'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Last Name:</span>
                    </div>
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <input onChange={evt => this.props.onChangeLname(evt)} value={this.props.formData.lname} id={this.props.type + '-lname'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <span className="checkout-guest-label">Address:</span>
            </div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <input onChange={evt => this.props.onChangeAddress(evt)} id={this.props.type + '-street'} value={this.props.formData.street} type="text" className="checkout-guest-input form-control" required />
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Address 2 (Optional):</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-street2'} value={this.props.formData.address2} onChange={evt => this.props.onChangeAddress2(evt)} type="text" className="checkout-guest-input form-control" />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Pincode:</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-pincode'} onChange={evt => this.props.onChangePincode(evt)} value={this.props.formData.pincode} type="number" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">City:</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-city'} type="text" onChange={evt => this.props.onChangeCity(evt)} value={this.props.formData.city} className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Region (Optional)</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-region'} onChange={evt => this.props.onChangeRegion(evt)} type="text" value={this.props.formData.region} className="checkout-guest-input form-control"/>
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Phone No:</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-phone'} onChange={evt => this.props.onChangePhone(evt)} value={this.props.formData.phone} type="tel" className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Country</span>
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input disabled id={this.props.type + '-country'} onChange={evt => this.props.onChangeCountry(evt)} value={'IN'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <button className="btn checkout-btn col-md-5" type='submit' id={this.props.type + 'AddressSubmit'}><span className="checkout-btn-text">SAVE ADDRESS&nbsp;&nbsp;&nbsp;----</span></button>
            </div>
        </form>

    }
}

export default AddressForm