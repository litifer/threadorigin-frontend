import React, {Component} from 'react'

class BillingAddressForm extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding checkout-border-right">
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                        <span className="checkout-guest-text">Billing Address</span>
                    </div>
                    
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                        <span className="checkout-guest-label">Email Address:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                        <input type="text" className="checkout-guest-input form-control"  />
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">First Name:</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Last Name:</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                        <span className="checkout-guest-label">Address:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                        <input type="text" className="checkout-guest-input form-control"  />
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Address 2 (Optional):</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Pincode:</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">City:</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Region (Optional)</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Phone No:</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <span className="checkout-guest-label">Country</span>
                            </div>  
                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                <input type="text" className="checkout-guest-input form-control"  />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <button className="btn checkout-btn col-md-5"><span className="checkout-btn-text">SAVE ADDRESS&nbsp;&nbsp;&nbsp;----</span></button>
                    </div>
                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                </div>
    }
}

export default BillingAddressForm