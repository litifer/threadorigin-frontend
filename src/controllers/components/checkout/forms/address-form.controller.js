import React, {Component} from 'react'

class AddressForm extends Component{
    constructor(props){
        super(props)
        this.returnForm = this.returnForm.bind(this)
    }

    componentDidMount(){
        console.log("inside this component did mount")
        console.log(this.props.previousData)
        if(this.props.previousData){
            Object.keys(this.props.perviousData).map(key => {
                let element = document.getElementById(this.props.type + key);
                element.value = this.previousData[key];
            })
        }
    }

    returnForm(evt){
        evt.preventDefault();
        console.log(evt.target);
        this.props.returnData(evt.target);
    }

    render(){
        return <form id={this.props.type + "AddressForm"} onSubmit={this.returnForm} >
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right"> 
                <span className="checkout-guest-label">Email Address:</span>
            </div>  
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <input id={this.props.type + '-email'} type="email" className="checkout-guest-input form-control" required />
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">First Name:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <input id={this.props.type + '-fname'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Last Name:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                        <input id={this.props.type + '-lname'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <span className="checkout-guest-label">Address:</span>
            </div>  
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <input id={this.props.type + '-street'} type="text" className="checkout-guest-input form-control" required />
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Address 2 (Optional):</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-street2'} type="text" className="checkout-guest-input form-control" />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Pincode:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-pincode'} type="number" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">City:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-city'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Region (Optional)</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-region'} type="text" className="checkout-guest-input form-control"/>
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block no-padding">
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Phone No:</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input id={this.props.type + '-phone'} type="tel" className="checkout-guest-input form-control" required />
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 d-inline-block no-padding">
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <span className="checkout-guest-label">Country</span>
                    </div>  
                    <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                        <input disabled value={"IN"} id={this.props.type + '-country'} type="text" className="checkout-guest-input form-control" required />
                    </div>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-11 col-sm-11 col-11 mx-auto d-inline-block padding-right">
                <button className="btn checkout-btn col-md-5" type='submit' id={this.props.type + 'AddressSubmit'}><span className="checkout-btn-text">SAVE ADDRESS&nbsp;&nbsp;&nbsp;----</span></button>
            </div>
        </form>
        
    }
}

export default AddressForm