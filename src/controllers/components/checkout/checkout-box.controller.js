import React, {Component} from 'react'
import CheckoutLoginAddressBox from './forms/checkout-login-address.controller';
import CheckoutTypeBox from '../checkout/checkoutTypeBox.controller'
import CheckoutAddressBox from '../checkout/checkoutAddressBox.controller'
import CheckoutPaymentBox from '../checkout/checkoutPaymentBox.controller'

class CheckoutBox extends Component{
    constructor(props){
        super(props)
        this.returnFormData = this.returnFormData.bind(this)
    }

    returnFormData(data){
        console.log('The data inside checkout box controller is...')
        console.log(data)
        this.props.returnBoxData(data);
    }

    handleBox(returnFormData, box_type){
        console.log('the box_type inside the checout box controller is ', box_type)
        switch(box_type){
            case 'TYPE_BOX': return <CheckoutTypeBox returnFormData={returnFormData}/>
            case 'ADDRESS_BOX': return <CheckoutAddressBox  handleSameShipping={this.props.handleSameShipping} returnFormData={returnFormData} billing_address={this.props.parent.billing_address} shipping_address={this.props.parent.shipping_address}/>
            case 'PAYMENT_BOX': return <CheckoutPaymentBox returnFormData={returnFormData} />
            case 'ADDRESS_AFTER_LOGIN': return <CheckoutLoginAddressBox returnFormData={returnFormData} />
            // case '': return <CheckoutLoginAddressBox returnFormData={returnFormData} />
            default: return <CheckoutTypeBox returnFormData={returnFormData}/>
        }
       
    }

    render(){
        return  this.handleBox(this.returnFormData, this.props.boxType)
    }
}

export default CheckoutBox;