import React, {Component} from 'react'
import CheckoutTypeForm from './forms/checkout-type-form.controller' 
import CheckoutLoginForm from './forms/checkout-login-form.controller'

class CheckoutTypesBox extends Component{
    constructor(props){
        super(props)

        this.returnForm = this.returnForm.bind(this);
    }

    returnForm(data){
        console.log('The data inside the checkout type box controller is...');
        console.log(data)
        this.props.returnFormData(data)
    }

    render(){
        return  <div className="col-md-11 col-sm-11 col-12 mx-auto no-padding checkout-margin">
                    <div className="col-md-6 col-sm-6 col-6 d-inline-block no-padding checkout-border-right align-top checkout-mobile-none">
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block no-padding">
                            <span className="checkout-box-top-text">You can either continue as a Guest User or Register as a User to continue placing the order:</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block no-padding">
                            <CheckoutTypeForm returnData={this.returnForm}/>

                        </div>
                    </div>

                    <div className="col-md-6 col-sm-6 col-12 d-inline-block mobile-no-padding">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block checkout-login-text">
                            Login to your Account
                        </div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block">
                            <span className="checkout-box-top-text">If you’re a returning customer you can login and continue to place your order:</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-6  d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block">
                            <CheckoutLoginForm returnData={this.returnForm} />
                        </div>
                    </div>

                    <div className="col-md-6 col-sm-6 col-12 d-inline-block mobile-no-padding checkout-web-none">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block checkout-login-text">
                            Register as a User
                        </div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block">
                            <span className="checkout-box-top-text">If you’re a returning customer you can login and continue to place your order:</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-6 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                            <label className="checkout-input-label">First Name* :</label><br />
                            <input type="text" className="form-control checkout-input-box" placeholder="username"/><br/>
                            {/* {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null} */} 
                            <label className="checkout-input-label">Last Name* :</label><br />
                            <input type="text" className="form-control checkout-input-box" placeholder="username"/><br/>

                            <label className="checkout-input-label">E-mail Address* : </label><br />
                            <input type="email" className="form-control checkout-input-box" placeholder="username"/><br/>

                            <label className="checkout-input-label">Password* :</label><br />
                            <input type="password" className="form-control checkout-input-box" placeholder="username"/><br/>

                            <label className="checkout-input-label">Confirm Password* :</label><br />
                            <input type="password" className="form-control checkout-input-box" placeholder="username"/><br/>
                            <button class="col-md-5 col-sm-7 col-6  btn checout-btn"><span className="style12_prod_text">REGISTER<span className="checkout-btn-line"></span></span></button>
                        </div>
                    </div>
                    
                    <div className="col-md-6 col-sm-6 col-12 d-inline-block mobile-no-padding checkout-web-none">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block checkout-login-text">
                            Checkout as a Guest User
                        </div>
                        <div className="col-md-12 col-sm-12 col-12  d-inline-block">
                            <span className="checkout-box-top-text">You can checkout as a guest user as well after providing the billing and shipping address in the next steps:</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-6 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                            <form onSubmit={this.returnForm} id="checkoutType">
                                <label className="checkout-input-label">E-mail Address* :</label><br />
                                <input type="email" className="form-control checkout-input-box" placeholder="username"/><br/>
                                {/* {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null} */} 
                                <button class="col-md-5 col-sm-7 col-6  btn checout-btn"><span className="style12_prod_text">CONTINUE<span className="checkout-btn-line"></span></span></button>
                            </form>
                        </div>
                    </div>
                </div>
    }
}

export default CheckoutTypesBox