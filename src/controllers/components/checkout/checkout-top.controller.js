import React, {Component} from 'react'

class CheckoutTop extends Component{
    
    constructor(props){
        super(props)
    }

  

    render(){
        return  <div class="col-md-10 col-sm-10 col-12 mx-auto no-padding">
                    <div class="col-md-6 col-sm-6 col-6 d-inline-block checkout-mobile-none no-padding checkout-heading-margin"><span className="no-padding checkout-heading">Checkout</span><br /><span className="checkout-heading-subscript-text">Check all your products in your cart before checking out below</span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6 d-inline-block no-padding checkout-mobile-none">
                        <div class="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div class="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                            <div class="col-md-4 col-sm-4 col-4 d-inline-block no-padding text-center">
                                <span className={this.props.boxType === 'TYPE_BOX'?"checkout-active-text": "checkout-other-text"}>Login/Register</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 d-inline-block no-padding text-right">
                                <span className={this.props.boxType === 'ADDRESS_BOX'?"checkout-active-text": "checkout-other-text"}>Address Confirmation</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-4 d-inline-block no-padding text-right">
                                <span className={this.props.boxType === 'PAYMENT_BOX'?"checkout-active-text": "checkout-other-text"}>Payment Options</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 no-padding checkout-web-none">
                        <div className={this.props.boxType === 'TYPE_BOX'?"checkout-top-box-active": "checkout-top-box"}>
                            Login/ Register
                        </div>
                        <div className={this.props.boxType === 'ADDRESS_BOX'?"checkout-top-box-active checkout-top-box-margin": "checkout-top-box checkout-top-box-margin"}>
                            Address Confirmation
                        </div>
                        <div className={this.props.boxType === 'PAYMENT_BOX'?"checkout-top-box-active": "checkout-top-box"}>
                            Payment Options
                        </div>
                    </div>
                </div>
    }
}


export default CheckoutTop