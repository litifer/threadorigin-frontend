import React, {Component} from 'react'
import BillingAddressForm from './forms/billing-address-form.controller'
import AddressForm from './forms/address-form.controller'

class CheckoutAddressBox extends Component{
    constructor(props){
        super(props)
        this.returnForm = this.returnForm.bind(this);
        this.handleSameShipping = this.handleSameShipping.bind(this);
    }

    returnForm(data){
        console.log('The data inside the checkout type box controller is...');
        console.log(data, "after all the addresses have been added");
        this.props.returnFormData(data)
    }

    handleSameShipping(evt){
        if(evt.target.checked){
            this.props.handleSameShipping(true, this.props.billing_address);
        }
    }

    render(){
        return  <div className="col-md-11 col-sm-11 d-inline-block no-padding checkout-margin">
                    <input id="sameAsBillingAddressCheckbox" type="checkbox" className="checkout-guest-checkbox" onChange={this.handleSameShipping}/><span className="checkout-guest-checkbox-text"> Set Delivery Address same as the Billing Address</span><br /> 
                    <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 d-inline-block no-padding">
                        
                        <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding checkout-right-border ">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block padding-right">
                                <span className="checkout-guest-text">Billing Address</span>
                            </div>
                            {
                                this.props.billing_address? <AddressForm type="billing" returnData={this.returnForm} previousData={this.props.billing_address}/>
                                                                :
                                                            <AddressForm type="billing" returnData={this.returnForm}/>
                            }
                            
                        </div> 
                        <div className="col-md-6 col-sm-6 d-inline-block no-padding checkout-mobile-none">
                            <div className="col-md-12 col-sm-12 d-inline-block padding-right">
                                <span className="checkout-guest-text">Shipping Address</span>
                            </div>
                            {
                                this.props.shipping_address? <AddressForm type="shipping" returnData={this.returnForm} previousData={this.props.shipping_address}/>
                                                                    :
                                                             <AddressForm type="shipping" returnData={this.returnForm}/>
                            }
                            
                        </div>    
                    </div>
                    <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
                    <div className="col-md-5 col-sm-5 col-12 mx-auto">
                        <button id="addressView" className="btn checkout-guest-bottom-btn col-md-12" onClick={this.returnForm}><span className="checkout-guest-bottom-btn-text">CONTINUE TO LAST STEP&nbsp;&nbsp;&nbsp;----</span></button>
                    </div>
                </div>
    }
}

export default CheckoutAddressBox