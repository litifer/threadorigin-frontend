import React, {Component} from 'react'
import CheckoutTypeForm from './forms/checkout-type-form.controller' 
import CheckoutLoginForm from './forms/checkout-login-form.controller'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {mergeUserAndGuestCarts} from './../../../actions/login.actions'
import {initOrder} from './../../../actions/checkout.actions'
import paymentIcon1 from '../../../img/debit_card.png'
import paymentIcon2 from '../../../img/cod.png'
import paymentIcon3 from '../../../img/e_wallets.png'
import Razorpay from 'razorpay';
import {push} from "react-router-redux";
import {bindActionCreators} from "redux";
// import {bindActionCreators} from "redux/index";

class CheckoutPaymentBox extends Component{
    constructor(props){
        super(props)
        this.state = {
            addressGotten: false
        };
        this.returnForm = this.returnForm.bind(this);
    }


    returnForm(data){
        console.log('The data inside the checkout type box controller is...');
        console.log(data)
        this.props.returnFormData(data)
    }

    componentDidMount() {

        if (this.props.defaultAddress) {
            console.log(this.props.defaultAddress);
        }

        if (this.props.orderSuccessfull) {
            mergeUserAndGuestCarts();
            this.goToOrders();
            alert('payment successful');
        }
    }

    goToOrders = () => {

        this.props.history.replace('/accountOrders');
    };

    componentDidUpdate() {
        if (this.props.defaultAddress) {
            console.log(this.props.defaultAddress);
        }

        if (!!window.localStorage.getItem('userCartId') && this.props.orderSuccessfull) {
            mergeUserAndGuestCarts();
            this.goToOrders();
            alert('payment successful');
        } else {
            window.localStorage.removeItem('cartId');
            this.props.history.push('/dev');
            alert('payment successful');
        }
    }


    render(){
        let rzp = new Razorpay({
            key_id: "key_id", // your `KEY_ID`
            key_secret: "key_secret"// your `KEY_SECRET`
        });
        return  <div className="col-md-11 col-sm-11 col-11  d-inline-block no-padding checkout-margin">
                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                        <div className="checkout-payment-heading-text">Choose any of the following Payment Options to place an order:</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding checkout-payment-container-margin">
                            <div style={{cursor: 'pointer'}} onClick={() => alert('debit, credit or netbanking selected')} className="col-md-3 col-sm-3 col-3 d-inline-block no-paddding checkout-payment-icon-box">
                                <img src={paymentIcon1} className="mx-auto checkout-payment-img-padding" /><br />
                                <span className="checkout-payment-text">Debit Cards, Credit Cards,<br />Netbanking etc</span>
                            </div>
                            <div style={{cursor: 'pointer'}} onClick={() => alert('debit, credit or netbanking selected')} className="col-md-3 col-sm-3 col-3 d-inline-block no-paddding checkout-payment-icon-box">
                                <img src={paymentIcon3} className="mx-auto checkout-payment-img-padding" /><br />
                                <span className="checkout-payment-text">E-Wallets (PayTM, Amazon <br /> Pay & Freecharge)</span>
                            </div>
                        </div>
                        <button onClick={() => {
                            if (this.props.defaultAddress) {
                                let add = {...this.props.defaultAddress};
                                var options = {
                                    "key": "rzp_test_Nok4cJqK6E7z9a",
                                    "amount": this.props.checkout_bill.totals.grand_total * 100, // 2000 paise = INR 20
                                    "name": add.firstname + " " + add.lastname,
                                    "description": "Thread Origin Order",
                                    "image": "/your_logo.png",
                                    "handler": (response) => {
                                        let userOrGuest = !!window.localStorage.getItem('userCartId');
                                        if (userOrGuest) {
                                            this.props.initOrder(this.props.defaultAddress, userOrGuest);
                                        } else {
                                            this.props.initOrder(this.props.billing_address, userOrGuest);
                                        }
                                    },
                                    "prefill": {
                                        "name": add.firstname + " " + add.lastname,
                                        "email": "example@email.com"
                                    },
                                    "notes": {
                                        "address": add.street.join(' ')
                                    },
                                    "theme": {
                                        "color": "#c4a67c"
                                    }
                                };
                                var rzp1 = new window.Razorpay(options);
                                rzp1.open();
                            } else {
                                let add = {...this.props.billing_address};
                                var options = {
                                    "key": "rzp_test_Nok4cJqK6E7z9a",
                                    "amount": this.props.checkout_bill.totals.grand_total * 100, // 2000 paise = INR 20
                                    "name": add.fname + " " + add.lname,
                                    "description": "Thread Origin Order",
                                    "image": "/your_logo.png",
                                    "handler": (response) => {
                                        let userOrGuest = !!window.localStorage.getItem('userCartId');
                                        if (userOrGuest) {
                                            this.props.initOrder(this.props.defaultAddress, userOrGuest);
                                        } else {
                                            this.props.initOrder(this.props.billing_address, userOrGuest);
                                        }
                                    },
                                    "prefill": {
                                        "name": add.fname + " " + add.lname,
                                        "email": add.email
                                    },
                                    "notes": {
                                        "address": add.street + ', ' + add.street2
                                    },
                                    "theme": {
                                        "color": "#c4a67c"
                                    }
                                };
                                var rzp1 = new window.Razorpay(options);
                                rzp1.open();
                            }


                            // document.getElementById('rzp-button1').onclick = function(e){

                            //     e.preventDefault();
                            // }

                        }} className="btn checkout-guest-bottom-btn col-md-4"><span className="checkout-guest-bottom-btn-text">CONTINUE TO LAST STEP&nbsp;&nbsp;&nbsp;----</span></button>
                    </div>
                </div>
    }
}

const mapStateToProps = state => ({
    defaultAddress: state.checkout.defaultAddress,
    orderSuccessfull: state.checkout.orderSuccessfull,
    billing_address: state.checkout.billing_address,
    checkout_bill: state.checkout.checkout_bill,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    initOrder
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckoutPaymentBox))