import React from 'react'
import '../../css/campignLooks.css'


const CampaignLooksTop = (props) => (
    <div className="container-fluid no-padding homepage-container">
        <div className="camp-container">
            <div className="camp-inner-container camp-inner-margin">
                <div className="d-md-inline-block campignlooks-camp-top-box1 campignlooks-left-camp-imgbox">
                    <img src={props.imgBig} className="campignlooks-landing-img" alt=""/>
                    <div className="campignlooks-circle campignlooks-bottom-btn1-margin">
                        <button onClick={props.onTopPlusButtonClick} className="btn campignlooks-circle-btn">+</button>
                    </div>
                    <div className="campignlooks-circle campignlooks-bottom-btn2-margin">
                        <button onClick={props.onBottomPlusButtonClick} className="btn campignlooks-circle-btn">+</button>
                    </div>
                </div>
                <div className="no-padding d-md-inline-block campignlooks-camp-top-box2 campignlooks-right-camp-imgbox">
                    <img src={props.imgSmall} className="campignlooks-landing-img" alt=""/>
                    <div className="col-9 campignlooks-shopthislook-div"><a href={props.lookLink}>Shop This Look</a></div>
                </div>
            </div>
        </div>

    </div>
);

export default CampaignLooksTop