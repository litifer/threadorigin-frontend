import React, {Component} from 'react'
import LandingImg from '../../img/lookbooks_landing.png'
import PlayImg from '../../img/play-img.png'
import '../../css/campignLooks.css'


class CampignLooksLanding extends Component{

    constructor(props){
        super(props)
    }
    componentDidMount(){
    }
    render(){
        return(
                <div className="container-fluid no-padding campignlooks-landing-box main-container">
                    <img className="campignlooks-landing-img" src={LandingImg} />
                    <div className="col-md-10 col-sm-10 col-10 campignlooks-landing-margin-left no-padding campignlooks-absolute">
                        <div className="emptydiv d-inline-block"></div>
                        <div className="col-md-12 col-sm-12 col-12 no-padding d-inline-block align-middle">
                            <div className="col-md-5 col-sm-5 col-5 no-padding d-inline-block">
                                <div className="campignlooks-landing-recatangle">LIMITED STOCK</div>
                                <div className="campignlooks-landing-heading">Bonus Look: All-Day, Everyday<br /> Fashion</div>
                                <div className="campignlooks-landing-text">As for the collection itself, the 72 looks comprised of dramatic tulle ballgowns, sexy patent leather, sparkling crystal-encrusted dresses, unexpected headpieces, and so much more. </div>
                            </div>
                            {/* <div className="col-md-7 col-sm-7 col-7 text-right no-padding d-inline-block align-middle">
                                <img src={PlayImg} />
                            </div> */}
                        </div>    
                    </div>
                </div>
        )
    }
}
 
export default CampignLooksLanding