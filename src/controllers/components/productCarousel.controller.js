import React, {Component} from 'react'
import PropTypes from 'prop-types';
import Modal from './modal.controller'
import * as  config_ from '../../config.js';
import oval1 from '../../img/Oval 1.png';
import oval2 from '../../img/Oval 2.png';
import '../../css/header.css'
import $ from 'jquery'
import { hasData } from '../../helpers/general';

const {attribute_array_creator} = require('../../helpers/general')

export class Product extends Component{
    constructor(props){
        super(props)
    }

    componentDidMount(){
        // console.clear()
        console.log('inside the prodcut carousel component....')
        console.log(this.props.sku)
    }

    render(){
        return (
                <div className= "col-md col anim_scroll_reveal_sequence_body2 mobile-no-padding" ref="body2Item">
                    <div className="overflow product-height position-relative" >
                        <a href = {this.props.url}>
                            <img src={this.props.img} className="carousel-img img-responsive" alt=""/>
                        </a>
                        <button className="btn card-button" data-toggle="modal" data-target={`#productModal`}  onClick={event => this.props.handleModalClick(this.props.id)}><span>QUICK VIEW <span className='carousel-btn-line'></span></span></button>
                        {/* <Modal sku={this.props.sku} img={this.props.img} url={this.props.url} name={this.props.name} price={this.props.price} id={this.props.id} color_array={this.props.color_array} size_array={this.props.size_array}/> */}
                    </div>
                    <div className="row category-item-padding">
                        <div className="col-md-8 col-sm-8 col-8 text-left">
                            <span className="category-item-name">{this.props.name}</span>
                        </div>
                        <div className="col-md- col-sm-4 col-4 text-right">
                            <span className="category-item-rs">Rs.</span><span className="category-item-price"> {this.props.price}</span>
                        </div>                            
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-sm-6 col-6 text-left">

                            {
                                hasData(this.props.color_array).map((color)=>{
                                    return <div className = "product-size-circle float-left product-margin-circles"/*  onClick = {event => this.handleVariant({type: 'CHANGE_COLOR', value:options.value})} */>
                                              <div  className = "product-color-circle" style = {{'background-color':color.label}} ></div>
                                            </div>
                                })
                            }
                        </div>
                        <div className="col-md-6 col-sm-6 col-6 text-right">
                            {
                                hasData(this.props.size_array).map((size)=>{
                                    return <span className="category-item-size">{size.label}</span>
                                })
                            }
                        </div>                            
                    </div>
                </div>
        )
    }
} 

// Required data for each products
Product.propTypes = {
    name: PropTypes.string.isRequired,
    colors: PropTypes.arrayOf(PropTypes.string),
    price: PropTypes.number.isRequired,
    url: PropTypes.string,
    img: PropTypes.string.isRequired
};

class CarouselItem extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return <div className={`carousel-item product-carousel-item ${this.props.active}`}>
                    <div className="row">
                        {
                            this.props.children.map((item, index) => {
                                
                               return <Product sku={item.sku} name={item.name} url={item.link} img={item.url_img} price={item.price} id={item.id} color_array={item.color_array} size_array={item.size_array} handleModalClick={this.props.handleModalClick}/>
                            })
                        }    
                    </div>
                </div>
    }
} 

// Required data for each carousel-item i.e children are required
CarouselItem.propTypes = {
    children: PropTypes.arrayOf(PropTypes.object).isRequired
};

export class ProductCarousel extends Component{

    constructor(props){
        super(props)
        this.handleChildrenElements = this.handleChildrenElements.bind(this)
    }

    componentDidMount(){
        
        /*Currently done in jquery but in future will be shifted to react*/ 
        // swipable carousel code
        $(`#carousel${this.props.id}`).on("touchstart", function (event) {
            var xClick = event.originalEvent.touches[0].pageX;
            $(this).one("touchmove", function (event) {
                var xMove = event.originalEvent.touches[0].pageX;
                if (Math.floor(xClick - xMove) > 5) {
                    $(this).carousel('next');
                }
                else if (Math.floor(xClick - xMove) < -5) {
                    $(this).carousel('prev');
                }
            });
          
        });
    }

    handleChildrenElements(){
        let children = []
        let carouselItems = []

        console.log(this.props.product_details)
        
        if (this.props.product_details.items) {
            this.props.product_details.items.map((items, index) => {
                let color_array = attribute_array_creator(this.props.color_array, items, 'color')
                let size_array = attribute_array_creator(this.props.size_array, items, 'size')
                
                let img_url = items.custom_attributes.find(i => {
                    return i.attribute_code == "image"
                })
                
                let item = {
                    id: items.id,
                    sku: items.sku,
                    link: '../product?id='+items.sku,
                    name: items.name,
                    price: items.price,
                    url_img: config_.IMG_URL + (img_url? img_url.value : ''),
                    color_array: color_array,
                    size_array: size_array
                }
    
                children.push(item);
                if(window.innerWidth < 420)
                {
                        if(index == 0){
                        carouselItems.push(<CarouselItem children={children} active="active" handleModalClick={this.props.handleModalClick}/>)
                        }
                        else{
                        carouselItems.push(<CarouselItem children={children} handleModalClick={this.props.handleModalClick}/>)
                        }
                        children = []
                    
                }
                else{
                    if(index%4 == 3){
                        if(index == 3){
                        carouselItems.push(<CarouselItem children={children} active="active" id={index} handleModalClick={this.props.handleModalClick}/>)
                        }
                        else{
                        carouselItems.push(<CarouselItem children={children} id={index} handleModalClick={this.props.handleModalClick}/>)
                        }
                        children = []
                    }
                }
            
        })
        }

    return carouselItems
    }

    render(){
        console.log("I am in render...")
        return(
        <div className="container-fluid no-padding homepage-container"> 
            <div id="body2-container-animation-trigger" className="col-md-12 no-padding">
                <div id="body2-top" class="body2-top anim_scroll_reveal_default product-body-heading-text">Beyond Fashion with Exclusive Products</div>
                <div className="col-md-12 no-padding body2_width carousel-container" >
                    <div id={`carousel${this.props.id}`} className="carousel slide" data-interval="0" data-ride="carousel">
                        <div class="carousel-inner">
                        {
                            this.handleChildrenElements().map((item)=>{
                                return item
                            })
                        }
                        
                        </div>

                        <a class="carousel-control-prev carousel-control-btn" href={`#carousel${this.props.id}`} data-slide="prev">
                            <span class="carousel-control-prev-icon" ></span>
                        </a> 
                            <a class="carousel-control-next carousel-control-btn" href={`#carousel${this.props.id}`} data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
export default ProductCarousel