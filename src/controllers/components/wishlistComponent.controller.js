import React from 'react';

const wishlistComponent = (props) => (<tr className="cart-tr-border">
    <td className="cart-border">{props.index}</td>
    <td className="row no-margin cart-border" style={{"border": "none"}}>
        <div className="col-md-3 col-sm-3 col-3 d-inline-block no-padding">
            <img src={props.img} width='100px' height='116px'/>
        </div>
        <div className="col-md-9 col-sm-9 col-9 d-inline-block align-top wishlist-padding"
             style={{"padding-right": "0%"}}>
            <p className="cart-item-name">{props.item.name}</p>
            <p className="cart-item-name">&#x20b9; 250</p>
        </div>
    </td>
    <td style={{textAlign: 'center', verticalAlign: 'middle'}}
        className="cart-border">
        {/* <div className="dropdown show">
            <a className="btn dropdown-toggle" href="#" role="button"
               id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false"> 
                MEDIUM &nbsp;&nbsp;&nbsp;&nbsp;
            </a>

            <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <p style={{cursor: 'pointer'}} onClick={() => props.onSizeSelected('small')} className="dropdown-item" >SMALL</p>
                <p style={{cursor: 'pointer'}} onClick={() => props.onSizeSelected('medium')} className="dropdown-item" >MEDIUM</p>
                <p style={{cursor: 'pointer'}} onClick={() => props.onSizeSelected('large')} className="dropdown-item" >LARGE</p>
            </div>
        </div> */}
        <select className="wishlist-size-options">
            <option onClick={() => props.onSizeSelected('small')}>SMALL</option>
            <option onClick={() => props.onSizeSelected('medium')}>MEDIUM</option>
            <option onClick={() => props.onSizeSelected('large')}>LARGE</option>
        </select>
    </td>

    <td style={{verticalAlign: 'middle', textAlign: 'center'}}
        className="cart-border">
        <button
            className="col-md-5 btn text-center font_family1 font_size2 font_wt2 style11_prod"
            onClick={props.onAddToCartButtonClick} data-toggle="modal"
            data-target="#product_popup">
                                                    <span className='style11_prod_text'>ADD TO CART <span
                                                        className='btn-line'></span></span>
        </button>
        <button onClick={props.onDeleteButtonClick} style={{
            background: 'transparent',
            border: 'none',
            cursor: 'pointer'
        }} className="cart-item-remove col-md-2 font_family2 font_wt2 "><i
            className="fa fa-trash-o fa-2x cart-remove-btn-padding"></i><br />REMOVE
        </button>
    </td>
</tr>);

export default wishlistComponent;