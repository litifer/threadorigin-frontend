import React from 'react';

const dashboardComponent = (props) => (<div className="col-md-5 col-sm-5 col-12 d-inline-block padding-left">
    <div className="col-md-12 col-sm-12 d-inline-block no-padding account-box">
        <div className="col-md-10 col-sm-10 offset-1 d-inline-block no-padding account-box-top-margin absolute">
            <div className="col-md-4 col-sm-4 col-4 d-inline-block account-top-box">
                <span className="account-top-box-text">{props.heading}</span>
            </div>
        </div>
        <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
        <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
        {Object.keys(props.data).map(entity => (<div className="col-md-12 col-sm-12 d-inline-block">
            <div className="col-md-2 col-sm-2 d-inline-block no-padding">
                <span className="account-box-text">{entity}:</span>
            </div>
            <div className="col-md-10 col-sm-10 d-inline-block no-padding">
                <span className="account-box-text">{props.data[entity]}</span>
            </div>
        </div>))}

        <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
        <div className="col-md-10 col-sm-10 offset-1 d-inline-block no-padding">
            <button onClick={props.onEditButtonClick} className="btn col-2 acoount-btn"><span className="account-btn-text">EDIT</span></button>
        </div>
        <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
    </div>
</div>);

export default dashboardComponent;