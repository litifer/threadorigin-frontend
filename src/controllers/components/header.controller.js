import React, {Component} from 'react'

import indiaImg from '../../img/Bitmap-india.png'
import logo from '../../img/homepage_logo.png'
import responsive_logo from '../../img/white-logo-mobile.png'
import indiaArrow from '../../img/keyboard-right-arrow-button.png'
import searchIcon from '../../img/searchbar.png'
import cartIcon from '../../img/cart.png'
import myaccount from '../../img/myaccount.png'

import '../../css/header.css'
import {Urls} from '../../consts'

// const Urls = {
//     dresses : "/category?id=10",
//     bottoms : "/category?id=13",
//     tops : "/category?id=12",
//     lookBooks : "/lookbooks",
//     collections : "/category?id=5",
//     sandook : "/category?id=6",
//     cart : "/cart"
// }
class Header extends Component{

    constructor(props){
        super(props)
        this.state = {value : ""}
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event){
        this.setState({value: event.target.value});
    }

    // componentDidMount() {
    //     console.log(window.localStorage.getItem('authToken'), "homepage header token");
    // }

    render(){
        var link_categ = Urls.SEARCH_URL+this.state.value;
        return(

            <nav className="navbar navbar-expand-lg navbar-light no-padding">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                { (window.innerWidth > 420) ? 
                        <a className="navbar-brand" href="/" id="brand-none"><img class="logo-img" src={logo}/></a>
                        : <a className="navbar-brand" href="/" id="brand-none"><img class="logo-img" src={responsive_logo}/></a>
                }
                <a className="navbar-brand" id="brand-none"><img src={cartIcon}/></a>
                <div className="col-11 mx-auto no-padding">
                    <div>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <div className="no-padding col-md-4">
                            <div className="no-padding header-margin"><span id="none" className="header-left-text">Get Free Shipping on Orders above Rs. 1999</span></div>
                            <div className="no-padding links_head">
                                <ul className="navbar-nav">
                                    <li className="nav-item active nav_header_list">
                                        <a className="nav-link header-padding" href={Urls.DRESSES_URL}><span className="header-left-item-text">DRESSES</span></a>
                                    </li>
                                    <li className="nav-item active nav_header_list">
                                        <a className="nav-link header-padding" href={Urls.BOTTOMS_URL}><span className="header-left-item-text">BOTTOMS</span></a>
                                    </li>
                                    <li className="nav-item active nav_header_list">
                                        <a className="nav-link header-padding" href={Urls.TOPS_URL}><span className="header-left-item-text">TOPS</span></a>
                                    </li>
                                    <li className="nav-item active nav_header_list">
                                        <a className="nav-link header-padding" href={Urls.LOOKBOOKS_URL}><span className="header-left-item-text">LOOKBOOKS</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className='d-md-inline-block navbar-logo col-md-4 text-center no-padding'>
                            <a className="nav-link" href="/"><img src={logo} /></a>
                        </div>
                        <div className='d-md-inline-block col-md-4 no-padding text-right'>
                            <div className='d-md-inline-block col-md-8 no-padding'>
                                <input className="form-control header-search-background d-inline-block" type="text" placeholder="Search your products here" value = {this.state.value} onChange={this.handleChange} />
                                <a className="nav-link d-inline-block header-search-border nav-padding"><img src={searchIcon}/></a>
                            </div>
                            <div className='d-md-inline-block header-icon-container col-md-4 no-padding '>
                                <div className="d-inline-block active header-padding-left">
                                        <a className="nav-link" href={Urls.MYACCOUNT_URL}><img src={myaccount}/></a>
                                </div>
                                <div className="d-inline-block active header-padding-left">
                                        <a className="nav-link" href={Urls.CART_URL}><img src={cartIcon}/></a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header 