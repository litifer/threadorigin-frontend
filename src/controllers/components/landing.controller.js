import React, {Component} from 'react'

import indiaImg from '../../img/Bitmap-india.png'

import '../../css/landing.css'

import line7Img from '../../img/Line 7.png'
import line2Img from '../../img/Line 2.png'
import playbuttonImg from '../../img/play-img.png'
import backgroundImg from '../../img/background.png' 

import prewebsitelaunch1 from '../../img/prel1.jpg'
import prewebsitelaunch2 from '../../img/prel2.jpg'
import prewebsitelaunch3 from '../../img/prel3.jpg'
import prewebsitelaunch4 from '../../img/prel4.jpg'
import prewebsitelaunch5 from '../../img/prel5.jpg'
import prewebsitelaunch6 from '../../img/prel6.jpg'
// import line2 from '../../img/Line 2.png'
// import mobileview from '../../img/prelaunchimage.png'
import mobileview1 from '../../img/prelaunchmobile1.jpg'
import mobileview2 from '../../img/prelaunchmobie2.jpg'
import mobileview3 from '../../img/prelaunchmobile3.jpg'
import mobileview4 from '../../img/prelaunchmobile4.jpg'
import mobileview5 from '../../img/prelaunchmobile5.jpg'
import mobileview6 from '../../img/prelaunchmobile6.jpg'

class Landing extends Component{

    constructor(props){
        super(props)
    }

    handleSubmit = () => {
        let category = {
            id : 1
        }
        this.props.loadCategoryPage(category)
    }

    render(){
        return(
            

            <div id="demo" class="carousel slide background-div landing-container" data-ride="carousel">

                {/* <!-- Indicators --> */}
                {/* <ol class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active indicators"></li>
                    <li data-target="#demo" data-slide-to="1" className = 'indicators'></li>
                    <li data-target="#demo" data-slide-to="2" className = 'indicators'></li>
                    <li data-target="#demo" data-slide-to="2" className = 'indicators'></li>
                    <li data-target="#demo" data-slide-to="2" className = 'indicators'></li>
                    <li data-target="#demo" data-slide-to="2" className = 'indicators'></li>
                </ol> */}
                
                {/* <!-- The slideshow --> */}
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        {/* <img src={backgroundImg} width="100%" className="background"/> */}
                        <img src={prewebsitelaunch1} className="preWebsitelaunch-img background" id="none" />
                        <img src={mobileview1} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    <div className="carousel-item landing-overlay">
                        <img src={prewebsitelaunch2} className="preWebsitelaunch-img" id="none" />
                        <img src={mobileview2} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    <div className="carousel-item">
                        <img src={prewebsitelaunch3} className="preWebsitelaunch-img" id="none" />
                        <img src={mobileview3} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    <div className="carousel-item">
                        <img src={prewebsitelaunch4} className="preWebsitelaunch-img" id="none" />
                        <img src={mobileview4} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    <div className="carousel-item">
                        <img src={prewebsitelaunch5} className="preWebsitelaunch-img" id="none" />
                        <img src={mobileview5} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    <div className="carousel-item">
                        <img src={prewebsitelaunch6}  className="preWebsitelaunch-img" id="none" />
                        <img src={mobileview6} className="preWebsitelaunch-img hide_nav" />
                    </div>
                    
                </div>

                <div className="carousel-caption landing-caption col-md-12 col-sm-12 float-sm-left float-md-left no-padding" >
                     <div className="col-md-6 offset-md-3 col-sm-6 float-sm-left float-md-left no-padding">
                         <div className="col-md-12 float-md-left col-sm-12 float-sm-left no-padding landing-top">
                         <div className="preWebsitelaunch-heading">{this.props.title}</div>
                         </div>
                         {/* <div className="col-md-12 float-md-left col-sm-12 float-sm-left no-padding text-center landing_desc landing-description-margin">
                             <div className="landing-text">{this.props.description}</div>
                         </div> */}
                         {/* <div className="col-md-12 float-md-left col-sm-12 float-sm-left no-padding">
                             <a href="/category?id=2" type="button" className="btn landing-item-middle2-center-button" onClick={() => this.handleSubmit()}>{this.props.landingButton}&nbsp;&nbsp;&nbsp;&nbsp;
                                 <img className="landing-item-middle2-center-img" src={line2Img}/>
                             </a>
                         </div> */}
                         {/* <div className="col-md-12 float-md-left col-sm-12 float-sm-left no-padding d-none mx-auto d-md-block">
                             <br /><br id="none" />
                             <img className="landing-play-btn" src={playbuttonImg}/>
                         </div> */}
                         {/* <div className="col-md-12 float-md-left col-sm-12 float-sm-left no-padding text-center text-play d-none d-md-block">
                        
                             <span className="play-text">PLAY VIDEO</span>
                         </div> */}
                     </div>                    
                 </div>
            </div>
        )
    }
}

export default Landing