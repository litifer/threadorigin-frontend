import React from 'react';


const addressComponent = (props) => {
    return (<div style={{marginTop: '3%'}} className="col-md-5 col-sm-5 col-12 d-inlne-block padding-left">
        <div className="col-md-12 col-sm-12 d-inline-block no-padding account-box">
            <div className="col-md-10 col-sm-10 offset-1 d-inline-block no-padding account-box-top-margin absolute">
                <div className="col-md-4 col-sm-4 col-4 d-inline-block account-top-box">
                    <span className="account-top-box-text">{props.addressType}</span>
                </div>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
            <div className="col-md-12 col-sm-12 d-inline-block">
                <span className="account-box-text">{props.fullName}<br />{props.street}<br />{props.city}<br />{props.pinCode}<br />{props.country}<br /><br />{props.phone}</span>
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block">
                <div className="col-md-12 col-sm-12 d-inline-block no-padding">
                    <div className="col-md-8 col-sm-8 float-md-right float-sm-right no-padding">
                        <button onClick={props.onEditButtonClick} className="btn col-md-3 col-3 account-btn-margin float-right acoount-btn "><span className="account-btn-text">EDIT</span></button>
                        <button onClick={props.onDeleteButtonClick} className="btn col-md-4 col-4 account-btn-margin float-right acoount-btn"><span className="account-btn-text">DELETE</span></button>
                        {props.default ? null : <button onClick={props.setAsDefault} className="btn col-md-8 m-1 col-8 account-btn-margin float-right acoount-btn"><span className="account-btn-text">SET AS DEFAULT</span></button>}
                    </div>
                </div>

                {/* <button className="btn col-md-2 float-right acoount-btn"><span className="account-btn-text">EDIT</span></button> */}
            </div>
            <div className="col-md-12 col-sm-12 d-inline-block no-padding">&nbsp;</div>
        </div>
    </div>);
};

export default addressComponent;