import React from 'react';

const wishlistComponentMobile = (props) => (<div className="row no-margin cart-mobile-top-container-padding">
<div className="col-4 no-padding">
    <img src='' width="96px" height= "145px" />
</div>
<div className="col-8">
    <div className="col-12 d-inline-block no-padding cart-mobile-items-text">{props.item.name}</div>
    <div className="col-12 d-inline-block no-padding cart-mobile-price">&#x20b9; 983247</div>
    <div className="col-12 d-inline-block no-padding cart-mobile-qty-padding">
        
        <button
            className="col-8 btn text-center font_family1 font_size2 font_wt2 style11_prod"
            onClick={props.onAddToCartButtonClick} data-toggle="modal"
            data-target="#product_popup">
                                                    <span className='style11_prod_text'>ADD TO CART <span
                                                        className='btn-line'></span></span>
        </button>
        <div className="col-4 d-inline-block cart-item-remove text-center align-middle">
            <i onClick={props.onDeleteButtonClick} class="fa fa-trash-o fa-2x"></i><br />REMOVE
        </div>
    </div>
</div>
</div>);

export default wishlistComponentMobile;