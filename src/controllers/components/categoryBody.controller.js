import React, {Component} from 'react'
import '../../css/category.css'
import item1 from '../../img/item1.png'
import item2 from '../../img/item2.png'
import item3 from '../../img/item3.png'
import item4 from '../../img/item4.png'
import oval1 from '../../img/Oval 1.png'
import oval2 from '../../img/Oval 2.png'


class CategoryBody extends Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className="container">
                <div className="col-md-12 col-sm-12 float-sm-left float-md-left" style={{"margin-top":"2%"}}>
                    <div className="col-md-3 col-sm-6 float-sm-left float-md-left">
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding overflow">
                            <img src={item1} className="galleryImg img-responsive" />
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <br />
                            <div className="col-md-7 col-sm-7 float-sm-left float-md-left no-padding">
                                <span>Robe Chemise Harvard</span>
                            </div>
                            <div className="col-md-5 col-sm-5 float-sm-left float-md-right no-padding" >
                                <span>Rs. 2499</span>
                            </div>                            
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                            <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  
                            <br />                          
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 float-sm-left float-md-left">
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding overflow">
                            <img src={item2} className="galleryImg img-responsive" />
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <br />
                            <div className="col-md-7 col-sm-7 float-sm-left float-md-left no-padding">
                                <span>Robe Chemise Harvard</span>
                            </div>
                            <div className="col-md-5 col-sm-5 float-sm-left float-md-right no-padding" >
                                <span>Rs. 2499</span>
                            </div>                            
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                            <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  
                            <br />                          
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 float-sm-left float-md-left">
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding overflow">
                            <img src={item3} className="galleryImg img-responsive" />
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <br />
                            <div className="col-md-7 col-sm-7 float-sm-left float-md-left no-padding">
                                <span>Robe Chemise Harvard</span>
                            </div>
                            <div className="col-md-5 col-sm-5 float-sm-left float-md-right no-padding" >
                                <span>Rs. 2499</span>
                            </div>                            
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                            <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  
                            <br />                          
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6 float-sm-left float-md-left">
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding overflow">
                            <img src={item4} className="galleryImg img-responsive" />
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                        <br />
                            <div className="col-md-7 col-sm-7 float-sm-left float-md-left no-padding">
                                <span>Robe Chemise Harvard</span>
                            </div>
                            <div className="col-md-5 col-sm-5 float-sm-left float-md-right no-padding" >
                                <span>Rs. 2499</span>
                            </div>                            
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                            <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  
                            <br />                          
                        </div>
                    </div>                                                         
                </div>
            </div>
            
        )
    }
}

export default CategoryBody