import React, {Component} from 'react'
import * as  config_ from '../../config.js';
import keyboard_left_arrow_button from '../../img/keyboard-left-arrow-button.png';
import keyboard_down_arrow_button from '../../img/keyboard-down-arrow-button.png';
import oval1 from '../../img/Oval 1.png';
import oval2 from '../../img/Oval 2.png';
import Line_2 from '../../img/Line 2.png';
import '../../css/header.css'

const Urls = {
    dresses : "/category?id=1",
    bottoms : "/category?id=2",
    tops : "/category?id=3",
    lookBooks : "/category?id=4",
    collections : "/category?id=5",
    sandook : "/category?id=6",
    cart : "/cart"
}
class BODY2 extends Component{

    constructor(props){
        super(props)
    }
    componentDidMount(){
        this.props.fetchprodBody();
    }
    render(){
        return(
        <div className="container-fluid no-padding " id="none"> 
            <div id="body2-container-animation-trigger" className="col-md-12 float-md-left no-padding product-body-margin-bottom">
                <div id="body2-top" class="body2-top anim_scroll_reveal_default product-body-heading-text">Beyond Fashion with Exclusive Products</div>
                <div className="col-md-12 float-md-left no-padding body2_width carousel-container" >
                    <div id="carousel" className="carousel slide" data-interval="0" data-ride="carousel">
                        <div class="carousel-inner">
                        <div class="carousel-item product-carousel-item active">
                            <div class="row">
                            {
                                this.props.product_details.items.map((items, index)=> {
                                if(index < 4 ){
                                    var classN = ''
                                    var link = '../product/?id='+items.sku;
                                    var img_item = items.custom_attributes[4]
                                    if(img_item == undefined){}
                                    else{
                                    classN = 'col-md anim_scroll_reveal_sequence_body2'
                                    var url_img = `${config_.IMG_URL}${img_item.value}`
                                    return  <div className= {classN} ref="body2Item">
                                                <div className="overflow">
                                                    <a href = {link}> <img src={url_img} className="galleryImg carousel-img img-responsive" /></a>
                                                </div>
                                                <div className=""><br />
                                                    <div className="">
                                                        <span>{items.name}</span>
                                                    </div>
                                                    <div className="" >
                                                        <span>{items.price}</span>
                                                    </div>                            
                                                </div>
                                                <div className="">
                                                    <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  <br />                          
                                                </div>
                                            </div>
                                        }
                                    }
                                })
                            }
                            </div>
                        </div>
                        <div class="carousel-item product-carousel-item">
                        <div class="row">
                            {
                                this.props.product_details.items.map((items, index)=> {
                                if(index >= 4 && index<8 ){
                                    var classN = ''
                                    var link = '../product/?id='+items.sku
                                    var img_item = items.custom_attributes[4]
                                    if(img_item == undefined){}
                                    else{

                                        if(index == 4){classN = 'col-md'}
                                        else {classN = 'col-md'}
                                    var url_img = `${config_.IMG_URL}${img_item.value}`
                                    return  <div className={classN}>
                                                <div className="overflow">
                                                    <a href = {link}><img src={url_img} className="galleryImg carousel-img img-responsive" /></a>
                                                </div>
                                                <div className=""><br />
                                                <div className="">
                                                    <span>{items.name}</span>
                                                </div>
                                                <div className="" >
                                                    <span>{items.price}</span>
                                                </div>                            
                                                </div>
                                                <div className="">
                                                <img src={oval2} />&nbsp;&nbsp;<img src={oval1} />  <br />                          
                                                </div>
                                            </div>
                                        }
                                        }
                                    })
                            }

                            </div>
                        </div>
                        </div>

                        <a class="carousel-control-prev carousel-control-btn" href="#carousel" data-slide="prev">
                            <span class="carousel-control-prev-icon" ></span>
                        </a> 
                            <a class="carousel-control-next carousel-control-btn" href="#carousel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

BODY2.defaultProps = {
    product_details : {
      items: [
       {
         custom_attributes:[{}]
        }
      ]
    }
}

export default BODY2