import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import '../../css/checkout.css'
import CheckoutFinalSummary from '../components/checkoutFinalSummary.controller'

class CheckoutGuest extends Component{
    render(){
        return(
            <div>
                <HeaderProduct/>



                <div class="main-container checkout-big-img no-padding">
                    <div class="col-md-12 col-sm-12 float-sm-left float-md-left no-padding"><br />
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div class="col-md-6 col-sm-6 float-sm-left float-md-left no-padding checkout-heading-margin"><span className="no-padding checkout-heading">Checkout</span><br /><span className="checkout-top-text">Just a few steps and you’ll complete the order</span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 float-left float-sm-left float-md-right no-padding">
                                <div class="col-md-12 col-sm-12 col-12 float-left float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div class="col-md-12 col-sm-12 col-12 float-left float-sm-left float-md-left no-padding">
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-center">
                                        <span className="checkout-other-text">Login/Register</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-right">
                                        <span className="checkout-active-text">Address Confirmation</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-right">
                                        <span className="checkout-other-text">Payment Options</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div className="col-md-9 col-sm-12 float-sm-left float-md-left no-padding checkout-box">
                                <div className="col-md-11 col-sm-11 float-sm-left float-md-left no-padding checkout-margin">
                                    <input type="checkbox" className="checkout-guest-checkbox" /><span className="checkout-guest-checkbox-text">Set Delivery Address same as the Billing Address</span><br /> 
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <span className="checkout-guest-text">Billing Address</span>
                                    <br /><br />
                                    <button className="btn col-md-3 checkout-guest-btn"><span className="checkout-guest-btn-text">ADD NEW ADDRESS &nbsp;&nbsp;&nbsp;----</span></button>

                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                      
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                      
                                    <div className="col-md-11 col-sm-11 float-md-left float-sm-left no-padding">
                                    <div className="col-md-6 col-sm-6 float-md-left float-sm-left padding-left">
                                            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding account-box">
                                                <div className="col-md-11 col-sm-11 offset-1 float-md-left float-sm-left no-padding account-box-top-margin absolute">
                                                    <div className="col-md-6 col-sm-6 float-md-left float-sm-left account-top-box">
                                                        <span className="account-top-box-text">Additional Address 1</span>
                                                    </div>                                        
                                                </div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left">    
                                                    <div className="col-md-8 col-sm-8 float-md-left float-sm-left">    
                                                        <span className="account-box-text">Bruce Banner<br />S.H.E.I.L.D<br />U-52/23 GF, DLF Phase 3, Gurgaon<br />Gurgaon, Haryana 122002<br />India<br /><br />9873467209</span>                                      
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 float-md-right float-sm-left no-padding">
                                                        
                                                        <button className="checkout-guest-btn-margin btn col-md-10 account-btn-margin acoount-btn"><span className="account-btn-text">SELECT</span></button>
                                                    </div>                                     
                                                </div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 float-md-left float-sm-left padding-right">
                                            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding account-box">
                                                <div className="col-md-11 col-sm-11 offset-1 float-md-left float-sm-left no-padding account-box-top-margin absolute">
                                                    <div className="col-md-6 col-sm-6 float-md-left float-sm-left account-top-box">
                                                        <span className="account-top-box-text">Additional Address 2</span>
                                                    </div>                                        
                                                </div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left">    
                                                    <div className="col-md-8 col-sm-8 float-md-left float-sm-left">    
                                                        <span className="account-box-text">Bruce Banner<br />S.H.E.I.L.D<br />U-52/23 GF, DLF Phase 3, Gurgaon<br />Gurgaon, Haryana 122002<br />India<br /><br />9873467209</span>                                      
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 float-md-right float-sm-left no-padding">
                                                        
                                                        <button className="checkout-guest-btn-margin btn col-md-10 account-btn-margin acoount-btn"><span className="account-btn-text">SELECT</span></button>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                     
                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                     
                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                                            <button className="btn checkout-guest-bottom-btn col-md-4 float-md-right checkout-guest-btn-margin-right"><span className="checkout-guest-bottom-btn-text">CONTINUE TO LAST STEP&nbsp;&nbsp;&nbsp;----</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <CheckoutFinalSummary />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default CheckoutGuest