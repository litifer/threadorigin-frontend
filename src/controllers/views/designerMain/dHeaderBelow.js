import React from 'react'; 
import './../../../css/designerMain/dheaderbelow.css';
import SearchImage from './../../../img/designerMain/search.png'

const dHeaderbelow = (props) => {
    return (
        <div className="designerHeader">
            <div className="row">

        <div className="col-lg-6 col-md-6">
                 <p className="SearchName"> Search Designers by Name</p>
        </div>
       
        <div className="d-none d-md-block col-lg-6 col-md-6 ">
                 <img src={SearchImage} className="searchImage"/>
        </div>
        
            </div>
        </div>

    )
}

export default dHeaderbelow;