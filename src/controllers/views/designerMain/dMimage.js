import React from "react";
import Cmain1 from "../../../img/designerMain/cmain1.png";
import Cmain2 from "../../../img/designerMain/cmain2.png";
import Cmain3 from "../../../img/designerMain/cmain3.png";
import Cmain4 from "../../../img/designerMain/cmain4.png";
import './../../../css/designerMain/dimage.css';




const imageComponent = (props) => {
    return (
<div>
      <div className="imageparent">
          <div className="row">
            <div className="col-md-3 col-3 col-sm-3 no-padding"> 
                <img src={Cmain1} className="image-component-class1"/>
            </div>
            <div className="col-md-3 col-3 col-sm-3 no-padding "> 
               <img src={Cmain2} className="image-component-class1"/>
          </div>
          <div className="col-md-3 col-3 col-sm-3 no-padding "> 
              <img src={Cmain3} className="image-component-class1"/>
          </div>
          <div className="col-md-3 col-3 col-sm-3 no-padding "> 
              <img src={Cmain4} className="image-component-class1"/>
          </div>
      </div>
      </div>
      <div className="text-content-d d-none d-lg-block "> 
      <h1 className="heading-1">Our Designers</h1>
       <p className="pheading-1">Thread Origin has some really taleneted designers under its hood. We have collaborated with them and you can check out there products, lookbooks, campaigns etc.</p> 
      </div>
    </div>

    )
}

export default imageComponent;