import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import * as  config from '../../config.js';
import Footer from '../components/footer.controller'
import ScrollMagic from 'scrollmagic'
import '../../css/category.css'
import rightArrow from '../../img/keyboard-right-arrow-button.png'
import filter from '../../img/filter.png'
import sort from '../../img/sort.png'
import categoryHeader from '../../img/category_header.png'

import {Product} from "../components/productCarousel.controller"
import Modal from "../components/modal.controller"

const {getAccessToken} = require("../../helpers/auth")

const {hasData, find, findProductById} = require("../../helpers/general")

class Category extends Component{
    
    constructor(props){
        super(props)
            // This binding is necessary to make `this` work in the callback
        this.handleColorFilter = this.handleColorFilter.bind(this);
        this.handleSizeFilter = this.handleSizeFilter.bind(this);
        this.handleModalClick = this.handleModalClick.bind(this);
        this.state = {
            areColorsFetched: false,
            areSizesFetched: false,
            areVariantsFetched: false
        }
    }

    

    componentDidMount(){
        let query = new URLSearchParams(this.props.location.search);
        if(query.get('id')){
            this.props.getRelevantItems({
                api: "CATEGORY",
                categoryId: query.get('id')
            });
        }
        else if(query.get('search')){
            this.props.getRelevantItems({
                api: "SEARCH",
                search: query.get('search')
            })
        }

        
        // this.props.fetchImage();
        this.props.fetchColor();
        this.props.fetchSize();
        let scrollfooterController = new ScrollMagic.Controller();
    
    }

    componentDidUpdate(){
        if(!this.props.products_updated && hasData(this.props.products))
        {
            this.props.products.map( (product,index) => {
                this.props.fetchProductData(product);
                this.props.productsDidUpdate();
            })
        }
    }

    // hasData(data){
    //     if(data){
    //         return data
    //     }
    //     else return []
    // }

    handleColorFilter(evt){
        console.log(this.props.color_filters)
        if(evt.target.checked)
            this.props.addColorFilter(evt.target.value);
        else this.props.removeColorFilter(evt.target.value);    
    }

    handleSizeFilter(evt){
        console.log(evt)
        if(evt.target.checked)
            this.props.addSizeFilter(evt.target.value);
        else this.props.removeSizeFilter(evt.target.value);    
    }

    handleDisplay(color_filters, size_filters, attributes){
        if(!attributes) return

        var colorFound = false
        var sizeFound = false
        
        if(color_filters.length === 0){
            colorFound = true
            sizeFound= true
        }
        else{
            for(var color of color_filters){
                if(attributes[0].values.includes(color))
                {
                    colorFound = true
                }
            }
        }
        
        if(size_filters.length === 0){
            sizeFound = true
        }
        else{
            for(var size of size_filters){
                if(attributes[1].values.includes(size))
                {
                    sizeFound = true
                }
            }
        }
        
        return colorFound && sizeFound        
    }

    

    handleModalClick(product_id){
        this.setState({areColorsFetched: false, areSizesFetched: false, areVariantsFetched: false})
        this.props.changeModal(findProductById(product_id, this.props.product_data));
    }

    render(){
        return(
            <div id = 'loader'>
                
                <HeaderProduct/>

                <div className="no-padding">
                    <div className="col-md-12 col-sm-12 col-12 float-sm-left no-padding float-md-left category-image">
                        <img src={categoryHeader} width="100%" height="372px" className="category-img-height" />
                    </div>
                    <div className="col-md-12 col-sm-12 col-12 float-sm-left no-padding float-md-left category-image-text">
                        <div className="col-md-12 col-sm-12 col-12 float-sm-left float-md-left" ><span className="category-main-heading">Dresses</span>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 col-12 float-sm-left float-md-left category-nav-z-index" style={{"margin-top":"-2%"}} id="none">
                    <div className="row col-md-10 col-sm-10 col-10 filter-container" style={{"background-color": "#6b573b","padding-right": "0%"}}>
                        <div className="row col-md-8 col-sm-10 filter-row">
                            <div className="padding-cat filter-heading"><span className="category-nav-item" style={{"text-decoration":"none"}}>Showing Products:</span></div>
                            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" className="category-nav-text">Based on Popularity<img src={rightArrow} /></a>
                            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" className="category-nav-text">All Colors<img src={rightArrow} /></a>
                            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" className="category-nav-text">All Sizes<img src={rightArrow} /></a>                            
                        </div>
                        {/* <div className="no-padding category-filter-btn-container">
                            <button className="category-filter-btn"><span><img src={sort} /></span></button>
                            <button className="category-filter-btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><span><img src={filter} /></span></button>
                        </div> */}
                        <div class="collapse category-filter-collapse" id="collapseExample">
                            <div class="category-filter-bg">
                                <div className="row no-margin">
                                    <div className="category-filter-col">
                                        <span className="category-filter-text">Sort By:</span>
                                    </div>
                                    <div className="category-filter-col">
                                        <label class="radio-container"><span className="category-filter-text-radio">Price Low to High</span>
                                            <input type="checkbox" className="radio-btn" checked="checked" />
                                            <span class="checkmark checkmark-btn"></span>
                                        </label>
                                        <label class="radio-container"><span className="category-filter-text-radio">Price High to Low</span>
                                            <input type="checkbox" className="radio-btn"/>
                                            <span class="checkmark checkmark-btn"></span>
                                        </label>
                                        <label class="radio-container"><span className="category-filter-text-radio">Sort By Newness</span>
                                            <input type="checkbox" className="radio-btn"/>
                                            <span class="checkmark checkmark-btn"></span>
                                        </label>
                                        <label class="radio-container"><span className="category-filter-text-radio">Sort by Trending</span>
                                            <input type="checkbox" className="radio-btn" />
                                            <span class="checkmark checkmark-btn"></span>
                                        </label>
                                        <label class="radio-container"><span className="category-filter-text-radio">Based on Popularity</span>
                                            <input type="checkbox" className="radio-btn" />
                                            <span class="checkmark checkmark-btn"></span>
                                        </label>
                                    </div>
                                    <div className="category-filter-col">
                                        <span className="category-filter-text">Colors Shades:</span>
                                    </div>
                                    <div className="category-filter-col">
                                    
                                    
                                    {  
                                        
                                       this.props.color_val.options.map((options,index)=>{
                                            var opt_color = ''
                                            if(options.label == " "){
                                                opt_color = "All Colors"
                                            }
                                            else 
                                                opt_color = options.label
                
                                            return <label class="checkbox-container" for={`check-${opt_color}`}><span className="category-filter-text-radio">{opt_color}</span>
                                                        <input type="checkbox" className="radio-btn" value={options.value} onChange={this.handleColorFilter} id={`check-${opt_color}`}/>
                                                        <span class="checkmark"></span>
                                                    </label>
                                        })
                                    
                                    }
                                       
                                    </div>
                                    <div className="category-filter-col">
                                        <span className="category-filter-text">Sizes:</span>
                                    </div>
                                    <div className="category-filter-col">
                                    {
                                        this.props.size_val.options.map((options,index)=>{
                                            var opt_size = ''
                                            if(options.label == " "){
                                                opt_size = "All Sizes"
                                            }
                                            else 
                                                opt_size = options.label
                                          return <label class="checkbox-container" for={`check-${opt_size}`}><span className="category-filter-text-radio">{opt_size}</span>
                                                    <input type="checkbox" className="radio-btn" value={options.value} id={`check-${opt_size}`} onChange={this.handleSizeFilter}/>
                                                    <span class="checkmark"></span>
                                                </label>
                                      })
                                    }
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div className="row col category-nav-background-color filter-container hide_nav">
                    <div className="row filter-row no-margin">
                        {/* <div className="filter-heading"><span className="category-mobile-nav-text">Show Filters  </span><img src={rightArrow} /></div> */}
                        <a data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample1" className="category-nav-text">Show Filters <img src={rightArrow} /></a>
                    </div>    
                    {/* <div className="no-padding category-filter-btn-container">
                        <button className="category-filter-btn"><span><img src={sort} /></span></button>
                        <button className="category-filter-btn"><span><img src={filter} /></span></button>
                    </div>  */}
                    <div class="collapse category-filter-collapse" id="collapseExample1">
                            <div class="category-filter-bg">
                                <div className="row no-margin">
                                    <div className="col-12 category-filter-margin">
                                        <div className="category-filter-col-mobile-1">
                                            <span className="category-filter-text">Sort By:</span>
                                        </div>
                                        <div className="category-filter-col-mobile-2">
                                            <label class="radio-container"><span className="category-filter-text-radio">Price Low to High</span>
                                                <input type="checkbox" className="radio-btn" checked="checked" />
                                                <span class="checkmark checkmark-btn"></span>
                                            </label>
                                            <label class="radio-container"><span className="category-filter-text-radio">Price High to Low</span>
                                                <input type="checkbox" className="radio-btn"/>
                                                <span class="checkmark checkmark-btn"></span>
                                            </label>
                                            <label class="radio-container"><span className="category-filter-text-radio">Sort By Newness</span>
                                                <input type="checkbox" className="radio-btn"/>
                                                <span class="checkmark checkmark-btn"></span>
                                            </label>
                                            <label class="radio-container"><span className="category-filter-text-radio">Sort by Trending</span>
                                                <input type="checkbox" className="radio-btn" />
                                                <span class="checkmark checkmark-btn"></span>
                                            </label>
                                            <label class="radio-container"><span className="category-filter-text-radio">Based on Popularity</span>
                                                <input type="checkbox" className="radio-btn" />
                                                <span class="checkmark checkmark-btn"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-12 category-filter-margin">
                                        <div className="category-filter-col-mobile-1">
                                            <span className="category-filter-text">Colors Shades:</span>
                                        </div>
                                        <div className="category-filter-col-mobile-2">
                                        {   
                                            this.props.color_val.options.map((options,index)=>{
                                                var opt_color = ''
                                                if(options.label == " "){
                                                    opt_color = "All Colors"
                                                }
                                                else 
                                                    opt_color = options.label
                    
                                                return <label class="checkbox-container"><span className="category-filter-text-radio">{opt_color}</span>
                                                            <input type="checkbox" className="radio-btn"/>
                                                            <span class="checkmark"></span>
                                                        </label>
                                            })
                                        }
                                        </div>
                                    </div>
                                    <div className="col-12 category-filter-margin">
                                        <div className="category-filter-col-mobile-1">
                                            <span className="category-filter-text">Sizes:</span>
                                        </div>
                                        <div className="category-filter-col-mobile-2">
                                        {
                                            this.props.size_val.options.map((options,index)=>{
                                                var opt_size = ''
                                                if(options.label == " "){
                                                    opt_size = "All Sizes"
                                                }
                                                else 
                                                    opt_size = options.label
                                            /*return <option value = {opt_size}>{opt_size}</option>*/
                                            return <label class="checkbox-container"><span className="category-filter-text-radio">{opt_size}</span>
                                                        <input type="checkbox" className="radio-btn"/>
                                                        <span class="checkmark"></span>
                                                    </label>
                                            })
                                        }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                </div>


                <div className="container-fluid category-background-color no-padding">
                <div className="col-md-10 mx-auto no-padding">
                <div className="row container-fluid no-margin category-top-padding no-padding category-container-margin-top">
                   {    
                        hasData(this.props.product_data).map((items,index)=>{
                            var img_item = find(items.custom_attributes, 'attribute_code' , 'image')
                            if(img_item == undefined){}
                            else{
                              var classNow = '';
                              var id_sku = items.sku
                              var link = '../product?id='+items.sku
                              var url_img = `${config.IMG_URL}${img_item.value}`
                              var class1 = 'col-md-3 col-sm-6 col-6 float-sm-left float-md-left display category-items-bottom-padding category-box no-padding'
                              var class2 = 'col-md-3 col-sm-6 col-6 float-sm-left float-md-left hide category-items-bottom-padding category-box no-padding'

                              if((this.props.array_color == undefined) &&  (this.props.array_size == undefined) && (this.props.search_item == undefined)){
                                classNow = class1
                              }

                              if(this.props.search_item == undefined){}
                              else{
                                var search_len = this.props.search_item.length;
                                console.log(search_len);
                                console.log(this.props.search_item);
                                classNow = class2;
                                for(var i =0; i<search_len; i++){
                                    var sku = this.props.search_item[i].sku[0];
                                     if(sku == items.sku){
                                      classNow = class1;
                                     }
                                }
                              }
                              
                              if(this.props.array_color){
                                  var color_len = this.props.array_color.length;
                                  classNow = class2
                                  for(var i = 0; i< color_len; i++){
                                    var color_get = this.props.array_color[i]
                                     if (id_sku == color_get){
                                       classNow = class1;
                                     }
                                  }
                                  if(this.props.array_size){
                                     classNow = class2;
                                    var size_len = this.props.array_size.length;
                                    for(var i = 0; i< col_len; i++){
                                      var col_get = this.props.array_color[i]
                                      for(var j =0; j<size_len; j++){
                                        if(this.props.array_size[i] == col_get){
                                          if(this.props.array_color[j] == id_sku){
                                             classNow = class1;
                                           }
                                        }
                                      }
                                    }
                                  }
                                }
                              if(this.props.array_size){
                                var size_len = this.props.array_size.length;
                                classNow = class2
                                 for(var i = 0; i< size_len; i++){
                                  var size_get = this.props.array_size[i]
                                   if (id_sku == size_get){
                                     classNow = class1;
                                   }
                                 }
                                 if(this.props.array_color){
                                    classNow = class2;
                                  var col_len = this.props.array_color.length;
                                   for(var i = 0; i< size_len; i++){
                                    var size_get = this.props.array_size[i]
                                     for(var j =0; j<col_len; j++){
                                       if(this.props.array_color[j] == size_get){
                                           if(this.props.array_color[j] == id_sku){
                                             classNow = class1;
                                           }
                                       }
                                     }
                                  }
                                }
                              }

                              if(this.handleDisplay(this.props.color_filters, this.props.size_filter, items.configurable_product_options))
                              {
                                  classNow = classNow + ' d-none'
                              }

                              /*
                                Getting available colors and sizes
                              */
                             let color_array = []
                             let size_array = []
                             

                             console.log()

                              if(find(hasData(items.extension_attributes.configurable_product_options), 'label', 'Color')){
                                 color_array = find(hasData(items.extension_attributes.configurable_product_options), 'label', 'Color').values.map((val) => {
                                    return hasData(this.props.color_val.options).find((element) => {
                                        console.log("%c props are: ", "color: yellow");
                                        console.log(element.value, val.value_index)
                                        return element.value == val.value_index
                                    });
                                })
                              }

                              if(find(hasData(items.extension_attributes.configurable_product_options), 'label', 'size')){
                                 size_array = find(hasData(items.extension_attributes.configurable_product_options), 'label', 'size').values.map((val) => {
                                    return hasData(this.props.size_val.options).find((element) => {
                                        console.log("%c props are: ", "color: yellow");
                                        console.log(element.value, val.value_index)
                                        return element.value == val.value_index
                                    });
                                })
                              }
                                       

                            //    console.clear()
                               console.log("%cavailable colors and sizes are:", "color: green");
                               console.log(size_array)
                               console.log(color_array) 
                            console.log("sku is", items.sku)
                              return  <div className={classNow} id = {id_sku}>
                                            <Product sku={items.sku} name={items.name} url={link} img={url_img} price={items.price} id={items.id} color_array = {color_array} size_array={size_array} handleModalClick={this.handleModalClick}/>
                                      </div>
                              } 
                            
                        })
                    }
                </div>
                </div>
                </div>



               
                <div id="product-footer" className="col-md-12 product-footer">
                    {/* <div className="col-md-12 footer_bg_col">&nbsp;</div> */}
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className="footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div>
                {
                    this.props.modal_item? <Modal 
                    isColorFetched={this.state.areColorsFetched} 
                    isSizeFetched={this.state.areSizesFetched} 
                    isVariantFetched={this.state.areVariantsFetched} 
                    modal_item={this.props.modal_item}
                    setColorFetchedTrue={() => this.setState({areColorsFetched: true})}
                    setSizeFetchedTrue={() => this.setState({areSizesFetched: true})} 
                    setVariantFetchedTrue={() => this.setState({areVariantsFetched: true})} 
                    all_colors={this.props.color_val.options} 
                    all_sizes={this.props.size_val.options}/>
                                            : null
                }
                        
            </div>
        )
    }
}

Category.defaultProps = {
    camp : {
      items:[
        {
          custom_attributes:[{}]
        }
     ]      
    },
    img_body:{
        items:[
            {
                custom_attributes:[{}]
            }
        ]
    },
    color_val:{
      options:[{
          label : ""
      }]
    },
    size_val:{
        options:[{}]
    },
    modal_item: {
        name: "",
        url: "",
        sku: ""
    }
}

export default Category