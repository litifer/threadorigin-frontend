import React, {Component} from 'react'
// import HeaderProduct from '../components/header_product.controller'
import HeaderProduct from '../components/header_product.controller'
import * as  config from '../../config.js';
import '../../css/cart.css'

import cartItem from '../../img/cart_item.png'

import Bill from '../components/cartBill.controller'
class Cart extends Component{
    constructor(props){
        super(props)
        this.state = {value : ""}
        this.handleDecrement   = this.handleDecrement.bind(this)
        this.handleIncrement   = this.handleIncrement.bind(this)
        this.handleRemove      = this.handleRemove.bind(this)
        this.handleChange      = this.handleChange.bind(this)
        this.handlegetDiscount = this.handlegetDiscount.bind(this)
        this.handleQuantity = this.handleQuantity.bind(this)
    }
    successMessage(data){
        return <span className="discount-success-message"><i class="fa fa-check-circle" aria-hidden="true"></i> {data}</span>
    }
    errorMessage(data){
        return <span className="discount-error-message"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> {data}</span>
    }
   handleDecrement(qty, cartid,itemid){
    this.props.decrement_val(qty, cartid, itemid)
   }
   handleIncrement(qty,cartid,itemid){
    this.props.increment_val(qty, cartid, itemid)
   }

   handleQuantity(qty, itemId){
     let cartId = window.localStorage.getItem('userCartId') ? window.localStorage.getItem('userCartId') : window.localStorage.getItem('cartId');
     console.log(cartId, "cart id that is going with the api call")
      this.props.updateQuantity(qty, cartId, itemId);
   }

   handleRemove(item_id){
    this.props.remove_item(item_id)
   }


    handleChange(event) {
      this.setState({value: event.target.value});
    }


   handlegetDiscount(evt)
   {
     evt.preventDefault();
    let cartid = window.localStorage.getItem('cartId');
    let coupon = evt.target[0].value; 
    console.log(cartid);
    console.log(coupon);
    this.props.get_discount(cartid, coupon)
  }

  hasData(data){
    if(data){
        return data
    }
    else return []
  }

   componentWillMount(){
        
    }
      
   componentDidMount(){
    this.props.loadCart();
    this.props.loadPrices();
   }

   componentDidUpdate(){
     if(this.props.discount_applied && this.props.calculate_price){
       this.props.loadPrices();
       this.props.changeStatus(false);
     }

     if((this.props.item_removed && this.props.calculate_price) || (this.props.updated_quantity && this.props.calculate_price)){
       this.props.loadCart();
       this.props.loadPrices();
       this.props.changeStatus(false); 
     }
   }

   goToCheckout(){

   }

   showEmptyCart(){
    return <tr><td colspan="4">No items in the cart</td></tr>
   }


    render(){
        return(
            <div id = 'loader'>
                <HeaderProduct/>

                <div class="container-fluid cart-img no-padding cart-main-container">
                    <div class="row no-margin cart-hide"><br />
                        <div class="col-md-10 col-sm-10 col-10 offset-1 no-padding cart-heading-center">
                            <div class="col-md-12 col-sm-12 col-12 no-padding" style={{"margin-left": "-2%"}}><span className="no-padding cart-heading-text">Shopping Cart</span><br /><span className="cart-heading-subtext">Check all your products in your cart before checking out below</span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">&nbsp;</div>
                            <div class="col-md-12 col-sm-12 col-12 no-padding">
                                <div class="col-md-9 col-sm-12 col-12 d-inline-block align-top no-padding" style={{"margin-left": "-2%"}}>
                                    <table class="table table-bordered" style={{"border": "solid 1px #cbcbcb","background": "#ffffff"}}>
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th className="cart-table-header">Product Name</th>
                                            <th className="cart-table-header">Quantity</th>
                                            <th className="cart-table-header">Total Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         {
                                           this.hasData(this.props.cart_get.items).length === 0? this.showEmptyCart(): this.hasData(this.props.cart_get.items).map((product, index) => {
                                               if(this.props.item_images === undefined || this.props.item_images[product.sku] === undefined) return
                                               let url = `${config.IMG_URL}${this.props.item_images[product.sku][0].file}`
                                               return <tr className="cart-tr-border">
                                                        <td className="cart-border">{index + 1}</td>
                                                        <td className="row no-margin cart-border" style={{"border": "none"}}>
                                                            <div class="col-md-3 col-sm-3 col-3 d-inline-block no-padding">
                                                              <img src={url}  width = '100px' height = '116px'/>
                                                            </div>
                                                            <div class="col-md-9 col-sm-9 col-9 d-inline-block align-top" style={{"padding-right": "0%"}}>
                                                               <p className="cart-item-name">{product.name}</p>
                                                               {/* <p className="cart-item-name">{Product_Info.custom_attributes[0].value}</p> */}
                                                               <span className="cart-item-remove" onClick = {event => this.handleRemove(product.item_id)}>REMOVE<i class="fa fa-trash-o cart-remove-btn-padding"></i></span>
                                                            </div>
                                                        </td>
                                                        <td className="cart-border">
                                                           <div class="col-md-4 col-sm-4 col-lg-4 col-4 d-inline-block btn no-padding cart-item-qty">
                                                              <span className="cart-item-inr" onClick= {event=> this.handleQuantity(product.qty - 1, product.item_id)}>-</span>
                                                           </div>
                                                           <div class="col-md-4 col-sm-4 col-lg-4 col-4 d-inline-block btn no-padding cart-item-qty" style={{"background-color": "#eeeeee"}}>
                                                               <span className="cart-item-inr">{product.qty}</span>
                                                           </div>
                                                           <div class="col-md-4 col-sm-4 col-lg-4 col-4 d-inline-block btn no-padding cart-item-qty">
                                                             <span className="cart-item-inr" onClick= {event=> this.handleQuantity(product.qty + 1, product.item_id)}>+</span>
                                                          </div>
                                                        </td>
                                                        <td className="cart-border">&#x20b9; {this.props.bill? this.props.bill.totals.items[index].row_total: product.price}</td>
                                                       </tr>
                                                
                                           }) 
                                         }
                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-md-3 no-padding d-inline-block cart-payment-margin">
                                  <div class="col-md-12 d-inline-block no-padding cart-right-rectangle position-relative">
                                      <div class="col-md-12 col-sm-12 d-inline-block" style={{"border-bottom" : "solid 2px #cbcbcb","padding-top": "2%","padding-bottom": "3%"}}><span className="cart-right-rectangle-text" style={{"letter-spacing":"0px"}}>Final Summary</span></div>
                                  {
                                    this.props.bill.totals? <Bill bill={this.props.bill}/> : ""
                                  }
                                </div>
                                <a href="/checkout" className="col-md-12 col-sm-12 col-12 float-left float-sm-left float-md-left checkout-menu-btn btn no-padding"><span className="checkout-menu-btn-text">Proceed To Checkout </span><span className="cart-btn-line"></span></a>
                                </div>
                                                             
                            </div>
                            <div class="col-md-12 col-sm-12 d-inline-block no-padding cart-hide">
                                <div class="col-md-9 col-sm-12 d-inline-block no-padding" id="coupans" style={{"margin-left": "-2%"}}>
                                    <p className="discountCoupan collapse show" style={{"font-size": "14px","color": "#ffffff"}}>If you have any promotional/discount codes, apply them &nbsp;
                                       <a data-toggle="collapse" data-target=".discountCoupan" role="button" aria-expanded="false" aria-controls="collapseDiscouunt" className="discount-here">here</a>.
                                    </p>

                                    <div className='row collapse discountCoupan' id="collapseDiscount">
                                       <div className = 'col-lg-3 col-md-5 col-sm-6 discount'>Discount Codes</div>
                                       <div className = 'col-lg-8 col-md-6 col-md-6'>
                                       <form onSubmit= {this.handlegetDiscount}>
                                                  <div className = 'row'>
                                                    <div className = 'col-lg-9 col-md-8 input'>
                                                      <input type = 'text' name = 'discount' className = 'discount_val' value = {this.state.value} onChange = {this.handleChange}/>
                                                    </div>
                                                    <div className = 'col-lg-3 col-md-4 button'>
                                                     <button type = 'submit' className = 'btn discount_btn'>Apply</button>
                                                    </div>
                                                  </div>
                                        </form>

                                        <div className="error-msg">
                                          {this.props.discount_applied === true? this.successMessage("Congratulations ! Your promotional code has been successfully applied.") :(this.props.discount_applied === false? this.errorMessage("Invalid Coupon"): "")}
                                        </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div> 
                    <div className="row no-margin d-md-none">
                        <div className="col-12 text-center"><span className="no-padding cart-heading-text">Shopping Cart</span><br /><span className="cart-heading-subtext">Check all your products in your cart before checking out below</span></div>
                        <div className="col-11 mx-auto cart-mobile-container-top">
                            {/* <div className="row no-margin">
                                <div className="col-4 no-padding">
                                  <img src="" />
                                </div>
                                <div className="col-8 no-padding"></div>
                            </div> */}

                            {
                                this.hasData(this.props.cart_get.items).length === 0? this.showEmptyCart(): this.hasData(this.props.cart_get.items).map((product, index) => {
                                    if(this.props.item_images === undefined || this.props.item_images[product.sku] === undefined) return
                                    let url = `${config.IMG_URL}${this.props.item_images[product.sku][0].file}`
                                            return  <div className="row no-margin cart-mobile-top-container-padding">
                                                        <div className="col-4 no-padding">
                                                            <img src={url} width="96px" height= "145px" />
                                                        </div>
                                                        <div className="col-8">
                                                            <div className="col-12 d-inline-block no-padding cart-mobile-items-text">{product.name}</div>
                                                            <div className="col-12 d-inline-block no-padding cart-mobile-price">&#x20b9; {this.props.bill? this.props.bill.totals.items[index].row_total: product.price}</div>
                                                            <div className="col-12 d-inline-block no-padding cart-mobile-qty-padding">
                                                                <div className="col-8 d-inline-block no-padding">
                                                                    <div className="col-4 d-inline-block cart-item-qty">
                                                                        <span className="cart-item-inr" onClick= {event=> this.handleQuantity(product.qty - 1, product.item_id)}>-</span>
                                                                    </div>
                                                                    <div className="col-4 d-inline-block cart-item-qty" style={{"background-color": "#eeeeee"}}>
                                                                        <span className="cart-item-inr">{product.qty}</span>
                                                                    </div>
                                                                    <div className="col-4 d-inline-block cart-item-qty">
                                                                        <span className="cart-item-inr" onClick= {event=> this.handleQuantity(product.qty + 1, product.item_id)}>+</span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-4 d-inline-block cart-item-remove text-center align-middle" onClick = {event => this.handleRemove(product.item_id)}>
                                                                     <i class="fa fa-trash-o fa-2x"></i><br />REMOVE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    
                                }) 
                            }
                        </div>
                        <button type="button" className="btn col-11 mx-auto d-block cart-mobile-container-top-collapse-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Apply Discount
                        </button>
                        {/* <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Button with data-target
                        </button> */}

                        <div class="collapse" id="collapseExample">
                            <div class="card col-11 mx-auto card-body cart-collapse-bg">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                            </div>
                        </div>
                        
                        <div className="col-11 mx-auto cart-mobile-container-top-collapse">
                            <div className="col-12 d-inline-block no-padding cart-mobile-amt-padding">
                                <div className="col-8 d-inline-block cart-mobile-amt-text no-padding">Payable Amount</div>
                                <div className="col-4 d-inline-block text-right cart-mobile-amt-text no-padding">&#x20b9;{this.props.bill.totals.grand_total}</div>
                            </div>
                        </div>
                        <a href="/checkout" className="btn col-11 mx-auto cart-mobile-btn">PROCEED TO CHECKOUT <span className="btn-line"></span></a>
                    </div>
                </div>

            </div>
        )
    }
   }

Cart.defaultProps = {
    cart_get : {
      items: []       
    },
    bill: {
      totals: {
        grand_total: 0,
        subtotal: 0,
        shipping_amount: 0
      }
    }
}

export default Cart