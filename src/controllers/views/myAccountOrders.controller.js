import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import '../../css/myaccount.css'
import ScrollMagic from 'scrollmagic'
import OrderComponent from '../components/orderComponent.controller';

import accountRight from '../../img/account_dashboard.png'

class AccountOrders extends Component{

    constructor(props){
        super(props);
        this.state = {
            orderDetails: null,
            pendingOrders: null,
            completedOrders: null
        }
    }

    componentDidUpdate() {
        if (this.props.ordersData) {
            const pendingOrders = [];
            const completedOrders = [];
            for (let i = 0; i < this.props.ordersData.items.length; i++) {
                console.log(this.props.ordersData.items[i]);
                if (this.props.ordersData.items[i].status === "processing") {
                    pendingOrders.push(this.props.ordersData.items[i])
                } else {
                    completedOrders.push(this.props.ordersData.items[i])
                }
            }
            if (!this.state.orderDetails) {
                this.setState({orderDetails: this.props.ordersData});
                this.setState({pendingOrders});
                this.setState({completedOrders});
            }
        }
    }
    
   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();
       if (!window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/login');
           // return <Redirect to="localhost:3000/login"/>
       }

       if (!this.props.ordersData) {
           this.props.getOrders();
       }

    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){
        let pendingOrders = [], completedOrder = [];
        if (this.state.pendingOrders) {
            pendingOrders = this.state.pendingOrders.map(order => {
                return <OrderComponent
                    orderDate={order.updated_at}
                    personName={`${order.customer_firstname} ${order.customer_lastname}`}
                    orderAmount={order.grand_total}
                    orderStatus={order.status}
                    orderNumber={order.entity_id} />
            })
        }
        if (this.state.completedOrders) {
            completedOrder = this.state.completedOrders.map(order => {
                return <OrderComponent
                    orderDate={order.updated_at}
                    personName={`${order.customer_firstname} ${order.customer_lastname}`}
                    orderAmount={order.grand_total}
                    orderStatus={order.status}
                    orderNumber={order.entity_id} />
            })
        }
        return(
            <div>
                <HeaderProduct />


                <div className="main-container row no-margin account-mobile-none">
                    <div className="col-md-2 col-sm-2 col-12 d-md-inline account-bg-img no-padding position-fixed">
                        {/* <img src={accountRight} /> */}
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding absolute">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-10 col-sm-10 mx-auto col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountDashboard" className="account-img-text">Account Dashboard</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountInfo" className="account-img-text">Account Information</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountAddress" className="account-img-text">Address Book</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountOrders" className="account-img-text-active">—  My Orders</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/wishlist" className="account-img-text">Wishlist</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <span onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-img-text">Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-9 col-12 d-inline-block no-padding account-content-margin">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Hello Bruce Banner,</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-text">Here you can find all your existing and previously placed order</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <div className="col-md-3 col-sm-3 col-12 d-inline-block no-padding">
                                <span className="account-name font-14">ORDERS IN PROGRESS ({pendingOrders.length})</span>
                            </div>
                            <div className="col-md-9 col-sm-9 col-12 d-inline-block account-order-border no-padding">
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {pendingOrders}
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <div className="col-md-3 col-sm-3 col-12 d-inline-block no-padding">
                                <span className="account-name font-14">MY PREVIOUS ORDERS ({completedOrder.length})</span>
                            </div>
                            <div className="col-md-9 col-sm-9 col-12 d-inline-block account-order-border no-padding">
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {completedOrder}
                        </div>
                    </div>
                </div>

                <div className="account-bg row no-margin main-container account-web-none">
                    <div className="col-12">
                        <button type="button" className="btn col-12 mx-auto d-block account-mobile-container-top-collapse-btn" type="button" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">
                            Account Information<br /><span className="account-mobile-collapse-text">(Click here for more account settings)</span>
                        </button>
                        {/* <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Button with data-target
                        </button> */}

                        <div class="collapse" id="collapseAccount">
                            <div class="card col-12 mx-auto card-body account-collapse-bg">
                                <ul className="account-ul no-padding">
                                    <li><a href="/accountDashboard" className="account-mobile-li-text">Account Dashboard</a></li>
                                    <li><a href="/accountInfo" className="account-mobile-li-text">Account Information</a></li>
                                    <li><a href="/accountAddress" className="account-mobile-li-text">Address Book</a></li>
                                    <li><a href="/accountOrders" className="account-mobile-li-text">My Orders</a></li>
                                    <li><a href="/wishlist" className="account-mobile-li-text">Wishlist</a></li>
                                    <li onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-mobile-li-text">Logout</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 d-inline-block account-bg-color">
                            <div className="col-12 d-inline-block no-padding account-name">Hello Bruce Banner,</div>
                            <div className="col-12 d-inline-block no-padding account-text">Here you can find all your existing and previously placed order</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                <div className="col-md-3 col-sm-3 col-12 d-inline-block no-padding">
                                    <span className="account-name font-14">ORDERS IN PROGRESS ({pendingOrders.length})</span>
                                </div>
                                <div className="col-md-9 col-sm-9 col-12 d-inline-block account-order-border no-padding">
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                {pendingOrders}
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                <div className="col-md-3 col-sm-3 col-12 d-inline-block no-padding">
                                    <span className="account-name font-14">MY PREVIOUS ORDERS ({completedOrder.length})</span>
                                </div>
                                <div className="col-md-9 col-sm-9 col-12 d-inline-block account-order-border no-padding">
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                {completedOrder}
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default AccountOrders