import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import '../../css/myaccount.css'
import ScrollMagic from 'scrollmagic'

import accountRight from '../../img/account_dashboard.png'

class AccountInfo extends Component{

    constructor(props){
        super(props);
        this.state = {
            editName: false,
            editEmail: false,
            dataReceived: true,
            fname: '',
            email: '',
            lname: '',
            errors: {
                fnameError: null,
                lnameError: null,
                emailError: null,
            }
        }

    }


    handleChangeFname = (event) => {
        this.setState({fname: event.target.value});
    }
    handleChangeLname = (event) => {
        this.setState({lname: event.target.value});
    }
    handleChangeEmail = (event) => {
        this.setState({email: event.target.value});
    }

    validateAllInputs = () => {
        const errors = {

            fnameError: null,
            lnameError: null,
            emailError: null,
        };

        errors.fnameError = this.validateFirstName(this.state.fname);
        errors.lnameError = this.validateFirstName(this.state.lname);
        errors.emailError = this.validateEmail(this.state.email);

        this.setState({errors})
    };


    componentDidUpdate() {
        if (this.props.userData && this.state.dataReceived) {
            this.setState({fname: this.props.userData.firstname});
            this.setState({lname: this.props.userData.lastname});
            this.setState({email: this.props.userData.email});
            this.setState({dataReceived: false})
        }
        if (this.props.isInfoUpdated === false) {
            alert('info could not be updated');
            window.location.reload();
        } else if (this.props.isInfoUpdated === true) {
            alert('info updated');
            window.location.reload();
        }

    }
    componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

       if (!window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/login');
           // return <Redirect to="localhost:3000/login"/>
       }

       this.props.getUserData();

    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }


    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }

            return null;
        }

        return 'Email address cannot be empty';

    };

    validateFirstName = (sFname) => {
        if (sFname) {

            if(sFname.toString().length < 2) {
                // alert("Invalid email address");
                return 'Name cannot be shorter than 2 characters';
            }

            return null;
        }
        return 'Name cannot be empty';

    };

    handleSubmit_update = () => {
        this.validateAllInputs();
        if (this.state.errors.emailError || this.state.errors.fnameError || this.state.errors.lnameError) {
            return;
        }

        const originalEmail = this.props.userData ? this.props.userData.email : null;
        const originalFname = this.props.userData ? this.props.userData.firstname : null;
        const originalLname = this.props.userData ? this.props.userData.lastname : null;

        let data = {
            email: this.state.editEmail === true ? this.state.email : originalEmail,
            fname: this.state.editName === true ? this.state.fname : originalFname,
            lname: this.state.editName === true ? this.state.lname : originalLname,
        };

        this.props.updateCustomerInfo(data);
    }


    render(){
        const style = {display: 'inline',
            background: 'transparent',
            border: 'none',
            color: 'blue',
            textDecoration: 'underline',
            cursor: 'pointer'};
        return(
            <div>
                <HeaderProduct />

                
                <div className="main-container row no-margin account-mobile-none">
                    <div className="col-md-2 col-sm-2 col-12 d-inline-block account-bg-img no-padding position-fixed">
                        {/* <img src={accountRight} /> */}
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding absolute">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-10 col-sm-10 mx-auto col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountDashboard" className="account-img-text">Account Dashboard</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountInfo" className="account-img-text-active">—  Account Information</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountAddress" className="account-img-text">Address Book</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountOrders" className="account-img-text">My Orders</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/wishlist" className="account-img-text">Wishlist</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <span onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-img-text">Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-9 col-12 d-inline-block no-padding account-content-margin">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Hello{this.props.userData ? ` ${this.props.userData.firstname} ${this.props.userData.lastname},` : ','}</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-text">From Account Information you can add personal details about you and adjust emailer settings</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block padding-left">
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <span className="account-label-text">First Name*:</span>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <input onChange={this.handleChangeFname} disabled={!this.state.editName} value={this.state.fname} type="text" className="form-control account-input-box account-input-text" placeholder="Bruce" required />
                                            {this.state.errors.fnameError ? <p style={{color: 'red'}}>First name should be more than 2 characters</p> : null}
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block padding-right">
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <span className="account-label-text">Last Name*:</span>
                                            <button className="btn btn-link btn-edit" onClick={() => {this.setState({editName: true})}} >Edit</button>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <input onChange={this.handleChangeLname} disabled={!this.state.editName} value={this.state.lname} type="text" className="form-control account-input-box account-input-text" placeholder="Bruce" required />
                                            {this.state.errors.lnameError ? <p style={{color: 'red'}}>Last name should be more than 2 characters</p> : null}

                                        </div>
                                        </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                        <span className="account-label-text">Email Address*:</span>
                                        <button className="btn btn-link btn-edit" onClick={() => {this.setState({editEmail: true})}}>Edit</button>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                        <input onChange={this.handleChangeEmail} value={this.state.email} disabled={!this.state.editEmail} type="email" className="form-control account-input-box account-input-text" placeholder="brucebanner@gmail.com" required />
                                        {this.state.errors.emailError ? <p style={{color: 'red'}}>{this.state.errors.emailError}</p> : null}

                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                        <span className="account-label-text">Change Password:</span>
                                        <button className="btn btn-link btn-edit" onClick={() => this.props.history.push('/newPassword')}>Edit</button>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                        <input disabled={true} type="password" className="form-control account-input-box account-input-text" value={'***********'} required />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                                    <input class="form-check-input" type="checkbox" checked  /> <span className="register-checkbox-text">Keep me in your list, I want to know about your upcoming campaigns and stuff</span>                                  
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                                    <input class="form-check-input" type="checkbox" /> <span className="register-checkbox-text">Text me updates about my order! And yes, I agree to these terms.</span>                                    
                                </div> 
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <button  onClick={this.handleSubmit_update} class="col-md-2 col-sm-2 btn account-submit-btn" ><span className="account-submit-btn-text">UPDATE PROFILE</span><span className="account-btn-line"></span></button>
                                </div>                                                                                   
                            </div>
                        </div>
                    </div>                                                                                                       
                </div>
                <div className="account-bg row no-margin main-container account-web-none">
                    <div className="col-12">
                        <button type="button" className="btn col-12 mx-auto d-block account-mobile-container-top-collapse-btn" type="button" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">
                            Account Information<br /><span className="account-mobile-collapse-text">(Click here for more account settings)</span>
                        </button>
                        {/* <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Button with data-target
                        </button> */}

                        <div class="collapse" id="collapseAccount">
                            <div class="card col-12 mx-auto card-body account-collapse-bg">
                                <ul className="account-ul no-padding">
                                    <li><a href="/accountDashboard" className="account-mobile-li-text">Account Dashboard</a></li>
                                    <li><a href="/accountInfo" className="account-mobile-li-text">Account Information</a></li>
                                    <li><a href="/accountAddress" className="account-mobile-li-text">Address Book</a></li>
                                    <li><a href="/accountOrders" className="account-mobile-li-text">My Orders</a></li>
                                    <li><a href="/wishlist" className="account-mobile-li-text">Wishlist</a></li>
                                    <li onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-mobile-li-text">Logout</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 d-inline-block account-bg-color">
                            <div className="col-12 d-inline-block no-padding account-name">Hello{this.props.userData ? ` ${this.props.userData.firstname} ${this.props.userData.lastname},` : ','}</div>
                            <div className="col-12 d-inline-block no-padding account-text">From Account Information you can add personal details about you and adjust emailer settings</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                        <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                                <span className="account-label-text">First Name*:</span>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                                <input onChange={this.handleChangeFname} disabled={!this.state.editName} value={this.state.fname} type="text" className="form-control account-input-box account-input-text" placeholder="Bruce" required />
                                                {this.state.errors.fnameError ? <p style={{color: 'red'}}>First name should be more than 2 characters</p> : null}
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                                <span className="account-label-text">Last Name*:</span>
                                                <button className="btn btn-link btn-edit" onClick={() => {this.setState({editName: true})}} >Edit</button>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                                <input onChange={this.handleChangeLname} disabled={!this.state.editName} value={this.state.lname} type="text" className="form-control account-input-box account-input-text" placeholder="Bruce" required />
                                                {this.state.errors.lnameError ? <p style={{color: 'red'}}>Last name should be more than 2 characters</p> : null}

                                            </div>
                                            </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <span className="account-label-text">Email Address*:</span>
                                            <button className="btn btn-link btn-edit" onClick={() => {this.setState({editEmail: true})}}>Edit</button>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <input onChange={this.handleChangeEmail} value={this.state.email} disabled={!this.state.editEmail} type="email" className="form-control account-input-box account-input-text" placeholder="brucebanner@gmail.com" required />
                                            {this.state.errors.emailError ? <p style={{color: 'red'}}>{this.state.errors.emailError}</p> : null}

                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <span className="account-label-text">Change Password:</span>
                                            <button className="btn btn-link btn-edit" onClick={() => this.props.history.push('/newPassword')}>Edit</button>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                            <input disabled={true} type="password" className="form-control account-input-box account-input-text" value={'***********'} required />
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                     
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                                        <input class="form-check-input" type="checkbox" checked  /> <span className="register-checkbox-text">Keep me in your list, I want to know about your upcoming campaigns and stuff</span>                                  
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                                        <input class="form-check-input" type="checkbox" /> <span className="register-checkbox-text">Text me updates about my order! And yes, I agree to these terms.</span>                                    
                                    </div> 
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div> 
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                        <button  onClick={this.handleSubmit_update} class="col-md-2 col-sm-2 btn account-submit-btn" ><span className="account-submit-btn-text">UPDATE PROFILE</span><span className="account-btn-line"></span></button>
                                    </div>                                                                                   
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default AccountInfo