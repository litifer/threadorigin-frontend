import React from 'react';
import './../../../css/lookbooks/lookbox.css';
import lookbooksp1 from "../../../img/lookbook/lookbooksp1.png";
import lookbooksp2 from "../../../img/lookbook/lookbooksp2.png";
import lookbooksp3 from "../../../img/lookbook/lookbooksp3.png";
import lookbooksp4 from "../../../img/lookbook/lookbooksp4.png";
import lookbooksp5 from "../../../img/lookbook/look3cover4.png";
import lookbooksp6 from "../../../img/lookbook/look4cover.png";



const lookBookbox= (props) => {
    //tnc-header-img
    return (
        <div className="lbmain">
            <div className="row">

            <div className="col-md-3 col-lg-3 col-sm-12 col-12">
                <div className="d-sm-block d-md-none">
                         <img src={lookbooksp1}  className="img-profile-lm rounded"/>
                         <div className="text-below-img-profile-m"> By Jennifer Connelley </div>
                    </div>
                </div>
                <div className="col-md-9 col-lg-9 col-sm-12 col-12">
                    <h1 className="lookboxH">Summer is Back | 2018 June Lookbook </h1> 
                    <p className='lookboxP'> As for the collection itself, the 72 looks comprised of dramatic tulle ballgowns, sexy patent leather, sparkling crystal-encrusted dresses, unexpected headpieces, and so much more
                    </p>
                    <p className="lookboxP1"> Products Used: Sasha BodyCon Dress , Sasha BodyCon Dress , Sasha BodyCon Dress , Sasha BodyCon Dress
                    </p>
                </div>
                <div className="col-md-3 col-lg-3 col-sm-12 col-12">
                <div className="d-none d-md-block">
                         <img src={lookbooksp1}  className="img-profile-l rounded"/>
                         <div className="text-below-img-profile"> By Jennifer Connelley </div>
                    </div>
                </div>
            </div>
            <div className="row class1">
                <div className="col-lg-3 col-md-3 no-padding"> 
                    <img src={lookbooksp3} className="lookbooki"/>
                    <div className="row">
                    <div className="col-lg-8 col-md-8">
                        <p className="imagebelow"> Robe Chemise Harvard </p>
                        <div className="d-none d-md-block">
                            <div className="row new-rule">
                                <div className="s-l-box"> </div>
                                <div className="s-l-box1"> </div>    
                            </div>
                        </div>
                    </div>
                        <div className="col-lg-4 col-md-4">
                        <p className="imagebelow2"> Rs. 2499 </p>
                    </div>
                    </div>
                </div>
                
                <div className="col-lg-3 col-md-3 no-padding">
                     <img src={lookbooksp2} className="lookbooki"/>
                     <div className="row">
                     <div className="col-lg-8 col-md-8">
                        <p className="imagebelow"> Robe Chemise Harvard </p>
                        <div className="d-none d-md-block">
                            <div className="row new-rule">
                                <div className="s-l-box"> </div>
                                <div className="s-l-box1"> </div>    
                            </div>
                        </div>
                    </div>
                        <div className="col-lg-4 col-md-4">
                        <p className="imagebelow2"> Rs. 2499 </p>
                    </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-3 no-padding">
                <img src={lookbooksp5} className="lookbooki"/>
                <div className="row">
                    <div className="col-lg-8 col-md-8">
                        <p className="imagebelow"> Robe Chemise Harvard </p>
                        <p className="look-underline"> Out of Stock </p>
                        </div>
                        <div className="col-lg-4 col-md-4">
                        <p className="imagebelow2"> Rs. 2499 </p>
                    </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-3 no-padding">
                    <img src={lookbooksp6} className="lookbooki"/>
                    <div className="row">
                    <div className="col-lg-8 col-md-8">
                   
                        <p className="imagebelow"> Robe Chemise Harvard </p>
                        <div className="d-none d-md-block">
                            <div className="row new-rule">
                                <div className="s-l-box"> </div>
                                <div className="s-l-box1"> </div>    
                            </div>
                        </div>
                    </div>
                        <div className="col-lg-4 col-md-4">
                        <p className="imagebelow2"> Rs. 2499 </p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        

    )
}

export default lookBookbox;