import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import HeaderProduct from './../../components/header_product.controller';
import Footer from '../../components/footer.controller';
import MainImage from './mainImage.js'
import './../../../css/lookbooks/lookbooks.css';
import LookbooksBox1 from './lookbooksBox1';
import LookbooksBox2 from './lookbooksBox2';
import LookbooksBox3 from './lookbooksBox3';
import LookbooksBox4 from './lookbooksBox4';
import {getLookbooks, getColors, getProductDetails, getSizes} from "../../../actions/campignLooks.actions";
import CampignLooks from "../campignLooks.controller";

class lookbooks extends Component {

    state = {
        productsGotten: false
    };


    componentDidMount() {
        this.props.getLookbooks();
        this.props.getColors();
        this.props.getSizes();
    }

    componentDidUpdate() {
        if (this.props.lookbookFullData && !this.state.productsGotten) {
            for (let i = 0; i < this.props.lookbookFullData.length; i++) {
                // console.log(this.props.lookbookData[i].product1.sku);
                // console.log(this.props.lookbookData[i].product2.sku);
                for (let j = 0; j < this.props.lookbookFullData[i].products.length; j++) {
                    // if (!this.props.productDetails[this.props.lookbookFullData[i].products[j].sku]) {
                       this.props.getProductDetails(this.props.lookbookFullData[i].products[j].sku);


                }
                // this.props.getProductDetails(this.props.lookbookData[i].product1.sku);
                // this.props.getProductDetails(this.props.lookbookData[i].product2.sku);
            }
            this.setState({productsGotten: true});
        }

        if (this.props.productDetails) {
            console.log(this.props.productDetails, "products");
        }
        if (this.props.sizeChart) {
            console.log(this.props.sizeChart, "size chart")
        }
        if (this.props.colorChart) {
            console.log(this.props.colorChart, "color chart")
        }
    }

    render () {
        return (
            <div> 
            <HeaderProduct />
            <MainImage />
                {this.props.lookbookFullData ? this.props.lookbookFullData.map(lookbook => {
                    return (<div className={'lookbooks2'}>
                        <LookbooksBox1
                            description={lookbook.description}
                            products={lookbook.products}
                            productData={this.props.productDetails}
                            designer={lookbook.designer} />
                    </div>);
                }) : null}
            <div className="d-none d-md-block">
            <button type="button" class="btn btn-default btn-b-l">View more</button>
            </div>
            <div className="d-sm-block d-md-none">
            <button type="button" class="btn btn-default btn-b-l-m">View more</button>
            </div>
            <Footer />
            </div>
        )
    }
}


const mapStateToProps = state => ({
    lookbookFullData: state.lookbook.lookbookFullData,
    sizeChart: state.lookbook.sizeChart,
    colorChart: state.lookbook.colorChart,
    productDetails: state.lookbook.productDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getLookbooks,
    getColors,
    getSizes,
    getProductDetails
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(lookbooks)
