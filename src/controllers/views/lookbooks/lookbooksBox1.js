import React from 'react';
import './../../../css/lookbooks/lookbox.css';
import {IMG_URL} from './../../../config'
import {withRouter} from 'react-router-dom'
import lookbooksp1 from "../../../img/lookbook/lookbooksp1.png";
import lookbooksp2 from "../../../img/lookbook/lookbooksp2.png";
import lookbooksp3 from "../../../img/lookbook/lookbooksp3.png";
import lookbooksp4 from "../../../img/lookbook/lookbooksp4.png";



const lookBookbox= (props) => {
    //tnc-header-img
    return (
        <div className="lbmain">
            <div className="row">
                <div className="col-md-9 col-lg-9 col-sm-12 col-12">
                    <h1 className="lookboxH"> {props.description.name} </h1>
                    <p className='lookboxP'> {props.description.mainDesc}
                    </p>
                    <p className="lookboxP1"> Products Used: <span className="text-dec-l">Sasha BodyCon Dress , Sasha BodyCon Dress , Sasha BodyCon Dress , Sasha BodyCon Dress </span>
                    </p>
                </div>
                
                <div className="col-md-3 col-lg-3 col-sm-12 col-12">
                    <div className="d-none d-md-block">
                         <img src={IMG_URL + props.designer.image}  className="img-profile-l rounded"/>
                         <div className="text-below-img-profile"> By {props.designer.name}</div>
                    </div>
                </div>
            </div>
            <div className="row class1">

                {props.products ? props.products.map(product => {
                    let lookBox = null;
                    if (props.productData && props.productData[product.sku]) {
                        lookBox = <div onClick={() => props.history.push(`/product?id=${product.sku}`)} className="col-lg-3 col-md-3 no-padding">
                            <img src={IMG_URL + product.imgSmall} className="lookbooki"/>
                            <div className="row">
                                <div className="col-lg-8 col-md-8">
                                    <p className="imagebelow"> {props.productData[product.sku].name} </p>
                                    <div className="d-none d-md-block">
                                        <div className="row new-rule">
                                            <div className="s-l-box"></div>
                                            <div className="s-l-box1"></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <p className="imagebelow2"> Rs. {props.productData[product.sku].price} </p>
                                </div>
                            </div>
                        </div>;
                    }
                    return lookBox;
                }) : null}

                {/*<div className="col-lg-3 col-md-3 no-padding">*/}
                    {/*<img src={lookbooksp1} className="lookbooki"/>*/}
                    {/*<div className="row">*/}
                    {/*<div className="col-lg-8 col-md-8">*/}
                        {/*<p className="imagebelow"> Robe Chemise Harvard </p>*/}
                        {/*<div className="d-none d-md-block">*/}
                            {/*<div className="row new-rule">*/}
                                {/*<div className="s-l-box"> </div>*/}
                                {/*<div className="s-l-box1"> </div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                        {/*<div className="col-lg-4 col-md-4">*/}
                        {/*<p className="imagebelow2"> Rs. 2499 </p>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}

                {/*<div className="col-lg-3 col-md-3 no-padding">*/}
                     {/*<img src={lookbooksp2} className="lookbooki"/>*/}
                     {/*<div className="row">*/}
                     {/*<div className="col-lg-8 col-md-8">*/}
                        {/*<p className="imagebelow"> Robe Chemise Harvard </p>*/}
                        {/*<div className="d-none d-md-block">*/}
                            {/*<div className="row new-rule">*/}
                                {/*<div className="s-l-box"> </div>*/}
                                {/*<div className="s-l-box1"> </div>    */}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                        {/*<div className="col-lg-4 col-md-4">*/}
                        {/*<p className="imagebelow2"> Rs. 2499 </p>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="col-lg-3 col-md-3 no-padding">*/}
                {/*<img src={lookbooksp3} className="lookbooki"/>*/}
                {/*<div className="row">*/}
                    {/*<div className="col-lg-8 col-md-8">*/}
                        {/*<p className="imagebelow"> Robe Chemise Harvard </p>*/}
                        {/*<p className="look-underline"> Out of Stock </p>*/}
                        {/*</div>*/}
                        {/*<div className="col-lg-4 col-md-4">*/}
                        {/*<p className="imagebelow2"> Rs. 2499 </p>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="col-lg-3 col-md-3 no-padding">*/}
                    {/*<img src={lookbooksp4} className="lookbooki"/>*/}
                    {/*<div className="row">*/}
                    {/*<div className="col-lg-8 col-md-8">*/}
                        {/*<p className="imagebelow"> Robe Chemise Harvard </p>*/}
                        {/*<div className="d-none d-md-block">*/}
                            {/*<div className="row new-rule">*/}
                                {/*<div className="s-l-box"> </div>*/}
                                {/*<div className="s-l-box1"> </div>    */}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                        {/*<div className="col-lg-4 col-md-4">*/}
                        {/*<p className="imagebelow2"> Rs. 2499 </p>*/}
                    {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
            </div>
        </div>
        

    )
}

export default withRouter(lookBookbox);