import React from 'react'
import ImageBelowHeader from "../../../img/pandc/1.png";
import './../../../css/aboutus/vision.css';

const vision = (props) => {
    return (
    <div>
            <div className="row cus-v-lr">
                <div className="col-md-8 col-lg-8"> 
                    <h1 className="v-heading"> Vision </h1>
                    <div className="vision-line"> </div>

                    <p className="v-para"> THREAD ORIGIN is born out of love for craft, culture, fashion and a connect. It is an inclusive platform, for all fashion dreams to be turned into reality.                     
                    </p>

                    <p className="v-para"> A platform, which weaves a connect with one another by the thread. Our belief, that each one of us is a unique thread, woven into the beautiful fabric of our collective consciousness.
                    </p>
              </div>

              <div className="col-md-4 col-lg-4 no-padding">
                  <img src={ImageBelowHeader} className="img-responsive image-vision-class"/>
              </div>
            </div>
        <div className="vision-line-container-bottom"> </div>
    </div>
    )
}

export default vision 