import React from 'react'
import './../../../css/aboutus/ourjourney.css';
const ourjourney = (props) => {
    return (
    <div className="cus-j-c">
        <div className="container ">
            <div clasName="ourjourney"> 
                <h1 className="text-center o-j-h-1"> Our Journey </h1>
                   <div className="ourjourney-line"> </div>
               
                <p className="text-center o-j-p-1"> The journey of Thread Origin began with the driving force; the founder’s strong vision. She envisioned women as strong, independent and believed that every woman should celebrate her personality with the clothes she wears and that is the woman, the world looks upto. She lived and dedicated her life to this vision and Thread Origin glorifies her way of life, the philosophy that is a gift to womanhood. 
                </p>
                
                <p className="text-center o-j-p-1">At Thread Origin, following the vision, we celebrate women in their true individual style and bring fashion that takes them to the ultimate goal of empowerment.
                </p>

                <div className="col-md-12 col-lg-12 col-sm-12 col-12 cus-o-j-i">
                </div>
            </div>
         </div>
    </div>

    )
}

export default ourjourney;