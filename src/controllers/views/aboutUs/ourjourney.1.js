import React from 'react'
import './../../../css/aboutus/ourjourney.css';
import './../../../css/aboutus/ourjourney1.css';

const ourjourney1 = (props) => {
    return (
        <div className="cus-j-c">
        <h1 className="text-center o-j-h-1"> Our Journey </h1>
        <div className="ourjourney-line"> </div>
            <p className="text-center o-j-p-1"> The platform is deeply rooted and developed for the best personalized customer experience and to enhance it.  The platform has put in months of research and expertise to present ahead of time technology which has bridged the gap between online and offline shopping with chat bot assistance for creating preferences.</p>
            <p className="text-center o-j-p-1"> Thread Origin, prioritizes customer experience and brings forward curated looks by designers from all across India.  An amalgamation of styles, both from young and celebrated designers is what sets Thread Origin apart. We are honored to present to you the young and very talented Mrinali Sharma, Shraddha Pawar, Kanika Nair, Rashmi Sah and the very celebrated Tah-weave, Ka-sha, House of Sohn, Mati, Paio, Chambray and Company, The Pot Plant Clothing, Awadh, Manika and many more.</p>
            <p className="text-center o-j-p-1 o1-j-p-1"> Offering quality, trend driven styles with an enhanced shopping experience, we open doors to exclusive designer wear for all.</p>
     </div>
    )
}

export default ourjourney1;