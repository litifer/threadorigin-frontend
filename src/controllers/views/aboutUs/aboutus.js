import React, {Component} from "react";
import ScrollReveal from 'scrollreveal'
import ScrollMagic from 'scrollmagic'
import Header from "../../components/header.controller.js";
//import Landing from "../../components/landing.controller.js";
import Footer from '../../components/footer.controller';
import OurJourney from './ourJourney.js';
import OurJourney1 from './ourjourney.1.js';
import Vision from "./vision.js";
import './../../../css/aboutus/aboutus.css';
import ImageBelowHeader from "../../../img/pandc/1.png";


class aboutUs extends Component {

    constructor(props){
        super(props)
        let newImg = ''
        this.state = {value : ""}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        let imageurl = "../../img/item1.png"
        this.state = {sr : ScrollReveal()}
    }

    handleChange(event) {
      this.setState({value: event.target.value});
    }

    handleSubmit(event) {
      alert('An email was submitted: ' + this.state.value);
      event.preventDefault();
      this.props.subscribe_email(this.state.value);
    }

    componentDidCatch(){
        console.log("Error here")
    }

    componentDidMount(){
        

    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

    new ScrollMagic.Scene({triggerElement:"#home-footer-visibility-start"})
        .triggerHook("onEnter")
        .setClassToggle("#home-footer-animation-block","home-footer-visible")
        .addTo(scrollfooterController)

    let scrollMagicController = new ScrollMagic.Controller();

}

    render () {
        return (
                <div>
                    <div className="aboutusc">
                    <Header />
                    <p className="p-a-c"> About Us </p>
                    <div className="l-a-u"> </div>
                </div>
                <Vision />
                <OurJourney />
                <OurJourney1 />


         
             
             {/* footer */}

                <div className="container-fluid no-padding">
                   <div className="col-md-12 col-sm-12 col-lg-12 float-md-left float-md-left float-sm-left float-lg-left no-padding">
                    <div className="col-md-12 col- val_initial foot_head d-lg-inline-block d-none " >
                        <div className="row col-md-12 val_initial foot_hides newsletter-container">
                            {/* <div className="newsletter-box">
                                <div className="email-form">
                                    <form onSubmit = {this.handleSubmit}>
                                    <input class="email anim_scroll_reveal_sequence_footer" placeholder="Enter Your Email Address" type="email" value = {this.state.value} onChange={this.handleChange}  />
                                    <button type = 'submit' class="btn btn-primary btn-1 anim_scroll_reveal_sequence_footer" >SUMBIT&nbsp;&nbsp;&nbsp;&nbsp;
                                        <img className = 'margin_img_foot' src={line2Img}  alt=""/>
                                    </button>
                                    </form>
                                </div>
                            </div> */}

                            <div className="col-md-12 footer-newsletter-text">Join our Newsletter</div>
                            <div className="col-md-12 footer-newsletter-subscript-text">Join our newsletter to receive monthly offeres, updates about our designers and lots of other updates</div>
                            <div className="mx-auto email-form-padding">
                                <div className="email-form">
                                    <form onSubmit = {this.handleSubmit}>
                                    <input class="email anim_scroll_reveal_sequence_footer" placeholder="Enter Your Email Address" type="email" value = {this.state.value} onChange={this.handleChange}  />
                                    <button type = 'submit' class=" btn footer-submit-btn" >SUMBIT<span className='btn-line'></span>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 

                        <div id="home-footer-animation-block" class="col-md-12"> 
                            <div class="col-md-12 footer_bg_col footer_ht foot_hide"></div>
                            <Footer/>
                            <div class="col-md-12 col-xs-12 footer_bg2">
                            <p className = 'footer_end' >All Rights Reserved | 2018</p>
                            </div>
                        </div>
                    <div id="home-footer-visibility-start"></div>
                   </div>
                </div>
            </div>
        )
    }
}

export default aboutUs;