import React, {Component} from 'react'
import {withRouter} from 'react-router-dom';
import {IMG_URL} from './../../config'
import HeaderProduct from '../components/header_product.controller'
import CampaignLooksLanding from '../components/campignLooksLanding.controller'
import CampaignLooksTopCamp from '../components/campignLooksCampTop.controller'
import CampaignLooksBottomCamp from '../components/campignLooksCampBottom.controller'
import Footer from '../components/footer.controller'
import '../../css/campignLooks.css'


class CampignLooks extends Component{
    constructor(props){
        super(props);
        this.state = {
            imageDataReceived: false
        }
    }

    componentDidUpdate(){
        if (this.props.lookbookData && !this.state.imageDataReceived) {
            for (let i = 0; i < this.props.lookbookData.length; i++) {
                console.log(this.props.lookbookData[i].product1.sku);
                console.log(this.props.lookbookData[i].product2.sku);
                this.props.getProductDetails(this.props.lookbookData[i].product1.sku);
                this.props.getProductDetails(this.props.lookbookData[i].product2.sku);
            }
            this.setState({imageDataReceived: true});
        }

        if (this.props.productDetails) {
            console.log(this.props.productDetails, "products");
        }
        if (this.props.sizeChart) {
            console.log(this.props.sizeChart, "size chart")
        }
        if (this.props.colorChart) {
            console.log(this.props.colorChart, "color chart")
        }
    }

    componentDidMount() {

        this.props.getCampaignLooks();
        this.props.getColors();
        this.props.getSizes();

    }

    render(){
        return(
            <div>
                <HeaderProduct/>
                <CampaignLooksLanding />

                {this.props.lookbookData ? this.props.lookbookData.map((lookbook, index) => {
                    if (index % 2 === 0) {
                        return <CampaignLooksTopCamp onTopPlusButtonClick={() => this.props.history.push('/product?id=' + lookbook.product1.sku)} onBottomPlusButtonClick={() => this.props.history.push('/product?id=' + lookbook.product2.sku)} imgSmall={IMG_URL + lookbook.product2.imgSmall} imgBig={IMG_URL + lookbook.product1.imgBig} />
                    } else {
                        return <CampaignLooksBottomCamp onTopPlusButtonClick={() => this.props.history.push('/product?id=' + lookbook.product1.sku)} onBottomPlusButtonClick={() => this.props.history.push('/product?id=' + lookbook.product2.sku)} prodSmallSku={lookbook.product2.sku} prodBigSku={lookbook.product1.sku} imgSmall={IMG_URL + lookbook.product2.imgSmall} imgBig={IMG_URL + lookbook.product1.imgBig} />
                    }
                }) : null}

                <div className="row no-margin">
                    <button className="btn campignlooks-btn d-inline-block mx-auto">VIEW MORE<span className="campignlooks-btn-line"></span></button>
                </div>

                <div id="product-footer" className="col-md-12 product-footer">
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className="footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div>
            </div>
        )
    }
} 

export default withRouter(CampignLooks)