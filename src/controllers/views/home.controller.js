import React, {Component} from 'react'
import Header from '../components/header.controller';

console.log("7. Home Component")

class Home extends Component{
    render(){
        return(
            
            
            <div>
                <Header/>
                <h1>Home</h1>
                <p>Count: {this.props.count}</p>
    
                <p>
                    <button onClick={this.props.increment} disabled={this.props.isIncrementing}>Increment</button>
                    <button onClick={this.props.incrementAsync} disabled={this.props.isIncrementing}>Increment Async</button>
                </p>
    
                <p>
                    <button onClick={this.props.decrement} disabled={this.props.isDecrementing}>Decrementing</button>
                    <button onClick={this.props.decrementAsync} disabled={this.props.isDecrementing}>Decrement Async</button>
                </p>
    
                <p>
                    <button onClick={() => this.props.changePage()}>Go to about page via redux</button>
                </p>
            </div>
    
        )
    }


}

export default Home