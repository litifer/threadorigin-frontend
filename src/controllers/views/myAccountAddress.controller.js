import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import AddressForm from './../components/checkout/forms/address-form.controller';
import CustomizedAddressForm from './../components/checkout/forms/customizedAddressForm.controller';
import '../../css/myaccount.css'
import { Redirect, withRouter } from 'react-router-dom';
import AddressComp from './../components/address.controller';
import ScrollMagic from 'scrollmagic'

import accountRight from '../../img/account_dashboard.png'

class AccountAddress extends Component{

    constructor(props){
        super(props)
        
        this.state = {
            defaultAddress: null,
            otherAddresses: null,
            showAddressForm: false,
            addressInputData: null,
            showEditAddress: false,
            editAddress: {
                fname: null,
                lname: null,
                street: null,
                pincode: null,
                city: null,
                region: null,
                address2: null,
                phone: null,
                country: null,
                addressId: null
            }
        }
    }

    componentDidUpdate() {

        if (this.props.addressData) {
            const otherAddresses = [];
            for (let i = 0; i < this.props.addressData.addresses.length; i++) {
                console.log(this.props.addressData.addresses[i]);
                if (this.props.addressData.addresses[i].default_billing) {
                    if (!this.state.defaultAddress) {
                        this.setState({defaultAddress: {...this.props.addressData.addresses[i]}});
                    }
                } else {
                    otherAddresses.push(this.props.addressData.addresses[i]);
                }
            }
            if (!this.state.otherAddresses) {
                this.setState({otherAddresses});
            }
        }

        if (this.props.defaultAddressChanged) {
            alert('default address changed');
            window.location.reload();
        }

        if (this.props.defaultAddressAdded === false) {
            alert('address couldn\'t be added');
            window.location.reload();
        } else if (this.props.defaultAddressAdded === true) {
            alert('default address added');
            window.location.reload();
        }

        if (this.props.addressEdited === false) {
            alert('address couldn\'t be edited');
            window.location.reload();
        } else if (this.props.addressEdited === true) {
            alert('address edited');
            window.location.reload();
        }

        if (this.props.addressDeleted === false) {
            alert('address couldn\'t be deleted');
            window.location.reload();
        } else if (this.props.addressDeleted === true) {
            alert('address deleted');
            window.location.reload();
        }

    }

    editButtonPressed = (data) => {
        this.setState({editAddress: data});
        this.setState({showEditAddress: true})
    };

    editAddressForm = (rawFormData) => {

        let address = {
            "country": "IN",
            "street": rawFormData[2].value + ', ' + rawFormData[3].value + ', ' + rawFormData[6].value,
            "telephone": rawFormData[7].value,
            "postcode": rawFormData[4].value,
            "city": rawFormData[5].value,
            "firstname": rawFormData[0].value,
            "lastname": rawFormData[1].value,
            "addressId": this.state.editAddress.addressId
        };
        console.log(address);
        this.props.editExistingAddress(address);
    };

    onChangeLnameHander = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, lname: evt.target.value}});
    };
//     onChangeFname={this.onChangeFnameHandler}
// onChangeAddress={this.onChangeAddressHandler}
// onChangeAddress2={this.onChangeAddress2Handler}
// onChangePincode={this.onChangePincodeHandler}
// onChangeCity={this.onChangeCityHandler}
// onChangeRegion={this.onChangeRegionHandler}
// onChangePhone={this.onChangePhoneHandler}
// onChangeCountry={this.onChangeCountryHandler}
    onChangeFnameHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, fname: evt.target.value}});
    };
    onChangeAddressHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, street: evt.target.value}});
    };
    onChangeAddress2Handler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, address2: evt.target.value}});
    };
    onChangePincodeHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, pincode: evt.target.value}});
    };
    onChangeCityHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, city: evt.target.value}});
    };
    onChangeRegionHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, region: evt.target.value}});
    };
    onChangePhoneHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, phone: evt.target.value}});
    };
    onChangeCountryHandler = (evt) => {
        this.setState({editAddress: {...this.state.editAddress, country: evt.target.value}});
    };



    getAddressFromForm = (rawFormData) => {

        let address = {
            "country": "IN",
            "street": rawFormData[3].value + ' ' + rawFormData[4].value,
            "telephone": rawFormData[8].value,
            "postcode": rawFormData[5].value,
            "city": rawFormData[6].value,
            "firstname": rawFormData[1].value,
            "lastname": rawFormData[2].value
        };
        this.props.addNewDefaultAddress(address);
    };
    
   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

       console.log(window.localStorage.getItem('authToken') ? 'hello' : 'bye', 'auth token from my account address');

       if (!window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.push('/login');
           // return <Redirect to="localhost:3000/login"/>
       }


       if (!this.props.addressData) {
           this.props.getAddresses();
       }


    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){

        return(
            <div>
                <HeaderProduct />
                <div className="main-container row no-margin account-mobile-none">
                    <div className="col-md-2 col-sm-2 col-12 d-inline-block account-bg-img no-padding position-fixed">
                        {/* <img src={accountRight} /> */}
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding absolute">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-10 col-sm-10 mx-auto col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountDashboard" className="account-img-text">Account Dashboard</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountInfo" className="account-img-text">Account Information</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountAddress" className="account-img-text-active">—  Address Book</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/accountOrders" className="account-img-text">My Orders</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/wishlist" className="account-img-text">Wishlist</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <button onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-img-text btn btn-link">Logout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-9 col-9 d-inline-block no-padding account-content-margin">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Hello {this.state.defaultAddress ? this.state.defaultAddress.firstname + ' ' + this.state.defaultAddress.lastname : null},</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-text">From Account Information you can add personal details about you and adjust emailer settings</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {this.state.showEditAddress ? <button className="col-md-3 btn account-submit-btn" onClick={() => this.setState({showEditAddress: false})}>Hide edit address</button> : null}
                            {this.state.showEditAddress ? <CustomizedAddressForm
                                onChangeLname={this.onChangeLnameHander}
                                onChangeFname={this.onChangeFnameHandler}
                                onChangeAddress={this.onChangeAddressHandler}
                                onChangeAddress2={this.onChangeAddress2Handler}
                                onChangePincode={this.onChangePincodeHandler}
                                onChangeCity={this.onChangeCityHandler}
                                onChangeRegion={this.onChangeRegionHandler}
                                onChangePhone={this.onChangePhoneHandler}
                                onChangeCountry={this.onChangeCountryHandler}
                                returnData={(rawData) => this.editAddressForm(rawData)} formData={this.state.editAddress}/> : null}
                            {this.state.showAddressForm ? <AddressForm returnData={(rawData) => this.getAddressFromForm(rawData)} /> : null}
                            {this.state.defaultAddress ? <AddressComp addressType={'Default Address'}
                                                                      fullName={this.state.defaultAddress.firstname + ' ' + this.state.defaultAddress.lastname}
                                                                      street={this.state.defaultAddress.street}
                                                                      city={this.state.defaultAddress.city}
                                                                      setAsDefault={() => alert('this is the default address')}
                                                                      default={true}
                                                                      onEditButtonClick={() => this.editButtonPressed({
                                                                          fname: this.state.defaultAddress.firstname,
                                                                          lname: this.state.defaultAddress.lastname,
                                                                          street: this.state.defaultAddress.street,
                                                                          pincode: this.state.defaultAddress.postcode,
                                                                          city: this.state.defaultAddress.city,
                                                                          address2: '',
                                                                          phone: this.state.defaultAddress.telephone,
                                                                          country: this.state.defaultAddress.country_id,
                                                                          addressId: this.state.defaultAddress.id

                                                                      })}
                                                                      onDeleteButtonClick={() => this.props.deleteSelectedAddress(this.state.defaultAddress.id)}
                                                                      pinCode={this.state.defaultAddress.postcode}
                                                                      country={this.state.defaultAddress.country_id}
                                                                      phone={this.state.defaultAddress.telephone} /> : null}
                            <div className="col-md-5 col-sm-5 col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <button onClick={() => this.setState({showAddressForm: !this.state.showAddressForm})} class="col-md-8 col-sm-6 btn account-submit-btn float-right" ><span className="account-submit-btn-text">ADD NEW DEFAULT ADDRESS</span><span className="account-btn-line"></span></button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block account-address-border no-padding"></div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-10 col-sm-10 col-12 d-inline-block no-padding">
                            {this.state.otherAddresses ? this.state.otherAddresses.map((otherAddress, ind) => (
                                <AddressComp addressType={'Address ' + (ind + 1)}
                                             fullName={otherAddress.firstname + ' ' + otherAddress.lastname}
                                             street={otherAddress.street}
                                             city={otherAddress.city}
                                             default={false}
                                             setAsDefault={() => this.props.setAddressAsDefault(otherAddress.id)}
                                             onDeleteButtonClick={() => this.props.deleteSelectedAddress(otherAddress.id)}
                                             pinCode={otherAddress.postcode}
                                             onEditButtonClick={() => this.editButtonPressed({
                                                fname: otherAddress.firstname,
                                                lname: otherAddress.lastname,
                                                street: otherAddress.street,
                                                pincode: otherAddress.postcode,
                                                city: otherAddress.city,
                                                address2: '',
                                                phone: otherAddress.telephone,
                                                country: otherAddress.country_id,
                                                addressId: otherAddress.id

                                })}
                                             country={otherAddress.country_id}
                                             phone={otherAddress.telephone} />
                            )): null}
                        </div>
                    </div>
                </div>

                <div className="account-bg row no-margin main-container account-web-none">
                    <div className="col-12">
                        <button type="button" className="btn col-12 mx-auto d-block account-mobile-container-top-collapse-btn" type="button" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">
                            Account Information<br /><span className="account-mobile-collapse-text">(Click here for more account settings)</span>
                        </button>
                        {/* <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Button with data-target
                        </button> */}

                        <div class="collapse" id="collapseAccount">
                            <div class="card col-12 mx-auto card-body account-collapse-bg">
                                <ul className="account-ul no-padding">
                                    <li><a href="/accountDashboard" className="account-mobile-li-text">Account Dashboard</a></li>
                                    <li><a href="/accountInfo" className="account-mobile-li-text">Account Information</a></li>
                                    <li><a href="/accountAddress" className="account-mobile-li-text">Address Book</a></li>
                                    <li><a href="/accountOrders" className="account-mobile-li-text">My Orders</a></li>
                                    <li><a href="/wishlist" className="account-mobile-li-text">Wishlist</a></li>
                                    <li onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-mobile-li-text">Logout</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 d-inline-block account-bg-color">
                            <div className="col-12 d-inline-block no-padding account-name">Hello {this.state.defaultAddress ? this.state.defaultAddress.firstname + ' ' + this.state.defaultAddress.lastname : null},</div>
                            <div className="col-12 d-inline-block no-padding account-text">From Account Information you can add personal details about you and adjust emailer settings</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {this.state.showEditAddress ? <button className="col-md-6 btn account-submit-btn" onClick={() => this.setState({showEditAddress: false})}>Hide edit address</button> : null}
                            {this.state.showEditAddress ? <CustomizedAddressForm
                                onChangeLname={this.onChangeLnameHander}
                                onChangeFname={this.onChangeFnameHandler}
                                onChangeAddress={this.onChangeAddressHandler}
                                onChangeAddress2={this.onChangeAddress2Handler}
                                onChangePincode={this.onChangePincodeHandler}
                                onChangeCity={this.onChangeCityHandler}
                                onChangeRegion={this.onChangeRegionHandler}
                                onChangePhone={this.onChangePhoneHandler}
                                onChangeCountry={this.onChangeCountryHandler}
                                returnData={(rawData) => this.editAddressForm(rawData)} formData={this.state.editAddress}/> : null}
                            {this.state.showAddressForm ? <AddressForm returnData={(rawData) => this.getAddressFromForm(rawData)} /> : null}
                            {this.state.defaultAddress ? <AddressComp addressType={'Default Address'}
                                                                      fullName={this.state.defaultAddress.firstname + ' ' + this.state.defaultAddress.lastname}
                                                                      street={this.state.defaultAddress.street}
                                                                      city={this.state.defaultAddress.city}
                                                                      default={true}
                                                                      setAsDefault={() => alert('this is the default address')}
                                                                      onEditButtonClick={() => this.editButtonPressed({
                                                                          fname: this.state.defaultAddress.firstname,
                                                                          lname: this.state.defaultAddress.lastname,
                                                                          street: this.state.defaultAddress.street,
                                                                          pincode: this.state.defaultAddress.postcode,
                                                                          city: this.state.defaultAddress.city,
                                                                          address2: '',
                                                                          phone: this.state.defaultAddress.telephone,
                                                                          country: this.state.defaultAddress.country_id,
                                                                          addressId: this.state.defaultAddress.id

                                                                      })}
                                                                      onDeleteButtonClick={() => this.props.deleteSelectedAddress(this.state.defaultAddress.id)}
                                                                      pinCode={this.state.defaultAddress.postcode}
                                                                      country={this.state.defaultAddress.country_id}
                                                                      phone={this.state.defaultAddress.telephone} /> : null}
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <button onClick={() => this.setState({showAddressForm: !this.state.showAddressForm})} class="col-9 mx-auto btn account-submit-btn" ><span className="account-submit-btn-text">ADD NEW DEFAULT ADDRESS</span><span className="account-btn-line"></span></button>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block account-address-border no-padding"></div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-10 col-sm-10 col-12 d-inline-block no-padding">
                                {this.state.otherAddresses ? this.state.otherAddresses.map((otherAddress, ind) => (
                                    <AddressComp addressType={'Address ' + (ind + 1)}
                                                fullName={otherAddress.firstname + ' ' + otherAddress.lastname}
                                                street={otherAddress.street}
                                                city={otherAddress.city}
                                                 default={false}
                                                 setAsDefault={() => this.props.setAddressAsDefault(otherAddress.id)}
                                                onDeleteButtonClick={() => this.props.deleteSelectedAddress(otherAddress.id)}
                                                pinCode={otherAddress.postcode}
                                                onEditButtonClick={() => this.editButtonPressed({
                                                    fname: otherAddress.firstname,
                                                    lname: otherAddress.lastname,
                                                    street: otherAddress.street,
                                                    pincode: otherAddress.postcode,
                                                    city: otherAddress.city,
                                                    address2: '',
                                                    phone: otherAddress.telephone,
                                                    country: otherAddress.country_id,
                                                    addressId: otherAddress.id

                                    })}
                                                country={otherAddress.country_id}
                                                phone={otherAddress.telephone} />
                                )): null}
                            </div>
                        </div>
                    </div>
                    
                </div>


            </div>


        )
    }
}

export default withRouter(AccountAddress);