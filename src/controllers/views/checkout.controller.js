import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller'
import CheckoutFinalSummary from '../components/checkoutFinalSummary.controller'
import '../../css/checkout.css'
import CheckoutTop from '../components/checkout/checkout-top.controller'
import CheckoutBox from '../components/checkout/checkout-box.controller'

class Checkout extends Component{

    state = {
        guestBillingAddressSet: false,
        guestShippingAddressSet: false,
    }

    constructor(props){
        super(props);
        this.handleBoxComponent = this.handleBoxComponent.bind(this)
    }

    componentWillUpdate(){
        console.log(this.props.shipping_address)
    }

    componentDidUpdate() {
        if (this.props.billing_address && !this.state.guestBillingAddressSet) {
            alert('billing address has been set');
            this.setState({guestBillingAddressSet: true});
        }
        console.log(this.props.shipping_info);
        if (this.props.shipping_info && !this.state.guestShippingAddressSet) {
            alert('shipping address has been set');
            this.setState({guestShippingAddressSet: true});
        }
    }
    componentDidMount() {
        // if (!window.localStorage.getItem('authToken')) {
        //     console.log(this.props);
        //     return this.props.history.replace('/login');
        //     // return <Redirect to="localhost:3000/login"/>
        // }

        this.props.getCartContent();
        this.props.loadCheckoutPrices();
        this.handleBoxComponent({id: 'kjdabsnkj'});
    }



    handleBoxComponent(data){
        console.log("Inside the checkout ciontroller and the data is...")
        console.log(data)
        
        let type;
        switch(data.id || data.target.id){
            case 'checkoutType': {
                type='ADDRESS_BOX'
                console.log(type)
                break;
            }
            case 'billingAddressForm':{
                type = 'ADDRESS_BOX'
                this.props.setBillingAddress(window.localStorage.getItem('cartId'), data);
                break;
            }
            case 'shippingAddressForm':{
                type = 'ADDRESS_BOX'
                this.props.setShippingAddress(window.localStorage.getItem('cartId'), data);
                break;
            }
            case 'addressView': {
                if(this.props.billing_address && this.props.shipping_info){
                    type = 'PAYMENT_BOX'
                }
                else type = 'ADDRESS_BOX'
                break;
            }
            case 'addressLogin':{
                type = 'PAYMENT_BOX'
                // this.props.loginAddress(data.data)
                break;
            }
            default: {
                type= window.localStorage.getItem('authToken') ? 'ADDRESS_AFTER_LOGIN' : 'TYPE_BOX';
            }
        }
        this.props.changeBox(type);
    }

    render(){
        return(
            <div>
                <HeaderProduct/>

                <div class="col-md-12 col-sm-12 col-12 checkout-big-img main-container no-padding">
                    <div class="col-md-12 col-sm-12 col-12 d-inline-block no-padding"><br />
                        <CheckoutTop boxType={this.props.box_type}/>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">&nbsp;</div>

                        <div class="col-md-10 col-sm-10 col-12 mx-auto no-padding padding-mobile">
                            <div className="col-md-9 col-sm-12 col-12 d-inline-block no-padding checkout-box" >
                                <CheckoutBox returnBoxData={this.handleBoxComponent} boxType={this.props.box_type} parent={this.props} handleSameShipping={this.props.handleSameShipping}/>
                            </div>
                            <CheckoutFinalSummary checkout_items={this.props.checkout_items} checkout_bill = {this.props.checkout_bill} checkout_item_images = {this.props.checkout_item_images}/>
                        </div>
                    </div>
                </div>

                
            </div>
        )
    }
}

Checkout.defaultProps = {
    box_type: 'TYPE_BOX',
    checkout_items : {
        items: []       
    },
    checkout_bill: {
        totals: {
          grand_total: 0,
          subtotal: 0,
          shipping_amount: 0
        }
    }
}

export default Checkout