import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import '../../css/login.css'
import {withRouter} from 'react-router-dom';
import ScrollMagic from 'scrollmagic'

import loginRight from '../../img/login_bg_2.png'
import loginleft from '../../img/login_bg_right.png'

class Forgot extends Component{

    state = {
        email: null,
        errors: {
            emailError: null
        }
    };

    componentDidUpdate() {
        if (this.props.isForgotMailSent) {
            this.props.setIsForgotFalse();
            this.props.history.push('/login');
        }
    }

    updateEmailHandler = (event) => {
        this.setState({email: event.target.value});
    };


    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }

            return null;
        }

        return 'Email address cannot be empty';

    };

    validateAllInputs = () => {
        const errors = {

            emailError: null,
        };
        errors.emailError = this.validateEmail(this.state.email);

        this.setState({errors})
    };



    initForgetPassword = () => {
        this.validateAllInputs();
        if (this.state.errors.emailError) {
            return;
        }
        console.log('error did not occur during forgot');
        this.props.forgot(this.state.email);

    };

    componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){
        return(
            <div>
                <HeaderProduct />

                <div className="login-main-container row no-margin">
                    <div className="col-md-4 col-sm-4 col-12 d-inline-block no-padding">
                        <img src={loginleft} />                         
                    </div>
                    <div className="col-md-5 col-sm-5 d-inline-block mx-auto no-padding text-position">
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-9 col-sm-9 offset-md-1 col-12 d-inline-block login-middle-text no-padding">
                            Forgot Password
                        </div>                        
                    </div>
                    <div className="col-md-3 col-sm-3 d-inline-block no-padding login-hide">
                        <img className="float-right login-right-img" src={loginRight} />
                    </div> 
                </div>

                <div className="row no-margin col-md-12 no-padding absolute login-box-top">
                    <div className="col-md-7 col-sm-7 col-11 offset-md-3 no-padding login-bg-color margin-left">
                    <div className="login-transform-text absolute login-hide">Login<span className="login-transform-line"></span></div>
                        <div className="col-md-8 col-sm-8 col-11 mx-auto no-padding">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding mobile-text-center">
                                <span className="login-text">Enter your email ID registered with us. We’ll send a password reset link and you can set your new password  by clicking on that link</span>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <span className="login-label-text">Email Address:</span>
                                <input onChange={this.updateEmailHandler} type="email" className="form-control login-input" placeholder="brucebanner@gmail.com" />
                                {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null}
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>   
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                        
                            <div className="mx-auto col-md-12 col-sm-12 col-12 d-inline-block text-center">
                               <button onClick={this.initForgetPassword} className="col-md-6 col-sm-6 mx-auto col-8 btn checout-btn"><span className="style12_prod_text">RETRIEVE PASSWORD<span className="login-btn-line"></span></span></button>    
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <div className="login-border mx-auto"></div>
                            </div> 
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-center">
                                <span className="login-bottom-text">If you are a first time user, click here to <a href="/register" className="login-bottom-text-bold">Register as a User</a></span>
                            </div>   
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        </div>
                    </div>
                </div>

                {/* <div id="footer-visibility-start" className = "foot_start">
                </div>
                <div id="product-footer" className="col-md-12 product-footer" >
                    <div className="col-md-12 footer_bg_col">&nbsp;</div>
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className = "footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div> */}

            </div>
        )
    }
}

export default withRouter(Forgot)