import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import '../../css/login.css'
import {withRouter} from 'react-router-dom';
import ScrollMagic from 'scrollmagic'

import loginRight from '../../img/login_bg_2.png'
import loginleft from '../../img/login_bg_right.png'

class Login extends Component{

    constructor(props){
        super(props);
        this.state = {
             fname : "",
             lname : "",
             email : "",
             pswd  : "",
            errors: {
                fnameError: null,
                lnameError: null,
                emailError: null,
                passwordError: null
            }
         };
        this.handleChangeFname = this.handleChangeFname.bind(this);
        this.handleChangeLname = this.handleChangeLname.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePass = this.handleChangePass.bind(this)
        this.handleSubmit_register = this.handleSubmit_register.bind(this);
    }

    handleChangeFname(event){
        this.setState({fname: event.target.value});
    }
    handleChangeLname(event){
        this.setState({lname: event.target.value});
    }
    handleChangeEmail(event){
        this.setState({email: event.target.value});
    }
    handleChangePass(event){
        this.setState({pswd: event.target.value});
    }

    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }

            return null;
        }

        return 'Email address cannot be empty';

    };

    validatePassword = (sPassword) => {
        if (sPassword) {
            let regPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
                if(!sPassword.match(regPassword)) {
                    // alert("Invalid email address");
                    return 'Password has to be at least 8 chars long including one small, one capital and one special character';
                }

                return null;
            }
        return 'Password cannot be empty';

    };

    validateFirstName = (sFname, type) => {
        if (sFname) {

            if(sFname.toString().length < 2) {
                // alert("Invalid email address");
                return 'Name cannot be shorter than 2 characters';
            }

            return null;
        }
        return 'Name cannot be empty';

    };


    validateAllInputs = () => {
        const errors = {

            fnameError: null,
            lnameError: null,
            emailError: null,
            passwordError: null
        };

        errors.fnameError = this.validateFirstName(this.state.fname);
        errors.lnameError = this.validateFirstName(this.state.lname);
        errors.passwordError = this.validatePassword(this.state.pswd);
        errors.emailError = this.validateEmail(this.state.email);

        this.setState({errors})
    };

    handleSubmit_register(event) {
      event.preventDefault();
        this.validateAllInputs();
        if (this.state.errors.emailError || this.state.errors.passwordError || this.state.errors.fnameError || this.state.errors.lnameError) {
            return;
        }
      this.props.register(this.state.fname, this.state.lname, this.state.email, this.state.pswd);
    }

    componentDidUpdate() {
        console.log(this.props.isRegistered);
        if (this.props.isRegistered) {
            this.props.setIsRegisteredFalse();
            this.props.history.push('/login');
        }

    }

   componentDidMount(){

    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();
       if (window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/dev');
           // return <Redirect to="localhost:3000/login"/>
       }
    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
        }

    render(){
        return(
            <div>
                <HeaderProduct />

                <div className="login-main-container row no-margin">
                    <div className="col-md-4 col-sm-4 d-inline-block no-padding">
                        <img src={loginleft} className="login-hide" />
                        <img src={loginRight} className="login-web-hide" width="100%" height="100%" />                         
                    </div>
                    <div className="col-md-5 col-sm-5 d-inline-block mx-auto no-padding text-position">
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-9 col-sm-9 offset-md-1 col-12 d-inline-block login-middle-text no-padding">
                            Register as a User
                        </div>                        
                    </div>
                    <div className="col-md-3 col-sm-3 col-12 d-inline-block no-padding login-hide">
                        <img className="float-right login-right-img" src={loginRight} />
                    </div>
                </div>

                <div className="row col-md-12 col-sm-12 col-12 no-padding no-margin absolute register-box-top">
                    <div className="col-md-7 col-sm-7 col-11 offset-md-3 no-padding login-bg-color margin-left">
                        <div className="register-transform-text absolute login-hide">Register<span className="register-transform-line"></span></div>
                        <div className="col-md-8 col-sm-8 col-11 mx-auto no-padding">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding mobile-text-center">
                                <span className="login-text">If you have an account already registered with us, please login in with your credentials</span>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            {/*<form onSubmit = {this.handleSubmit_register}>*/}
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block no-padding">
                                       <span className="login-label-text">First Name*:</span>
                                       <input name = 'fname' type="text" className="form-control login-input" placeholder="John" onChange={this.handleChangeFname}  />
                                        {this.state.errors.fnameError ? <p style={{color: 'red'}}>First name should be more than 2 characters</p> : null}
                                    </div>
                                    <div className="col-md-6 col-sm-6 col-12 d-inline-block register-padding-right">
                                       <span className="login-label-text">Last Name*:</span>
                                       <input name = 'lname' type="text" className="form-control login-input" placeholder="Doe" onChange={this.handleChangeLname}/>
                                        {this.state.errors.lnameError ? <p style={{color: 'red'}}>Last name should be more than 2 characters</p> : null}
                                    </div>  
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <span className="login-label-text">Email Address*:</span>
                                    <input name = 'email' type="email" className="form-control login-input" placeholder="brucebanner@gmail.com" onChange={this.handleChangeEmail} />
                                    {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null}
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <span className="login-label-text">Set your Password*:</span>
                                    <input name = 'pswd' type="password" className="form-control login-input" placeholder="******" onChange={this.handleChangePass} />
                                    {this.state.errors.passwordError ? <p style={{color: 'red'}}>Password should have atleast 6 characters including a capital character and a special character</p> : null}
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block">
                                    <input class="form-check-input" type="checkbox" /> <span className="register-checkbox-text">Keep me in your list, I want to know about your upcoming campaigns and stuff</span>
                                </div>   
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                                <div className="mx-auto col-md-12 col-sm-12 col-12 d-inline-block text-center">
                                    <button onClick={this.handleSubmit_register} className="col-md-4 col-sm-4 mx-auto col-5 btn checout-btn"><span className="style12_prod_text">REGISTER<span className="login-btn-line"></span></span></button>
                                </div>
                            {/*</form>*/}
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <div className="login-border mx-auto"></div>
                            </div> 
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-center">
                                <span className="login-bottom-text">Already an existing user, click here to <a href="/login" className="login-bottom-text-bold">Login to your Account</a></span>
                            </div>   
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        </div>
                    </div>
                </div>

                {/* <div id="footer-visibility-start" className = "foot_start">
                </div>
                <div id="product-footer" className="col-md-12 product-footer" >
                    <div className="col-md-12 footer_bg_col">&nbsp;</div>
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className = "footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div> */}

            </div>
        )
    }
}

export default withRouter(Login)