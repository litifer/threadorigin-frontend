import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import {connect} from 'react-redux';
import Footer from '../components/footer.controller';
import { withRouter } from 'react-router-dom';
import '../../css/login.css'
import ScrollMagic from 'scrollmagic'
import loginRight from '../../img/login_bg_2.png'
import loginleft from '../../img/login_bg_right.png'

class Login extends Component{

    constructor(props){
        super(props);
        this.state = {
            user : "",
            pass : "",
            dummy: '',
            errors: {

                emailError: null,
                passwordError: null
            }
        };
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePass  = this.handleChangePass.bind(this);
        this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
    }


    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }
            else
            return null;
        }

        return 'Email address cannot be empty';

    };

    validatePassword = (sPassword) => {
        if (sPassword) {
            let regPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
            if(!sPassword.match(regPassword)) {
                // alert("Invalid email address");
                return 'Password has to be at least 8 chars long including one small, one capital and one special character';
            }
            else
            return null;
        }
        return 'Password cannot be empty';

    };


    validateAllInputs = () => {
        const errors = {

            emailError: null,
            passwordError: null
        };

        errors.passwordError = this.validatePassword(this.state.pass);
        errors.emailError = this.validateEmail(this.state.user);

        this.setState({errors})
    };


    componentDidUpdate() {


        if (window.localStorage.getItem('authToken')) {
            console.log(this.props);
            return this.props.history.replace('/dev');
            // return <Redirect to="localhost:3000/login"/>
        }

        // if (this.props.dataFlag === false) {
        //     if (this.state.errors.passwordError !== "Incorrect Email or password") {
        //         console.log("Email password is incorrect")
        //         const errors = {
        //             emailError: null,
        //             passwordError: "Incorrect Email or password"
        //         };
        //         this.setState({errors});
        //         this.props.setDataFlagNull();
        //     }
        // }

        if (this.props.loggedInData) {
            console.log(this.props.loggedInData, "lakdsmkladsmas");
            if (this.props.loggedInData.success === "false" && this.state.errors.passwordError !== "Incorrect Email or password") {
                const errors = {
                                emailError: null,
                                passwordError: "Incorrect Email or password"
                            };
                this.setState({errors})
            } else {
                if (this.state.errors.passwordError !== null && this.state.errors.emailError !== null) {
                    const errors = {
                        emailError: null,
                        passwordError: null
                    };
                    this.setState({errors})
                }
            }
            this.props.setLoginDataToNull();
        }
        console.log('asjdnhijasnidas im here ');

    }
    
    handleChangeEmail(event){
        this.setState({user: event.target.value});
    }

    handleChangePass(event){
        this.setState({pass: event.target.value});
    }

    handleSubmitLogin = () => {
        // event.preventDefault();
        this.validateAllInputs();
        if (this.state.errors.emailError || this.state.errors.passwordError) {
            return;
        }
        console.log('error did not occur during login');
        this.props.login(this.state.user, this.state.pass);
        console.log(window.localStorage.getItem('authToken'));
    };

   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();
       if (window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/dev');
           // return <Redirect to="localhost:3000/login"/>
       }

       console.log(window.localStorage.getItem('authToken'), 'login controller');

       new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){
        return(
            <div>
                <HeaderProduct />

                <div className="login-main-container row no-margin">
                    <div className="col-md-4 col-sm-4 col-12 d-inline-block no-padding">
                        <img src={loginleft} />                         
                    </div>
                    <div className="col-md-5 col-sm-5 d-inline-block mx-auto no-padding text-position">
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-9 col-sm-9 offset-md-1 col-12 d-inline-block login-middle-text no-padding">
                            Login to your Account
                        </div>                        
                    </div>
                    <div className="col-md-3 col-sm-3 d-inline-block no-padding login-hide">
                        <img className="float-right login-right-img" src={loginRight} />
                    </div> 
                </div>

                <div className="row no-margin col-md-12 no-padding absolute login-box-top">
                    <div className="col-md-7 col-sm-7 col-11 offset-md-3 no-padding login-bg-color margin-left">
                    <div className="login-transform-text absolute login-hide">Login<span className="login-transform-line"></span></div>
                        <div className="col-md-8 col-sm-8 col-11 mx-auto no-padding">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding mobile-text-center">
                                <span className="login-text">If you have an account already registered with us, please login in with your credentials</span>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            {/*<form onSubmit = {this.handleSubmitLogin}>*/}
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                   <span className="login-label-text">Username:</span>
                                   <input name = 'user' type="email" className="form-control login-input" placeholder="example@example.com" onChange = {this.handleChangeEmail} />
                                    {this.state.errors.emailError ? <p style={{color: 'red'}}>Please enter a valid email address</p> : null}

                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                    <span className="login-label-text">Password:</span>
                                    <input name = 'pass' type="password" className="form-control login-input" placeholder="password" onChange= {this.handleChangePass} />
                                    {this.state.errors.passwordError ? <p style={{color: 'red'}}>{this.state.errors.passwordError}</p> : null}

                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-right">
                                   <span className="login-forgot-pass"><a href="/forgot" className="login-forgot-pass">Forgot your Password</a></span>
                                </div>   
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                                <div className="mx-auto col-md-12 col-sm-12 col-12 d-inline-block text-center">
                                    <button onClick={this.handleSubmitLogin} className="col-md-4 col-sm-4 mx-auto col-4 btn checout-btn"><span className="style12_prod_text">LOGIN<span className="login-btn-line"></span></span></button>
                                </div>
                             {/*</form>*/}
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 mx-auto d-inline-block no-padding">
                                <div className="login-border mx-auto"></div>
                            </div> 
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-center">
                                <span className="login-bottom-text">If you are a first time user, click here to <a href="/register" className="login-bottom-text-bold">Register as a User</a></span>
                            </div>   
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        </div>
                    </div>
                </div>

               <div id="product-footer" className="col-md-12 product-footer">
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className="footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    loginToken: state.login
});

export default connect(mapStateToProps, null)(withRouter(Login));