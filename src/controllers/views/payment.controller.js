import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import '../../css/checkout.css'

import cartItem from '../../img/cart_item.png'

class Payment extends Component{

    componentDidMount() {
        if (!window.localStorage.getItem('authToken')) {
            console.log(this.props);
            return this.props.history.replace('/login');
            // return <Redirect to="localhost:3000/login"/>
        }
    }

    render(){
        return(
            <div>
                <HeaderProduct/>



                <div class="main-container checkout-img no-padding">
                    <div class="col-md-12 col-sm-12 float-sm-left float-md-left no-padding cart-img-opacity"><br />
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div class="col-md-6 col-sm-6 float-sm-left float-md-left no-padding" style={{"margin-left": "-2%"}}><span className="no-padding" style={{"font-size": "24px","font-weight": "bold","color": "#ffffff"}}>Checkout</span><br /><span style={{"font-size": "14px","color": "#ffffff"}}>Check all your products in your cart before checking out below</span>
                            </div>
                            <div class="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                <div class="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div class="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                    <div class="col-md-4 col-sm-4 float-sm-left float-md-left no-padding text-center">
                                        <span style={{"font-size": "16px","font-weight": "900","color": "#ffffff","text-decoration":"underline"}}>Login/Register</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 float-sm-left float-md-left no-padding text-center">
                                        <span style={{"font-size": "16px","font-weight": "500","color": "#989898"}}>Address Confirmation</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 float-sm-left float-md-left no-padding text-center">
                                        <span style={{"font-size": "16px","font-weight": "500","color": "#989898"}}>Payment Options</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div className="col-md-8 col-sm-12 float-sm-left float-md-left no-padding" style={{"background-color":"#ffffff","margin-left":"-2%"}}>
                                <div className="col-md-12 col-sm-12 float-left payment-padding">
                                    <span className="payment-text">Choose any of the following Payment Options to place an order:</span>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12 float-sm-left float-md-left no-padding margin" style={{"background-color":"#ffffff"}}>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                    <div className="col-md-6 col-sm-6 float-sm-left float-md-left">
                                        <span style={{"font-size": "14px","font-weight": "900","color": "#4a4a4a"}}>Final Summary</span>
                                    </div> 
                                    <div className="col-md-6 col-sm-6 float-sm-left float-md-left text-right">
                                        <span style={{"font-size": "14px","font-style": "oblique","color": "#9b9b9b"}}>3 Products</span>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding" style={{"border-bottom":"solid 2px #cbcbcb"}}>&nbsp;</div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left" style={{"margin-top":"2%"}}>
                                    <div className="col-md-8 col-sm-8 float-sm-left float-md-left no-padding">
                                        <div className="col-md-4 col-sm-4 float-sm-left float-md-left no-padding"><img src={cartItem} /></div>
                                        <div className="col-md-8 col-sm-8 float-sm-left float-md-left"><span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Sasha BodyCon Dress</span ><br /><span style={{"font-size": "10px","color": "#4a4a4a"}}>Quantity: 2</span></div>
                                    </div>
                                    <div className="col-md-4 col-sm-4 float-sm-left float-md-left no-padding text-right">
                                        <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 4598</span>
                                    </div>                                    
                                </div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left" style={{"margin-top":"2%"}}>
                                    <div className="col-md-8 col-sm-8 float-sm-left float-md-left no-padding">
                                        <div className="col-md-4 col-sm-4 float-sm-left float-md-left no-padding"><img src={cartItem} /></div>
                                        <div className="col-md-8 col-sm-8 float-sm-left float-md-left"><span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Sasha BodyCon Dress</span ><br /><span style={{"font-size": "10px","color": "#4a4a4a"}}>Quantity: 2</span></div>
                                    </div>
                                    <div className="col-md-4 col-sm-4 float-sm-left float-md-left no-padding text-right">
                                        <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 4598</span>
                                    </div>                                    
                                </div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding" style={{ "background-color": "#fafafa","border": "solid 1px #dcdcdc"}}>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left"><span style={{"font-size": "14px","font-weight": "900","color": "#4a4a4a"}}>Shipping<span style={{"font-size": "10px","font-weight": "500","color": "#9b9b9b"}}>(Express Delivery for faster delivery)</span></span><br /></div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left">
                                        <div className="col-md-9 col-sm-9 float-sm-left float-md-left no-padding">
                                            <form>
                                                <input type="radio" /><span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Standard Delivery (5-7 Days)</span>
                                            </form>
                                        </div>
                                        <div className="col-md-3 col-sm-3 float-sm-left float-md-left no-padding text-right">
                                            <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 0</span>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left">
                                        <div className="col-md-9 col-sm-9 float-sm-left float-md-left no-padding">
                                            <form>
                                                <input type="radio" /><span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Express Delivery (2-3 Days)</span>
                                            </form>
                                        </div>
                                        <div className="col-md-3 col-sm-3 float-sm-left float-md-left no-padding text-right">
                                            <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 250</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left" style={{"border-bottom": "solid 2px #cbcbcb"}}>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>                                
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                        <div className="col-md-9 col-sm-9 float-sm-left float-md-left no-padding">
                                            <span style={{"font-size": "14px","font-weight": "900","color": "#4a4a4a"}}>Sub Total</span>                                                
                                        </div>
                                        <div className="col-md-3 col-sm-3 float-sm-left float-md-left no-padding text-right">
                                            <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 12,139</span>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                        <div className="col-md-9 col-sm-9 float-sm-left float-md-left no-padding">
                                            <span style={{"font-size": "14px","font-weight": "900","color": "#4a4a4a"}}>Shipping Costs</span>                                                
                                        </div>
                                        <div className="col-md-3 col-sm-3 float-sm-left float-md-left no-padding text-right">
                                            <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 0</span>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left">&nbsp;</div>                   
                                </div> 
                                <div className="col-md-12 col-sm-12 float-sm-left float-md-left">
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>                                
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                        <div className="col-md-9 col-sm-9 float-sm-left float-md-left no-padding">
                                            <span style={{"font-size": "14px","font-weight": "900","color": "#4a4a4a"}}>Payable Amount</span>                                                
                                        </div>
                                        <div className="col-md-3 col-sm-3 float-sm-left float-md-left no-padding text-right">
                                            <span style={{"font-size": "12px","font-weight": "900","color": "#4a4a4a"}}>Rs. 12,569</span>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left">&nbsp;</div>                   
                                </div>                                                               
                            </div>
                        </div>
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding margin">
                            <div id="none" className="col-md-7 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                            <div className="col-md-4 col-sm-12 float-sm-left float-md-left checkout-menu-btn btn no-padding margin-btn"><span className="checkout-menu-btn-text">Back to Shopping &nbsp;&nbsp;&nbsp;&nbsp; -----</span></div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Payment