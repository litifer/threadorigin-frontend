import React, {Component} from 'react'
import '../../css/prewebsitelaunch.css'
// import prewebsitelaunch from '../../img/background.png'
import prewebsitelaunch1 from '../../img/prel1.jpg'
import prewebsitelaunch2 from '../../img/prel2.jpg'
import prewebsitelaunch3 from '../../img/prel3.jpg'
import prewebsitelaunch4 from '../../img/prel4.jpg'
import prewebsitelaunch5 from '../../img/prel5.jpg'
import prewebsitelaunch6 from '../../img/prel6.jpg'
// import line2 from '../../img/Line 2.png'
// import mobileview from '../../img/prelaunchimage.png'
import mobileview1 from '../../img/prelaunchmobile1.jpg'
import mobileview2 from '../../img/prelaunchmobie2.jpg'
import mobileview3 from '../../img/prelaunchmobile3.jpg'
import mobileview4 from '../../img/prelaunchmobile4.jpg'
import mobileview5 from '../../img/prelaunchmobile5.jpg'
import mobileview6 from '../../img/prelaunchmobile6.jpg'
// import mobileview4 from '../../img/pm4.jpg'
// import mobileview5 from '../../img/pm5.jpg'
// import mobileview6 from '../../img/pm6.jpg'
import logo from '../../img/prelaunchlogo1.svg'

class PreWebsiteLaunch extends Component{

    constructor(props){
        super(props)
        this.state = {value : ""}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    validateEmail = (sEmail) => {
        if (sEmail) {

            let reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

            if(!sEmail.match(reEmail)) {
                // alert("Invalid email address");
                return 'Invalid email address';
            }
            else
            return null;
        }

        return 'Email address cannot be empty';

    };


    handleSubmit(event) {
        let emailError = this.validateEmail(event.target[0].value);
        if (emailError) {
            alert('Invalid email address');
        } else {
            console.log("%c event is", "color:black; background-color: white")
            console.log(event)
            console.log("hey",event.target[0].value)
            alert('An email was submitted: ' + event.target[0].value);
            event.preventDefault();
            this.props.subscribe_email(event.target[0].value);
        }
    }
    
   componentDidMount(){
          
        }

    render(){
        return(
            <div className="pre-launch-container">
                <div id="demo" className="carousel slide carousel-size" data-ride="carousel">
                    {/* <!-- The slideshow --> */}
                    <div className="carousel-inner pre-launch-website-carousel-inner">
                        <div className="carousel-item active">
                            <img src={prewebsitelaunch1} className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview1} className="preWebsitelaunch-img hide_nav" />
                        </div>
                        <div className="carousel-item">
                            <img src={prewebsitelaunch2} className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview2} className="preWebsitelaunch-img hide_nav" />
                        </div>
                        <div className="carousel-item">
                            <img src={prewebsitelaunch3} className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview3} className="preWebsitelaunch-img hide_nav" />
                        </div>
                        <div className="carousel-item">
                            <img src={prewebsitelaunch4} className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview4} className="preWebsitelaunch-img hide_nav" />
                        </div>
                        <div className="carousel-item">
                            <img src={prewebsitelaunch5} className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview5} className="preWebsitelaunch-img hide_nav" />
                        </div>
                        <div className="carousel-item">
                            <img src={prewebsitelaunch6}  className="preWebsitelaunch-img" id="none" />
                            <img src={mobileview6} className="preWebsitelaunch-img hide_nav" />
                        </div>
                    </div>

                    <div className="carousel-caption preWebsitelaunch-caption">
                    <div className="d-lg-none">
                        <div className="">
                            <img src={logo} />
                        </div>
                        <div className="preWebsitelaunch-heading">Stay tuned, we’re going to launch soon!</div> 
                        <div className="">
                            <div className="preWebsitelaunch-text">Join our newsletter and get vouchers when we launch.</div>
                            <div className="newsletter1-box">
                            <form onSubmit = {this.handleSubmit}>
                                <input type="email" className="preWebsitelaunch-box form-control pull-right pre-website-launch-submit" placeholder="Enter your E-Mail Address" required />
                                <button className="btn preWebsitelaunch-btn" type="submit"><span className="preWebsitelaunch-btn-text">SUBMIT <span className='btn-line'></span></span></button>
                            </form>   
                            </div>
                        </div>
                    </div>
                        {/* <div className="emptydiv"></div> */}
                        <div className="d-none d-lg-block">
                            <div className="preWebsitelaunch-heading">Let's Start Shopping this August</div>
                            <div className="row no-margin preWebsitelaunch-bottom">
                                <div className="col-md-6 col-sm-12 col-12 no-padding bottom-logo-container d-none d-md-inline-block">                        
                                    <img src={logo} className="preWebsitelaunch-logo-position" />    
                                </div>
                                <div className="col-md-6 col-s col-12 newsletter1-container mx-auto">
                                    <span className="preWebsitelaunch-text">Join our newsletter and get vouchers when we launch.</span>
                                    <div className="newsletter1-box">
                                    <form onSubmit = {this.handleSubmit}>
                                        <input type="email" className="preWebsitelaunch-box form-control pull-right pre-website-launch-submit" placeholder="Enter your E-Mail Address" required />
                                        <button className="btn preWebsitelaunch-btn" type="submit"><span className="preWebsitelaunch-btn-text">SUBMIT <span className='btn-line'></span></span></button>
                                    </form>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Left and right controls --> */}
                    {/* <a className="carousel-control-prev arrow-width-left" href="#demo" data-slide="prev">
                        <div className="oval-button left">
                            <i className="fa fa-caret-left fa-2x carousel-icon-left" aria-hidden="true"></i>
                        </div>
                    </a>
                    <a className="carousel-control-next arrow-width-right" href="#demo" data-slide="next">
                        <div className="oval-button right"> 
                            <i className="fa fa-caret-right fa-2x carousel-icon-right" aria-hidden="true"></i>
                        </div>
                    </a> */}
                </div>
            </div>
        )
    }
}

export default PreWebsiteLaunch