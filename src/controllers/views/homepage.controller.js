import React, {Component} from 'react';
import ScrollReveal from 'scrollreveal'
import ScrollMagic from 'scrollmagic'
import * as  config_ from '../../config.js';
import Footer from '../components/footer.controller';
import Header from '../components/header.controller';
import ProductCarousel from '../components/productCarousel.controller';
import Landing from '../../containers/landing.container';
import '../../css/body1.css'
import '../../css/body2.css'
import '../../css/body3.css'
import '../../css/body5.css'
import '../../css/body6.css'
import '../../css/footer.css'
import '../../css/homepage.css'
import '../../css/product_footer.css'
import '../../css/mediaqueries.css'

import secureImg from '../../img/secure.png'
import freeImg from '../../img/free.png'
import returnImg from '../../img/return.png'

import Modal from "../components/modal.controller"
import { hasData, getImgUrl, findProductById } from '../../helpers/general';




const images = require.context('../../img', true);

class HomePage extends Component{

    constructor(props){
        super(props)
        let newImg = ''
        this.state = {value : ""}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        let imageurl = "../../img/item1.png"
        this.state = {
            areCategoriesFetched: false,
            areColorsFetched: false,
            areSizesFetched: false,
            areVariantsFetched: false,
            products_updated: false,
            sr : ScrollReveal()
        }

        this.handleModalClick = this.handleModalClick.bind(this)
    }

    handleModalClick(product_id){
        this.setState({areColorsFetched: false, areSizesFetched: false, areVariantsFetched: false})
        this.props.changeModal(findProductById(product_id, this.props.product_data.items));
    }

    handleChange(event) {
      this.setState({value: event.target.value});
    }

    handleSubmit(event) {
      alert('An email was submitted: ' + this.state.value);
      event.preventDefault();
      this.props.subscribe_email(this.state.value);
    }

    componentDidCatch(){
        console.log("Error here")
    }

    componentDidMount(){
    this.props.getAllCategories();    
    this.props.getColor();
    this.props.getSize();
    this.props.getCamp1();
    this.props.getCamp2();
    this.props.getCamp3();
    this.props.getBody5();
    // this.props.fetchprodBody();
    this.props.getImgBody5();
    this.props.getInsta();


    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

    new ScrollMagic.Scene({triggerElement:"#home-footer-visibility-start"})
        .triggerHook("onEnter")
        .setClassToggle("#home-footer-animation-block","home-footer-visible")
        .addTo(scrollfooterController)

    let scrollMagicController = new ScrollMagic.Controller();

}

componentDidUpdate(){
   
    
    this.state.sr.reveal('.anim_scroll_reveal_default')
    if(this.refs.body1Item !== undefined && this.refs.body1Item_text !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body1', 50)
    }
    if(this.refs.body2Item !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body2', 40) 
    }

    if(this.refs.body3Item !== undefined && this.refs.body3Item_text !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body3', 50)
    }

    if(this.refs.body4Item !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body4', 40) 
    }

    if(this.refs.body6Item !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body6', 40) 
    }

    if(this.refs.body7Item !== undefined){
        this.state.sr.reveal('.anim_scroll_reveal_sequence_body7', 40) 
    }

    this.state.sr.reveal('.anim_scroll_reveal_sequence_body7_footer', 40)
    
    if(this.props.categories && !this.state.areCategoriesFetched){
        let requiredCategories = hasData(this.props.categories.children_data).find((category => {
            return category.name === 'Collections'
        }))

        if(!requiredCategories) return

        requiredCategories.children_data.map((category) => {
            this.props.getRelevantItems({
                api: "CATEGORY",
                categoryId: category.id
            });
        })

        this.setState({areCategoriesFetched: true})
    }

    if(this.props.product_details && this.props.product_details.length  && !this.state.products_updated)
        {
            hasData(this.props.product_details).map(product => {
                product.map((product) => {
                    this.props.fetchProductData(product);
                })
                
            })

            this.setState({products_updated: true})
        }

}

    render(){

        return(
           
            <div id = 'loader'>
                   
                    <Header/>
                    <Landing/>
                    
                    {/*************** body1 ***************/}


                    <div className="container-fluid no-padding homepage-container homepage-body1-margin-top starting-container">
                        <div className="">
                            <div class="row camp-container">
                            <div class="camp-inner-container camp-inner-margin">
                            {
                                this.props.camp1.media_gallery_entries.map((media_gallery_entries, index) => {
                                    var classN = '',imgclass = '', id =''
                                    var url = `${config_.IMG_URL}${media_gallery_entries.file}`

                                    if(index == 0){
                                        classN = 'col-md-5 col-sm-12 col-lg-5 no-padding img1_CAMPS d-md-inline-block big_camp_img_box left_camp_img_box' 
                                        imgclass = 'anim_scroll_reveal_sequence_body1 img-responsive camp1-img-size animation_img'
                                    }
                                    else if (index == 1){
                                        classN = ' col-md-6 col-sm-6 col-lg-6 no-padding img2_CAMPS d-md-inline-block small_camp_img_box right_camp_img_box'
                                        imgclass = 'anim_scroll_reveal_sequence_body1 animation_img'
                                        id = ''
                                    }

                                    return <div className={classN} ref="body1Item"> 
                                                <img className={imgclass} id = {id} src={url}  alt=""/>
                                            </div>
                                })
                            }

                            <div className="anim_scroll_reveal_sequence_body1 col-md-4 col-sm-12 col-lg-4 no-padding d-inline-block absolute camp-box" ref="body1Item_text">
                                <div className="campaign-box"> 
                                    <div className="campaign-box1 text-center">
                                    <span className="anim_scroll_reveal_default campaign-heading">{this.props.camp1.name}</span>
                                        {/* {
                                            this.props.camp1.custom_attributes.map((custom_attributes, index) => {
                                                if(index == 2){
                                                    return <span className="anim_scroll_reveal_sequence_body1 campaign-heading">
                                                    {custom_attributes.value}
                                                    </span>
                                                }
                                            })
                                        } */}
                                        
                                        
                                        <div className="campaign-border mx-auto"></div>
                                        {
                                            this.props.camp1.custom_attributes.map((custom_attributes, index) => {
                                                if(index == 0){
                                                    return <div className = 'campaign_desc'><span className="anim_scroll_reveal_default campaign-text">
                                                    {custom_attributes.value}
                                                    </span></div>
                                                }
                                            })
                                        }
                                        {/* href="/category?id=2"  */}
                                        <a className="anim_scroll_reveal_default btn campaign-btn">
                                          <span className="campaign-btn-text">EXPLORE COLLECTION <span className="btn-line"></span></span>
                                          </a>
                                    </div>
                                </div>
                                        
                                </div>
                            </div>
                            </div>
                        {/* <div class="campaign-color-box d-block d-sm-block"></div> */}
                                
                        </div>

                    </div>

                {/*************** body2 ***************/}
                {this.props.product_data ? <ProductCarousel 
                                                    product_details={this.props.product_data} 
                                                    id={3} 
                                                    handleModalClick={this.handleModalClick}
                                                    color_array={this.props.all_colors} 
                                                    size_array = {this.props.all_sizes}
                                            /> 
                                            : null} 
                
                {/*************** body3***************/}
                    <div className="container-fluid homepage-container no-padding">
                        <div className="">
                             <div class="row camp-container">
                             <div class="camp-inner-container">
                            {
                                this.props.camp2.media_gallery_entries.map((media_gallery_entries, index) => {
                                    var classN = '',imgclass = '', id =''
                                    var url = `${config_.IMG_URL}${media_gallery_entries.file}`
                                    if(index == 0){
                                    classN = 'anim_scroll_reveal_sequence_body3 col-md-5 col-sm-12 col-lg-5 no-padding d-inline-block img1_CAMPS small_camp_img_box left_camp_img_box' 
                                    imgclass = 'img-responsive camp1-img-size animation_img'
                                    }
                                    else if (index == 1){
                                        classN = 'anim_scroll_reveal_sequence_body3 col-md-6 col-sm-6 col-lg-6 no-padding img2_CAMPS d-inline-block big_camp_img_box right_camp_img_box camp2-second-index'
                                        imgclass = 'animation_img'
                                        id = ''
                                    }
                                        return <div className={classN} ref="body3Item"> 
                                                <img className={imgclass} id = {id} src={url} alt=""/>
                                            </div>
                                })
                            }
                            <div className="anim_scroll_reveal_sequence_body3 col-md-4 col-sm-12 col-lg-4 no-padding d-inline-block absolute camp-box" ref="body3Item_text">
                                <div className="campaign-box"> 

                                    <div className="campaign-box1 text-center">
                                    <span className="anim_scroll_reveal_default campaign-heading">{this.props.camp2.name}</span>
                                        {/* {
                                            this.props.camp2.custom_attributes.map((custom_attributes, index) => {
                                                if(index == 2){
                                                    return <span className="anim_scroll_reveal_sequence_body3 campaign-heading">
                                                    {custom_attributes.value}
                                                    </span>
                                                }
                                            })
                                        } */}
                                        <div className="campaign-border mx-auto border-body5"></div>
                                        {
                                            this.props.camp2.custom_attributes.map((custom_attributes, index) => {
                                                if(index == 0){
                                                    return  <div className = 'text-box-camp'><span className="anim_scroll_reveal_default campaign-text_body3">
                                                    {custom_attributes.value}
                                                    </span></div>
                                                }
                                            })
                                        }
                                    {/* href="/category?id=2"  */}
                                        <a className="anim_scroll_reveal_default btn campaign-btn">
                                          <span className="campaign-btn-text">EXPLORE COLLECTION <span className="btn-line"></span></span>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        {/* <div class="campaign-color-box d-block d-sm-block"></div> */}
                    </div>  
                                  

                {/*************** body4 ***************/}
                {this.props.product_data ? <ProductCarousel 
                    product_details={this.props.product_data} 
                    id={3} 
                    handleModalClick={this.handleModalClick}
                    color_array={this.props.all_colors} 
                    size_array = {this.props.all_sizes}
                    /> 
            : null} 

                 {/*************** body5 ***************/}
                    <div className="container-fluid no-padding homepage-container"> 
                        <div className="" >
                        <div class="row camp-container">
                        <div class="camp-inner-container">
                            {
                                this.props.camp3.media_gallery_entries.map((media_gallery_entries, index) => {
                                    var classN = '',imgclass = '', id =''
                                    var url = `${config_.IMG_URL}${media_gallery_entries.file}`
                                    if(index == 0){  
                                    classN = 'anim_scroll_reveal_sequence_body5 col-md-5 col-sm-12 col-lg-5 no-padding d-inline-block img1_CAMPS  big_camp_img_box left_camp_img_box' 
                                    imgclass = 'img-responsive camp1-img-size animation_img'
                                    }
                                    else if (index == 1){
                                        classN = 'anim_scroll_reveal_sequence_body5 col-md-6 col-sm-6 col-lg-6 no-padding img2_CAMPS d-inline-block small_camp_img_box right_camp_img_box'
                                        imgclass = 'animation_img'
                                        id = ''
                                    }
                                        return <div className={classN} ref="body5Item"> 
                                                <img className={imgclass} id = {id} src={url} alt=""/>
                                            </div>
                                })
                            }
                            <div className="anim_scroll_reveal_sequence_body5 col-md-4 col-sm-12 col-lg-4 no-padding d-inline-block absolute camp-box" ref="body5Item_text">
                                <div className="campaign-box"> 
                                    <div className="campaign-box1 text-center">
                                        <span className="anim_scroll_reveal_default campaign-heading">{this.props.camp3.name}</span>
                                        <div className="campaign-border mx-auto border-body5"></div>
                                        {
                                            this.props.camp3.custom_attributes.map((custom_attributes, index) => {
                                                if(index == 0){
                                                    return <div className = 'campaign_desc'><span className="anim_scroll_reveal_default">
                                                    {custom_attributes.value}
                                                    </span></div>
                                                }
                                            })
                                        }
                                        {/* href="/category?id=2" */}
                                        <a className="anim_scroll_reveal_default btn campaign-btn"> 
                                            <span className="campaign-btn-text">EXPLORE COLLECTION <span className="btn-line"></span></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        {/* <div class="campaign-color-box d-block d-sm-block"></div> */}
                    </div>

                   {/* body6 */}
                                    {this.props.product_data ? <ProductCarousel 
                                                                    product_details={this.props.product_data} 
                                                                    id={3} 
                                                                    handleModalClick={this.handleModalClick}
                                                                    color_array={this.props.all_colors} 
                                                                    size_array = {this.props.all_sizes}
                                                                    /> 
                                                            : null} 
                   {
                    this.props.modal_item? <Modal 
                                                isColorFetched={this.state.areColorsFetched} 
                                                isSizeFetched={this.state.areSizesFetched} 
                                                isVariantFetched={this.state.areVariantsFetched} 
                                                modal_item={this.props.modal_item}
                                                setColorFetchedTrue={() => this.setState({areColorsFetched: true})}
                                                setSizeFetchedTrue={() => this.setState({areSizesFetched: true})} 
                                                setVariantFetchedTrue={() => this.setState({areVariantsFetched: true})} 
                                                all_colors={this.props.all_colors} 
                                                all_sizes={this.props.all_sizes}/>
                                            : null
                    }

                    {/*************** body7 ****************/}
                <div className="container-fluid no-padding instagram-top-padding"> 
                    <div className="col-md-12 col-sm-12 col-lg-12 float-md-left float-md-left float-sm-left float-lg-left no-padding" >
                        <div class="body2-top anim_scroll_reveal_default">
                              <span> Instagram</span><br/>
                               <span class = 'thread-origin'>#ThreadOrigins</span>
                        </div>
                        <div className="row body7-margin insta-photo-bar">
                            { 
                                this.props.insta_images.map((insta_images, index)=>{
                                    if(index == 0){
                                    var div_className = 'col-md col col-sm body7-width margins_intial anim_scroll_reveal_sequence_body7 img_get insta-photo-div'
                                    var img_className = 'imgs_insta img_visible insta-photo'
                                    }
                                    else{
                                        var div_className = 'col-md col col-sm body7-width margins_intial anim_scroll_reveal_sequence_body7 d-lg-inline-block d-none insta-photo-div'
                                        var img_className = 'imgs_insta img_hide insta-photo'
                                    }
                                    var img_insta = insta_images.images.standard_resolution
                                        if(img_insta == undefined){}
                                        else{
                                            var url_img = `${img_insta.url}`
                                            return <div className = {div_className} ref="body7Item">
                                                    <img className = {img_className} src = {url_img}  alt=""/>
                                                    </div>
                                            }
                                })
                            }
                        </div>
                        <div className="col-md-12 margins_body6 details_b6 feature-bar">
                            <div class="row feature-row">
                            <div className="row col-md anim_scroll_reveal_sequence_body7_footer">
                                    <div className="offset-md-1 col-md-2">
                                        <img  src={secureImg}  alt=""/>
                                    </div>
                                    <div className="col-md-9 margins_b7">
                                        <span className= 'span_body6'>
                                            100% Secure Payments
                                        </span><br />
                                        <span className = "span_body7_desc">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </span>
                                    </div>
                                </div>
                                <div className="row col-md anim_scroll_reveal_sequence_body7_footer">
                                    <div className="offset-md-1  col-md-2">
                                        <img src={freeImg}  alt=""/>
                                    </div>
                                    <div className="col-md-9">
                                        <span className = 'span_body6'>
                                            Free Delivery Over Rs.2500
                                        </span><br />
                                        <span className = 'span_body7_desc'>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </span>
                                    </div>
                                </div>
                                <div className="row col-md anim_scroll_reveal_sequence_body7_footer">
                                    <div class="offset-md-1 col-md-2">
                                        <img src={returnImg}  alt=""/>
                                    </div>
                                    <div className="col-md-9">
                                        <span className = 'span_body6'>
                                            Easy Return Policies
                                        </span><br />
                                        <span className = 'span_body7_desc'>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </span>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </div>


                    {/* footer */}

                <div className="container-fluid no-padding">
                   <div className="col-md-12 col-sm-12 col-lg-12 float-md-left float-md-left float-sm-left float-lg-left no-padding">
                    <div className="col-md-12 col- val_initial foot_head d-lg-inline-block d-none " >
                        <div className="row col-md-12 val_initial foot_hides newsletter-container">
                            <div className="col-md-12 footer-newsletter-text">Join our Newsletter</div>
                            <div className="col-md-12 footer-newsletter-subscript-text">Join our newsletter to receive monthly offeres, updates about our designers and lots of other updates</div>
                            <div className="mx-auto email-form-padding">
                                <div className="email-form">
                                    <form onSubmit = {this.handleSubmit}>
                                    <input class="email anim_scroll_reveal_sequence_footer" placeholder="Enter Your Email Address" type="email" value = {this.state.value} onChange={this.handleChange}  />
                                    <button type = 'submit' class=" btn footer-submit-btn" >SUMBIT<span className='btn-line'></span>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 

                        <div id="home-footer-animation-block" class="col-md-12"> 
                            <div class="col-md-12 footer_bg_col footer_ht foot_hide"></div>
                            <Footer/>
                            <div class="col-md-12 col-xs-12 footer_bg2">
                            <p className = 'footer_end' >All Rights Reserved | 2018</p>
                            </div>
                        </div>
                    <div id="home-footer-visibility-start"></div>
                   </div>
                </div>
             </div>

    
        )
    }
}

HomePage.defaultProps = {
    camp1 :{
        custom_attributes: [{}],
        media_gallery_entries: [{}]
    },
    camp2 : {
        custom_attributes: [{}],
        media_gallery_entries: [{}]
    },
    camp3 : {
        custom_attributes: [{}],
        media_gallery_entries: [{}]
    },
    product_details : {
      items: [
        {
          custom_attributes:[{}]
        }
      ]
    },
     
    body3 : {
         items: [
                  {
                    custom_attributes:[{}]
                  }
         ]
    },
    body1 : {
        items: [
                 {
                   custom_attributes:[{}]
                 }
        ]
   },
   img_body1:{
     media_gallery_entries:[{}]
    },
    body5 : {
         items: [
         {
          custom_attributes:[{}]
         }
             ]
     },
    img_body3:{
      media_gallery_entries:[{}]
     }, 
    
    img_body5: {
      media_gallery_entries:[{}]
      },
     
    insta_images:[{
        images : {
            thumbnail:{}
        }
    }],
    modal_item: {
        name: "",
        url: "",
        sku: ""
    },
    all_colors: [],
    all_sizes: []

}

export default HomePage

