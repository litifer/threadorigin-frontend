import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import '../../css/login.css'
import {withRouter} from 'react-router-dom'
import ScrollMagic from 'scrollmagic'

import loginRight from '../../img/login_bg_2.png'
import loginleft from '../../img/login_bg_right.png'

class NewPass extends Component{

    constructor(props){
        super(props);
        this.state = {
            old_pass : "",
            new_pass : "",
            errors: {
                passwordError: null
            }
        };
        this.handleChangeOldPass = this.handleChangeOldPass.bind(this);
        this.handleChangeNewPass = this.handleChangeNewPass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    validatePassword = (sPassword) => {
        if (sPassword) {
            let regPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;
            if(!sPassword.match(regPassword)) {
                alert("New password should be at least 6 characters long including one capital, one small and one special character");
                return "New password should be at least 8 characters long including one capital, one small and one special character";
            }
            // if (sPassword !== this.state.old_pass) {
            //     return "Passwords do not match";
            // }

            return null;
        }
        return 'Password cannot be empty';

    };


    validateAllInputs = () => {
        const errors = {
            passwordError: null
        };

        errors.passwordError = this.validatePassword(this.state.new_pass);

        this.setState({errors})
    };


    componentDidUpdate() {

        if (this.props.isPasswordChanged ===true) {
            this.props.history.push('/dev');
        } else {
            const errors = {passwordError: null};
            errors.passwordError = "Incorrect Old Password";
            this.props.isPasswordChanged && this.props.isPasswordChanged.message ? this.setState({errors}) : null;
        }
    }

    handleChangeOldPass(event) {
        this.setState({old_pass: event.target.value})
    }
    handleChangeNewPass(event){
        this.setState({new_pass: event.target.value})
    }
    handleSubmit(){

        this.validateAllInputs();
        if (this.state.errors.passwordError) {
            return;
        }
        this.props.set_newpass(this.state.old_pass, this.state.new_pass)
    }
    
   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();


    if (!window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/login');
           // return <Redirect to="localhost:3000/login"/>
       }



    
    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){
        return(
            <div>
                <HeaderProduct />

                <div className="login-main-container row no-margin">
                    <div className="col-md-4 col-sm-4 col-12 d-inline-block no-padding">
                        <img src={loginleft} />                         
                    </div>
                    <div className="col-md-5 col-sm-5 d-inline-block mx-auto no-padding text-position">
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 d-inline-block no-padding login-hide">&nbsp;</div>
                        <div className="col-md-9 col-sm-9 offset-md-1 col-12 d-inline-block login-middle-text no-padding">
                            Set Up New Password
                        </div>                        
                    </div>
                    <div className="col-md-3 col-sm-3 d-inline-block no-padding login-hide">
                        <img className="float-right login-right-img" src={loginRight} />
                    </div> 
                </div>

                <div className="row no-margin col-md-12 no-padding absolute login-box-top">
                    <div className="col-md-7 col-sm-7 col-11 offset-md-3 no-padding login-bg-color margin-left">
                    <div className="login-transform-text absolute login-hide">Login<span className="login-transform-line"></span></div>
                        <div className="col-md-8 col-sm-8 col-11 mx-auto no-padding">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding mobile-text-center">
                                <span className="login-text">If you have an account already registered with us, please login in with your credentials</span>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            {/*<form onSubmit = {this.handleSubmit}>*/}
                               <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                   <span className="login-label-text">Old Password:</span>
                                   <input name = 'old_pass' type="password" className="form-control login-input" placeholder="brucebanner@gmail.com" onChange={this.handleChangeOldPass}/>
                               </div>
                               <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                               <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                   <span className="login-label-text">New Password:</span>
                                   <input name = 'new_pass' type="password" className="form-control login-input" placeholder="brucebanner@gmail.com" onChange={this.handleChangeNewPass} />
                                   {this.state.errors.passwordError ? <p style={{color: 'red'}}>{this.state.errors.passwordError}</p> : null}

                               </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                                <div className="mx-auto col-md-12 col-sm-12 col-12 d-inline-block text-center">
                                    <button onClick={this.handleSubmit} className="col-md-6 col-sm-6 mx-auto col-8 btn checout-btn"><span className="style12_prod_text">SET NEW PASSWORD<span className="login-btn-line"></span></span></button>
                                </div>
                            {/*</form>*/}
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                         
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">
                                <div className="login-border mx-auto"></div>
                            </div> 
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding text-center">
                                <span className="login-bottom-text">If you are a first time user, click here to <a href="/register" className="login-bottom-text-bold">Register as a User</a></span>
                            </div>   
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        </div>
                    </div>
                </div>

                {/* <div id="footer-visibility-start" className = "foot_start">
                </div>
                <div id="product-footer" className="col-md-12 product-footer" >
                    <div className="col-md-12 footer_bg_col">&nbsp;</div>
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className = "footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div> */}

            </div>
        )
    }
}

export default withRouter(NewPass);