import React, {Component} from 'react'
import * as  config_ from '../../config.js';
import SimpleZoom from 'react-simple-zoom'
import Footer from '../components/footer.controller';
import Header1 from '../components/header_product.controller';
import ProductCarousel from '../components/productCarousel.controller';
import ProductImages from '../components/productImages.controller';
import BODY2 from '../../containers/body2.container';
import ScrollMagic from 'scrollmagic'
import * as config from '../../config.js';
import product_header from '../../img/product_header.png';
import product_line1 from '../../img/product_line1.png';
import {a} from 'react-router-dom';
import size_icon from '../../img/size_icon.png';
import Bitmap_india from '../../img/Bitmap-india.png';
import product_dropdown from '../../img/product_dropdown.png';
import product_search from '../../img/product_search.png';
import product_cart from '../../img/product_cart.png';
import product_icon from '../../img/product_icon.png';
import product_body1_img1 from '../../img/product_body1_img1.png';
import product_body1_img2 from '../../img/product_body1_img2.png';
import product_body1_img3 from '../../img/product_body1_img3.png';
import product_body1_img4 from '../../img/product_body1_img4.png';
import product_body2 from '../../img/product_body2.png';
import keyboard_left_arrow_button from '../../img/keyboard-left-arrow-button.png';
import keyboard_down_arrow_button from '../../img/keyboard-down-arrow-button.png';
import oval1 from '../../img/Oval 1.png';
import oval2 from '../../img/Oval 2.png';
import Oval from '../../img/Oval.png';
import item1 from '../../img/item1.png';
import item2 from '../../img/item2.png';
import item3 from '../../img/item3.png';
import item4 from '../../img/item4.png';
import Line_2 from '../../img/Line 2.png';
import secureImg from '../../img/secure.png'
import freeImg from '../../img/free.png'
import returnImg from '../../img/return.png'

import '../../css/product_header.css';
import '../../css/product1_body.css';
import '../../css/body2.css';

import Modal from "../components/modal.controller"



const {hasData, find, attribute_array_creator, findProductById} = require('../../helpers/general')

const Urls = {
    dresses: "/category?id=1",
    bottoms: "/category?id=2",
    tops: "/category?id=3",
    lookBooks: "/category?id=4",
    collections: "/category?id=5",
    sandook: "/category?id=6",
    cart: "/cart",
    product: '/product'
}

class Product1 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            areColorsFetched: false,
            areSizesFetched: false,
            areVariantsFetched: false,
            products_updated: false,
        }
        
        this.addToCart = this.addToCart.bind(this)
        this.handleVariant = this.handleVariant.bind(this)
        this.handleModalClick = this.handleModalClick.bind(this)
    }

/*     componentWillMount(){
        window.onload = function(){
          setTimeout(function() {
            document.getElementById('loader').style.display = 'block'
              },3000);  
        }
    }
*/
    componentDidMount() {

        // this.props.fetchprodBody();

        // var result = String(window.location);
        // var index = result.indexOf('=');
        // var sku = result[index + 1]
        let query = new URLSearchParams(this.props.location.search);
        let sku = query.get('id');
        
        this.props.fetchDetails(sku);
        this.props.getColor(sku);
        this.props.getSize(sku);
        this.props.getProductVariants(sku);

        // this.props.get_initial_price(sku);
        // this.props.getPrice(sku);

        let scroll2Controller = new ScrollMagic.Controller();

        //product description scroll
        new ScrollMagic
            .Scene({triggerElement: "#scroll-start", duration: 1320})
            .setPin("#product-scroll-2")
            .triggerHook("onLeave")
            .offset(-120) 
            .addTo(scroll2Controller)

        //product description background scroll
        // new ScrollMagic
        //     .Scene({triggerElement: "#scroll-start", duration: 300})
        //     .setPin("#product-scroll")
        //     .triggerHook("onLeave")
        //     .addTo(scroll2Controller)
        //p-body1-container product description transparent background after scroll
        // new ScrollMagic
        //     .Scene({triggerElement: "#body1-container-animation-trigger"})
        //     .setClassToggle("#product-scroll-2", "change-body2-background")
        //     .triggerHook("onEnter")
        //     .offset(1250)
        //     .addTo(scroll2Controller)

        // new ScrollMagic
        //     .Scene({triggerElement: "#body2-container-animation-trigger"})
        //     .setClassToggle("#product-scroll-2", "animation-fade-product-description")
        //     .triggerHook("onEnter")
        //     .offset(0)
        //     .addTo(scroll2Controller)

        //footer scroll
        // let scrollfooterController = new ScrollMagic.Controller();

        // new ScrollMagic
        //     .Scene({triggerElement: "#footer-visibility-start"})
        //     .triggerHook("onEnter")
        //     .setClassToggle("#product-footer", "footer-visible")
        //     .addTo(scrollfooterController)
        // .setClassToggle("#product-footer","footer-visible")

    }

    handleModalClick(product_id){
        this.setState({areColorsFetched: false, areSizesFetched: false, areVariantsFetched: false})
        this.props.changeModal(findProductById(product_id, this.props.related_products.items));
    }

    handleChangeColor(color) {
        this.props.changeColor(color);
    }

    handleChangeSize(options) {
        this.props.changeSize(options);
    }
    goto_cart(qty,size,col,product_get,col_array, size_array){
        this.props.goto_cart(qty,size,col,product_get,col_array, size_array);
    }

    handleVariant(options){
        let params = {
            attribute_code : (options.type === 'CHANGE_COLOR')? 'color' : 'size',
            size : (options.type === 'CHANGE_SIZE')? options.value : this.props.size,
            color: (options.type === 'CHANGE_COLOR')? options.value : this.props.color,
            value: options.value
        }
        this.props.changeVariant(this.props.variants, params);
    }

    addToCart(){
        this.props.addToCart(this.props.sku, this.props.qty, this.props.color_attribute_id, this.props.color, this.props.size_attribute_id, this.props.size)
    }

    addToWishlist = () =>{
        this.props.addItemToWishlist(this.props.sku, this.props.qty, this.props.color_attribute_id, this.props.color, this.props.size_attribute_id, this.props.size, this.props.name, this.props.price);
    };

    componentDidUpdate(){
        console.log("%c props", "color:red")
        console.log(this.props)
        if(this.props && this.props.product_get.product_links && this.props.product_get.product_links.length > 0 && !this.props.carousel_products_obtained){
            this.props.product_get.product_links.map((related_product)=> {
                this.props.getRelatedProduct(related_product.linked_product_sku);
            })
        }
        console.log("%c props", "color:red");
        console.log(this.props);
        if (this.props.itemAdded === false) {
            alert('item could not be added to wishlist');
            // window.location.reload();
        } else if (this.props.itemAdded === true) {
            alert('item added to wishlist');
            // window.location.reload();
        }

    }
    render() {
       return (
            <div id='product_page'>
                <Header1/>
                <div className="container-fluid no-padding main-container">
                    <div className='bootstrap_container'>

                    {
                    hasData(this.props.product_get.media_gallery_entries).map((media_gallery_entries, index) => {
                            if (index == 0) {
                                let url = `${config.IMG_URL}${media_gallery_entries.file}`
                                return  <div className="product-main-img-box" id="none">
                                            <img className='img_wid' src={url}/>
                                        </div>
                            }
                        })
                }
                        {/* <img className='img_wid' id="none" src={product_header}/> */}
                        <div className='top-right' id="none">
                            <div className="col-md-12 row no-padding no-margin product-landing-top-margin">
                                <div id="scroll-start" className="col-md-8">
                                    <div className="col-md-6 product-body1-margin1">
                                        <div className="col-md-12 product-backtodress-border style2_prod text-right no-padding">
                                        {
                                            hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                                    
                                                    if (custom_attributes.attribute_code == 'category_ids') {
                                                        return <a href={"/category/?id="+custom_attributes.value} className='font_family1 color1 font_wt1 font_size2'>Back to Dresses</a>
                                                    }
                                                })
                                        }
                                            {/* <a href={this.props.} className='font_family1 color1 font_wt1 font_size2'>Back to Dresses</a> */}
                                        </div>
                                    </div>
                                </div>
                                {/* ------ */}


                                <div className="col-md-4 val_initial product-info ">
                                    <div id="product-scroll " className="product-header-rectangle style3_prod">
                                        <div id="product-scroll-2" className="product-info-width">
                                            <div className="col-md-12 style4_prod">
                                                <div className="row col-md-11 offset-md-1 style5_prod pdng_rt_left">
                                                    <div className="col-md-8 text-left">
                                                        <span className='font_family2 font_wt3 color1 style_span2_prod'>
                                                            {this.props.name}
                                                        </span>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <span
                                                            className='text-center color1 font_family1 font_wt1 font_size4 style_span3_prod'>
                                                            <span className='font_family3 font_size1 font_wt4'>Rs.</span>{this.props.price}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-10 offset-md-1 style8_prod padding-right">
                                                    {
                                                        hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                                               
                                                                if (custom_attributes.attribute_code == 'description') {
                                                                    return <p className='pstyle_common pstyle3_prod'>{custom_attributes.value}</p>
                                                                }
                                                            })
                                                    }
                                                </div>
                                                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                <div className="row col-md-10 offset-md-1 pdng_rt_left">
                                                    <div className="col-md-5">
                                                        <p className='pstyle_common pstyle2_prod'>Fabrication:</p>
                                                    </div>
                                                    <div className="col-md-7">
                                                        {
                                                            hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                                                    if (custom_attributes.attribute_code == 'fabrication') {
                                                                        return <p className='pstyle_common pstyle3_prod'>{custom_attributes.value}</p>
                                                                    }
                                                                })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row col-md-10 offset-md-1 pdng_rt_left">
                                                    <div className="col-md-5">
                                                        <p className='pstyle_common pstyle2_prod'>Product Reference:</p>
                                                    </div>
                                                    <div className="col-md-7">
                                                        {
                                                            hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                                                    if (custom_attributes.attribute_code == 'product_reference') {
                                                                        return <p className='pstyle_common pstyle3_prod'>{custom_attributes.value}</p>
                                                                    }
                                                                })
                                                        }
                                                    </div>
                                                </div>
                                                <div className=" row col-md-10 offset-md-1 pdng_rt_left">
                                                    <div className="col-md-5">
                                                        <p className='pstyle_common pstyle2_prod'>Maintenance:</p>
                                                    </div>
                                                    <div className="col-md-7">
                                                        {
                                                            hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {

                                                                    if (custom_attributes.attribute_code == 'maintenance') {
                                                                        return <p className='pstyle_common pstyle3_prod'>{custom_attributes.value}</p>
                                                                    }
                                                                })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-md-10 offset-md-1 style_prod"></div>
                                            </div>
                                            <div className="col-md-12  no-padding position-product-desc">
                                            <div className="collapse1 collapse show product-detail-margin">
                                            <br /><br />
                                                <div className="row col-md-10 offset-md-1">
                                                    <div className="col-md-2 padding-left text-left "><br/>
                                                        <span className="pstyle_common pstyle2_prod">Size:</span>
                                                    </div>
                                                    <div className="col-md-4 no-padding text-center style91_prod">

                                                        {
                                                           hasData(this.props.size_list).map((options, index) => {
                                                            var divclassN = 'float-left product-margin-circles'
                                                            
                                                            // Defining Classes 
                                                            if(this.props.size == undefined){
                                                                if(index == 0){ divclassN = "float-left product-margin-circles product-size-circle"}
                                                            }
                                                            else{
                                                                if(this.props.size == options.value){
                                                                    divclassN = "float-left product-margin-circles product-size-circle"
                                                                }
                                                            }

                                                                let current = hasData(this.props.all_sizes).find(element => {
                                                                    return element.value === options.value
                                                                })

                                                                options = current;
                                                            
                                                             var spanclassN =  "product-size-text"  ; 
                                                             var get_opt = ""; 
                                                             // Defining the labels 
                                                             if(options === undefined || options.label === undefined){}
                                                             else if(options.label == "Extra Small"){get_opt = "ES"}
                                                             else if(options.label == "Extra Large"){get_opt = "EL" } 
                                                             else{
                                                                if(options.label == undefined){}
                                                                    else {
                                                                        get_opt = options.label.charAt(0)
                                                                    }
                                                             }
                                                                return <div className = {divclassN} onClick = {event => this.handleVariant({type: 'CHANGE_SIZE', value:options.value})}>
                                                                         <span className = {spanclassN}>
                                                                             {get_opt}
                                                                        </span>
                                                                       </div>
                                                                        })
                                                        }
                                                    </div>
                                                    <div className="col-md-6 style9_prod">
                                                        <span className="product-size-guide-padding"><img src={size_icon}/></span>
                                                        <span className='font_family1 font_size1 text-center color1 style_span5_prod'>Size Guide</span>
                                                    </div>
                                                </div>
                                                <div className="row col-md-10 offset-md-1 margins2_prod">
                                                    <div className="col-md-2 pdng_rt_left"><br/>
                                                        <p className='pstyle_common pstyle2_prod'>Color:</p>
                                                    </div>
                                                    <div className="col-md-4 pdng_rt_left style91_prod">
                                                        {
                                                         hasData(this.props.color_list).map((options,index)=> {
                                                            var divclassN = 'float-left product-margin-circles' , id = ''
                                                            if(this.props.color == undefined){
                                                                if(index == 0){
                                                                   divclassN = "product-size-circle float-left product-margin-circles"
                                                                }
                                                            }
                                                            else {
                                                                if(this.props.color == options.value){
                                                                   divclassN = 'product-size-circle float-left product-margin-circles'
                                                                }
                                                            }

                                                            let current = hasData(this.props.all_colors).find(element => {
                                                                return element.value === options.value
                                                            })

                                                            options = current;
                                                             var spanclassN =  "product-color-circle"; 
                                                             if(options === undefined || options.label == undefined){}
                                                            else{
                                                                var ind = options.label.indexOf('-');
                                                                var get_color = options.label.slice(0,ind)
                                                                var val_color = options.label.slice(ind+1, options.label.length)
                                                            }

                                                             return <div className = {divclassN} onClick = {event => this.handleVariant({type: 'CHANGE_COLOR', value:options.value})}>
                                                                     <div  className = {spanclassN} style = {{'background-color':val_color}} ></div>
                                                                    </div>
                                                            })
                                                        }
                                                    </div>

                                                    
                                                    <div className="row col-md-10 offse-md-1 margins_prod">
                                                        <div className="col-md-2 pdng_rt_left"><br/>
                                                            <p className=' pstyle_common pstyle2_prod'>Qty:</p>
                                                        </div>
                                                        <div className="col-md-4 product-qty-margin pdng_rt_left">
                                                            <div className="col-md-4 btn style10_prod border1">
                                                                <span className='color1 font_wt3 font_size4' onClick={this.props.decrement}>-</span>
                                                            </div>
                                                            <div className="col-md-4 btn style10_prod border1 product-qty-bg product-qty-padding-top">
                                                                <span className='color1 product-counter-font'>{this.props.qty}</span>
                                                            </div>
                                                            <div className="col-md-4 btn style10_prod bg_color1 border1">
                                                                <span className='color1 font_wt3 font_size4' onClick={this.props.increment}>+</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-10 offset-md-1 margins2_prod">
                                                    <button className="col-md-5 btn text-center font_family1 product-btn-margin-left font_size2 font_wt2 style11_prod" onClick={this.addToCart} data-toggle="modal" data-target="#product_popup">
                                                        <span className='style11_prod_text'>ADD TO CART <span className='btn-line'></span></span>
                                                    </button>
                                                    <button className="col-md-5 btn style12_prod" onClick={this.addToWishlist}>
                                                        <span></span>
                                                        <span className='style12_prod_text'><i class="heart-margin fa fa-heart-o fa-1x" aria-hidden="true"></i>Add to Wishlist</span>
                                                    </button>
                                                   {  
                                                      <div className="modal" id="product_popup">
                                                         <div className="modal-dialog popup-dialogue">
                                                            <div className="modal-content">
                                                                <div className="modal-header popup-header">
                                                                    <span className="modal-title popup-header-text">{this.props.cart_message}</span>
                                                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                                                </div>
                                                                <div className="modal-body">
                                                                   <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                                   <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                                                        <div className="col-md-2 col-sm-2 float-md-left float-sm-left no-padding">

                                                                            {
                                                                                hasData(this.props.product_get.media_gallery_entries).map((media_gallery_entries, index) => {
                                                                                            if (index == 3) {
                                                                                                        let url = `${config.IMG_URL}${media_gallery_entries.file}`
                                                                                                        return <img  src={url} height="116px" width="76px"/>
                                                                                            }  
                                                                                    })
                                                                            }
                                                                        </div>
                                                                        <div className="col-md-7 col-sm-7 float-md-left float-sm-left text-left">
                                                                            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                                                                                <span className="popup-item-text">{this.props.product_get.name}</span>
                                                                            </div>
                                                                            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">


                                                                                    {
                                                                                        hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                                                                                if (custom_attributes.attribute_code == 'description') {
                                                                                                    return <span className="popup-item-desc">
                                                                                                    {custom_attributes.value}
                                                                                                    </span>
                                                                                                }
                                                                                            })
                                                                                    }
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-md-3 col-sm-3 float-md-left float-sm-left text-right">
                                                                            <span className="popup-currency">Rs.&nbsp;</span><span className="popup-price">{this.props.price}</span>
                                                                        </div>
                                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                                                                            <a className="col-md-3 btn float-md-right popup-btn style12_prod" href="/checkout">
                                                                                <span className='style12_prod_text'>CHECKOUT <span className='btn-line'></span></span>
                                                                            </a>
                                                                            <a className="col-md-3 btn btn-default popup-btn float-md-right style11_prod" type="submit" href="/cart"> 
                                                                                <span className='style11_prod_text'>VIEW CART <span className='btn-line'></span></span>
                                                                            </a>
                                                                            
                                                                        </div>
                                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>

                                                                   </div>
                                                                </div>
                                                            </div>
                                                         </div>
                                                    </div> 
                                                   }

                                                </div>
                                                </div>
                                                {/* <div className="row col-md-12 style13_prod"><br/>
                                                <div className="col-md-8 .font_family1 font_size3 font_wt2 color1 ">
                                                    <span className='style_span9_prod'>View Returns and Shipping Policies</span>
                                                </div>
                                                <div className="col-md-4 pull-right style15_prod">
                                                    <span className='style_span9_prod'>+</span>
                                                </div><br/>
                                            </div> */}
                                            <div class="row col-md-12 mx-auto  product-collapsed product-return-policy-bg">
                                                <button className='btn btn-link product-collapse-btn collapsed product-text' type="button" data-toggle="collapse" data-target=".collapse1" aria-expanded="false" aria-controls="collapseExample">
                                                    <span className="style_span9_prod product-collapse-plus-text">View Returns and Shipping Policies</span>
                                                </button>
                                                {/* <button class="btn btn-default plusBtn" type="button" data-toggle="collapse" data-target=".collapse1" aria-expanded="false" aria-controls="collapseExample">
                                                    +
                                                </button> */}
                                                {/* <div className="card">
                                                    <div class="card-header" id="headingOne">
                                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target=".collapse1" aria-expanded="true" aria-controls="collapseOne">
                                                            <h5 class="mb-0">
                                                            View Returns and Shipping Policies                
                                                            </h5>
                                                        </button>
                                                    </div>
                                                </div> */}
                                            </div>
                                                <div class="collapse collapse1" id="collapseShipping">
                                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding product-policy-padding">
                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                                            <div className="col-md-2 col-sm-2 float-md-left float-sm-left">
                                                                <img src={returnImg} />
                                                            </div> 
                                                            <div className="col-md-10 col-sm-10 float-md-left float-sm-left text-left">
                                                                <span className="product-collapse-heading">7 Day Return Policy *</span><br />
                                                                <span className="product-collapse-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                                                            </div>                                                        
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left product-policy-padding">
                                                            <div className="col-md-2 col-sm-2 float-md-left float-sm-left">
                                                                <img src={secureImg} />
                                                            </div> 
                                                            <div className="col-md-10 col-sm-10 float-md-left float-sm-left text-left"> 
                                                                <span className="product-collapse-heading">7 Day Return Policy *</span><br />
                                                                <span className="product-collapse-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                                                            </div>                                                        
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                                            <div className="col-md-2 col-sm-2 float-md-left float-sm-left">
                                                                <img src={freeImg} />
                                                            </div> 
                                                            <div className="col-md-10 col-sm-10 float-md-left float-sm-left text-left">
                                                                <span className="product-collapse-heading">7 Day Return Policy *</span><br />
                                                                <span className="product-collapse-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                                                            </div>                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>



                <div id="demo" className="carousel slide product-carousel" data-ride="carousel">

                    {/* <!-- Indicators --> */}
                    <ol class="carousel-indicators product-carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" className="active"></li>
                        <li data-target="#demo" data-slide-to="1" className = 'indicators'></li>
                        <li data-target="#demo" data-slide-to="2" className = 'indicators'></li>
                        <li data-target="#demo" data-slide-to="3" className = 'indicators'></li>
                    </ol>

                    {/* <!-- The slideshow --> */}
                    <div class="carousel-inner">

                        {
                            hasData(this.props.product_get.media_gallery_entries).map((media_gallery_entries, index) => {
                                    if (index > 0 && index < 5) {
                                        let url = `${config.IMG_URL}${media_gallery_entries.file}`
                                        if(index == 1){
                                            return <div className="carousel-item active">
                                            <img width="100%" height="100%" src={url}/>
                                        </div>
                                        }
                                        return <div className="carousel-item">
                                            <img width="100%" height="100%" src={url}/>
                                        </div>
                                    }
                                })
                        }
                        
                    </div>
                </div>

                <div className="product-carousel container-fluid product-mobile-top-padding"> 
                    <div className="row">
                        <div className="col-sm-10 col-10 pr-0 pl-5 product-mobile-border-right">
                            <span className="product-mobile-heading">{this.props.product_get.name}</span><br />

                            {
                                hasData(this.props.product_get.custom_attributes).map((custom_attributes, index) => {
                                        
                                        if (custom_attributes.attribute_code == 'description') {
                                            return  <span className="product-mobile-text">{custom_attributes.value}</span>
                                        }
                                    })
                            }
                            {/* <span className="product-mobile-text">Dinosaurs love this super soft sweater dress, except T-Rex, his arms just don’t fill it out.</span> */}
                        </div>
                        <div className="col-sm-2 col-2 product-mobile-top-padding">
                            <span className="product-mobile-rs">Rs.</span><span className="product-mobile-price">{this.props.price}</span>
                        </div>
                    </div>
                    <div className="row ml-3 product-mobile-top-padding">
                        <div className="col-sm-12 col-12 pl-0">
                            <div className="col-sm-2 col-2 float-sm-left float-left no-padding product-mobile-top-padding">
                                <span className="product-mobile-text">Size:</span>
                            </div>
                            <div className="col-sm-10 col-10 text-center style91_prod float-sm-left float-left no-padding">
                            {
                                    hasData(this.props.size_list).map((options, index) => {
                                    var divclassN = 'float-left product-margin-circles'
                                    
                                    // Defining Classes 
                                    if(this.props.size == undefined){
                                        if(index == 0){ divclassN = "float-left product-margin-circles product-size-circle"}
                                    }
                                    else{
                                        if(this.props.size == options.value){
                                            divclassN = "float-left product-margin-circles product-size-circle"
                                        }
                                    }

                                        let current = hasData(this.props.all_sizes).find(element => {
                                            return element.value === options.value
                                        })

                                        options = current;
                                    
                                        var spanclassN =  "product-size-text"  ; 
                                        var get_opt = ""; 
                                        // Defining the labels 
                                        if(options === undefined || options.label === undefined){}
                                        else if(options.label == "Extra Small"){get_opt = "ES"}
                                        else if(options.label == "Extra Large"){get_opt = "EL" } 
                                        else{
                                        if(options.label == undefined){}
                                            else {
                                                get_opt = options.label.charAt(0)
                                            }
                                        }
                                        options.value && index === 0 ? this.handleVariant({type: 'CHANGE_SIZE', value:options.value}) : null;
                                        return <div className = {divclassN} onClick = {event => this.handleVariant({type: 'CHANGE_SIZE', value:options.value})}>
                                                    <span className = {spanclassN}>
                                                        {get_opt}
                                                </span>
                                                </div>
                                                })
                                }
                            </div>
                        </div>
                        <div className="col-sm-12 col-12 product-mobile-top-padding">
                            <div className="col-sm-2 col-2 float-left float-sm-left no-padding product-mobile-top-padding">
                                <span className="product-mobile-text">Color:</span>
                            </div>
                            <div className="col-sm-10 col-10 float-left text-center style91_prod float-sm-left no-padding">
                                {
                                    hasData(this.props.color_list).map((options,index)=> {
                                    var divclassN = 'float-left product-margin-circles' , id = ''
                                    if(this.props.color == undefined){
                                        if(index == 0){
                                           divclassN = "product-size-circle float-left product-margin-circles"
                                        }
                                    }
                                    else {
                                        if(this.props.color == options.value){
                                           divclassN = 'product-size-circle float-left product-margin-circles'
                                        }
                                    }

                                    let current = hasData(this.props.all_colors).find(element => {
                                        return element.value === options.value
                                    })

                                    options = current;
                                        var spanclassN =  "product-color-circle"; 
                                        if(options === undefined || options.label == undefined){}
                                    else{
                                        var ind = options.label.indexOf('-');
                                        var get_color = options.label.slice(0,ind)
                                        var val_color = options.label.slice(ind+1, options.label.length)
                                    }

                                        return <div className = {divclassN} onClick = {event => this.handleVariant({type: 'CHANGE_COLOR', value:options.value})}>
                                                <div  className = {spanclassN} style = {{'background-color':val_color}} ></div>
                                            </div>
                                    })
                                }
                            </div>
                        </div>
                        <div className="col-sm-12 col-12 product-mobile-top-padding">
                            <div className="col-sm-2 col-2 float-left float-sm-left no-padding product-mobile-top-padding">
                                <span className="product-mobile-text">Qty:</span>
                            </div>
                            <div className="col-sm-10 col-10 text-center float-left float-sm-left no-padding ">
                                <div className="col-sm-4 col-4 no-padding">
                                    <div className="col-sm-4 col-4 btn style10_prod border1">
                                        <span className='color1 font_wt3 font_size4' onClick={this.props.decrement}>-</span>
                                    </div>
                                    <div className="col-sm-4 col-4 btn style10_prod border1 product-qty-bg product-qty-padding-top">
                                        <span className='color1 product-counter-font'>{this.props.qty}</span>
                                    </div>
                                    <div className="col-sm-4 col-4 btn style10_prod bg_color1 border1">
                                        <span className='color1 font_wt3 font_size4' onClick={this.props.increment}>+</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-12 product-mobile-top-padding">
                            <button className="col-sm-5 col-5 btn text-center font_family1 font_size2 font_wt2 style11_prod" onClick={this.addToCart}>
                                <span className='style11_prod_text'>ADD TO CART <span className='btn-line'></span></span>
                            </button>
                            <button className="col-sm-5 col-5 btn style12_prod" onClick={this.props.add_wish}>
                                <span></span>
                                <span className='style12_prod_text'><i class="heart-margin fa fa-heart-o fa-1x" aria-hidden="true"></i>Add to Wishlist</span>
                            </button>
                        </div>
                    </div>
                </div>







                <div className="row absolute product-top-icons" id="none"> 
                    <div className="col"><i class="fa fa-facebook product-icons-styles" aria-hidden="true"></i></div>
                    <div className="col"><i class="fa fa-pinterest-p product-icons-styles" aria-hidden="true"></i></div>
                    <div className="col"><i class="fa fa-twitter product-icons-styles" aria-hidden="true"></i></div>
                </div>

                {/* <div id="body1-container-animation-trigger" id="none" className="p-body1-container"> 
                    
                    {
                        this.props.product_get.media_gallery_entries.map((media_gallery_entries, index) => {
                                if (index > 0 && index < 5) {
                                    if (index == 3) {
                                        var classN = `p-body1-container-top`
                                    } else if (index == 4) {
                                        var classN = `p-body1-container-bottom`
                                    } else {
                                        var classN = `p-body1-container-left${index}`
                                    }
                                    let imgclass = `img_responsive style1_body_prod margin${index - 1}_body_prod`
                                    let url = `${config.IMG_URL}${media_gallery_entries.file}` */}
                                     {/* return <div className={classN}> */}
                                        {/* <img className={imgclass} src={url} data-zoom-image={url} /> */}
                                        {/* return <SimpleZoom
                                            className={imgclass}
                                            thumbUrl={url}
                                            fullUrl={product_header}
                                            zoomScale={3.6}
                                            onExitTimeout={2000}
                                        />
                                    // </div>
                                }
                            })
                    }
                    <div className="p-body1-container-right"></div>
                </div> */}
                     <ProductImages product_get={this.props.product_get}/>

                {
                    hasData(this.props.product_get.media_gallery_entries).map((media_gallery_entries, index) => {
                            if (index == 5) {
                                let url = `${config.IMG_URL}${media_gallery_entries.file}`
                                return <div className="col-md-12 product-body3-img no-padding margin4_body_prod" id="none">
                                    <img  src={url} style={{"width" : "100%","height":"911px"}}/>
                                </div>
                            }
                        })
                }
                    {
                        console.log(this.props.product_details)
                    }
                <div class="container-fluid">    
                     <ProductCarousel product_details={this.props.related_products} color_array={this.props.all_colors} size_array={this.props.all_sizes} handleModalClick={this.handleModalClick}/> 
                </div>

                {
                    this.props.modal_item? <Modal 
                                                isColorFetched={this.state.areColorsFetched} 
                                                isSizeFetched={this.state.areSizesFetched} 
                                                isVariantFetched={this.state.areVariantsFetched} 
                                                modal_item={this.props.modal_item}
                                                setColorFetchedTrue={() => this.setState({areColorsFetched: true})}
                                                setSizeFetchedTrue={() => this.setState({areSizesFetched: true})} 
                                                setVariantFetchedTrue={() => this.setState({areVariantsFetched: true})} 
                                                all_colors={this.props.all_colors} 
                                                all_sizes={this.props.all_sizes}/>
                                            : null
                }

                 {/* <div id="footer-visibility-start" className="foot_start"></div> */}
                 <div id="product-footer" className="col-md-12 product-footer">
                    {/* <div className="col-md-12 footer_bg_col">&nbsp;</div> */}
                    <Footer/>
                    <div className="col-md-12 footer_bg2">
                        <p className="footer_end">All Rights Reserved | 2018</p>
                    </div>
                </div>

            </div>
        )
    }
}

Product1.defaultProps = {
    product_get: {
        custom_attributes: [],
        extension_attributes: {
            stock_item: {},
            configurable_product_options: [{}]
        },

        media_gallery_entries: []
    },
    related_products : {
        items: [
          {
            custom_attributes:[{}]
          }
        ]
    },
    color_val: {
        options: [{}]
    },
    size_val: {
        options: [{}]
    },
    cart_message: ''
}

export default Product1