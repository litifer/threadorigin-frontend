import './../../../../css/PandC/TermsContent.css';
import React from 'react'
import { ContactInfo } from '../../../../consts';

const TermsContent = () => {

return (
    <div className="main-class-TermsContent">
    <h3 className="heading-class-TermsContent"> INTRODUCTION </h3>
    <p className="p-class-TermsContent">The domain name ThreadOrigin.com is registered in the name of Thread Origin FASH-TECH PRIVATE LIMITED which is a registered company under the Companies Act 1956. Its registered office is at 6th Floor, Plot No.4 Sector-44 GURGAON HR 122004 IN(CIN U74999HR2018PTC073591.). THREAD ORIGIN is in the business of selling Desiger  wear for day to day lifwe which are pocket frendily . </p>

    <h3 className="heading-class-TermsContent"> RIGHT TO CHANGE </h3>
    <p className="p-class-TermsContent"> THREAD ORIGIN reserves the sole right to update or modify these Terms and Conditions at any time without prior notice. For this reason, we encourage you to review these Terms and Conditions every time you purchase products from us or use our Web Site. </p>

    <h3 className="heading-class-TermsContent">PRIVACY</h3>
    <p className="p-class-TermsContent"> We view protection of your privacy as a very important principle. We understand clearly that You and Your Personal Information is one of our most important assets. We store and process Your Information including any sensitive financial information collected (as defined under the Information Technology Act, 2000), if any, on computers that may be protected by physical as well as reasonable technological security measures and procedures in accordance with Information Technology Act 2000 and Rules there under. If you object to our current Privacy Policy and your information being transferred or used in this way please do not use Website. </p>

    <h3 className="heading-class-TermsContent">ELECTRONIC COMMUNICATIONS </h3>
    <p className="p-class-TermsContent">When you use any of our Services, or send e-mails to us, you are communicating with us electronically. You by using our services consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this site or through other Services. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing. The legal requirements would be in conformity with the Indian Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. </p>

    <h3 className="heading-class-TermsContent"> INTELLECTUAL PROPERTY RIGHTS </h3>
    <p className="p-class-TermsContent">Unless otherwise indicated or anything contained to the contrary or any proprietary material owned by a third party and so expressly mentioned, THREAD ORIGIN owns all Intellectual Property Rights to and into the Website, including, without limitation, any and all rights, title and interest in and to copyright, related rights, patents, utility models, trademarks, trade names, service marks, designs, know-how, trade secrets and inventions (whether patentable or not ), goodwill, source code, meta tags, databases, text, content, graphics, icons, and hyperlinks. You acknowledge and agree that you shall not use, reproduce or distribute any content from the Website belonging to THREAD ORIGIN without obtaining authorization from it.

    User shall not upload post or otherwise make available on the Site any material protected by copyright, trademark or other proprietary right without the express permission of the owner of the copyright, trademark or other proprietary right. THREAD ORIGIN does not have any express burden or responsibility to provide the Users with indications, markings or anything else that may aid the User in determining whether the material in question is copyrighted or trademarked. User shall be solely liable for any damage resulting from any infringement of Copyrights, trademarks, proprietary rights or any other harm resulting from such a submission. By submitting material to any public area of the Site, User warrants that the owner of such material has expressly granted THREAD ORIGIN the royalty-free, perpetual, irrevocable, non- exclusive right and license to use, reproduce, modify, adapt, publish, translate and distribute such material (in whole or in part) worldwide and/or to incorporate it in other works in any Form, media or technology now known or hereafter developed for the full term of any copyright that may exist in such material. User also permits any other end user to access, view, and store or reproduce the material for that end user’s personal use. User hereby grants THREAD ORIGIN, the right to edit, copy, publish and distribute any material made available on the Site by the User. The foregoing provisions of Section 25 apply equally to and are for the benefit of THREAD ORIGIN, its subsidiaries, affiliates and its third party content providers and licensors and each shall have the right to assert and enforce such provisions directly or on its own behalf. </p>

    <h3 className="heading-class-TermsContent">  TRADEMARKS </h3>
    <p className="p-class-TermsContent">The Site contain copyrighted material, trademarks and other proprietary information, including, but not limited to, text, software, photos, video, graphics, music , sound, and the entire contents of THREAD ORIGIN protected by copyright as a collective work under the applicable copyright laws. THREAD ORIGIN owns a copyright in the selection, coordination, arrangement and enhancement of such content, as well as in the content original to it. Users may not modify, publish, transmit, participate in the transfer or sale, create derivative works, or in any way exploit, any of the content, in whole or in part. Users may download / print / save copyrighted material for the User’s personal use only. Except as otherwise expressly stated under copyright law, no copying, redistribution, retransmission, publication or commercial exploitation of downloaded material without the express permission of THREAD ORIGIN and the copyright owner is permitted.
    If copying, redistribution or publication of copyrighted material is permitted, no changes in or deletion of author attribution, trademark legend or copyright notice shall be made. The User acknowledges that he/she/it does not acquire any ownership rights by downloading copyrighted material. Trademarks that are located within or on the Site or a website otherwise owned or operated in conjunction with THREAD ORIGIN shall not be deemed to be in the public domain but rather the exclusive property of THREAD ORIGIN, unless such site is under license from the trademark owner thereof in which case such license is for the exclusive benefit and use of THREAD ORIGIN, unless otherwise stated.
    </p>

    <h3 className="heading-class-TermsContent"> INTELLECTUAL PROPERTY RIGHTS COMPLAINTS </h3>
    <p className="p-class-TermsContent">Thread Origin respects the intellectual property of others. If you believe that your work has been copied in a way that constitutes any Intellectual Property Right infringement, please address a complaint to the ‘Customer Care’ by way of an email as provided herein below. </p>

    <h3 className="heading-class-TermsContent"> USER RESPONSIBILITY AND REGISTRATION OBLIGATIONS </h3>
    <p className="p-class-TermsContent">If you use the Website as Registered User, you agree that you are competent to contract within the meaning of the Indian Contract Act, 1872. THREAD ORIGIN reserves the right to terminate your membership and refuse to provide you with access to the Website if it is brought to its notice or if it is discovered that you are under the age of 18 years. You are solely responsible for maintaining the confidentiality of your User ID and Password. You are responsible for all activities that occur under your User ID and Password. You agree, inter-alia, to provide true, accurate, current and complete information about yourself as prompted by Website registration form or provided by You as a Visitor or user of a third party site through which You access the Website. If you provide any information that is untrue, inaccurate, not current or incomplete or we have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the User Agreement, THREAD ORIGIN has the right to indefinitely suspend or terminate or block access of your membership with the Website and refuse to provide you with access to the Website.
    As a condition of purchase, the Site requires your permission to send you administrative and promotional emails. We will send you information regarding your account activity and purchases, as well as updates about our products and promotional offers. You can opt-Out of our promotional emails anytime by clicking the unsubscribe link at the bottom of any of our email correspondences. Please see our Privacy Policy for details. The offers made in any promotional messages sent via emails /SMS/MMS shall be subject to change at the sole discretion of THREAD ORIGIN and THREAD ORIGIN owes no responsibility to provide you any information regarding such change.
    You undertake not to host, display, upload, modify, publish, transmit, update or share any information that is contained on the website that: belongs to another person and to which you does not have any right to; is grossly harmful, harassing, blasphemous; defamatory, obscene, pornographic, paedophilic, libellous, invasive of another’s privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever; harm minors in any way; infringes any patent, trademark , copyright or other proprietary rights; violates any law for the time being in force; deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature; impersonate another person; contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource; threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognisable offence or prevents investigation of any offence or is insulting any other nation. That in case it is found that you are in violation of these express conditions, you shall be solely responsible and shall face all legal consequences of the same by yourself and that THREAD ORIGIN shall in no manner be held responsible for the same. </p>

    <h3 className="heading-class-TermsContent"> BILLING </h3>
    <p className="p-class-TermsContent">The price of our merchandise as mentioned on THREAD ORIGIN.com is the Maximum Retail Price (MRP) for the said product. Such MRP shall be inclusive of all local taxes as are applicable in India. Additional applicable taxes may be charged depending upon the destination where the order has to be shipped to. The tax rate applied and charged upon the order shall include combined tax rate for both state and local tax rates in accordance with the address where the order is being shipped. THREAD ORIGIN reserves the right to collect taxes and/or such other levy/ duty/ surcharge that it may have to incur in addition to the normal taxes it may have to pay. We may also charge delivery Charges which may include postal charges / shipment charges etc. That may be applicable for your country. </p>

    <h3 className="heading-class-TermsContent"> Cancellations and Modifications </h3>
    <p className="p-class-TermsContent"><b>What is THREAD ORIGIN Cancellation Policy by Customer? </b></p>
    <p className="p-class-TermsContent">You can cancel an order until it has not been packed in our Warehouse on App/Website/M-site. This includes items purchased on sale also. Any amount paid will be credited into the same payment mode using which the payment was made</p>

    <p className="p-class-TermsContent"><b>Can I modify the shipping address of my order after it has been placed?</b></p>
    <p className="p-class-TermsContent">Yes, You can modify the shipping address of your order before we have processed (packed) it, by updating it under 'change address' option which is available under ‘My order’ section of App/Website/M-site</p>

    <p className="p-class-TermsContent"><b>How do I cancel my Order?</b></p>
    <p className="p-class-TermsContent">Tap on “My Orders” section under the main menu of your App/Website/M-site and then select the item or order you want to cancel</p>

    <p className="heading-class-TermsContent"><b>I just cancelled my order. When will I receive my refund?</b></p>
    <p className="p-class-TermsContent">If you had selected Cash on Delivery, there is no amount to be refunded because you haven't paid for your order. For payments made via Credit Card, Debit Card, Net Banking, or Wallet you will receive refund into the source account within 7-10 days from the time of order cancellation. If payment was made by redeeming PhonePe wallet balance then, refund will be initiated into Phonepe wallet within a day of your order cancellation, which can be later transferred into your bank account, by contacting PhonePe customer support team.</p>

    <h3 className="heading-class-TermsContent"> ORDER CANCELLATION BY THREAD ORIGIN </h3>
    <p className="p-class-TermsContent">Due to unavoidable circumstances there may be times when certain orders having been validly placed may not be processed or capable of being dispatched. THREAD ORIGIN reserves the exclusive right to refuse or cancel any order for any reason. Some situations that may result in your order being cancelled shall include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, problems identified by our credit and fraud avoidance department or any defect regarding the quality of the product. We may also require additional verifications or information before accepting any order. We will contact you if all or any portion of your order is cancelled or if additional information is required to accept your order. If your order is cancelled after your credit card/ debit card/ any other mode of payment has been charged, the said amount will be reversed into your Account/ as the case may be to the source of the payment within a period of 30 working days. Any type of voucher used in these orders shall be returned and made available to the user in case of cancellation by <b>THREAD ORIGIN.</b></p>
    
    <h3 className="heading-class-TermsContent"> SHIPPING & PROCESSING FEE </h3>
    <p className="p-class-TermsContent">Our shipping and processing charges are intended to compensate <b>THREAD ORIGIN</b> for the cost of processing your order, handling and packing the products you purchase and delivering them to you. For further information please refer to our Shipping & Payment Policy.</p>

    <h3 className="heading-class-TermsContent"> PAYMENT </h3>
    <p className="p-class-TermsContent">While availing any of the payment method/s offered by us, we are not responsible or take no liability of whatsoever nature in respect of any loss or damage arising directly or indirectly to you including but not limited to the following: (a) lack of authorization for any transaction/s; (b) or exceeding the present limit mutually agreed by you and between your “Bank /s”; (c) or any payment issues arising out of the transaction; (d) or decline of transaction for any other reason/s.
        All payments made against the purchases /services on Thread Origin.com by you shall be as against the MRP displayed on the website and shall be in terms and conditions of the third party Online Payment Gateway Services as adopted and applicable to your transaction as approved by <b>THREAD ORIGIN</b>. Before shipping your order to you, we may request you to provide supporting documents (including but not limited to Govt. Issued ID and address proof) to establish the ownership of the payment instrument used by you for your purchase. This is done so as to ensure a safe and a full proof online shopping environment to our users.
        <b>THREAD ORIGIN</b> may employ PaisaPay, PayPal and such other third party facilities, for Payments on the Website. Such payments can be made through the electronic or through Cash on delivery transactions, as may be permitted by THREAD ORIGIN which shall be at its sole discretion. Use of such third party services will be governed by their User Agreement, Seller Terms, Conditions and other rules and policies as may be required and applicable for your nature of activities.</p>
    
    <h3 className="heading-class-TermsContent"> GIFT CARDS </h3>
    <p className="p-class-TermsContent">
    <ol>
        <li>Gift Card can be redeemed online at www.Thread Origin.com or at our stores</li>
        <li>Gift Cards cannot be used to purchase other Gift Cards</li>
        <li>If the order value exceeds the Gift Card amount, the balance must be paid by Credit Card/Debit Card/Internet Banking.</li>
        <li>Gift Cards will expire after 6 months from the date of issue and any corresponding unused balance shall be forfeited thereafter</li>
        <li>Gift Cards cannot be redeemed for Cash or Credit</li>
        <li>You are solely responsible for the safety and security of the Gift Cards</li>
        <li>Gift Card number is confidential. In the event of any misuse of Gift Card due to loss of any such confidential details due to the fault of the purchaser, Thread Origin shall not be responsible for the same and no refund can be issued</li>
        <li>Validity of Gift Cards cannot be extended, new Gift Cards cannot be provided against the expired/unused Gift Cards</li>
        <li>Gift Cards are a Prepaid Instrument subjected to regulations by Reserve Bank of India. Thread Origin will be legally required to share the details of the purchase of the Gift Cards and/or transaction undertaken using the Gift Card with RBI or such statutory authorities.</li>
        <li>Gift Cards and transaction undertaken using the Gift Card. The issuer is also required to share the Know Your Customer (KYC) details of the purchaser/ redeemer of the Gift Cards with RBI or such statutory authorities, as per statutory guidelines issued from time to time. Thread Origin may contact the purchaser of the Gift Card in this regard</li>
        <li>There is no fee or other charges associated with Gift Card purchase. </li>
    </ol>
    </p>


    <h3 className="heading-class-TermsContent"> DELIVERY </h3>
    <p className="p-class-TermsContent">
    THREAD ORIGIN endeavours but does not guarantee delivery of products in the stipulated time period as mentioned on the purchase of the product. In no manner can the contract be repudiated if THREAD ORIGIN fails to deliver any one or more products in the stipulated time frame. However, if you fail to take the delivery of the goods, THREAD ORIGIN may at its discretion charge you for additional shipping cost.
    </p>

    <h3 className="heading-class-TermsContent"> LOSS IN TRANSIT </h3>
    <p className="p-class-TermsContent">
    THREAD ORIGIN shall make all endeavours to deliver defect free products to the purchasers. THREAD ORIGIN does not take title to any returned items purchased by the user unless the item is received by THREAD ORIGIN. Any item purchased on our website does not qualify for any return unless the product delivered is damaged or has manufacturing defects. The defective and/or damaged goods so received shall be communicated to THREAD ORIGIN within 48 hours of its receipt. Any communication received after 48 hours of delivery shall not qualify for return unless expressly covered by the product warranty even in case the said product has been wrongly delivered.
    <br></br><br></br>
    THREAD ORIGIN holds the sole discretion to determine whether a refund can be issued. For further information please read our Returns & Exchanges Policy.
    </p>

    <h3 className="heading-class-TermsContent"> REFUNDS & RETURNS </h3>
    <p className="p-class-TermsContent">
    purchasers. THREAD ORIGIN does not take title to any returned items purchased by the user unless the item is received by THREAD ORIGIN. Any item purchased on our website does not qualify for any return unless the product delivered is damaged or has manufacturing defects. The defective and/or damaged goods so received shall be communicated to THREAD ORIGIN within 3 working days of its receipt. Any communication received after 48 hours of delivery shall not qualify for return unless expressly covered by the product warranty even in case the said product has been wrongly delivered.
    <br></br><br></br>
    THREAD ORIGIN holds the sole discretion to determine whether a refund can be issued. For further information please read our Returns & Exchanges Policy.
    <br></br><br></br>
    Fair Usage Policy: We always strive hard to provide the best experience to our customers. However, we have noticed that few accounts abuse our liberal returns policy. These accounts typically return most of the items bought or choose to not accept our shipments. Hence, our regular customers are deprived of the opportunity to buy these items. To protect the rights of our customers, we reserve the right to collect shipping charge of Rs. 150 for all orders and disable cash on delivery option for accounts which have high percentage of returns and shipments not accepted by value of orders placed. </p> 
    
    <h3 className="heading-class-TermsContent"> PRODUCT DESCRIPTIONS </h3>
    <p className="p-class-TermsContent">Products displayed on THREAD ORIGIN attempts to be as accurate as possible. However, THREAD ORIGIN does not warrant that product descriptions or other content is accurate, complete, reliable, current, or error-free. If a product offered by THREAD ORIGIN is not as described, your sole remedy is to contact THREAD ORIGIN within 3 working days of receipt and THREAD ORIGIN riser vest the right to provide a solution as per its discretion. THREAD ORIGIN is not liable to issue any refunds or allow exchanges and/or returns as a matter of policy.</p>
    
    <h3 className="heading-class-TermsContent"> PRODUCT COMPLIANCE </h3>
    <p className="p-class-TermsContent">Products displayed/ sold on the THREAD ORIGIN website are manufactured/ procured as per the applicable Local Laws of India and are in conformity with the required Indian industry standards</p>
    
    
    <h3 className="heading-class-TermsContent"> PRODUCT PRICING DISCLAIMER </h3>
    <p className="p-class-TermsContent">The prices displayed on our website may differ from prices that are available in stores. Further the prices displayed in our catalogues are quotes which may vary from country to country for the same product. Prices shown on the website both for India and in other International Countries are subject to change without prior notice. These prices only reflect the MRP and do not include shipping and taxes which may be extra as applicable.</p>
    
    <h3 className="heading-class-TermsContent"> INACCURACY DISCLAIMER </h3>
    <p className="p-class-TermsContent">From time to time there may be information on our Website or in our catalogue that may contain typographical errors, inaccuracies, or omissions that may relate to product descriptions, pricing, and availability. THREAD ORIGIN reserves the right to correct any errors, inaccuracies or omissions and to change or update information at any time without prior notice.</p>

    <h3 className="heading-class-TermsContent"> WARRANTIES & LIABILITY </h3>
    <p className="p-class-TermsContent">All information, content, materials, products (including software) and other services included on or otherwise made available to you by THREAD ORIGIN are provided on an AS IS and AS AVAILABLE basis, unless otherwise specified in writing. THREAD ORIGIN makes no representations or warranties of any kind, express or implied, as to the operation of the services, or the information, content, materials, products (including software) or other services included on or otherwise made available to you through THREAD ORIGIN, unless otherwise specified in writing. You expressly agree that your use of the website is at your sole risk.
        <br></br><br></br>
        THREAD ORIGIN does not warrant that this Website will be constantly available, or available at all or that any information on this Website is complete, true, accurate or non-misleading.
        <br></br><br></br>
        We will not be liable to you in any way or in relation to the Contents of, or use of, or otherwise in connection with, the Website. You acknowledge, by your use of this Website, that your use of this Website is at your sole risk , that you assume full responsibility for all risks associated with all necessary servicing or repairs of any equipment you use in connection with your use of this Website, and that THREAD ORIGIN shall not be liable for any damages of any kind related to your use of this Website.
        <br></br><br></br>
        Though THREAD ORIGIN shall make all endeavour to protect its websites from any viruses or other illegal use of its website. However we do not warrant that this site; information, Content, materials, product (including software) or services included on or otherwise made available to You through the Website; its servers; or electronic communication sent from us are free of viruses or other harmful components. Nothing on Website constitutes, or is meant to constitute, advice of any kind.
        <br></br><br></br>
        All the Products sold on Website shall be solely governed by the Indian Laws. In the event we are unable to deliver such Products due to implications of different territorial laws, we will return or will give credit for the amount (if any) received in advance by us from the sale of such Product that could not be delivered to You. It is YOUR responsibility to ensure that the products purchased on this website is not restricted in your territory. THREAD ORIGIN shall not be responsible for any non-compliance with regard to the local laws of that territory for any product available on this website.
        <br></br><br></br>
        THREAD ORIGIN will not be liable for any damages of any kind arising from the use of any service, or from any information, content, materials, products (including software) or other services included on or otherwise made available to you through the website, including, but not limited to direct, indirect, incidental, punitive, and consequential damages, unless otherwise specified in writing.</p>  
    

    <h3 className="heading-class-TermsContent"> BREACH </h3>
    <p className="p-class-TermsContent">In the event you are found to be in breach of the Terms of Use or Privacy Policy or other rules and policies or if we are unable to verify or authenticate any information you provide or if it is believed that your actions may cause legal liability for you, other users or us, without limiting to the present, without prior notice immediately limit your activity, remove your information, temporarily/indefinitely suspend or terminate or block your membership, and/or refuse to provide you with access to this Website. Any user that has been suspended or blocked may not register or attempt to register with us or use the Website in any manner whatsoever until such time that such user is reinstated by us.
        <br></br>
        Notwithstanding the foregoing, if you breach the Terms of Use or Privacy Policy or other rules and policies, we reserve the right to recover any amounts due and owing by you to us and to take strict legal action including but not limited to a referral to the appropriate police or other authorities for initiating criminal or other proceedings against you.
        <br></br>
        Any breach of any applicable local laws of that territory shall also result in, without prior notice immediately limit your activity, remove your information, temporarily/indefinitely suspend or terminate or block your membership, and/or refuse to provide you with access to this Website.</p>
            
    <h3 className="heading-class-TermsContent"> SEVERABILITY </h3>
    <p className="p-class-TermsContent">We reserve the right to make changes to our site, policies, Service Terms, and these Conditions of Use at any time. If any of these conditions shall be deemed invalid, void, or for any reason unenforceable, that condition shall be deemed severable and shall not affect the validity and enforceability of any remaining condition.</p>
    
    <h3 className="heading-class-TermsContent"> WAIVER </h3>
    <p className="p-class-TermsContent">The failure by THREAD ORIGIN to enforce at any time or for any period any one or more of the terms or conditions of the Agreement shall not be a waiver by THREAD ORIGIN of them or of the right any time subsequent to enforce all Terms and Conditions of this agreement.</p>
    

    <h3 className="heading-class-TermsContent"> FORCE MAJEURE </h3>
    <p className="p-class-TermsContent">Failure on the part of THREAD ORIGIN to perform any of its obligations and the non-furnishing of the Service, shall not entitle you to raise any claim against THREAD ORIGIN or be a breach hereunder to the extent that such failure arises from an event of Force Majeure. If through force Majeure the fulfilment by either party of any obligation set forth in this Agreement will be delayed, the period of such delay will not be counted on in computing periods prescribed by this Agreement. Force Majeure will include any war, civil commotion, strike, governmental action, lockout, accident, epidemic or any other event of any nature or kind whatsoever beyond the control of THREAD ORIGIN that directly or indirectly hinders or prevents THREAD ORIGIN from commencing or proceeding with consummation of the transactions contemplated hereby. You expressly agree that lack of funds shall not in any event constitute or be considered an event of Force Majeure.</p>
    
    
    <h3 className="heading-class-TermsContent"> DISPUTE RESOLUTION </h3>
    <p className="p-class-TermsContent">This agreement shall be construed and the legal relations between YOU and THREAD ORIGIN hereto shall be determined and governed according to the laws of India. If any dispute arises between you and THREAD ORIGIN regarding your use of the Website or Your dealing with THREAD ORIGIN in relation to any activity on the Website, in connection with the validity, interpretation, implementation or breach of any provision of the Terms & Conditions and/or Privacy Policy including but not limited to the rules and policies contained herein, the dispute shall be subject to the exclusive jurisdiction to the Courts of New Delhi.
        <br></br><br></br>
        Your obligations to pay the Payment Fees shall not be suspended during the pendency of such proceedings.</p>
            
    <h3 className="heading-class-TermsContent"> GRIEVANCE </h3>
    <p className="p-class-TermsContent">Any grievances you have by way of use of the website can be addressed to the ‘Customer Care’ by way of email to ${ContactInfo.EMAIL}
    <br></br><br></br>
    The ‘Customer Care’ shall thereafter consider the same and provide a response within one month of the date of such complaint/ grievance.</p>
    
    <h3 className="heading-class-TermsContent"> GOVERNING LAW </h3>
    <p className="p-class-TermsContent">These Terms and Conditions or the documents of third party payment channels shall be governed and construed in accordance with the laws of India.
    <br></br>
    This document is an electronic record in terms of Information Technology Act, 2000 and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000.
    <br></br>
    This electronic record is generated by a computer system and does not require any physical or digital signatures.</p>
    
    <h3 className="heading-class-TermsContent"> REFER A FRIEND </h3>
    <p className="p-class-TermsContent">The following are the terms and conditions for the THREAD ORIGIN Referral program, which shall be applicable to the original customer / user of THREAD ORIGIN (henceforth called the referrer) and the person / user referred by them (henceforth called the referral).
    <br></br>
    The Refer a Friend campaign is for customers who have placed an order worth Rs. 6,000 or above (as a onetime purchase or cumulative) from THREAD ORIGIN.
    <br></br>
    
    <ol>
        <li>Points earned can only redeemed in INR.</li>
        <li>The Refer a Friend campaign is for customers who have placed an order worth Rs. 6,000 or above (as a onetime purchase or cumulative) from Thread Origin.</li>
        <li>The referral must be a non-registered user at THREAD ORIGIN. An existing account will not be treated as a valid referral.</li>
        <li>Customers from both online and offline channels are eligible for the offer</li>
        <li>The referrer is eligible for this offer only after the referral/s have registered and made at least one purchase with THREAD ORIGIN.</li>
        <li>The referrer will get 1 point per new registration + purchase (one without the other does not suffice). Multiple transactions by the same referral will not result in multiple points.</li>
        <li>A referrer will get points if and only if the person he has referred is new to THREAD ORIGIN i.e. considering the referral has not registered with Thread Origin before</li>
        <li>The referral needs to make a purchase using/following the link shared by the referrer, for the points to be attributed to the referrer.</li>
        <li>In case of the same user being referred by multiple people, the unique link will be used to attribute the points.</li>
        <li>All details pertaining to this program are hosted and detailed on our website.</li>
        <li>The final redemption of this program can only be done through our online channel.</li>

        <li>The points can be redeemed only against the products available for redemption.</li>
        <li>THREAD ORIGIN reserves the right to remove and add products to the offer depending on their availability.</li>
        <li>The user can refer as many people to increase their chances for redemption.</li>
        <li>The fields shown against a product are indicative of the maximum number of people they need to bring on board (through registration and purchase), to be eligible to receive that product.</li>
        <li>In the case of the product being out of stock at the time of redemption, THREAD ORIGIN retains the right to recommend alternatives to the referrer.</li>
        <li>The benefits of this program cannot be clubbed with any other offers running simultaneously.</li>
        <li>The referrer will have to claim and redeem points in order to complete the circle and have the products shipped to them.</li>
        <li>The points/benefits accumulated are for the purpose of this program only, and cannot be used/transferred to any other activity that Thread Origin launches in the future.</li>
        <li>The points are non-transferrable and are tied to the referrer’s account.</li>
        <li>The points cannot be redeemed for cash or credit notes.</li>
        <li>In case the referrer holds multiple accounts, points will be attributed to the account that referrals are originally made from.</li>


        <li>Points accumulated by multiple accounts cannot be clubbed.</li>
        <li>The offer is applicable only for the period between 10 August 2018 to 31st March 2019.</li>
        <li>The redeemed product will be shipped to the referrer at no additional cost.</li>
        <li>Duties & taxes for international orders will apply as per the rules and regulations at the shipping destination.</li>
        <li>The offer is for all customers, but due to logistical issues, Federal/Govt. sanctions etc. the offer may not be applicable in certain areas.</li>
        <li>Standard return and refund policies apply for the referrer.</li>
        <li>For clothing items, the offer is only applicable for the colour and size mentioned in the offer.</li>
        <li>The referrer cannot exchange the redeemed product unless it is a piece of clothing (sizes only as mentioned in the offer), or due to a manufacturing/quality defect.</li>
        <li>The referrer cannot redeem the points to the currency value of the products showcased</li>
        <li>THREAD ORIGIN holds the rights to alter the terms and conditions of the program, and THREAD ORIGIN will inform users of the changes made.</li>
        <li>The points accumulated by the user automatically lapse after 31st March 2019 in case they are not redeemed.</li>
    </ol>
    </p>

    <h3 className="heading-class-TermsContent"> E-mail verification policy </h3>
    <p className="p-class-TermsContent"><b>What is Thread Origin E-mail verification policy? </b></p>
    <p className="p-class-TermsContent">We always strive hard to provide the best experience to our customers. However, we have noticed that few accounts abuse our liberal policies. Therefore, please ensure that the details you provide us are correct and complete and inform us immediately of any changes to the information that you provided while registering. THREAD ORIGIN verifies the email addresses to ensure important communication about orders and shipments are reaching our customers. Email addresses that have an invalid domain, no mail server, or no mail box, fail the verification processes set by THREAD ORIGIN and THREAD ORIGIN reserves the right to disable any cash on delivery option for orders placed from such accounts that fail the email verification process.</p>

    
    </div>
   )

}


export default TermsContent 