import React from 'react'
// import HeaderProduct from '../components/header_product.co;ntroller';
import HeaderProduct from './../../../components/header_product.controller';
import Footer from './../../../components/footer.controller.js';
import './../../../../css/PandC/TandC.css';
import TermsContent from './TermsContent.js'
import ImageComponent from "./../imageComponent"

const TermsAndCondition = () => {
   
        return(
            <div> 
                <div className="main-class main-container">
                    <HeaderProduct />
                    <div >
                        <ImageComponent />
                    </div>
                    <div className="termsAndCondtion">
                        <TermsContent />
                    </div>
                    <div id="product-footer" className="col-md-12 product-footer">
                        <Footer/>
                        <div className="col-md-12 footer_bg2">
                            <p className="footer_end">All Rights Reserved | 2018</p>
                        </div>
                    </div>
                </div> 
            </div>           
        )
}



export default TermsAndCondition;