import './../../../../css/PandC/TermsContent.css';
import React from 'react'

const PrivacyContent = () => {

return (
    <div className="main-class-TermsContent">
       <h3 className="heading-class-TermsContent">   PRIVACY POLICY </h3>

<p className="p-class-TermsContent">Welcome to the website of THREAD ORIGIN FASH –TECH PRIVATE LIMITED (“Thread Origin” or “The Company”). THREAD ORIGIN provides this Site as a service to its customers and prospective buyers. Please review our Privacy Policy. </p>

WHAT THIS PRIVACY POLICY COVERS

<p className="p-class-TermsContent">This Privacy Policy covers THREAD ORIGIN’s treatment of personally identifiable information that the Company collects when you are on the site. This policy does not apply to the practices of companies that Thread Origin does not own or control, or to people that THREAD ORIGIN does not employ or manage.
</p>


    <h3 className="heading-class-TermsContent">INFORMATION COLLECTION AND USAGE</h3>

<p className="p-class-TermsContent">THREAD ORIGIN collects personally identifiable information when you register through the site and when you enter online promotions and contests. THREAD ORIGIN may also receive personally identifiable information from business partners. We may use the information you share with us, to communicate with you through e-mails, text messages and calls, in order to provide our product or service related information and/or for promotional and marketing purposes for a period of ten years. When you register with THREAD ORIGIN, we ask for your contact information. 
Once you register with THREAD ORIGIN and sign in to our services, you are not anonymous to us. Thread Origin also automatically receives and records information on our server logs from your browser including your IP address, cookie information and the page you requested. THREAD ORIGIN may use third party contractors and service providers to process your data and / or to contact you for the purposes you have chosen. Such contractors and service providers are contractually bound to protect your information in the same way as we do. THREAD ORIGIN uses information for the purposes for which you have provided specific consent. You, at all times have the option to unsubscribe by clicking “Unsubscribe” and to be forgotten and requiring your information to be expunged from our records by contacting care@threadorigin.com .
</p>

    <h3 className="heading-class-TermsContent">OUR USE OF "COOKIES"</h3>

<p className="p-class-TermsContent">As is the case with many sites, when you visit our site and complete a registration form, we will place a "cookie" on your computer, which helps us identify you more quickly when you return. We will not use "cookies" or other devices to follow your click stream on the Internet generally, but will use them, and other devices, to determine which pages or information you find most useful or interesting at our own Web sites.Most browsers permit you to refuse to accept a "cookie" offered by a Web site. You will not be denied access to any part of the website on account of your refusal to accept a "cookie," but your transactions through this Web site may be delayed due to the time it takes you to re-enter basic information necessary to complete a transaction.Should you choose to unsubscribe or to be forgotten, the cookies will be disabled.
</p>

    <h3 className="heading-class-TermsContent">COLLECTION OF INFORMATION BY THIRD PARTIES</h3>

<p className="p-class-TermsContent">Our Web pages may offer promotions that are sponsored by or co-sponsored with identified third parties. By virtue of their sponsorship, these third parties may obtain Customer Identifiable Information that visitors voluntarily submit to participate in the activity. We have no control over these third parties' use of this information and we encourage you to consult their privacy policies for more information on their privacy practices.
</p>

    <h3 className="heading-class-TermsContent">SECURITY</h3>

<p className="p-class-TermsContent">Your THREAD ORIGINAccount Information is password-protected for your privacy and security.
</p>




    <h3 className="heading-class-TermsContent">LINKS</h3>

To improve your Web experience, and to offer you products in which you might be interested, we provide links to business alliance companies, and other third-party sites. When you click on these links, you will be transferred out of our Web site and connected to the Web site of the organization or company that you selected. Because Thread Origin does not control these sites (even if an affiliation exists between our Web sites and a third party site), you are encouraged to review their individual privacy notices. If you visit a Web site that is linked to our sites, you should consult that site's privacy policy before providing any Customer Identifiable Information. Thread Origin does not assume any responsibility or liability in elation with conduct of such third parties.

CHANGES TO THIS PRIVACY POLICY

THREAD ORIGIN may amend this policy from time to time.


    <h3 className="heading-class-TermsContent">TERMS and CONDITIONS </h3>

<p className="p-class-TermsContent">Welcome to Thread Origin.com (“Website”). THREAD ORIGIN and/or its affiliates provide website features and other products and services to you when you visit or shop at Thread Origin.com, use THREAD ORIGIN products or services, or use software provided by THREAD ORIGIN in connection with any of the foregoing. THREAD ORIGIN provides the Services subject to the following conditions. This user agreement (“Terms and Conditions” or “T&C” or “Terms” or “Agreement”) is between you (“you” or “End User” or “your” or “Buyer” or “Customer” or “Registered User”) and THREAD ORIGIN (“Company” or “us” or “We” or “Thread Origin.com”). This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures .Notwithstanding anything contained or said in any other document, if there is a conflict between the terms mentioned herein below and any other document, the terms contained in the present T&C shall alone prevail for the purposes of usage of the Site.
</p>



    <h3 className="heading-class-TermsContent">TERMS and CONDITIONS FOR FREE SHIPPING PERIOD</h3>
    <p className="p-class-TermsContent">
• This offer is only applicable on our online store www.Thread Origin.com from 1200 Hrs August1st, 2018 to 0900 Hrs 15 Otober , 2018 IST.
•  This offer is only for free shipping across India for orders placed online. All other charges remain.
</p>

    <h3 className="heading-class-TermsContent">TERMS and CONDITIONS FOR SPECIAL PRICING PERIOD</h3>
    <p className="p-class-TermsContent">
For THREAD ORIGIN’s special pricing offer from 1st August 2018 to 15 October 2018:
<ul>

    <li>  The current offer is applicable on all www.threadorigin.com.</li>
    <li> Special pricing will not be applicable on the purchase of gift cards</li>
    <li>We will be unable to accept any exchange or refund on products purchaseds during the period of special pricing</li>
    <li>We will be unable to tend to gift wrapping requests during the five days of special pricing</li>
    <li>The displayed price will be deemed final</li>
    <li>No other offer or discount can be clubbed during this period. However Gift Cards and Credit Notes can be applied</li>
    <li> All prices are inclusive of applicable taxes</li>
    <li>In case the product does not pass our quality check before shipping, the collected amount will be refunded to you</li>
    <li> All Duties and Taxes for international orders will apply as per the rules of the shipping destination and would be borne by the customer.</li>
    <li>Goods and Service Tax/Sales Tax levied by Indian law, if applicable on exports would be paid by THREAD ORIGIN FASH-TECH PRIVATE LIMITED (i.e THREAD ORIGIN) and will not be charged to the customer</li>
</ul>
</p>
</div>



   )

}


export default PrivacyContent 