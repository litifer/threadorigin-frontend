import React from "react";
import ImageBelowHeader from "../../../img/pandc/1.png";

const imageComponent = (props) => {
    return (
        <div className="col-md-12 col-12 col-sm-12 no-padding tnc-header-img"> 
            <img src={ImageBelowHeader} className="image-component-class"/>
        </div>
    )
}

export default imageComponent;