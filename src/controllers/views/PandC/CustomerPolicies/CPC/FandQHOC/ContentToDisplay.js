import React, {Component} from 'react'
import  './../../../../../../css/PandC/FaQPages/FandQcommon.css';


class postDesign extends Component  {

    state = {
        newContent: false
    }


    render () {
        var bodyContent = ""; 
        var styleH = "dFheadingToDisplayF";
        var styleP = "d-none";
        var cspan = "+";

     
        if ( this.props.ID ==  this.props.selectedPostId ) {
            bodyContent = this.props.body
            styleH = "dFheadingToDisplayT";
            styleP = "pcontentHeadingF";
            cspan = "-";
        }
    return (
    <div className="dFmaincontentToDisplay">
        <div className={styleH} onClick={this.props.clicked}>
             <h1 className="hcontentHeadingF">{this.props.title} <span className="dsigns d-none d-sm-block"> {cspan}</span></h1>
        </div>
                <p className={styleP}> {bodyContent} </p>       
    </div>
        )
    }
};

export default postDesign;