import React, {Component} from 'react'
import  './../../../../../css/PandC/commonCpc.css';
import { fetchData } from "./url.js"
import axios from 'axios';
import ContentLayout from './FandQHOC/ContentToDisplay.js';
import  './../../../../../css/PandC/FaQPages/FandQcommon.css';
import {ContactInfo} from './../../../../../consts'

import posts from './../../../../../consts/faq/shippingAndDelivery'

class SandD extends Component {
    
    //Adding content to state 
    state = {
        posts: [],
        selectedPostId: 1,
        error: false
    }

    componentDidMount () {
        // axios.get( 'https://jsonplaceholder.typicode.com/posts' )
        //     .then( response => {
        //         const posts = response.data.slice(0, 6); //remove this -> Json placeholder sends extensive data hence we are using it 
        //         const updatedPosts = posts.map(post => {
        //             return {
        //                 ...post
        //             }
        //         });
        //         this.setState({posts: updatedPosts});
        //     } )
        //     .catch(error => {
        //         this.setState({error: true});
        //     });

        this.setState({posts: posts});
    }

    //Getting the ID of Selected Post (through on click)
    postSelectedHandler = (id) => {
        this.setState({selectedPostId: id});
    
    }


    render() {
        //If Date Fetch is fail then throw 
        let posts = <p style={{textAlign: 'center'}}>Loading...</p>;

        //If no error means data is fetched 
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <ContentLayout
                    key={post.id} 
                    ID={post.id} 
                    clicked={() => this.postSelectedHandler(post.id)} 
                    title={post.title} 
                    body={post.body}
                    selectedPostId={this.state.selectedPostId}
                    />
                    )
            });
        }

        return (
            
            <div>
                <section>
                    <div className="dstaticContentF d-none d-sm-none d-md-block">
                     <h1 classname="contentstaticHeadingF">SHIPPING &amp; DELIVERY </h1>
                     <p className="contentstaticParaF"> This website is operated by Thread Origin. Throughout the site, the terms “we”, “us” and “our” refer to Thread Origin. Thread Origin offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. </p>
                 </div>
                    {posts}
                </section>
            </div>
        );
    }
}


export default SandD;