import React, {Component} from 'react'
import  './../../../../../css/PandC/commonCpc.css';
import { fetchData } from "./url.js"
import axios from 'axios';
import ContentLayout from './FandQHOC/ContentToDisplay.js';
import  './../../../../../css/PandC/FaQPages/FandQcommon.css';
import posts from './../../../../../consts/faq/exchangesAndReturns'




class SandD extends Component {
    
    //Adding conent to state 
    state = {
        posts: [],
        selectedPostId: 1,
        error: false
    }

    componentDidMount () {
            this.setState({posts: posts});
    }

    //Getting the ID of Selected Post (through on click)
    postSelectedHandler = (id) => {
        this.setState({selectedPostId: id});
    
    }


    render() {
        //If Date Fetch is fail then throw 
        let posts = <p style={{textAlign: 'center'}}>Loading...</p>;

        //If no error means data is fetched 
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <ContentLayout
                    key={post.id} 
                    ID={post.id} 
                    clicked={() => this.postSelectedHandler(post.id)} 
                    title={post.title} 
                    body={post.body}
                    selectedPostId={this.state.selectedPostId}
                    />
                    )
            });
        }

        return (
            
            <div>
                <section>
                    <div className="dstaticContentF d-none d-sm-none d-md-block">
                     <h1 classname="contentstaticHeadingF">EXCHANGES &amp; RETURNS </h1>
                     <p className="contentstaticParaF"> We endeavour to ensure that every transaction at our website is seamless. We take great care in delivering our products and adhere to the highest quality standards. If you are not happy with your purchase, please see options for returning or exchanging any of the items. </p>
                 </div>
                    {posts}
                </section>
            </div>
        );
    }
}


export default SandD;