import React, {Component} from 'react'
import  './../../../../../css/PandC/commonCpc.css';
import { fetchData } from "./url.js"
import axios from 'axios';
import ContentLayout from './FandQHOC/ContentToDisplay.js';
import  './../../../../../css/PandC/FaQPages/FandQcommon.css';
import posts from './../../../../../consts/faq/cancellation'




class SandD extends Component {
    
    //Adding conent to state 
    state = {
        posts: [],
        selectedPostId: 1,
        error: false
    }

    componentDidMount () {
        // axios.get( 'https://jsonplaceholder.typicode.com/posts' )
        //     .then( response => {
        //         const posts = response.data.slice(18, 20); //remove this -> Json placeholder sends extensive data hence we are using it 
        //         const updatedPosts = posts.map(post => {
        //             return {
        //                 ...post
        //             }
        //         });
        //         this.setState({posts: updatedPosts});
        //     } )
        //     .catch(error => {
        //         this.setState({error: true});
        //     });
            this.setState({posts: posts});
    }

    //Getting the ID of Selected Post (through on click)
    postSelectedHandler = (id) => {
        this.setState({selectedPostId: id});
    
    }


    render() {
        //If Date Fetch is fail then throw 
        let posts = <p style={{textAlign: 'center'}}>Loading...</p>;

        //If no error means data is fetched 
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <ContentLayout
                    key={post.id} 
                    ID={post.id} 
                    clicked={() => this.postSelectedHandler(post.id)} 
                    title={post.title} 
                    body={post.body}
                    selectedPostId={this.state.selectedPostId}
                    />
                    )
            });
        }

        return (
            
            <div>
                <section>
                    <div className="dstaticContentF d-none d-sm-none d-md-block">
                     <h1 classname="contentstaticHeadingF">CANCELLATIONS  &amp; MODIFICATIONS </h1>
                     <p className="contentstaticParaF"> Due to unavoidable circumstances there may be times when certain orders having been validly placed may not be processed or capable of being dispatched. THREAD ORIGIN reserves the exclusive right to refuse or cancel any order for any reason. Some situations that may result in your order being cancelled shall include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, problems identified by our credit and fraud avoidance department or any defect regarding the quality of the product. We may also require additional verifications or information before accepting any order. We will contact you if all or any portion of your order is cancelled or if additional information is required to accept your order. If your order is cancelled after your credit card/ debit card/ any other mode of payment has been charged, the said amount will be reversed into your Account/ as the case may be to the source of the payment within a period of 30 working days. Any type of voucher used in these orders shall be returned and made available to the user in case of cancellation by THREAD ORIGIN. </p>
                 </div>
                    {posts}
                </section>
            </div>
        );
    }
}


export default SandD;