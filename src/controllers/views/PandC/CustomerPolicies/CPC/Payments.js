import React, {Component} from 'react'
import  './../../../../../css/PandC/commonCpc.css';
import { fetchData } from "./url.js"
import axios from 'axios';
import ContentLayout from './FandQHOC/ContentToDisplay.js';
import  './../../../../../css/PandC/FaQPages/FandQcommon.css';
import posts from './../../../../../consts/faq/payments'




class SandD extends Component {
    
    //Adding conent to state 
    state = {
        posts: [],
        selectedPostId: 1,
        error: false
    }

    componentDidMount () {
        this.setState({posts: posts});
    }

    //Getting the ID of Selected Post (through on click)
    postSelectedHandler = (id) => {
        this.setState({selectedPostId: id});
    
    }


    render() {
        //If Date Fetch is fail then throw 
        let posts = <p style={{textAlign: 'center'}}>Loading...</p>;

        //If no error means data is fetched 
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <ContentLayout
                    key={post.id} 
                    ID={post.id} 
                    clicked={() => this.postSelectedHandler(post.id)} 
                    title={post.title} 
                    body={post.body}
                    selectedPostId={this.state.selectedPostId}
                    />
                    )
            });
        }

        return (
            
            <div>
                <section>
                    <div className="dstaticContentF d-none d-sm-none d-md-block">
                     <h1 classname="contentstaticHeadingF">PAYMENTS </h1>
                     <p className="contentstaticParaF"> While availing any of the payment method/s offered by us, we are not responsible or take no liability of whatsoever nature in respect of any loss or damage arising directly or indirectly to you including but not limited to the following: (a) lack of authorization for any transaction/s; (b) or exceeding the present limit mutually agreed by you and between your “Bank /s”; (c) or any payment issues arising out of the transaction; (d) or decline of transaction for any other reason/s.
 
 <br></br><br></br>All payments made against the purchases /services on Thread Origin.com by you shall be as against the MRP displayed on the website and shall be in terms and conditions of the third party Online Payment Gateway Services as adopted and applicable to your transaction as approved by THREAD ORIGIN. Before shipping your order to you, we may request you to provide supporting documents (including but not limited to Govt. Issued ID and address proof) to establish the ownership of the payment instrument used by you for your purchase. This is done so as to ensure a safe and a full proof online shopping environment to our users.
  
 <br></br><br></br>THREAD ORIGIN may employ PaisaPay, PayPal and such other third party facilities, for Payments on the Website. Such payments can be made through the electronic or through Cash on delivery transactions, as may be permitted by THREAD ORIGIN which shall be at its sole discretion. Use of such third party services will be governed by their User Agreement, Seller Terms, Conditions and other rules and policies as may be required and applicable for your nature of activities.</p>
                 </div>
                    {posts}
                </section>
            </div>
        );
    }
}


export default SandD;