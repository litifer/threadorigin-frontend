import React, {Component} from 'react'
// import HeaderProduct from '../components/header_product.co;ntroller';
import HeaderProduct from './../../../components/header_product.controller';
import Footer from './../../../components/footer.controller.js';
import  './../../../../css/PandC/faq.css';
import ImageComponent from "./../imageComponent";
import SandD from "./CPC/SandD.js";
import InShipping from './CPC/Inshipping.js';
import Payments from './CPC/Payments.js';
import EandR from './CPC/EandR.js';
import Cancellation from './CPC/Cancellation.js';
import Edelievery from './CPC/Cancellation.js';

//import some from ""




class customerPolicies extends Component{

    state = {
        SandD: true,
        Cancellation: false,
        Payments: false,
        EandR: false,
        InternationalShipping: false,
        ExpressDelievery: false,

    }

    contentToDisplay = (event) => {
        event.preventDefault()
        Object.keys(this.state).forEach(key => {
           if(event.target.name == key) {
               this.setState({[event.target.name]: true})
           } else {
               this.setState({[key]: false})
           }
        })

    }




    render(){

        return(

             <div className="main-class main-container">
               <HeaderProduct />
                <div>
                <ImageComponent />
                </div>

                {/* This is desktop Screen */}
                <div className="d-none d-sm-none d-md-block">
                    <div className="row">
                            <div className="sub-headings ">
                                <button className="btn customer-p-b" name="SandD" onClick={this.contentToDisplay}>  Shipping and Delivery </button>
                                <button className="btn customer-p-b" name="Cancellation" onClick={this.contentToDisplay}>Cancellation </button>
                                <button className="btn customer-p-b" name="Payments" onClick={this.contentToDisplay}>Payments </button>
                                <button className="btn customer-p-b" name="EandR" onClick={this.contentToDisplay}> Exchanges and Returns </button>
                                <button className="btn customer-p-b" name="InternationalShipping" onClick={this.contentToDisplay}> International Shipping </button>
                                <button className="btn customer-p-b" name="ExpressDelievery" onClick={this.contentToDisplay}> Express Delievery </button>
                            </div>
                            <div className="main-content">
                                <div className="header-box">
                                    { this.state.SandD ? <SandD />: null }
                                    { this.state.Payments ? <Payments />: null }
                                    { this.state.InternationalShipping ? <InShipping />: null }
                                    { this.state.EandR ? <EandR />: null }
                                    { this.state.Cancellation ? <Cancellation />: null }
                                    { this.state.ExpressDelievery ? <Edelievery />: null }
                                </div>
                         </div>
                 </div>
             </div>

                {/* This is mobile Screen Only */}
                <div className="d-block d-sm-block d-lg-none d-cus-m">
                    <div container="col-sm-12 col-12">
                    <div className="container">
                            <div className="sub-headings-m">
                                <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                    <button className="btn customer-p-b-m" name="SandD" onClick={this.contentToDisplay}> Shipping and Delievery </button>
                                    <div className="stateClass">  { this.state.SandD ? <SandD />: null } </div>
                                </div>

                                 <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                    <button className="btn customer-p-b-m" name="Cancellation" onClick={this.contentToDisplay}>Cancellation </button>
                                   <div className="stateClass"> { this.state.Cancellation ? <Cancellation />: null } </div>
                                </div>

                               <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                    <button className="btn customer-p-b-m" name="Payments" onClick={this.contentToDisplay}>Payments </button>
                                    <div className="stateClass">  { this.state.Payments ? <Payments />: null }</div>
                                </div>

                                <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                     <button className="btn customer-p-b-m" name="EandR" onClick={this.contentToDisplay}> Exchanges and Returns </button>
                                     <div className="stateClass">   { this.state.EandR ? <EandR />: null }</div>
                                 </div>

                                <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                    <button className="btn customer-p-b-m" name="InternationalShipping" onClick={this.contentToDisplay}> International Shipping </button>
                                    <div className="stateClass">  { this.state.InternationalShipping ? <InShipping />: null }</div>
                                </div>

                                 <div className="d-block d-sm-block d-sm-12 d-col-12 cus-cp">
                                    <button className="btn customer-p-b-m " name="ExpressDelievery" onClick={this.contentToDisplay}> Express Delievery </button>
                                    <div className="stateClass">   { this.state.ExpressDelievery ? <Edelievery />: null }</div>
                                </div>
                            </div>
                            </div>
                        </div>
                </div>
                      
                <div id="product-footer" className="col-md-12 product-footer">
                        <Footer/>
                        <div className="col-md-12 footer_bg2">
                            <p className="footer_end">All Rights Reserved | 2018</p>
                        </div>
                    </div>
           </div>


        )
    }
   }



export default customerPolicies;
