import React, {Component} from 'react'
// import HeaderProduct from '../components/header_product.controller'
import HeaderProduct from '../components/header_product.controller';
import WishlistComponent from './../components/wishlistComponent.controller';
import WishlistComponentMobile from './../components/wishlistComponentMobile.controller';
import * as  config from '../../config.js';
import '../../css/cart.css'

import cartItem from '../../img/cart_item.png'
import Bill from '../components/cartBill.controller'

class Cart extends Component{
    constructor(props){
        super(props);
        this.state = {
            images: {},
            dataGotten: false
        }

    }

    componentDidUpdate() {
        if (this.props.wishlistData && !this.state.dataGotten) {
            console.log(this.props.wishlistData);
            this.setState({dataGotten: true});
            this.props.wishlistData.wishlist
                .map((item, index) => {
                    // this.props.getImages(item.sku);
                })
        }


        if (this.props.addToCartMessage) {
            console.log(this.props.addToCartResult, this.props.addToCartMessage);
        }
    }

    componentDidMount() {
        this.props.getWishlist();
    }


    render(){
        return(
            <div id = 'loader'>
                <HeaderProduct/>

                <div class="container-fluid wishlist-img no-padding cart-main-container">
                    <div class="row no-margin cart-hide"><br />
                        <div class="col-md-10 col-sm-10 col-10 offset-1 no-padding cart-heading-center">
                            <div class="col-md-12 col-sm-12 col-12 no-padding" style={{"margin-left": "-2%"}}><span className="no-padding cart-heading-text">Wishlist</span><br /><span className="cart-heading-subtext">Dont miss out on your favorite styles</span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12">&nbsp;</div>
                            <div class="col-md-12 col-sm-12 col-12 no-padding">
                                <div class="col-md-12 col-sm-12 col-12 d-inline-block align-top no-padding" style={{"margin-left": "-2%"}}>
                                    <table class="table table-bordered" style={{"border": "solid 1px #cbcbcb","background": "#ffffff"}}>
                                        <thead>
                                        <tr>
                                            {/*<th></th>*/}
                                            <th className="cart-table-header">&nbsp;</th>
                                            <th className="cart-table-header">Product Name</th>
                                            <th className="cart-table-header">Size</th>
                                            <th className="cart-table-header">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        {this.props.wishlistData && this.props.wishlistData.wishlist.length !== 0 ? this.props.wishlistData.wishlist
                                            .map((item, index) => {
                                                console.log(this.props.item_images);
                                                if(this.props.item_images === undefined || this.props.item_images[item.sku] === undefined) return
                                                let url = `${config.IMG_URL}${this.props.item_images[item.sku][0].file}`

                                                return <WishlistComponent
                                                index={index + 1}
                                            img={url}
                                            onAddToCartButtonClick={() => {
                                                this.props.addToCart(item.sku, item.qty, item.color_id, item.color_value, item.size_id, item.size_value)
                                                this.props.deleteItemInWishlist(item.sku)
                                            }}
                                            onDeleteButtonClick={() => this.props.deleteItemInWishlist(item.sku)}
                                            item={item} />}) : <p>No items in the wishlist</p>}

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div className="row no-margin d-md-none">
                        <div className="col-12 text-center"><span className="no-padding cart-heading-text">Wishlist</span><br /><span className="cart-heading-subtext">Check all your products in your cart before checking out below</span></div>
                        <div className="col-11 mx-auto cart-mobile-container-top">
                        {this.props.wishlistData && this.props.wishlistData.wishlist.length !== 0 ? this.props.wishlistData.wishlist
                                            .map((item, index) => (<WishlistComponentMobile
                                            index={index + 1}
                                            onDeleteButtonClick={() => this.props.deleteItemInWishlist(item.sku)}
                                            item={item} />)) : <p>No items in the wishlist</p>}
                                    {/* <div className="row no-margin cart-mobile-top-container-padding">
                                        <div className="col-4 no-padding">
                                            <img src='' width="96px" height= "145px" />
                                        </div>
                                        <div className="col-8">
                                            <div className="col-12 d-inline-block no-padding cart-mobile-items-text">{this.props.item.name}</div>
                                            <div className="col-12 d-inline-block no-padding cart-mobile-price">&#x20b9; 983247</div>
                                            <div className="col-12 d-inline-block no-padding cart-mobile-qty-padding">
                                                <div className="col-4 d-inline-block cart-item-remove text-center align-middle">
                                                    <i class="fa fa-trash-o fa-2x"></i><br />REMOVE
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Cart