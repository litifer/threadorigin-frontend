import React, {Component} from 'react'
import '../../css/prelaunch.css'
import ScrollMagic from 'scrollmagic'

import background from '../../img/background.png'
import logo from '../../img/homepage_logo.png'

class PreLaunch extends Component{

    constructor(props){
        super(props)
        this.state = {value : ""}
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        console.log("hey",this.state.value)
        alert('An email was submitted: ' + this.state.value);
        event.preventDefault();
        this.props.subscribe_email(this.state.value);
    }
    
   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

    new ScrollMagic.Scene({triggerElement:"#footer-visibility-start"})
          .triggerHook("onEnter")
          .setClassToggle("#product-footer","footer-visible")
          .addTo(scrollfooterController)
          // .setClassToggle("#product-footer","footer-visible")
          
        }

    render(){
        return(
            <div>
                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                    <img src={background} className="prelaunch-img" />

                    
                </div>
                <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding absolute">
                    <div className="col-md-5 col-sm-5 offset-6 float-md-left float-sm-left no-padding prelaunch-logo-top-margin">
                        <img src={logo} className="prelaunch-logo-margin    " />
                    </div> 
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                        <div className="col-md-10 col-sm-10 offset-1 float-md-left float-sm-left no-padding">
                            <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">
                                <div className="col-md-6 col-sm-6 float-md-left float-sm-left no-padding prelaunch-middle-border">
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                           
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                        <span className="prelaunch-left-text">Join our newsletter and recieve special vouchers when we launch.</span>
                                    </div>  
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                           
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                    <form onSubmit = {this.handleSubmit}>
                                        <div className="col-md-9 col-sm-9 float-md-left float-sm-left no-padding">
                                            <input type="email" placeholder="brucelener@gmail.com" className="form-control prelaunch-input-box" value = {this.state.value} onChange={this.handleChange} />
                                        </div>                                        
                                        <div className="col-md-3 col-sm-3 float-md-left float-sm-left no-padding">
                                              <button type="submit" className="btn prelaunch-btn"><span className="prelaunch-btn-text">SUBMIT &nbsp;&nbsp;&nbsp;&nbsp;---</span></button>  
                                        </div> 
                                    </form>                                        
                                    </div>                           
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div> 
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                         
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                           
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                        <span className="prelaunch-left-text font-18">Thread Origins has helped thousands of forward thinking merchants make millions in sales.</span>
                                    </div> 
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                                                                                   
                                </div>
                                <div className="col-md-6 col-sm-6 float-md-left float-sm-left no-padding">
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                    
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left">
                                        <span className="prelaunch-left-text font-14">Do you love sobriety and seek timeless elegance? Do you prefer to play with the mix and match, combining nuances and seemingly antipodal materials? <br />Do you prefer a casualwear that finds its source of inspiration in the metropolitan lifestyle of overseas? No problem! <br /><br />All fashion is just a click away in our shop. You will have the opportunity to intercept the latest trends, but also to rediscover the great classics of the wardrobe. <br />What are the ingredients that make our collections unique? <br />
The variety of models, shades and materials, but also the quality of a selected fashion proposal, paying particular attention to details and finishes. 
                                        </span>
                                    </div> 
                                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                               
                                </div>
                            </div>                            
                        </div>                        
                    </div>
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                               
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                               
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                               
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                                                                                               
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding">&nbsp;</div>                
                    <div className="col-md-12 col-sm-12 float-md-left float-sm-left no-padding text-center">
                        <span className="prelaunch-left-text font-34">Stay tuned, we’ll be back with our site soon!</span>
                    </div>                    
                </div>
                
                
                

            </div>
        )
    }
}

export default PreLaunch