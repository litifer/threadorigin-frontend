import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import CheckoutFinalSummary from '../components/checkoutFinalSummary.controller'
import '../../css/checkout.css'

import cartItem from '../../img/cart_item.png'

class CheckoutGuestDiffAddress extends Component{
    render(){
        return(
            <div>
                <HeaderProduct/>



                <div class="main-container checkout-big-img no-padding">
                    <div class="col-md-12 col-sm-12 float-sm-left float-md-left no-padding"><br />
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div class="col-md-6 col-sm-6 float-sm-left float-md-left no-padding checkout-heading-margin"><span className="no-padding checkout-heading">Checkout</span><br /><span className="checkout-top-text">Just a few steps and you’ll complete the order</span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 float-left float-sm-left float-md-right no-padding">
                                <div class="col-md-12 col-sm-12 col-12 float-left float-sm-left float-md-left no-padding">&nbsp;</div>
                                <div class="col-md-12 col-sm-12 col-12 float-left float-sm-left float-md-left no-padding">
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-center">
                                        <span className="checkout-other-text">Login/Register</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-right">
                                        <span className="checkout-active-text">Address Confirmation</span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 float-left float-sm-left float-md-left no-padding text-right">
                                        <span className="checkout-other-text">Payment Options</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                        <div class="col-md-10 col-sm-10 offset-1 float-sm-left float-md-left no-padding">
                            <div className="col-md-9 col-sm-12 float-sm-left float-md-left no-padding checkout-box">
                                <div className="col-md-11 col-sm-11 float-sm-left float-md-left no-padding checkout-margin">
                                    <input type="checkbox" className="checkout-guest-checkbox" /><span className="checkout-guest-checkbox-text">Set Delivery Address same as the Billing Address</span><br /> 
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding checkout-border-right">
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                <span className="checkout-guest-text">Billing Address</span>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                <span className="checkout-guest-label">Email Address:</span>
                                            </div>  
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                <input type="text" className="checkout-guest-input form-control"  />
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">First Name:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Last Name:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                <span className="checkout-guest-label">Address:</span>
                                            </div>  
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                <input type="text" className="checkout-guest-input form-control"  />
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Address 2 (Optional):</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Pincode:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">City:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Region (Optional)</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Phone No:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <span className="checkout-guest-label">Country</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-left">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <button className="btn checkout-btn col-md-5"><span className="checkout-btn-text">SAVE ADDRESS&nbsp;&nbsp;&nbsp;----</span></button>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <span className="checkout-guest-text">Shipping Address</span>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <span className="checkout-guest-label">Email Address:</span>
                                            </div>  
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <input type="text" className="checkout-guest-input form-control"  />
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">First Name:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Last Name:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <span className="checkout-guest-label">Address:</span>
                                            </div>  
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <input type="text" className="checkout-guest-input form-control"  />
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Address 2 (Optional):</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Pincode:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">City:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Region (Optional)</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Phone No:</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 float-sm-left float-md-left no-padding">
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <span className="checkout-guest-label">Country</span>
                                                    </div>  
                                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                        <input type="text" className="checkout-guest-input form-control"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left padding-right">
                                                <button className="btn checkout-btn col-md-5"><span className="checkout-btn-text">SAVE ADDRESS&nbsp;&nbsp;&nbsp;----</span></button>
                                            </div>
                                            <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 float-sm-left float-md-left no-padding">&nbsp;</div>
                                    <div className="col-md-5 col-sm-5 offset-4 float-md-left float-sm-left no-padding">
                                        <button className="btn checkout-guest-bottom-btn col-md-12 float-md-right"><span className="checkout-guest-bottom-btn-text">CONTINUE TO LAST STEP&nbsp;&nbsp;&nbsp;----</span></button>
                                    </div>
                                </div>
                            </div>
                            <CheckoutFinalSummary />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default CheckoutGuestDiffAddress