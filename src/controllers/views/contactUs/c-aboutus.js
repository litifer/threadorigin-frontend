import React from 'react';
import './../../../css/contactus/c-aboutus.css';
import ImageLogo from "../../../img/contactus/caboutus.png";
import MadeInIndia from "../../../img/contactus/cmadeinIndia.png";


const CaboutUs = (props) => {

    return (
   
        <div className="CaboutUs">
           <div className="somethingtop">
            <div className="row">
                <div className="c-image-1"> 
                <img src={ImageLogo} className="image-component-class-cus-1"/>
                </div>
          
               
               
            
                     {/* Add a link for about us page in the end*/}
                     <p className="p-CaboutUs"> THREAD ORIGIN is born out of love for craft, culture, fashion and a connect. It is an inclusive platform, for all fashion dreams to be turned into reality. A platform, which weaves a connect with one another by the thread. Our belief, that each one of us is a unique thread, woven into the beautiful fabric of our collective consciousness. Read More
                     </p>
            
              
                <div className="c-image-2">               
                <img src={MadeInIndia} className="image-component-class-cus-2"/>            
                </div>


            </div>
            </div>
        </div>
    )
}

export default CaboutUs;