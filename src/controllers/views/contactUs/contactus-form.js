import React from 'react';
import './../../../css/contactus/c-form.css';

const Cform = (props) => {
    return (
        <div className="cformMain"> 
            <h1 className="main-heading-f"> Fill in the form below and we’ll get back to you! </h1>
            <form>
                <div class="form-group">
                <div class="form-group">
                    <label for="formGroupExampleInput" className="formgroup1">Name *:</label>
                    <input type="text" className="form-control formGroupExampleInput"  placeholder="Bruce Banner"></input>
                      </div>

                    <label for="exampleInputEmail1" className="formgroup2">Email address:*</label>
                    <input type="email" className="form-control formGroupExampleInput" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"></input>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1" className="formgroup2">Example textarea:*</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                </div>

                <button type="submit" className="btn btn-default btn-cus-change">Submit</button>
            </form>
        </div>

    )
}

export default Cform; 