import React from 'react';
import './../../../css/contactus/c-main.css'
import ContactUS from './contactus-form.js'
import {Urls,ContactInfo} from '../../../consts' 


const Cmain = (props) => {


    return (
        <div className="cMain">
                <div className="row">
                    <div className="col-md-6 col-lg-6  col-11 col-sm-11 cmain-0">
                        <div className="cmain-1">
                        <p className="p-cmain-1"> If you want to track your order click here </p>
                        <p className="p-cmain-1"> <span className="text-bold">Shipping Policies: </span> Have concerns about our shipping policies, read them <a href={Urls.FAQ_URL}>here</a></p>
                        <p className="p-cmain-1"> <span className="text-bold">Terms and Conditions: </span> Want to know about our terms and conditions, click <a href={Urls.TNC_URL}>here</a> </p>
                        </div>

                        <div className="cmain-2">
                            <h1 className="h-cmain"> Customer Care </h1>

                            <div className="contact-ab"> 
                             <div className="col-md-8 col-lg-8">
                                <div className="row">
                                    <p className="GiveACall"> Give us a call </p>
                                    <p className="GiveACall1"> {ContactInfo.MOBILE_NO} </p>
                                </div>
                                 <div className="row">
                                        <p className="GiveACall"> Drop us an email: </p>
                                        <p className="GiveACall1"> {ContactInfo.EMAIL}</p>
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-lg-4 cmain-1-f col-11 col-sm-11">
                    <div className="contactUsForm">
                        <ContactUS />
                </div>
                </div>
            </div>
    </div>
    )
}

export default Cmain;
