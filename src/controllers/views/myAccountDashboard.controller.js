import React, {Component} from 'react'
import HeaderProduct from '../components/header_product.controller'
import Footer from '../components/footer.controller';
import DashboardComponent from '../components/dashboardComponent.controller';
import AddressComponent from '../components/address.controller';
import {withRouter} from 'react-router-dom';
import '../../css/myaccount.css'
import ScrollMagic from 'scrollmagic'

import accountRight from '../../img/account_dashboard.png'

class AccountDashboard extends Component{

    constructor(props){
        super(props)
        this.state = {
            userData: null
        }
    }

    componentDidUpdate() {
        if (this.props.userData) {
            if (!this.state.userData) {
                this.setState({userData: this.props.userData});
            }
        }
    }
    
   componentDidMount(){
    //footer scroll
    let scrollfooterController = new ScrollMagic.Controller();

       if (!window.localStorage.getItem('authToken')) {
           console.log(this.props);
           return this.props.history.replace('/login');
           // return <Redirect to="localhost:3000/login"/>
       }

       if (!this.props.userData) {
           this.props.getUserData();
       }
          
        }

    render(){
        
        let defaultAddress = null;
        let otherAddresses = [];
        
        if (this.state.userData) {
            for (let i = 0; i < this.state.userData.addresses.length; i++) {
                let x = {...this.state.userData.addresses[i]};
                let ind = 0;
                if (this.state.userData.addresses[i].default_billing) {
                    defaultAddress = {...this.state.userData.addresses[i]};
                } else {
                    ind++;
                    otherAddresses.push(<AddressComponent addressType={'Address ' + ind}
                                                          fullName={x.firstname + ' ' + x.lastname}
                                                          street={x.street}
                                                          city={x.city}
                                                          onDeleteButtonClick={() => this.props.deleteAddress(x.id)}
                                                          pinCode={x.postcode}
                                                          country={x.country_id}
                                                          phone={x.telephone} />);
                }
            }
        }
        
        return(
            <div>
                <HeaderProduct />

                
                <div className="main-container row no-margin account-mobile-none">
                    <div className="col-md-2 col-sm-2 col-12 account-bg-img no-padding position-fixed">
                        {/* <img src={accountRight} /> */}
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding absolute">
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                            
                            <div className="col-md-10 col-sm-10 mx-auto col-12 d-inline-block no-padding">
                                <div className="col-md-12 col-sm-12 d-inline-block account-img-text-padding">
                                    <a href="/accountDashboard" className="account-img-text-active">—  Account Dashboard</a>
                                </div>
                                <div className="col-md-12 col-sm-12 d-inline-block account-img-text-padding">
                                    <a href="/accountInfo" className="account-img-text">Account Information</a>
                                </div>
                                <div className="col-md-12 col-sm-12 d-inline-block account-img-text-padding">
                                    <a href="/accountAddress" className="account-img-text">Address Book</a>
                                </div>
                                <div className="col-md-12 col-sm-12 d-inline-block account-img-text-padding">
                                    <a href="/accountOrders" className="account-img-text">My Orders</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <a href="/wishlist" className="account-img-text">Wishlist</a>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block account-img-text-padding">
                                    <span onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }}  className="account-img-text">Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-9 col-12 account-content-margin d-inline-block no-padding">
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Hello{this.state.userData ? ' ' + this.state.userData.firstname + ' ' + this.state.userData.lastname : null},</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-text">From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Account Information</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {this.state.userData ? <DashboardComponent onEditButtonClick={() => this.props.history.push('/accountInfo')} heading={'Contact Details'} data={{Name: this.state.userData.firstname + ' ' + this.state.userData.lastname,Email: this.state.userData.email}}/> : null}
                            <div className="col-md-5 col-sm-5 col-12 d-inline-block padding-right">
                                <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding account-box">
                                    <div className="col-md-10 col-sm-10 offset-1 col-12 d-inline-block no-padding account-box-top-margin absolute">
                                        <div className="col-md-4 col-sm-4 col-12 d-inline-block account-top-box">
                                            <span className="account-top-box-text">Subscriptions</span>
                                        </div>                                        
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block">    
                                        <span className="account-box-text">You are currently subscribed to our weekly newsletter.</span>                                      
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                    <div className="col-md-10 col-sm-10 offset-1 col-12 d-inline-block no-padding">
                                        <button className="btn col-md-2 float-right acoount-btn"><span className="account-btn-text">EDIT</span></button>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            <span className="account-name">Address Book</span><span className="account-text-subscript">Manage Addresses ({defaultAddress ? otherAddresses.length + 1 : otherAddresses.length})</span>
                        </div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                        
                        <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                            {defaultAddress ? <AddressComponent addressType={'Default Address'}
                                                           fullName={defaultAddress.firstname + ' ' + defaultAddress.lastname}
                                                           street={defaultAddress.street}
                                                           city={defaultAddress.city}
                                                           onDeleteButtonClick={() => this.props.deleteAddress(defaultAddress.id)}
                                                           pinCode={defaultAddress.postcode}
                                                           country={defaultAddress.country_id}
                                                           phone={defaultAddress.telephone} /> : null}
                            {otherAddresses.length > 0 ? otherAddresses : null}
                            <div className="col-md-12 col-sm-12 d-inline-block no-padding">
                                <button onClick={() => this.props.history.push('/accountAddress')} type='submit' className="col-md-2 col-sm-2 btn account-margin account-submit-btn"><span
                                    className="account-submit-btn-text">MANAGE ADDRESS</span><span className="account-btn-line"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="account-bg row no-margin main-container account-web-none">
                    <div className="col-12">
                        <button type="button" className="btn col-12 mx-auto d-block account-mobile-container-top-collapse-btn" type="button" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">
                            Account Information<br /><span className="account-mobile-collapse-text">(Click here for more account settings)</span>
                        </button>
                        {/* <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Button with data-target
                        </button> */}

                        <div class="collapse" id="collapseAccount">
                            <div class="card col-12 mx-auto card-body account-collapse-bg">
                                <ul className="account-ul no-padding">
                                    <li><a href="/accountDashboard" className="account-mobile-li-text">Account Dashboard</a></li>
                                    <li><a href="/accountInfo" className="account-mobile-li-text">Account Information</a></li>
                                    <li><a href="/accountAddress" className="account-mobile-li-text">Address Book</a></li>
                                    <li><a href="/accountOrders" className="account-mobile-li-text">My Orders</a></li>
                                    <li><a href="/wishlist" className="account-mobile-li-text">Wishlist</a></li>
                                    <li onClick={() => {
                                        window.localStorage.removeItem('authToken');
                                        window.localStorage.removeItem('cartId');
                                        window.localStorage.removeItem('userCartId');
                                        this.props.history.replace('/dev');
                                    }} className="account-mobile-li-text">Logout</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-12 d-inline-block account-bg-color">
                            <div className="col-12 d-inline-block no-padding account-name">Hello{this.state.userData ? ' ' + this.state.userData.firstname + ' ' + this.state.userData.lastname : null},</div>
                            <div className="col-12 d-inline-block no-padding account-text">From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-12 d-inline-block no-padding account-name">Account Information</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                {this.state.userData ? <DashboardComponent onEditButtonClick={() => this.props.history.push('/accountInfo')} heading={'Contact Details'} data={{Name: this.state.userData.firstname + ' ' + this.state.userData.lastname,Email: this.state.userData.email}}/> : null}
                                <div className="col-12 d-inline-block no-padding">&nbsp;</div>
                                <div className="col-12 d-inline-block no-padding">&nbsp;</div>                                
                                <div className="col-md-5 col-sm-5 col-12 d-inline-block padding-right">
                                    <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding account-box">
                                        <div className="col-md-10 col-sm-10 offset-1 col-12 d-inline-block no-padding account-box-top-margin absolute">
                                            <div className="col-md-4 col-sm-4 col-4 d-inline-block account-top-box">
                                                <span className="account-top-box-text">Subscriptions</span>
                                            </div>                                        
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block">    
                                            <span className="account-box-text">You are currently subscribed to our weekly newsletter.</span>                                      
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                        <div className="col-md-10 col-sm-10 offset-1 col-12 d-inline-block no-padding">
                                            <button className="btn col-md-2 col-2 d-inline-block acoount-btn"><span className="account-btn-text">EDIT</span></button>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                <span className="account-name">Address Book</span><span className="account-text-subscript">Manage Addresses ({defaultAddress ? otherAddresses.length + 1 : otherAddresses.length})</span>
                            </div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>
                            <div className="col-md-12 col-sm-12 col-12 d-inline-block no-padding">&nbsp;</div>                        
                            <div className="col-md-11 col-sm-11 col-12 d-inline-block no-padding">
                                {defaultAddress ? <AddressComponent addressType={'Default Address'}
                                                            fullName={defaultAddress.firstname + ' ' + defaultAddress.lastname}
                                                            street={defaultAddress.street}
                                                            city={defaultAddress.city}
                                                            onDeleteButtonClick={() => this.props.deleteAddress(defaultAddress.id)}
                                                            pinCode={defaultAddress.postcode}
                                                            country={defaultAddress.country_id}
                                                            phone={defaultAddress.telephone} /> : null}
                                {otherAddresses.length > 0 ? otherAddresses : null}
                                <div className="col-md-12 col-sm-12 d-inline-block no-padding">
                                    <button onClick={() => this.props.history.push('/accountAddress')} type='submit' className="col-md-2 col-sm-2 col-6 btn account-margin account-submit-btn"><span
                                        className="account-submit-btn-text">MANAGE ADDRESS</span><span className="account-btn-line"></span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>
                

            </div>
        )
    }
}

export default withRouter(AccountDashboard)