import React from 'react'
import {ContactInfo} from '../index'

export default [
    {
        userId: 1,
        id: 1,
        title: "Is my purchase eligible for free return or exchange?",
        body: <ul>Your purchase is eligible for return or exchange if it meets the following conditions:
                <li>An incorrect product has been delivered to you i.e. the product does not match the item in the order confirmation email</li>
                <li>If the product you receive has a genuine quality/manufacturing defect</li>
                Given the nature of our products, we reserve sole discretion to provide resolution as we deem fit. Every return or exchange request is treated as an individual case. We are unable to offer refunds if we’ve been given an incorrect or incomplete shipping address, or if there are three failed delivery attempts by our shipping agency and/or the package is refused by the recipient.
            </ul>
    }, 
    {
        userId: 1,
        id: 2,
        title: "How do I return my purchase?",
        body:<div> 
        <ul>If you would like to return any of the items purchased please contact our care team within 10 days of delivery with the following information:
            <li> Order number</li>
            <li> Delivery address</li>
            <li>Specify the reason for return and in case of a defective or incorrect product, please send us an image of the item</li>
        </ul><br></br><br></br>
        You may contact us via email at ${ContactInfo.EMAIL} or call our customer care at ${ContactInfo.MOBILE_NO} from ${ContactInfo.CUSTOMER_CARE_DAYS}, between ${ContactInfo.CUSTOMER_CARE_TIMINGS}. We will look into the issue and respond to you within 3 working days.
        
        <br></br><br></br>

        A reverse pick-up will be scheduled within 3-4 business days. Please ensure that the product you return is unused, unworn and the original tags are intact.
        </div>
    }, 
    {
        userId: 1,
        id: 3,
        title: "What if I need a different size in the same clothing style I have purchased?",
        body: <div>
            Please refer to the size chart before you make a purchase, and in case you are confused about your size, do write to us at ${ContactInfo.EMAIL} or chat online with us.
            <br></br><br></br>
            If you would like a different size in the same style of clothing that you have purchased, you will need to go through the following steps:
            <ol>
                <li>Contact us to schedule a reverse pickup.</li>
                <li>Once the unused product is received at our end, we will issue you a credit note (after deducting the applicable shipping charges).</li>
                <li>You can then use the credit note to place a new order and choose the correct size.</li>
            </ol><br></br><br></br>
            Please note that size exchanges will not be possible if the product is purchased during a period of special pricing.
        </div>
    },
    {
        userId: 1,
        id: 4,
        title: "When will I receive the exchanged item?",
        body: <div>Once the item dispatched from your end reaches our warehouse, the replacement product will be shipped out to you and will reach you in our standard delivery time i.e. 5 working days for domestic shipments.</div>
    },
    {
        userId: 1,
        id: 5,
        title: "When will I receive the store credit or refund?",
        body: <ul>
            <li>We will be happy to, following a quality check, offer you a refund or store credit, as applicable in your case. This will be processed within 10 business days</li>
            <li>Any refund will be made in the original method of payment</li>
            <li>If your purchase is eligible for a free return or exchange as explained above, the credit note or refund will also include the proportionate shipping cost. This does not apply to exchanges for size</li>
            </ul>
    },
    {
        userId: 1,
        id: 6,
        title: "Terms and conditions",
        body:<ul>
        <li>Credit notes for any returns or exchanges will only be sent to the registered email address or the email address has been used to make the purchase</li>
        <li>Replacements will be offered based on availability of product. If we don't have the replacement product, we'll be happy to offer you a credit note or a refund</li>
        <li>Proportionate shipping will be refunded only if it meets our conditions for a free return i.e. a quality or wrongly delivered product</li>
        <li>We offer replacements, or size exchanges in case of clothing, only for the same product or style you have purchased</li>
        <li>We are unable to entertain requests to replace a product with a different product or style, even if the price is the same</li>
        <li>House and Home products can be returned only in case of a genuine quality defect</li>
        <li>International offers are not eligible for exchanges or returns</li>
    </ul>
    }       
]