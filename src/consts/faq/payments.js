import React from 'react'
import {ContactInfo} from '../index'

export default [
    {
        userId: 1,
        id: 1,
        title: "What are the accepted payment methods for domestic transactions?",
        body: <ul>We accept the following modes of payment for domestic transactions:
            <li>All major Credit Cards - Mastercard, Visa and American Express</li>
            <li>Net Banking - all major banks</li>
            <li>All major Debit Carts - all major banks</li>
            <li>Digital Wallets</li>
        </ul>
    }, 
    {
        userId: 1,
        id: 2,
        title: "What are the accepted payment methods for international transactions?",
        body: <ul>We accept the following modes of payment for international transactions:
            <li> Credit cards - Mastercard, Visa and American Express</li>
            <li> Paypal</li>
        </ul>
    }, 
    {
        userId: 1,
        id: 3,
        title: "What are the accepted payment methods for international transactions?",
        body: "To use these online, simply enter the unique code provided when you check out. You may also redeem them at one our stores. If your order value exceeds the balance amount on the gift card or credit note, the balance must be paid at checkout."
    },
    {
        userId: 1,
        id: 4,
        title: "Terms of use for your credit note, gift card or voucher:",
        body: <ul>
        <li> To apply a credit note, login from the same email ID to which the note has been issued</li>
        <li> A gift card cannot be used to purchase another gift card</li>
        <li> A gift card cannot be cancelled so ensure you enter the correct details of the recepient</li>
        <li> Multiple gift cards and credit notes can be used together to pay for a single order. The same credit note can be used until the balance gets over</li>
        <li> No two vouchers can be clubbed. However, you may club a voucher with a credit note or gift card</li>
        <li> Validity of credit notes, gift cards and vouchers cannot be extended</li>
        <li> If the payment was made using a credit note/gift card, any refund against the purchase will be in the same form</li>
    </ul>
    },
    {
        userId: 1,
        id: 5,
        title: "What if my card has been charged multiple times for the same order?",
        body: `If your card has been debited multiple times at the payment gateway(s) while making a single purchase, call our customer care at ${ContactInfo.MOBILE_NO} or write to us at ${ContactInfo.MOBILE_NO}, and we will resolve it for you.
        Our customer care team is available from ${ContactInfo.CUSTOMER_CARE_DAYS}, from ${ContactInfo.CUSTOMER_CARE_TIMINGS}`
    },
    {
        userId: 1,
        id: 6,
        title: "I am being charged GST amount on my order. What is GST?",
        body: "GST is a single tax on the supply of goods and services that is levied on every value addition (through production and services) and is added to a product's sale price. GST has to borne/paid by the ultimate consumer of the product or service. If your order is fulfilled on or after July 1st 2017, GST will be applicable on your orders. GST subsumes all other taxes like Excise duty, VAT, Entry tax etc."
    },
    {
        userId: 1,
        id: 7,
        title: "How is the GST amount decided?",
        body: <ol>Following rules will govern whether or not additional GST will be applicable on the products purchased by you:
            <li>GST applicability: For a product, if the fulfilment is done on or after July 1st, 2017 and total discount percentage is more than 19% of MRP, then GST may be collected from customer in addition to product price, post discounts. The discounts include those resulting from special offers such as Buy 1 Get 1 and similar offers.</li>
            <li>GST amount: If applicable, the amount of GST collected from customer depends on category</li>
            <li>Apparel/Clothing: Max 12% of net payable (MRP - product discount - coupon discount) will be collected from the customer.</li>
            <li>Footwear: Max 18% of net payable (MRP - product discount - coupon discount) will be collected from the customer.</li>
            <li>Home Furnishing: Min. 5% - Max. 28% of net payable (MRP - product discount - coupon discount) will be collected from the customer.</li>
            <li>Accessories/Other Categories: Max 28% of net payable (MRP - product discount - coupon discount) will be collected from the customer. The amount of GST shown at the cart level is a mere estimate. The actual GST payable shall be determined basis the warehouse (based on availability of the products) from where the goods are being shipped, which will be checked once the order is placed. Only in case of GST being lower than the estimate GST (based on the warehouse from where the goods would be shipped), the customer would be notified of the same and any additional GST paid by the customer would be duly refunded to the customer.</li>
        </ol>
    },
    {
        userId: 1,
        id: 8,
        title: "If I return/cancel the purchased product will the GST/VAT amount charged be refunded?",
        body: "Yes. If you return the product the applicable GST/VAT amount will also be refunded into the source account selected at the time of return initiation. However no refunds of GST/VAT shall be made in relation to shipping charges collected from the consumer under Thread Origin shipping policy."
    },        
]