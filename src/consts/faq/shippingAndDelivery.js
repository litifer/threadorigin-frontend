import React from 'react'
import {ContactInfo} from '../index'

export default   
    [
    {
        userId: 1,
        id: 1,
        title: "Domestic Delivery - Charges and Timing",
        body: <ul><li>We offer free shipping for all domestic orders above INR 5,000.</li>
        <li>Total shipping charges vary between INR 150 and INR 500, and will be calculated at checkout (charges are based on final weight and volume of the package to be delivered).</li>
        <li>We will never charge you more than INR 500 for shipping, regardless of your basket size.</li>
        <li>Domestic orders are usually delivered within 3-5 business day</li></ul>
    },    
    {
        userId: 1,
        id: 2,
        title: "How do I track my order?",
        body: <div>Once your order is shipped, the Airway Bill number (AWB no.) and courier partner name is sent to you via
         email. Please visit the courier partner website (Bluedart, Delhivery) and enter the AWB no. shared with you in
          the relevant form to track the current location of your shipment and expected date of delivery.<br></br><br></br>
          Should you have any questions about your order dispatch, we’re just a quick email or phone call away.<br></br><br></br>
          ${ContactInfo.MOBILE_NO}<br></br>
          {ContactInfo.EMAIL} | {ContactInfo.DAYS} | {ContactInfo.TIMINGS}</div>
    },
    {
        userId: 1,
        id: 3,
        title: "Where is my order processed from?",
        body: <div>Orders are processed both from our warehouse in Delhi, India .We want you, our guests, to have access to our entire catalogue an dorder accordingly. Orders will be delivered to the shipping address you submit at checkout.
            </div>
    },    
    {
        userId: 1,
        id: 4,
        title: "Who will deliver my order?",
        body: <div>Within India, we ship through registered courier companies DTDC and other delivery partners. Our delivery partners will attempt to deliver the package thrice before they return it to us.
        <br></br><br></br>
        Please provide a mobile number that you are available at, and your complete shipping address including the PIN CODE. This will help us ensure smooth delivery of your order. At the time of delivery, if the packaging looks damaged or tampered, please do not accept the package.</div>
    },
    {
        userId: 1,
        id: 5,
        title: "Do you offer in-store pick-up for online orders?",
        body: "No, as of now we are available online only."
    },    
    {
        userId: 1,
        id: 6,
        title: "If I have placed two separate orders, can you combine the orders and offer me reduced shipping?",
        body: "Unfortunately, once your orders are placed we cannot combine multiple orders due to regulatory and compliance issues. Shipping will be applicable on every new order (if it does not fall under the free shipping criteria)."
    },
    {
        userId: 1,
        id: 7,
        title: "Shipping terms and conditions",
        body: <ul>
            <li>Any part cancellation or refund of the order may incur shipping charges on the balance order.</li>
            <li>Saturdays, Sundays and public holidays are not set as business days for standard deliveries. </li>
            <li>To qualify for free shipping, your product total after any discount should be above INR 5,000 (domestic orders) or USD 250 (international orders).</li>
        </ul>
    }
]
