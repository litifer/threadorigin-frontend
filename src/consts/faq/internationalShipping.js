import React from 'react'
import {ContactInfo} from '../index'

export default [
    {
        userId: 1,
        id: 1,
        title: "Charges and Timing",
        body: <ul>
                <li>A flat shipping fee of USD 30 is applied to international orders below total basket size of USD 250 </li>
                <li>THREAD ORIGIN offer free shipping for all international orders above USD 200</li>
                <li>International orders are usually delivered within 10 business days.</li>
                <li>International shipments are not eligible for returns or exchange</li>
            </ul>
    }, 
    {
        userId: 1,
        id: 2,
        title: "How do I track my order?",
        body: "Once your order is shipped, the Airway Bill number (AWB no.) and courier partner name is sent to you via sms & email. Please visit the courier partner website and enter the AWB no. shared with you in the relevant form to track the current location of your shipment and expected date of delivery."
    }, 
    {
        userId: 1,
        id: 3,
        title: "Who will deliver my order?",
        body: <span><b>THREAD ORIGIN</b> has partnered with registered international courier companies to deliver your order. At the time of delivery, if the packaging looks damaged or tampered, please do not accept the package.</span>
    }, 
    {
        userId: 1,
        id: 4,
        title: "Where is my order processed from?",
        body: <ul>
            <li>Orders are processed both from our warehouse in DELHI, India. We want you, our guest, to have access to our entire catalogue and check the stock avaialbility before ordering.</li>
            <li>Orders will be delivered to the shipping address mentioned at checkout.</li>
        </ul>
    }, 
    {
        userId: 1,
        id: 5,
        title: "Which countries do you deliver to?",
        body: "We deliver to more than 230 countries. At checkout, when entering your shipping address, you will be able to select your home country from the drop down list."
    }, 
    {
        userId: 1,
        id: 6,
        title: "Do THREAD ORIGIN deliver to PO Box addresses",
        body: "No, it is not possible for us to deliver to PO Box addresses."
    }, 
    {
        userId: 1,
        id: 7,
        title: "Do I have to pay customs and import charges?",
        body: "Duties and taxes for international orders are not included as part of product or shipping charges. Most countries charge duties on imported items that are levied at the time of port entry and vary based on the destination country and the products imported. You will need to pay the applicable duties and taxes directly to the shipping agency at the time of your order delivery after you receive an invoice from the shipping agent."
    }, 
    {
        userId: 1,
        id: 7,
        title: "International shipping terms and conditions",
        body: <ul>
            <li>Any part cancellation or refund of the order may attract shipping charges on the balance order</li>
            <li>Saturdays, Sundays and national holidays in India are not set as business days for standard deliveries</li>
            <li>To qualify for free shipping, your product total after any discount should be above USD 200</li>
            <li>Duty as mandated by your country of shipment will have to be borne by you. In case of non-payment of duties and taxes to receive the shipment, THREAD ORIGIN will unfortunately not be able to entertain a request for refund</li>
            <li> International orders are not eligible for exchanges and returns</li>
        </ul>
    }, 
]