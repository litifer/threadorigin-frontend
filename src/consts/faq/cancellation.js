import React from 'react'

export default   
    [
    {
        userId: 1,
        id: 1,
        title: "What is THREAD ORIGIN Cancellation Policy by Customer?",
        body: "You can cancel an order until it has not been packed in our Warehouse on App/Website/M-site. This includes items purchased on sale also. Any amount paid will be credited into the same payment mode using which the payment was made"
    },
    {
        userId: 1,
        id: 2,
        title: "Can I modify the shipping address of my order after it has been placed?",
        body: "Yes, You can modify the shipping address of your order before we have processed (packed) it, by updating it under 'change address' option which is available under ‘My order’ section of App/Website/M-site"
    },
    {
        userId: 1,
        id: 3,
        title: "I just cancelled my order. When will I receive my refund?",
        body: "If you had selected Cash on Delivery, there is no amount to be refunded because you haven't paid for your order. For payments made via Credit Card, Debit Card, Net Banking, or Wallet you will receive refund into the source account within 7-10 days from the time of order cancellation. If payment was made by redeeming PhonePe wallet balance then, refund will be initiated into Phonepe wallet within a day of your order cancellation, which can be later transferred into your bank account, by contacting PhonePe customer support team."
    },
    {
        userId: 1,
        id: 4,
        title: "ORDER CANCELLATION BY THREAD ORIGIN",
        body: "Due to unavoidable circumstances there may be times when certain orders having been validly placed may not be processed or capable of being dispatched. THREAD ORIGIN reserves the exclusive right to refuse or cancel any order for any reason. Some situations that may result in your order being cancelled shall include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, problems identified by our credit and fraud avoidance department or any defect regarding the quality of the product. We may also require additional verifications or information before accepting any order. We will contact you if all or any portion of your order is cancelled or if additional information is required to accept your order. If your order is cancelled after your credit card/ debit card/ any other mode of payment has been charged, the said amount will be reversed into your Account/ as the case may be to the source of the payment within a period of 30 working days. Any type of voucher used in these orders shall be returned and made available to the user in case of cancellation by THREAD ORIGIN."
    }
]
