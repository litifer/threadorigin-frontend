import React, {Component} from 'react'
import { Route, Link } from 'react-router-dom'
import Register from  './containers/register.container'
import NewPassword from './containers/newPassword.container'
import Login    from  './containers/login.container'
import HomePage from  './containers/homepage.container'
import Category from  './containers/category.container'
import Cart     from  './containers/cart.container'
import Product  from  './containers/product.container'
import CampignLooks  from  './containers/campignLooks.container'
import AccountDashboard  from  './containers/myAccountDashboard.container'
import Wishlist  from  './containers/wishlist.container'
import AccountInfo  from  './containers/myAccountInfo.container'
import AccountOrders  from  './containers/myAccountOrders.container'
import AccountAddress  from  './containers/myAccountAddress.container'
import PreLaunch  from  './containers/preLaunch.container'
import CheckoutGuest  from  './containers/checkoutGuest.container'
import CheckoutGuestDiffAddress  from  './containers/checkoutGuestDiffAddress.container'
import PreWebsiteLaunch  from  './containers/preWebsiteLaunch.container'

import TNC  from  './controllers/views/PandC/TandC/TandC.js'
import FAQ  from  './controllers/views/PandC/CustomerPolicies/FAQ.js'
import AboutUs  from  './controllers/views/aboutUs/aboutus.js'
import privacyPolicy from './controllers/views/PandC/TandC/PrivacyPolicy.js'
import ContactUs  from  './controllers/views/contactUs/contactus.js'
import LookBooks  from  './controllers/views/lookbooks/lookbooks'
import DesignerMania from './controllers/views/designerMain/designer'


// import CustomerPolicies from "./controllers/views/PandC/CustomerPolicies/CustomerPolicies.js"


// import Checkout from './controllers/views/checkout.controller'

import Forgot from './containers/forgot.container'
import designerProfile from './controllers/views/designerProfile.controller'
import Checkout from './containers/checkout.container'



class Routes extends Component{
  render(){
    return(
    <div>
      {/* <header>
        <Link to="/">Home</Link>
        <Link to="/about-us">About</Link>
      </header> */}

      <main>
        <Route exact path="/dev" component={HomePage} />
        <Route exact path="/category" component={Category} />
        <Route exact path="/cart" component={Cart} />
        <Route exact path="/product" component={Product} />
        <Route exact path="/checkout" component={Checkout} />        
        <Route exact path="/checkoutGuest" component={CheckoutGuest} />        
        <Route exact path="/checkoutGuestDiffAddress" component={CheckoutGuestDiffAddress} />        
        <Route exact path="/login" component={Login} />        
        <Route exact path="/forgot" component={Forgot} />        
        <Route exact path="/register" component={Register} />        
        <Route exact path="/newPassword" component={NewPassword} />    
        <Route exact path="/accountDashboard" component={AccountDashboard} />    
        <Route exact path="/designerProfile" component={designerProfile} />
        <Route exact path="/accountInfo" component={AccountInfo} />
        <Route exact path="/accountOrders" component={AccountOrders} />    
        <Route exact path="/wishlist" component={Wishlist} />
        <Route exact path="/accountAddress" component={AccountAddress} />
        <Route exact path="/preLaunch" component={PreLaunch} />
        <Route exact path="/" component={PreWebsiteLaunch} />
        <Route exact path="/search" component={Category} /> 
        <Route exact path="/termsAndCondition" component={TNC} /> 
        <Route exact path="/faq" component={FAQ} /> 
        <Route exact path="/campaignLooks" component={CampignLooks} />
        <Route exact path="/privacycontent" component={privacyPolicy} /> 
        <Route exact path="/aboutus" component={AboutUs} />
        <Route exact path="/contactus" component={ContactUs} />
        <Route exact path="/lookbooks" component={LookBooks} />
        <Route exact path="/designers" component={DesignerMania} />
        {/* <Route exact path="/customerPolicies" component={CustomerPolicies} />  */}
    </main>
    </div>


    )
  }
}

export default Routes