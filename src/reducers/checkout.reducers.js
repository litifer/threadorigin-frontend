import {DEFAULT_ADDRESS} from './../actions/checkout.actions';

const initialState = {}

export default (state = initialState, action) => {
    switch (action.type) {
      case 'CHANGE_BOX':{
          console.log('Changing box to address with box_type ',action.data)
          return {
              ...state,
              box_type: action.data
          }
      }
      case 'ADD_BILLING_ADDRESS':{
          return {
              ...state,
              billing_address_id: action.address_id,
              billing_address: action.data
          }
      }
        case 'ORDER_MADE':{
            return {
                ...state,
                orderSuccessfull: !!parseInt(JSON.parse(action.data))
            }
        }
      case 'SHIPPING_SAME_AS_BILLING': {
          console.log('isnide reducer');
          console.log(action.data);
          return {
              ...state,
              shippingSameAsBilling: action.data.isSame,
              shipping_address: action.data.data
          }
      }
        case DEFAULT_ADDRESS:
            console.log('aagya address');
            return {
                ...state,
                defaultAddress: action.data
            };
      case 'SHIPPING_INFO': {
          return {
              ...state,
              shipping_info: action.data
          }
      }

      case 'LOAD_CHECKOUT_ITEMS':{
          return {
              ...state,
              checkout_items: action.data
          }
      }

      case 'CHECKOUT_ITEM_IMAGES': {
        let prop = action.sku
        let item_images = {
          ...state.item_images
        }

        item_images[prop] = action.data
          return {
              ...state,
              checkout_item_images: item_images
          }
      }
      case 'LOAD_CHECKOUT_PRICE': {
        return {
            ...state,
            checkout_bill: action.data
          }
      }
      default:
        return state
      }
}
