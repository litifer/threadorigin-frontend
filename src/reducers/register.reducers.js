import {REGISTER} from '../actions/index'

const initialState = {}

export default (state = initialState, action) => {
    switch (action.type) {
      case REGISTER:
          console.log(action.data.confirmation, "register reducer");
          return {
        ...state,
        register:action.data,
        isRegistered: !!action.data.confirmation
       };
        case 'SET_IS_REGISTERED_FALSE':
        return {
            ...state,
            isRegistered: false
        };
      default:
        return state
      }
}