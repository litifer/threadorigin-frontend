import {COLORSELECT} from '../actions/index'
import {SIZESELECT} from '../actions/index'
import {CATEGORY_CAMP} from '../actions/index'
import {FETCH_CATEGORY} from '../actions/index'
import {SIZE_FETCH} from '../actions/index'
import {COLOR_FETCH} from '../actions/index'
import {SEARCH_CATEGORY} from '../actions/index'

const category = {}
const initialState = {
  products: [],
  product_data: [],
  products_updated: false,
  color_filters: [],
  size_filters: []
}

export default (state = initialState, action) => {
    switch (action.type) {
      case 'ALL_CATEGORIES': return {
        ...state,
        categories: action.data
      }
      case 'FETCH_PRODUCTS': return{
        ...state,
        products: action.data
      }
      case 'SEARCH_PRODUCTS': return{
        ...state,
        product_data: action.data
      }
      case 'FETCH_PRODUCT_DATA': return{
        ...state,
        product_data: [...state.product_data, action.data]
      }
      case 'FETCH_CAROUSEL_DATA': return{
        ...state,
        product_data: [...state.product_data, action.data]
      }
      case 'SET_PRODUCT_UPDATE_FLAG': return {
        ...state,
        products_updated: action.data
      }
      case 'ADD_COLOR_FILTER': return {
        ...state,
        color_filters: [...state.color_filters, action.data] 
      }
      case 'REMOVE_COLOR_FILTER': return {
        ...state,
        color_filters: state.color_filters.filter(color => color !== action.data)
      }
      case 'ADD_SIZE_FILTER': return {
        ...state,
        size_filters: [...state.color_filters, action.data] 
      }
      case 'REMOVE_SIZE_FILTER': return {
        ...state,
        size_filters: state.color_filters.filter(color => color !== action.data)
      }
      case COLOR_FETCH:
       return {
        ...state,
        color_val:{options:action.data.data}
       }
       case SIZE_FETCH:
       {
         console.log("%c The size is: ","color: yellow")
         console.log(action.data)
         return{
        ...state,
        size_val:{options: action.data.data}
       }}
      case COLORSELECT:
      console.log(action.array_color)
        return{
            ...state,
            array_color: action.array_color
          }
      case SIZESELECT: 
      console.log(action.array_size)
        return{
          ...state,
          array_size:action.array_size
         }
      case CATEGORY_CAMP:
        return{
          ...state,
          camp:action.data
        }
      
      case SEARCH_CATEGORY:
        return {
          ...state,
          search_item:action.data
        }
      default:
        return state
      }
}