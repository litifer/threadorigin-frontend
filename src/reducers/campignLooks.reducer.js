import {GET_CAMPAIGN_LOOKS} from '../actions/campignLooks.actions'
import {GET_COLORS} from '../actions/campignLooks.actions'
import {GET_SIZES} from '../actions/campignLooks.actions'
import {GET_LOOKBOOKS} from '../actions/campignLooks.actions'
import {GET_PRODUCT_DETAILS} from '../actions/campignLooks.actions'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CAMPAIGN_LOOKS:
            return {
                ...state,
                lookbookData: action.data
            };
        case GET_LOOKBOOKS:
            return {
                ...state,
                lookbookFullData: action.data
            };
        case GET_SIZES:
            return {
                ...state,
                sizeChart: action.data
            };
        case GET_COLORS:
            return {
                ...state,
                colorChart: action.data
            };
        case GET_PRODUCT_DETAILS:
            let productDetails;
            if (state.productDetails) {
                productDetails = {...state.productDetails};
            } else {
                productDetails = {};
            }
            productDetails[action.data.sku] = action.data;
            return {
                ...state,
                productDetails
            };

        default:
            return state
    }
}
