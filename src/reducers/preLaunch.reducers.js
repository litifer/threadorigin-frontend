import axios from 'axios';
import * as  configure from '../config.js'
import {LAUNCH_EMAIL_SUBSCRIBE} from '../actions/index'

const category = {}

const initialState = {} 

export default (state = initialState, action) => {
    // console.log("reducer",action.type)
    switch (action.type) {
      case LAUNCH_EMAIL_SUBSCRIBE:
       newsletter_subscription(action.data)
       console.log("reducer",action.data)
        return{
            ...state
        }
      default:
        return state
      }
}

// redux store contains the first name and the last name so when login is done extract that from the state
function newsletter_subscription(email){  
   //console.log("email",email);
   axios({
       method:'post',
       url: 'http://13.232.62.17:3000/api/user/newsletter/subscribe',
       headers:{'Authorization': 'uprogt4mdkv3opqjctig9uvkpuqv05ni','Content-Type': 'application/json'},
       data:{
        'email':email,
        'firstname':'Anvesha',
        'lastname':'Singhania'
       }
    }).then(function(res){
      console.log(res.data)
      window.location = '../product'
    })
}
