import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import counter from '../actions/counter.actions'
import landing from '../actions/landing.actions';
import addresses from '../reducers/myAccountAddress.reducers';
import homepage from '../reducers/homepage.reducers';
import lookbook from '../reducers/campignLooks.reducer';
import dashboard from '../reducers/myAccountDashboard.reducers';
import product from '../reducers/product.reducers'
import category from '../reducers/category.reducers'
import cart from '../reducers/cart.reducers';
import accountInfo from '../reducers/myAccountInfo.reducers';
import orders from '../reducers/myAccountOrders.reducers';
import forgot from '../reducers/forgot.reducer'
import register from '../reducers/register.reducers'
import login from '../reducers/login.reducers'
import wishlist from '../reducers/wishlist.reducers'
import newpass from '../reducers/newPassword.reducers'
import checkout from '../reducers/checkout.reducers'
import modal from '../reducers/modal.reducers'

console.log("4. Reducer")
export default combineReducers({
  routing: routerReducer,
  counter,
  homepage,
  cart,
  product,
    accountInfo,
    addresses,
    forgot,
    lookbook,
  landing,
    dashboard,
  register,
  login,
    wishlist,
    orders,
  newpass,
  category,
  checkout,
  modal
})