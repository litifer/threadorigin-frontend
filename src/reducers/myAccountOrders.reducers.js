import {GET_ORDERS} from '../actions/myAccountOrders.action'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ORDERS:

            return {
                ...state,
                ordersData: action.data
            };

        default:
            return state
    }
}