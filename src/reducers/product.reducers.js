import axios from 'axios';
import * as  config from '../config.js'
import {PRODUCT_BODY} from '../actions/index'
import {PRODUCT_NOW} from '../actions/index'
import {COLOR_GET} from '../actions/index'
import {SIZE_GET} from '../actions/index'
import {PRICE_GET} from '../actions/index'
import {INCREMENT} from '../actions/index'
import {DECREMENT} from '../actions/index'
import {CHANGESIZE} from '../actions/index'
import {CHANGECOLOR} from '../actions/index'
import {LOAD_PRICE} from '../actions/index'
import {ADD_KART} from '../actions/index'
import {ADD_WISHLIST} from '../actions/product.actions'
  
const initialState = {
   qty: 1,
   related_products: {
     items: []
   }
}


export default (state = initialState, action) => {
    switch (action.type) {
      case INCREMENT:
          return{
            ...state,
            qty:state.qty + 1,
            price: increase_price(state)
          };
        case ADD_WISHLIST:
            if (action.data.errorMessage) {
                return {
                    ...state,
                    itemAdded: false
                }
            } else {
                return {
                    ...state,
                    itemAdded: true
                }
            }
      case DECREMENT:
          return {
             ...state,
             qty:get_min_qty(state.qty),
             price:decrease_price(state)
            }
      case LOAD_PRICE:
          return{
            ...state,
            price:get_price(state.price_detail)
          }
      case 'AVAILABLE_COLORS': return{
        ...state,
        all_colors: action.data.data,
        color_attribute_id: action.data.attributeId
      }
      case 'AVAILABLE_SIZES': return{
        ...state,
        all_sizes: action.data.data,
        size_attribute_id: action.data.attributeId
      }    
      case 'AVAILABLE_VARIANTS':
          if (action.data.color_list[0] && action.data.size_list[0]) {
              return {
                  ...state,
                  variants: action.data.variants,
                  size_list: action.data.size_list,
                  color_list: action.data.color_list,
                  sku_list: action.data.sku_list,
                  name_list: action.data.name_list,
                  price_list: action.data.price_list,
                  color: action.data.color_list[0].value,
                  size: action.data.size_list[0].value
              }
          } else {
              return {
                  ...state,
                  variants: action.data.variants,
                  size_list: action.data.size_list,
                  color_list: action.data.color_list,
                  sku_list: action.data.sku_list,
                  name_list: action.data.name_list,
                  price_list: action.data.price_list,
                  // color: action.data.color_list[0].value,
                  // size: action.data.size_list[0].value
              }
          }
      case 'CHANGE_VARIANT': {
        console.log("%c Current Product get is: ", "color: orange");
        console.log(action.data);
        return {
        ...state, 
        product_get: action.data,
        size: action.size,
        color: action.color,
        name: action.data.name,
        price: action.data.price,
        sku:action.data.sku 
      }}
      case CHANGECOLOR:
          return{
            ...state,
            color: action.color,
            price:evaluate_price_color(action.color,state.price_detail,state.color_val.options)
          }
      case CHANGESIZE:
         return{
          ...state,
          size:action.size,
          price:evaluate_price_size(action.size,state.price_detail,state.size_val.options)
         }
      case PRODUCT_BODY:
        return {
            ...state,
            product_details:action.data
         }
      case PRODUCT_NOW:
        {console.log("%c Action data in reducer is ", "color: magenta")
        console.log(action.data)
          return{
           ...state,
           product_get:action.data,
           price: action.data.price,
           name: action.data.name,
           sku: action.data.sku
        }}
      case COLOR_GET:
        return{
          ...state,
          color_val:action.data
        }
      case SIZE_GET:
        return{
           ...state,
           size_val:action.data
        }
      case PRICE_GET:
        return{
        ...state,
        price_detail:action.data
      }
      case ADD_KART:
      console.log(action.data)
      console.log(action.message)
        return{
        ...state,
        result:action.data,
        cart_message: action.message
      }
      // case ADD_WISH:
      // console.log(state)
      // return {
      //   ... state,
      //   item_wish: add_to_wish(state)
      // }

      case 'RELATED_PRODUCT': return {
        ... state,
        related_products:{
          items: [...state.related_products.items, action.data]
        } ,
        carousel_products_obtained: true
      }
      default:
        return state
      } 
}

function get_min_qty(qty){
  var qty_val = 1;
  if(qty == 1 )
    qty_val = 1;
  else{
    qty_val = qty - 1;
  }
  return qty_val;
}

function increase_price(state){
  var price_return = 0;
  price_return = state.price/state.qty * (state.qty + 1)
    return price_return
}
function decrease_price(state){
    var price_fixed = 0, price_return = 0;
    console.log(state.qty)
    if(state.qty == 1) {
      price_return = state.price
    }
    else {
      price_return = state.price/state.qty * (state.qty - 1)
    }
      return price_return
}

function evaluate_price_color(col, array,options){
  var len = array.length;
  var opt_len = options.length;
  for(var i =0; i< len; i++){
    for(var j=0; j<opt_len; j++){
      if(options[j].label == array[i].color){
        if(options[j].value == col){
          console.log(array[i].price)
          return array[i].price
          }
        }
      }
    }
  }

function evaluate_price_size(size, array,options){
  var len = array.length;
  var opt_len = options.length;
  for(var i =0; i< len; i++){
    for(var j=0; j<opt_len; j++){
      if(options[j].label == array[i].size){
        if(options[j].value == size){
          console.log(array[i].price)
          return array[i].price
        }
      }
      }
    }
  }

function get_price(array){
       return array[0].price
}

function add_to_wish(state){
  var array_wishlist = []
  var id = state.product_get.id
  axios ({
      method: 'get',
      url   : config.BASE_URL+'user/wishlist/add/:'+id,
      headers:{'Authorization': config.token,'Content-Type': 'application/json'}
  }).then(function(res){
      console.log(res);
      array_wishlist.push(id)
      alert("The item is added to WishList")
    })
}

