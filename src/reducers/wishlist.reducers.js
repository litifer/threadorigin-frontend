import axios from 'axios';
import * as  configure from '../config.js'
import {GET_WISHLIST} from '../actions/wishlist.actions'
import {GET_IMAGES} from '../actions/wishlist.actions'

const initialState = {}

export default (state = initialState, action) => {
    // console.log("reducer",action.type)
    switch (action.type) {
        case GET_WISHLIST:
            return{
                ...state,
                wishlistData: action.data
            };
        case GET_IMAGES:
            let prop = action.sku
            let item_images = {
                ...state.item_images
            }

            item_images[prop] = action.data
            return {
                ...state,
                item_images: item_images
            }
        default:
            return state
    }
}


