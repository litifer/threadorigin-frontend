import {UPDATE_INFO} from '../actions/myAccountInfo.action'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_INFO:
            if (action.data.message) {
                return {
                    ...state,
                    infoUpdated: false
                }
            } else {
                return {
                    ...state,
                    infoUpdated: true
                }
            }

        default:
            return state
    }
}