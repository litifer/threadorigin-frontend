import {GET_ADDRESSES} from '../actions/myAccountAddress.action'
import {ADD_DEFAULT_ADDRESS, CHANGE_DEFAULT_ADDRESS} from '../actions/myAccountAddress.action'
import {BILLING_ADDRESS} from '../actions/myAccountAddress.action'
import {DELETE_ADDRESS} from '../actions/myAccountAddress.action'
import {EDIT_ADDRESS} from '../actions/myAccountAddress.action'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ADDRESSES:
            let defaultAddress = null;
            const otherAddresses = [];
            for (let address in action.data.addresses) {
                if (address.default_billing && address.default_shipping) {
                    defaultAddress = {...address}
                } else {
                    otherAddresses.push({...address});
                }
            }
            return {
                ...state,
                addressData: action.data,
                defaultAddress,
                otherAddresses
            };
        case ADD_DEFAULT_ADDRESS:
            if (action.data.errors) {
                return {
                    ...state,
                    defaultAddressAdded: false
                }
            } else {
                return {
                    ...state,
                    defaultAddressAdded: true
                }
            }
        case CHANGE_DEFAULT_ADDRESS:
            return {
                ...state,
                defaultAddressChanged: true
            };

        case BILLING_ADDRESS:
            // console.log(JSON.parse(action.data.payment_methods), "shipping address");
            if (action.data.payment_methods) {
                return {
                    ...state,
                    billingAddressSet: true
                }
            } else {
                return {
                    ...state,
                    billingAddressSet: false
                }
            }
        case EDIT_ADDRESS:
            if (action.data.message) {
                return {
                    ...state,
                    addressEdited: false
                }
            } else {
                return {
                    ...state,
                    addressEdited: true
                }
            }
        case DELETE_ADDRESS:
            if (action.data.message) {
                return {
                    ...state,
                    addressDeleted: false
                }
            } else {
                return {
                    ...state,
                    addressDeleted: true
                }
            }
        default:
            return state
    }
}