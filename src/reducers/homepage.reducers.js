import axios from 'axios';
import * as  configure from '../config.js'
import {LOAD_CAMP1} from '../actions/index'
import {LOAD_CAMP2} from '../actions/index'
import {LOAD_CAMP3} from '../actions/index'
import {LOAD_BODY3} from '../actions/index'
import {LOAD_BODY5} from '../actions/index'
import {IMG_BODY3} from '../actions/index'
import {IMG_BODY5} from '../actions/index'
import {LOAD_INSTA} from '../actions/index'
import {EMAIL_SUBSCRIBE} from '../actions/index'

const category = {}

const initialState = {} 


export default (state = initialState, action) => {
    // console.log("reducer",action.type)
    switch (action.type) {
      case LOAD_CAMP1:
       return {
        ... state,
        camp1: action.data
       }
       case LOAD_CAMP2:
       return {
        ... state,
        camp2: action.data
       }
       case LOAD_CAMP3:
       return {
        ... state,
        camp3: action.data
       }
      case LOAD_BODY3:
        return {
            ...state,
            body3:action.data
        }
      case LOAD_BODY5:
        return {
            ...state,
            body5:action.data
        }
      case EMAIL_SUBSCRIBE:
    //    newsletter_subscription(action.data)
        return{
            ...state
        }
      case IMG_BODY3:
       return {
           ... state,
           img_body3:action.data
       }
      case IMG_BODY5:
       return {
           ... state,
           img_body5:action.data
       }
      case LOAD_INSTA:
       return {
           ... state,
           insta_images:action.data
       }

      default:
        return state
      }
}

// redux store contains the first name and the last name so when login is done extract that from the state
function newsletter_subscription(email){  
   console.log(email);
   axios({
       method:'post',
       url: 'http://13.232.62.17:3000/api/user/newsletter',
       headers:{'Authorization': 'uprogt4mdkv3opqjctig9uvkpuqv05ni','Content-Type': 'application/json'},
       data:{
        'email':email,
        // 'firstname':'Anvesha',
        // 'lastname':'Singhania'
       }
    }).then(function(res){
      console.log(res.data)
      window.location = '../product'
    })
}
