import axios from 'axios';
import * as  configure from '../config.js'
import {LAUNCH_EMAIL_WEBSITE_SUBSCRIBE} from '../actions/index'

const category = {}

const initialState = {} 

export default (state = initialState, action) => {
    // console.log("reducer",action.type)
    switch (action.type) {
      case LAUNCH_EMAIL_WEBSITE_SUBSCRIBE:
        return{
            ...state
        }
      default:
        return state
      }
}


