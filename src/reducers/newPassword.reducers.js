import {NEW_PASS} from '../actions/newPassword.actions'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
      case NEW_PASS:
       return {
        ...state,
           passwordChanged: action.data
       };
      default:
        return state
      }
}


