import axios from 'axios';
import * as  config from '../config.js'
import {LOADCART} from '../actions/index'
import {INCREMENT_VALUE} from '../actions/index'
import {DECREMENT_VALUE} from '../actions/index'
import {REMOVE_ITEM} from '../actions/index'
import {DISCOUNT_VAL} from '../actions/index'

const category = {}

const initialState = {} 


export default (state = initialState, action) => {
    switch (action.type) {
      case LOADCART:
        return {
            ...state,
            cart_get:action.data
        }
      case INCREMENT_VALUE:

       console.log(action.data)
        return {
          ...state,
          final_data:action.data
        }
      case DECREMENT_VALUE:
      console.log(action.data)
        return {
          ...state,
          final_data:action.data
        }
      case REMOVE_ITEM:
       return{
        ...state,
        item_removed: action.data,
        calculate_price: true
       }
      case DISCOUNT_VAL:
        return{
          ...state,
          discount: action.data
        } 
      case 'ITEM_IMAGE': {
        let prop = action.sku
        let item_images = {
          ...state.item_images
        }

        item_images[prop] = action.data
        return {
        ...state,
        item_images: item_images
      }
    }
    case 'LOAD_CART_PRICE': {
      console.log("%c The data from payment api is: ", "color: cyan");
      console.log(action.data)
      return {
        ...state,
        bill: action.data
      }
    }
    case 'DISCOUNT_APPLIED': {
      return {
        ...state,
        discount_applied: action.data,
        calculate_price: true
      }
    }
    case 'CHANGE_STATUS': {
      return {
        ...state,
        calculate_price: action.data
      }
    }
    case 'UPDATE_QUANTITY': {
      return {
        ...state,
        updated_quantity: action.data,
        calculate_price: true
      }
    }
      default:
        return state
      }
}
