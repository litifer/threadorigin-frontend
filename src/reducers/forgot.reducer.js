import {FORGOT} from '../actions/forgot.actions'
import {SET_IS_FORGOT_FALSE} from '../actions/forgot.actions'

const initialState = {forgotResponse: null};

export default (state = initialState, action) => {
    switch (action.type) {
        case FORGOT:
            console.log(action.data, "register reducer");
            return {
                ...state,
                forgotResponse: action.data
            };
        case SET_IS_FORGOT_FALSE:
            return {
                ...state,
                forgotResponse:false
            }
        default:
            return state
    }
}