import {LOGIN} from '../actions/index'
import {SET_LOGIN_DATA_TO_NULL} from '../actions/login.actions'
import {push} from 'react-router-redux';
const initialState = {login: ''};

export default (state = initialState, action) => {
    switch (action.type) {
      case LOGIN:
          if (action.data.success !== false) {
              push('/dev');
          }
       return {
        ...state,
        loginData:action.data
       };
        case SET_LOGIN_DATA_TO_NULL:
            return {
                ...state,
                loginData: null
            };
      default:
        return state
      }
}