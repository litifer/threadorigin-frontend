import {GET_USER_DATA} from '../actions/myAccountDashboard.action'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_USER_DATA:
            return {
                ...state,
                userData: action.data
            };

        default:
            return state
    }
}