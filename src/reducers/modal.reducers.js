import axios from 'axios';
import * as  config from '../config.js'
import {PRODUCT_NOW} from '../actions/index'
import {ADD_KART} from '../actions/modal.actions'
import {ADD_WISHLIST} from '../actions/product.actions'
  
const initialState = {
   qty: 1,
   related_products: {
     items: []
   }
}


export default (state = initialState, action) => {
    switch (action.type) {
    
        case ADD_WISHLIST:
            if (action.data.errorMessage) {
                return {
                    ...state,
                    itemAdded: false
                }
            } else {
                return {
                    ...state,
                    itemAdded: true
                }
            }
      
      case 'MODAL_CHANGE_VARIANT': {
        console.log("%c Current Product get is: ", "color: orange");
        console.log(action.data);
        return {
        ...state, 
        product_get: action.data,
        size: action.size,
        color: action.color,
        name: action.data.name,
        price: action.data.price,
        sku:action.data.sku 
      }}

      
      case PRODUCT_NOW:
        {console.log("%c Action data in reducer is ", "color: magenta")
        console.log(action.data)
          return{
           ...state,
           product_get:action.data,
           price: action.data.price,
           name: action.data.name,
           sku: action.data.sku
        }}
      
      
      case ADD_KART:
      console.log(action.data)
      console.log(action.message)
        return{
        ...state,
        result:action.data,
        flash_message: action.message
      }
      // case ADD_WISH:
      // console.log(state)
      // return {
      //   ... state,
      //   item_wish: add_to_wish(state)
      // }

      case 'RELATED_PRODUCT': return {
        ... state,
        related_products:{
          items: [...state.related_products.items, action.data]
        } ,
        carousel_products_obtained: true
      }

      case 'MODAL_AVAILABLE_VARIANTS':
          console.log(action.data);
          if (action.data.color_list[0] && action.data.size_list[0]) {
            return {
              ...state, 
              variants: action.data.variants,
              size_list: action.data.size_list,
              color_list: action.data.color_list,
              sku_list: action.data.sku_list,
              name_list: action.data.name_list,
              price_list: action.data.price_list,
              color: action.data.color_list[0].value,
              size: action.data.size_list[0].value,
              sku: action.data.sku_list[0]
            }
          } else {
            return {
              ...state, 
              variants: action.data.variants,
              size_list: action.data.size_list,
              color_list: action.data.color_list,
              sku_list: action.data.sku_list,
              name_list: action.data.name_list,
              price_list: action.data.price_list,
              // color: action.data.color_list[0].value,
              // size: action.data.size_list[0].value,
              sku: action.data.sku_list[0]
            }
          }
      
      case 'CHANGE_MODAL_DATA': return {
        ...state,
        modal_item: action.data
      }

      case 'CREATE_ATTRIBUTE_ARRAY': {
        
        let data = {
          ...state
        }

        if(action.data.type === 'color')
          data['modal_color_array'] = action.data.data
        else if(action.data.type === "size")
          data['modal_size_array'] = action.data.data
        

        return data
      }
      default:
        return state
      } 
}

