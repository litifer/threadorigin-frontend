exports.Urls = {
    DRESSES_URL : "/category?id=10",
    BOTTOMS_URL : "/category?id=13",
    TOPS_URL : "/category?id=12",
    LOOKBOOKS_URL : "/lookbooks",
    COLLECTIONS_URL : "/category?id=5",
    SANDOOK_URL : "/category?id=6",
    CART_URL : "/cart",
    MYACCOUNT_URL : "/accountDashboard",
    SEARCH_URL : "/category?search=",
    TNC_URL : "/termsAndCondition",
    PRIVACY_POLICY_URL : "/privacycontent",
    FAQ_URL : "/faq",
    CONTACTUS_URL : "/contactus",
    ABOUTUS_URL : "/aboutus"
}

exports.ContactInfo = {
    MOBILE_NO : "+91 98219 72224",
    TIMINGS : "9:00 A.M. TO 5:00 P.M. IST",
    EMAIL : "CARE@THREADORIGIN.COM",
    DAYS: "Monday-Friday"
}