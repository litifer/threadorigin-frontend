import React, {Component} from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/jquery/dist/jquery.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';

import './index.css'

import Routes from './routes'

const target = document.querySelector('#root')


render(
  <Provider store={store}>
      <BrowserRouter>
    <ConnectedRouter history={history}>
        <div>
          <Routes />
      </div>
    </ConnectedRouter>
      </BrowserRouter>
  </Provider>,
  target
)